#!/bin/bash
#PBS -q regular
#PBS -l mppwidth=96
#PBS -l walltime=3:00:00
#PBS -N THOR-PROC-THOR-SMALL-NUMA
#PBS -e thor_proc_nonimm_numa.$PBS_JOBID.err
#PBS -o thor_proc_nonimm_numa.$PBS_JOBID.out
#PBS -m abe

export THREADS=8
export PERNODE=2
export PERNUMA=1
export IMMEDIATE=0

cd $PBS_O_WORKDIR

set -x
echo ">>>>> ORIG"
unset THOR_ENABLED
unset THOR_USE_PTHREADS
unset GASNET_DOMAIN_COUNT
export UPC_SHARED_HEAP_SIZE=1G

#aprun -n $THREADS -N $PERNODE -S $PERNUMA ./orig-comm-bench-aries 1 AP 512 $IMMEDIATE
#aprun -n $THREADS -N $PERNODE -S $PERNUMA ./orig-comm-bench-aries 1 AG 512 $IMMEDIATE

export THOR_ENABLED=1
export THOR_SERVERS_PER_DOMAIN=1
export UPC_SERVERS_PER_DOMAIN=1
export SUPC_NODE_COUNT=$THREADS
export THOR_CORES_PER_NUMA_DOMAIN=12
export THOR_CORES_PER_DOMAIN=12
export THOR_CORES_PER_NODE=24
export THOR_DOMAINS_PER_NODE=1
export THOR_DOMAIN_COUNT=$THREADS
export UPC_DOMAIN_COUNT=$THREADS

export OLDTHREADS=$THREADS
export OLDPERNODE=$PERNODE
export OLDPERNUMA=$PERNUMA
export THREADS=`python -c "print $OLDTHREADS+($OLDTHREADS*$THOR_SERVERS_PER_DOMAIN),"`
export PERNODE=`python -c "print $OLDPERNODE+($OLDPERNODE*$THOR_SERVERS_PER_DOMAIN),"`
export PERNUMA=`python -c "print $OLDPERNUMA+($OLDPERNUMA*$THOR_SERVERS_PER_DOMAIN),"`

echo ">>>>> PTHREAD Servers per domain: 1"

aprun -n $THREADS -N $PERNODE -S $PERNUMA ./thor-comm-bench-aries 1 AP 512 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA ./thor-comm-bench-aries 1 AG 512 $IMMEDIATE

echo ">>>>> PTHREAD Servers per domain: 2"

export THOR_SERVERS_PER_DOMAIN=2
export THREADS=`python -c "print $OLDTHREADS+($OLDTHREADS*$THOR_SERVERS_PER_DOMAIN),"`
export PERNODE=`python -c "print $OLDPERNODE+($OLDPERNODE*$THOR_SERVERS_PER_DOMAIN),"`
export PERNUMA=`python -c "print $OLDPERNUMA+($OLDPERNUMA*$THOR_SERVERS_PER_DOMAIN),"`

aprun -n $THREADS -N $PERNODE -S $PERNUMA ./thor-comm-bench-aries 1 AP 512 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA ./thor-comm-bench-aries 1 AG 512 $IMMEDIATE

echo ">>>>> PTHREAD Servers per domain: 3"

export THOR_SERVERS_PER_DOMAIN=3
export THREADS=`python -c "print $OLDTHREADS+($OLDTHREADS*$THOR_SERVERS_PER_DOMAIN),"`
export PERNODE=`python -c "print $OLDPERNODE+($OLDPERNODE*$THOR_SERVERS_PER_DOMAIN),"`
export PERNUMA=`python -c "print $OLDPERNUMA+($OLDPERNUMA*$THOR_SERVERS_PER_DOMAIN),"`
                                          
aprun -n $THREADS -N $PERNODE -S $PERNUMA ./thor-comm-bench-aries 1 AP 512 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA ./thor-comm-bench-aries 1 AG 512 $IMMEDIATE

set +x
unset THOR_USE_PTHREADS
unset GASNET_DOMAIN_COUNT

echo ">>>>> Done!"
