#!/usr/bin/python -u

import sys
import os

if len(sys.argv) != 3:
    sys.exit( "Usage %s  nodes, cpn " % sys.argv[0])

nodes = int(sys.argv[1])
cpn = int(sys.argv[2])
 

print "#PBS -q regular"
print "#PBS -l mppwidth=%d" % (nodes*24)
print "#PBS -l walltime=00:90:00"
print "#PBS -N costin-orig-%d-%d" % (nodes, cpn)
print "#PBS -e my_job.$PBS_JOBID.err"
print "#PBS -o my_job.$PBS_JOBID.out"
print "#PBS -V"

print "cd ~/THOR/tests/comm-bench"
sheap = nodes/2
print "setenv UPC_SHARED_HEAP_SIZE %dG" % (sheap)
for cpn in [1, 2, 4, 8, 12, 16, 20, 24]:
    print "aprun -n %d -N %d ./comm-bench-orig-gemini  1 APG  2 >> ORIG-%d-%d-hopper.$PBS_JOBID" % (nodes*cpn, cpn,  nodes, cpn)
    print "aprun -n %d -N %d ./comm-bench-orig-gemini  1 APG  4 >> ORIG-%d-%d-hopper.$PBS_JOBID" % (nodes*cpn, cpn,  nodes, cpn)
    print "aprun -n %d -N %d ./comm-bench-orig-gemini  1 APG  8 >> ORIG-%d-%d-hopper.$PBS_JOBID" % (nodes*cpn, cpn,  nodes, cpn)
    print "aprun -n %d -N %d ./comm-bench-orig-gemini  1 APG  16 >> ORIG-%d-%d-hopper.$PBS_JOBID" % (nodes*cpn, cpn,  nodes, cpn)
    print "aprun -n %d -N %d ./comm-bench-orig-gemini  1 APG  32 >> ORIG-%d-%d-hopper.$PBS_JOBID" % (nodes*cpn, cpn,  nodes, cpn)
    print "aprun -n %d -N %d ./comm-bench-orig-gemini  1 APG  64 >> ORIG-%d-%d-hopper.$PBS_JOBID" % (nodes*cpn, cpn,  nodes, cpn)
    print "aprun -n %d -N %d ./comm-bench-orig-gemini  1 APG  128  >> ORIG-%d-%d-hopper.$PBS_JOBID" % (nodes*cpn, cpn,  nodes, cpn)
