export THOR_USE_PTHREADS=1
export THOR_SERVERS_PER_DOMAIN=1
export GASNET_DOMAIN_COUNT=$THOR_SERVERS_PER_DOMAIN
export UPC_SERVERS_PER_DOMAIN=1
export SUPC_NODE_COUNT=4
export THOR_CORES_PER_NUMA_DOMAIN=12
export THOR_CORES_PER_DOMAIN=12
export THOR_CORES_PER_NODE=24
export THOR_DOMAINS_PER_NODE=1
export THOR_DOMAIN_COUNT=4
export UPC_DOMAIN_COUNT=4
export UPC_SHARED_HEAP_SIZE=1G

echo ">>>>> PTHREAD Servers per domain: 1"

aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 2 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 4 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 8 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 16 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 32 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 64 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 128 0

aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 2 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 4 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 8 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 16 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 32 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 64 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 128 0

echo ">>>>> PTHREAD Servers per domain: 2"

export THOR_SERVERS_PER_DOMAIN=2
export GASNET_DOMAIN_COUNT=$THOR_SERVERS_PER_DOMAIN
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 2 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 4 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 8 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 16 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 32 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 64 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 128 0

aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 2 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 4 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 8 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 16 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 32 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 64 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 128 0
                                          

echo ">>>>> PTHREAD Servers per domain: 3"

export THOR_SERVERS_PER_DOMAIN=3
export GASNET_DOMAIN_COUNT=$THOR_SERVERS_PER_DOMAIN
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 2 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 4 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 8 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 16 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 32 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 64 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AP 128 0

aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 2 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 4 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 8 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 16 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 32 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 64 0
aprun -n 4 -N 2 -S 1 ./thor-comm-bench-aries 1 AG 128 0

unset THOR_USE_PTHREADS
unset GASNET_DOMAIN_COUNT

echo ">>>> PROC Servers per domain: 1"
