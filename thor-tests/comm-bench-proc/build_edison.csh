#!/usr/bin/env csh
set OPT_VER=opt
set UPC_PATH=/global/u2/n/nchaimov/edison/upc-inst/$OPT_VER
set UPC_INST=$UPC_PATH/include
set THOR_PATH=/global/u2/n/nchaimov/edison/upc-runtime-thor/thor
set OPTS="MYCONDUIT=aries OPT_VER=$OPT_VER THOR_PATH=$THOR_PATH UPC_PATH=$UPC_PATH UPC_INST=$UPC_INST"
echo "make -f Makefile.edison $OPTS V=1 clean"
make -f Makefile.edison $OPTS V=1 clean
echo "make -f Makefile.edison $OPTS V=2 clean"
make -f Makefile.edison $OPTS V=2 clean
#echo "make -f Makefile.edison $OPTS V=3 clean"
#make -f Makefile.edison $OPTS V=3 clean
#echo "make -f Makefile.edison $OPTS V=4 clean"
#make -f Makefile.edison $OPTS V=4 clean
echo "make -f Makefile.edison $OPTS V=1 THOR_ENABLED=-DTHOR_ENABLED"
make -f Makefile.edison $OPTS THOR_ENABLED=-DTHOR_ENABLED V=1 

echo "make -f Makefile.edison $OPTS V=2"
make -f Makefile.edison $OPTS V=2

#echo "make -f Makefile.edison $OPTS V=3 THOR_ENABLED=-DTHOR_ENABLED"
#make -f Makefile.edison $OPTS THOR_ENABLED=-DTHOR_ENABLED V=3 
#
#echo "make -f Makefile.edison $OPTS V=4 THOR_ENABLED=-DTHOR_ENABLED"
#make -f Makefile.edison $OPTS THOR_ENABLED=-DTHOR_ENABLED V=4 
