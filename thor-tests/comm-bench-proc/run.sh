#!/bin/bash

if [ "$1" ]; then
  TARGET="$1"
  shift 1
  ARGS=$@
else
  TARGET=comm-bench
  ARGS="1 AP 8"
fi

set -x

#ulimit -c unlimited

CLIENT_DISPATCH_POLICY=${CDP:-2} \
SERVER_DISPATCH_POLICY=${SDP:-0} \
SUPC_NODE_COUNT=2 \
UPC_DOMAIN_COUNT=2 \
UPC_SERVERS_PER_DOMAIN=${SPD:-2} \
UPC_SHARED_HEAP_SIZE=512MB \
GASNET_BACKTRACE=1 \
upcrun \
./$TARGET $ARGS

set +x
