#include "supc.h"

#include <stdio.h>
#include <unistd.h>

#define BLKSZ 5000

shared [BLKSZ] double buf[BLKSZ*THREADS];

//#define THREAD_CHECK (MYTHREAD)
//#define THREAD_CHECK (MYTHREAD%2)
#define THREAD_CHECK (MYTHREAD%8)
//#define THREAD_CHECK 0

extern unsigned long long currentClock(void);

int main(int argc, char **argv) 
{
  int i;
  int myStart = MYTHREAD * BLKSZ;
  int neighborStart;
  int offset;
  double msecs, waitMsecs, injectMsecs, singleInjectMsecs;
  int iters = 30;
  int tries = 100;
  bupc_tick_t injectAcc, waitAcc, tickOverhead = 0;

  offset = (THREADS / 2) + 1;
  neighborStart = offset * BLKSZ;
  if (!MYTHREAD) printf("Neighbor is thread #%d (%s) = %d\n", offset, argv[1], neighborStart);

  if (argc > 1) {
    iters = atoi(argv[1]);
  }

  double totalActions = tries * iters;

  default_server_init();

  upc_barrier;
  double * bufLocal = bupc_cast(buf+myStart);
  for (i=0; i<BLKSZ; i++)
    bufLocal[i] = (1000.*MYTHREAD) + (1.*i);
  upc_barrier;

  if (THREAD_CHECK == 0) {
      usleep(100000); // Make sure "inactive" threads are actually asleep
      bupc_handle_t * handles = malloc(sizeof(bupc_handle_t) * iters);
      bupc_tick_t start, end;
      bupc_tick_t tA, tB, tC, tD;
      int gap = 2;
      // GET TICK OVERHEAD
      for (int k=0; k<1000; k++) {
        tA = currentClock();
        tB = currentClock();
        tickOverhead += tB - tA;
      }
      tickOverhead /= 1000;
      // GOT TICK OVERHEAD
      for (int warmup=1; warmup>=0; warmup--) {
        waitAcc = injectAcc = 0;
        int kMax = warmup ? 1000 : tries;
        start = currentClock();
        for (int k=0; k<kMax; k++) {
          tA = currentClock();
          // async sends
          for (int j=0; j<iters; j++) {
            int off = j * gap;
            handles[j] = bupc_memput_async(buf+neighborStart+off, bufLocal+off, sizeof(double));
          }
          tC = tB = currentClock();
          // async waits
          for (int j=0; j<iters; j++) {
            bupc_waitsync(handles[j]);
          }
          tD = currentClock();
          injectAcc += tB - tA - tickOverhead;
          waitAcc += tD - tC - tickOverhead;
        }
        end = (currentClock() - start);
        msecs = bupc_ticks_to_us(end) / 1000.;
        waitMsecs = bupc_ticks_to_us(waitAcc) / 1000.;
        injectMsecs = bupc_ticks_to_us(injectAcc) / 1000.;
      }
      upc_barrier;
  }
//  else if (THREAD_CHECK == offset) {
//      upc_barrier;
//      for (i=0; i<BLKSZ; i++) {
//        printf("%d received data %.0f @ %d\n", MYTHREAD, buf[myStart+i], i);
//      }
//  }
  else {
    sleep(30);
    upc_barrier;
  }

  upc_barrier;
  if (!MYTHREAD) printf("DONE!   (%.4f ms)\n", msecs);
  if (!MYTHREAD) printf("Wait:   (%.4f ms)\n", waitMsecs);
  if (!MYTHREAD) printf("Inject: (%.4f ms)\n", injectMsecs);
  if (!MYTHREAD) printf("Single Inject: (%.4f us)\n", injectMsecs / totalActions * 1000.);
  if (!MYTHREAD) printf("Single Wait:   (%.4f us)\n", waitMsecs / totalActions * 1000.);
  if (!MYTHREAD) printf("Single Inject: %f ticks\n", injectAcc / totalActions);
  if (!MYTHREAD) printf("               %lld ticks over %.0f total\n", injectAcc, totalActions);
  if (!MYTHREAD) printf("Single Wait:   %f ticks\n", waitAcc / totalActions);
  if (!MYTHREAD) printf("               %lld ticks over %.0f total\n", waitAcc, totalActions);
  if (!MYTHREAD) printf("Tick overhead: %lld ticks\n", tickOverhead);
  return 0;
}

