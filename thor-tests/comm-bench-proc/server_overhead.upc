#define THOR_ENABLED

#ifdef  THOR_ENABLED 
#include <supc.h>
#else 
#include <upc.h>
#endif 

extern void   thor_default_server_init();
extern void  thor_windup_comm();
extern void  thor_winddown_comm();
extern void   thor_killall();


unsigned long long current_time_ns(void);
#define DATA_SET 100000
typedef shared [] int * data_t;
shared data_t directory[THREADS];

int ROUNDS; 
int R_ELEM;
#define R_SEED 123
int main(int argc, char **argv) {

  int i, j, rnd, elem;
  shared [] int* peer;
  int peer_id = (MYTHREAD+1)%THREADS;
  bupc_tick_t start, init, sync;
  
  init = 0;
  sync = 0;
  if(argc !=3){ ROUNDS = 4; R_ELEM=4;} 
  else  {
    ROUNDS  = atoi(argv[1]);
    R_ELEM = atoi(argv[2]);
  }
  
  int *local;
  directory[MYTHREAD] = (shared [] int*)upc_alloc(DATA_SET*sizeof(int));
  local = (int*)directory[MYTHREAD];

  bupc_handle_t *handles;

  handles = malloc(R_ELEM*sizeof(bupc_handle_t));
  
  thor_default_server_init();
  thor_windup_comm();

  for(i=0; i < DATA_SET; i++){
    local[i] = 0;
  }
  local = (int*)upc_alloc(DATA_SET*sizeof(int));
  upc_barrier;
  peer = directory[(MYTHREAD+1)%THREADS];
  
  for(rnd = 0; rnd < ROUNDS; rnd++) {
    for(elem = 0; elem < R_ELEM; elem++) {
      local[rnd*R_ELEM+elem] = R_SEED + rnd*R_SEED + elem + peer_id;
    }
    start  = current_time_ns();
    for(elem = 0; elem < R_ELEM; elem++) {
      handles[elem] = sbupc_memput_async(peer+rnd*R_ELEM+elem, local+rnd*R_ELEM+elem, sizeof(int));
    }
    init += current_time_ns() - start;
    
    start = current_time_ns();
    for(elem = 0; elem < R_ELEM; elem++) {
      //printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n");
      sbupc_waitsync( handles[elem]);
    }
    sync += current_time_ns() - start;
    upc_barrier;
  }
  

  /* check it */
  local = (int*)directory[MYTHREAD];
  for(rnd = 0; rnd < ROUNDS; rnd++) {
    for(elem = 0; elem < R_ELEM; elem++) {
      if(local[rnd*R_ELEM+elem] != R_SEED + rnd*R_SEED + elem + MYTHREAD) {
		printf("ERROR at elem %d in THREAD %d: %d != %d\n", rnd*R_ELEM+elem, MYTHREAD,local[rnd*R_ELEM+elem],R_SEED + rnd*R_SEED + elem + MYTHREAD);
      } else {
//	printf("Ckeck [%d, %d]....\n", MYTHREAD,rnd*R_ELEM+elem );
      }
    }
  }

  upc_barrier;
  printf("%d : INIT = %llu nsec, SYNC = %llu nsec\n", MYTHREAD, init/(ROUNDS*R_ELEM), sync/(ROUNDS*R_ELEM));

  printf("Done!\n");
  thor_winddown_comm();
  thor_killall();

}

