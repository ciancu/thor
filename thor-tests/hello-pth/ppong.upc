#include <upc.h>

typedef shared [] int * data_t;
shared data_t directory[THREADS];
extern int thor_issue_immediate;

int ROUNDS; 
int R_ELEM;
#define R_ELEM 600
#define R_SEED 123
#define R_ROUNDS 2
int main(int argc, char **argv) {
  thor_issue_immediate=1;
  printf(">>> ppong benchmark starting, thread %d.\n", MYTHREAD);
  int i, j, rnd, elem;
  shared [] int* peer;
  int peer_id = (MYTHREAD+1)%THREADS;
  if(argc == 1) ROUNDS = R_ROUNDS;
  else ROUNDS  = atoi(argv[1]);
  
  int *local;
  size_t ENTRIES = R_ELEM * ROUNDS * THREADS;
  directory[MYTHREAD] = (shared [] int*)upc_alloc(ENTRIES*sizeof(int));
  local = (int*)directory[MYTHREAD];

  bupc_handle_t handles[R_ELEM];
  
  thor_default_server_init();
  thor_windup_comm();

  for(i=0; i < ENTRIES; i++){
    local[i] = 0;
  }
  local = (int*)upc_alloc(ENTRIES*sizeof(int));
  upc_barrier;
  peer = directory[(MYTHREAD+1)%THREADS];
  
  for(rnd = 0; rnd < ROUNDS; rnd++) {
    printf(">>> thread %d round %d.\n", MYTHREAD, rnd);
    for(elem = 0; elem < R_ELEM; elem++) {
      local[rnd*R_ELEM+elem] = R_SEED + rnd*R_SEED + elem + peer_id;
    }
    for(elem = 0; elem < R_ELEM; elem++) {
      printf(">>> thread %d round %d memput elem %d.\n", MYTHREAD, rnd, elem);
      handles[elem] = sbupc_memput_async(peer+rnd*R_ELEM+elem, local+rnd*R_ELEM+elem, sizeof(int));
    }

    for(elem = 0; elem < R_ELEM; elem++) {
      printf(">>> thread %d round %d begin waitsync elem %d.\n", MYTHREAD, rnd, elem);
      sbupc_waitsync( handles[elem]);
      printf(">>> thread %d round %d passed waitsync elem %d.\n", MYTHREAD, rnd, elem);
    }
    
    printf(">>> thread %d at barrier 1.\n", MYTHREAD);
    upc_barrier;
    printf(">>> thread %d at barrier 2.\n", MYTHREAD);
    upc_barrier;
    printf(">>> thread %d at barrier 3.\n", MYTHREAD);
    upc_barrier;
    printf(">>> thread %d at barrier 4.\n", MYTHREAD);
    upc_barrier;
    printf(">>> thread %d at barrier 5.\n", MYTHREAD);
    upc_barrier;
    printf(">>> thread %d past barriers.\n", MYTHREAD);
  }
  

  /* check it */
  local = (int*)directory[MYTHREAD];
  int error = 0;
  for(rnd = 0; rnd < ROUNDS; rnd++) {
    for(elem = 0; elem < R_ELEM; elem++) {
      if(local[rnd*R_ELEM+elem] != R_SEED + rnd*R_SEED + elem + MYTHREAD) {
        error++;
	printf("ERROR at elem %d in THREAD %d: %d != %d\n", rnd*R_ELEM+elem, MYTHREAD,local[rnd*R_ELEM+elem],R_SEED + rnd*R_SEED + elem + MYTHREAD);
      } else {
	printf("Ckeck [%d, %d]....\n", MYTHREAD,rnd*R_ELEM+elem );
      }
    }
  }

  printf("Done with %d errors!\n", error);
  thor_winddown_comm();
  thor_killall();

}

