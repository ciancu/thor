/* --- UPCR system headers --- */ 
#include "upcr.h" 
#include "whirl2c.h"
#include "upcr_proxy.h"
/*******************************************************
 * C file translated from WHIRL Mon Aug 11 14:53:00 2014
 *******************************************************/

/* UPC Runtime specification expected: 3.6 */
#define UPCR_WANT_MAJOR 3
#define UPCR_WANT_MINOR 6
/* UPC translator version: release 2.19.3, built on Aug 10 2014 at 22:22:58, host aphid linux-x86_64/64, gcc v4.2.4 (Ubuntu 4.2.4-1ubuntu4) */
/* Included code from the initialization script */
#include</global/homes/n/nchaimov/edison/upc-inst/opt/include/upcr_preinclude/upc_types.h>
#include "upcr_geninclude/stddef.h"
#include</global/homes/n/nchaimov/edison/upc-inst/opt/include/upcr_preinclude/upc_bits.h>
#include "upcr_geninclude/stdlib.h"
#include "upcr_geninclude/inttypes.h"
#include "upcr_geninclude/stdio.h"
#line 1 "hello.w2c.h"
/* Include builtin types and operators */

#ifndef UPCR_TRANS_EXTRA_INCL
#define UPCR_TRANS_EXTRA_INCL
extern int upcrt_gcd (int _a, int _b);
extern int _upcrt_forall_start(int _start_thread, int _step, int _lo, int _scale);
#define upcrt_forall_start(start_thread, step, lo, scale)  \
       _upcrt_forall_start(start_thread, step, lo, scale)
int32_t UPCR_TLD_DEFINE_TENTATIVE(upcrt_forall_control, 4, 4);
#define upcr_forall_control upcrt_forall_control
#ifndef UPCR_EXIT_FUNCTION
#define UPCR_EXIT_FUNCTION() ((void)0)
#endif
#if UPCR_RUNTIME_SPEC_MAJOR > 3 || (UPCR_RUNTIME_SPEC_MAJOR == 3 && UPCR_RUNTIME_SPEC_MINOR >= 8)
  #define UPCRT_STARTUP_SHALLOC(sptr, blockbytes, numblocks, mult_by_threads, elemsz, typestr) \
      { &(sptr), (blockbytes), (numblocks), (mult_by_threads), (elemsz), #sptr, (typestr) }
#else
  #define UPCRT_STARTUP_SHALLOC(sptr, blockbytes, numblocks, mult_by_threads, elemsz, typestr) \
      { &(sptr), (blockbytes), (numblocks), (mult_by_threads) }
#endif
#define UPCRT_STARTUP_PSHALLOC UPCRT_STARTUP_SHALLOC

/**** Autonb optimization ********/

extern void _upcrt_memput_nb(upcr_shared_ptr_t _dst, const void *_src, size_t _n);
#define upcrt_memput_nb(dst, src, n) \
       (upcri_srcpos(), _upcrt_memput_nb(dst, src, n))

#endif


/* Types */
/* File-level vars and routines */
extern int user_main();


#define UPCR_SHARED_SIZE_ 16
#define UPCR_PSHARED_SIZE_ 16

void UPCRI_ALLOC_hello_229468239085872(void) { 
UPCR_BEGIN_FUNCTION();

UPCR_SET_SRCPOS("_hello_229468239085872_ALLOC",0);
}

void UPCRI_INIT_hello_229468239085872(void) { 
UPCR_BEGIN_FUNCTION();
UPCR_SET_SRCPOS("_hello_229468239085872_INIT",0);
}

#line 3 "hello.upc"
extern int user_main()
#line 3 "hello.upc"
{
#line 3 "hello.upc"
  UPCR_BEGIN_FUNCTION();
  upcr_pshared_ptr_t p;
  upcr_shared_ptr_t _bupc_call0;
  upcr_pshared_ptr_t _bupc_Mstopcvt1;
  upcr_pshared_ptr_t _bupc_Mptra2;
  
#line 6 "hello.upc"
  thor_default_server_init();
#line 8 "hello.upc"
  upcr_barrier(1021362480, 1);
#line 10 "hello.upc"
  upcr_barrier(1021362481, 1);
#line 12 "hello.upc"
  upcr_barrier(1021362482, 1);
#line 14 "hello.upc"
  _bupc_call0 = upc_alloc((unsigned long) 256ULL);
#line 14 "hello.upc"
  _bupc_Mstopcvt1 = UPCR_SHARED_TO_PSHARED(_bupc_call0);
#line 14 "hello.upc"
  p = _bupc_Mstopcvt1;
#line 15 "hello.upc"
  _bupc_Mptra2 = UPCR_ADD_PSHARED1(p, 4ULL, 3);
#line 15 "hello.upc"
  UPCR_PUT_PSHARED_VAL(_bupc_Mptra2, 0, 5, 4);
#line 16 "hello.upc"
  printf("OIIIIII PASSED BARRIER 3 %d\n", ((int) upcr_mythread () ));
#line 17 "hello.upc"
  UPCR_EXIT_FUNCTION();
#line 17 "hello.upc"
  return 0;
} /* user_main */

#line 1 "_SYSTEM"
/**************************************************************************/
/* upcc-generated strings for configuration consistency checks            */

GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_GASNetConfig_gen, 
 "$GASNetConfig: (/tmp/upcc--25674-1407793979/hello.trans.c) RELEASE=1.22.5,SPEC=1.8,CONDUIT=ARIES(ARIES-0.3/ARIES-0.3),THREADMODEL=PAR,SEGMENT=FAST,PTR=64bit,noalign,pshm,nodebug,notrace,nostats,nodebugmalloc,nosrclines,timers_native,membars_native,atomics_native,atomic32_native,atomic64_native $");
GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_UPCRConfig_gen,
 "$UPCRConfig: (/tmp/upcc--25674-1407793979/hello.trans.c) VERSION=2.19.1,PLATFORMENV=shared-distributed,SHMEM=pshm,SHAREDPTRREP=struct/p32:t32:a64,TRANS=berkeleyupc,nodebug,nogasp,notv,dynamicthreads $");
GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_translatetime, 
 "$UPCTranslateTime: (hello.o) Mon Aug 11 14:53:00 2014 $");
GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_GASNetConfig_obj, 
 "$GASNetConfig: (hello.o) " GASNET_CONFIG_STRING " $");
GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_UPCRConfig_obj,
 "$UPCRConfig: (hello.o) " UPCR_CONFIG_STRING UPCRI_THREADCONFIG_STR " $");
GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_translator, 
 "$UPCTranslator: (hello.o) /usr/local/upc/nightly/translator/install/targ (aphid) $");
GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_upcver, 
 "$UPCVersion: (hello.o) 2.19.3 (UNSTABLE) $");
GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_compileline, 
 "$UPCCompileLine: (hello.o) /usr/local/upc/nightly/runtime/inst/bin/upcc.pl --at-remote-http --arch-size=64 --network=aries --lines -save-all-temps --trans --sizes-file=upcc-sizes hello.i $");
GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_compiletime, 
 "$UPCCompileTime: (hello.o) " __DATE__ " " __TIME__ " $");
#ifndef UPCRI_CC /* ensure backward compatilibity for http netcompile */
#define UPCRI_CC <unknown>
#endif
GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_backendcompiler, 
 "$UPCRBackendCompiler: (hello.o) " _STRINGIFY(UPCRI_CC) " $");

#ifdef GASNETT_CONFIGURE_MISMATCH
  GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_configuremismatch, 
   "$UPCRConfigureMismatch: (hello.o) 1 $");
  GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_configuredcompiler, 
   "$UPCRConfigureCompiler: (hello.o) " GASNETT_PLATFORM_COMPILER_IDSTR " $");
  GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_buildcompiler, 
   "$UPCRBuildCompiler: (hello.o) " PLATFORM_COMPILER_IDSTR " $");
#endif

/**************************************************************************/
GASNETT_IDENT(UPCRI_IdentString_hello_o_1407793980_transver_2,
 "$UPCTranslatorVersion: (hello.o) 2.19.3 (UNSTABLE), built on Aug 10 2014 at 22:22:58, host aphid linux-x86_64/64, gcc v4.2.4 (Ubuntu 4.2.4-1ubuntu4) $");
