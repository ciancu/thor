/* --- UPCR system headers --- */ 
#include "upcr.h" 
#include "whirl2c.h"
#include "upcr_proxy.h"
/*******************************************************
 * C file translated from WHIRL Mon Aug 11 14:53:14 2014
 *******************************************************/

/* UPC Runtime specification expected: 3.6 */
#define UPCR_WANT_MAJOR 3
#define UPCR_WANT_MINOR 6
/* UPC translator version: release 2.19.3, built on Aug 10 2014 at 22:22:58, host aphid linux-x86_64/64, gcc v4.2.4 (Ubuntu 4.2.4-1ubuntu4) */
/* Included code from the initialization script */
#include</global/homes/n/nchaimov/edison/upc-inst/opt/include/upcr_preinclude/upc_types.h>
#include "upcr_geninclude/stddef.h"
#include</global/homes/n/nchaimov/edison/upc-inst/opt/include/upcr_preinclude/upc_bits.h>
#include "upcr_geninclude/stdlib.h"
#include "upcr_geninclude/inttypes.h"
#line 1 "ppong.w2c.h"
/* Include builtin types and operators */

#ifndef UPCR_TRANS_EXTRA_INCL
#define UPCR_TRANS_EXTRA_INCL
extern int upcrt_gcd (int _a, int _b);
extern int _upcrt_forall_start(int _start_thread, int _step, int _lo, int _scale);
#define upcrt_forall_start(start_thread, step, lo, scale)  \
       _upcrt_forall_start(start_thread, step, lo, scale)
int32_t UPCR_TLD_DEFINE_TENTATIVE(upcrt_forall_control, 4, 4);
#define upcr_forall_control upcrt_forall_control
#ifndef UPCR_EXIT_FUNCTION
#define UPCR_EXIT_FUNCTION() ((void)0)
#endif
#if UPCR_RUNTIME_SPEC_MAJOR > 3 || (UPCR_RUNTIME_SPEC_MAJOR == 3 && UPCR_RUNTIME_SPEC_MINOR >= 8)
  #define UPCRT_STARTUP_SHALLOC(sptr, blockbytes, numblocks, mult_by_threads, elemsz, typestr) \
      { &(sptr), (blockbytes), (numblocks), (mult_by_threads), (elemsz), #sptr, (typestr) }
#else
  #define UPCRT_STARTUP_SHALLOC(sptr, blockbytes, numblocks, mult_by_threads, elemsz, typestr) \
      { &(sptr), (blockbytes), (numblocks), (mult_by_threads) }
#endif
#define UPCRT_STARTUP_PSHALLOC UPCRT_STARTUP_SHALLOC

/**** Autonb optimization ********/

extern void _upcrt_memput_nb(upcr_shared_ptr_t _dst, const void *_src, size_t _n);
#define upcrt_memput_nb(dst, src, n) \
       (upcri_srcpos(), _upcrt_memput_nb(dst, src, n))

#endif


/* Types */
struct _upcri_eop;
      /* File-level vars and routines */
extern int user_main(int, char **);


#define UPCR_SHARED_SIZE_ 16
#define UPCR_PSHARED_SIZE_ 16
upcr_pshared_ptr_t directory;
extern int thor_issue_immediate;
int UPCR_TLD_DEFINE_TENTATIVE(ROUNDS, 4, 4);
int UPCR_TLD_DEFINE_TENTATIVE(R_ELEM, 4, 4);

void UPCRI_ALLOC_ppong_229479004939872(void) { 
UPCR_BEGIN_FUNCTION();
upcr_startup_pshalloc_t _bupc_pinfo[] = { 
UPCRT_STARTUP_PSHALLOC(directory, 16, 1, 1, 16, "A1H_R1_PR0_i"), 
 };

UPCR_SET_SRCPOS("_ppong_229479004939872_ALLOC",0);
upcr_startup_pshalloc(_bupc_pinfo, sizeof(_bupc_pinfo) / sizeof(upcr_startup_pshalloc_t));
}

void UPCRI_INIT_ppong_229479004939872(void) { 
UPCR_BEGIN_FUNCTION();
UPCR_SET_SRCPOS("_ppong_229479004939872_INIT",0);
}

#line 12 "ppong.upc"
extern int user_main(
  int argc,
  char ** argv)
#line 12 "ppong.upc"
{
#line 12 "ppong.upc"
  UPCR_BEGIN_FUNCTION();
  register _INT32 _bupc_comma;
  register _INT32 _bupc_comma0;
  int peer_id;
  unsigned long ENTRIES;
  int * local;
  int i;
  upcr_pshared_ptr_t peer;
  int rnd;
  int elem;
  struct _upcri_eop * handles[600LL];
  int error;
  upcr_shared_ptr_t _bupc_call0;
  upcr_shared_ptr_t _bupc_call1;
  upcr_pshared_ptr_t _bupc_Mstopcvt2;
  upcr_pshared_ptr_t _bupc_Mptra3;
  upcr_pshared_ptr_t _bupc_Mptra4;
  upcr_pshared_ptr_t _bupc_spillld5;
  int * _bupc_Mcvtptr6;
  int * _bupc_Mcvtptr7;
  upcr_pshared_ptr_t _bupc_Mptra8;
  upcr_pshared_ptr_t _bupc_spillld9;
  upcr_pshared_ptr_t _bupc_Mptra10;
  upcr_pshared_ptr_t _bupc_Mptra11;
  upcr_pshared_ptr_t _bupc_Mptra12;
  upcr_pshared_ptr_t _bupc_spillld13;
  int * _bupc_Mcvtptr14;
  
#line 13 "ppong.upc"
  thor_issue_immediate = 1;
#line 14 "ppong.upc"
  printf(">>> ppong benchmark starting, thread %d.\n", ((int) upcr_mythread () ));
#line 17 "ppong.upc"
  peer_id = (((int) upcr_mythread () ) + 1) % ((int) upcr_threads () );
#line 18 "ppong.upc"
  if(argc == 1)
#line 18 "ppong.upc"
  {
#line 18 "ppong.upc"
    ROUNDS = 2;
  }
  else
#line 18 "ppong.upc"
  {
#line 19 "ppong.upc"
    _bupc_comma = atoi((const char *) * (argv + 1LL));
#line 19 "ppong.upc"
    ROUNDS = _bupc_comma;
  }
#line 22 "ppong.upc"
  ENTRIES = (_UINT64)((ROUNDS * ((int) upcr_threads () )) * 600);
#line 23 "ppong.upc"
  _bupc_call0 = upc_alloc((unsigned long)(ENTRIES * 4ULL));
#line 23 "ppong.upc"
  _bupc_Mptra3 = UPCR_ADD_PSHARED1(directory, 16ULL, ((int) upcr_mythread () ));
#line 23 "ppong.upc"
  _bupc_Mstopcvt2 = UPCR_SHARED_TO_PSHARED(_bupc_call0);
#line 23 "ppong.upc"
  UPCR_PUT_PSHARED(_bupc_Mptra3, 0, &_bupc_Mstopcvt2, 16);
#line 24 "ppong.upc"
  _bupc_Mptra4 = UPCR_ADD_PSHARED1(directory, 16ULL, ((int) upcr_mythread () ));
#line 24 "ppong.upc"
  UPCR_GET_PSHARED(&_bupc_spillld5, _bupc_Mptra4, 0, 16);
#line 24 "ppong.upc"
  _bupc_Mcvtptr6 = (int *) UPCR_PSHARED_TO_LOCAL(_bupc_spillld5);
#line 24 "ppong.upc"
  local = _bupc_Mcvtptr6;
#line 28 "ppong.upc"
  thor_default_server_init();
#line 29 "ppong.upc"
  thor_windup_comm();
#line 31 "ppong.upc"
  i = 0;
#line 31 "ppong.upc"
  while((_UINT64)(i) < ENTRIES)
#line 31 "ppong.upc"
  {
#line 32 "ppong.upc"
    * (local + i) = 0;
#line 33 "ppong.upc"
    _1 :;
#line 33 "ppong.upc"
    i = i + 1;
  }
#line 34 "ppong.upc"
  _bupc_call1 = upc_alloc((unsigned long)(ENTRIES * 4ULL));
#line 34 "ppong.upc"
  _bupc_Mcvtptr7 = (int *) UPCR_SHARED_TO_LOCAL(_bupc_call1);
#line 34 "ppong.upc"
  local = _bupc_Mcvtptr7;
#line 35 "ppong.upc"
  upcr_barrier(-1097685408, 1);
#line 36 "ppong.upc"
  _bupc_Mptra8 = UPCR_ADD_PSHARED1(directory, 16ULL, (((int) upcr_mythread () ) + 1) % ((int) upcr_threads () ));
#line 36 "ppong.upc"
  UPCR_GET_PSHARED(&_bupc_spillld9, _bupc_Mptra8, 0, 16);
#line 36 "ppong.upc"
  peer = _bupc_spillld9;
#line 38 "ppong.upc"
  rnd = 0;
#line 38 "ppong.upc"
  while(rnd < ROUNDS)
#line 38 "ppong.upc"
  {
#line 39 "ppong.upc"
    printf(">>> thread %d round %d.\n", ((int) upcr_mythread () ), rnd);
#line 40 "ppong.upc"
    elem = 0;
#line 40 "ppong.upc"
    while(elem <= 599)
#line 40 "ppong.upc"
    {
#line 41 "ppong.upc"
      * (local + (elem + (rnd * 600))) = (peer_id + (elem + (rnd * 123))) + 123;
#line 42 "ppong.upc"
      _3 :;
#line 42 "ppong.upc"
      elem = elem + 1;
    }
#line 43 "ppong.upc"
    elem = 0;
#line 43 "ppong.upc"
    while(elem <= 599)
#line 43 "ppong.upc"
    {
#line 44 "ppong.upc"
      printf(">>> thread %d round %d memput elem %d.\n", ((int) upcr_mythread () ), rnd, elem);
#line 45 "ppong.upc"
      _bupc_Mptra10 = UPCR_ADD_PSHAREDI(peer, 4ULL, rnd * 600);
#line 45 "ppong.upc"
      _bupc_Mptra11 = UPCR_ADD_PSHAREDI(_bupc_Mptra10, 4ULL, elem);
#line 45 "ppong.upc"
      _bupc_comma0 = sbupc_memput_async(_bupc_Mptra11, local + (elem + (rnd * 600)), 4ULL);
#line 45 "ppong.upc"
      (handles)[elem] = (struct _upcri_eop *)(_bupc_comma0);
#line 46 "ppong.upc"
      _4 :;
#line 46 "ppong.upc"
      elem = elem + 1;
    }
#line 48 "ppong.upc"
    elem = 0;
#line 48 "ppong.upc"
    while(elem <= 599)
#line 48 "ppong.upc"
    {
#line 49 "ppong.upc"
      printf(">>> thread %d round %d begin waitsync elem %d.\n", ((int) upcr_mythread () ), rnd, elem);
#line 50 "ppong.upc"
      sbupc_waitsync((handles)[elem]);
#line 51 "ppong.upc"
      printf(">>> thread %d round %d passed waitsync elem %d.\n", ((int) upcr_mythread () ), rnd, elem);
#line 52 "ppong.upc"
      _5 :;
#line 52 "ppong.upc"
      elem = elem + 1;
    }
#line 54 "ppong.upc"
    printf(">>> thread %d at barrier 1.\n", ((int) upcr_mythread () ));
#line 55 "ppong.upc"
    upcr_barrier(-1097685407, 1);
#line 56 "ppong.upc"
    printf(">>> thread %d at barrier 2.\n", ((int) upcr_mythread () ));
#line 57 "ppong.upc"
    upcr_barrier(-1097685406, 1);
#line 58 "ppong.upc"
    printf(">>> thread %d at barrier 3.\n", ((int) upcr_mythread () ));
#line 59 "ppong.upc"
    upcr_barrier(-1097685405, 1);
#line 60 "ppong.upc"
    printf(">>> thread %d at barrier 4.\n", ((int) upcr_mythread () ));
#line 61 "ppong.upc"
    upcr_barrier(-1097685404, 1);
#line 62 "ppong.upc"
    printf(">>> thread %d at barrier 5.\n", ((int) upcr_mythread () ));
#line 63 "ppong.upc"
    upcr_barrier(-1097685403, 1);
#line 64 "ppong.upc"
    printf(">>> thread %d past barriers.\n", ((int) upcr_mythread () ));
#line 65 "ppong.upc"
    _2 :;
#line 65 "ppong.upc"
    rnd = rnd + 1;
  }
#line 69 "ppong.upc"
  _bupc_Mptra12 = UPCR_ADD_PSHARED1(directory, 16ULL, ((int) upcr_mythread () ));
#line 69 "ppong.upc"
  UPCR_GET_PSHARED(&_bupc_spillld13, _bupc_Mptra12, 0, 16);
#line 69 "ppong.upc"
  _bupc_Mcvtptr14 = (int *) UPCR_PSHARED_TO_LOCAL(_bupc_spillld13);
#line 69 "ppong.upc"
  local = _bupc_Mcvtptr14;
#line 70 "ppong.upc"
  error = 0;
#line 71 "ppong.upc"
  rnd = 0;
#line 71 "ppong.upc"
  while(rnd < ROUNDS)
#line 71 "ppong.upc"
  {
#line 72 "ppong.upc"
    elem = 0;
#line 72 "ppong.upc"
    while(elem <= 599)
#line 72 "ppong.upc"
    {
#line 73 "ppong.upc"
      if(*(local + (elem + (rnd * 600))) != ((((int) upcr_mythread () ) + (elem + (rnd * 123))) + 123))
#line 73 "ppong.upc"
      {
#line 74 "ppong.upc"
        error = error + 1;
#line 75 "ppong.upc"
        printf("ERROR at elem %d in THREAD %d: %d != %d\n", (int)(elem + (rnd * 600)), ((int) upcr_mythread () ), *(local + (elem + (rnd * 600))), (int)((((int) upcr_mythread () ) + (elem + (rnd * 123))) + 123));
      }
      else
#line 75 "ppong.upc"
      {
#line 77 "ppong.upc"
        printf("Ckeck [%d, %d]....\n", ((int) upcr_mythread () ), (int)(elem + (rnd * 600)));
      }
#line 79 "ppong.upc"
      _7 :;
#line 79 "ppong.upc"
      elem = elem + 1;
    }
#line 80 "ppong.upc"
    _6 :;
#line 80 "ppong.upc"
    rnd = rnd + 1;
  }
#line 82 "ppong.upc"
  printf("Done with %d errors!\n", error);
#line 83 "ppong.upc"
  thor_winddown_comm();
#line 84 "ppong.upc"
  thor_killall();
  UPCR_EXIT_FUNCTION();
  return 0;
} /* user_main */

#line 1 "_SYSTEM"
/**************************************************************************/
/* upcc-generated strings for configuration consistency checks            */

GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_GASNetConfig_gen, 
 "$GASNetConfig: (/tmp/upcc--25811-1407793994/ppong.trans.c) RELEASE=1.22.5,SPEC=1.8,CONDUIT=ARIES(ARIES-0.3/ARIES-0.3),THREADMODEL=PAR,SEGMENT=FAST,PTR=64bit,noalign,pshm,nodebug,notrace,nostats,nodebugmalloc,nosrclines,timers_native,membars_native,atomics_native,atomic32_native,atomic64_native $");
GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_UPCRConfig_gen,
 "$UPCRConfig: (/tmp/upcc--25811-1407793994/ppong.trans.c) VERSION=2.19.1,PLATFORMENV=shared-distributed,SHMEM=pshm,SHAREDPTRREP=struct/p32:t32:a64,TRANS=berkeleyupc,nodebug,nogasp,notv,dynamicthreads $");
GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_translatetime, 
 "$UPCTranslateTime: (ppong.o) Mon Aug 11 14:53:14 2014 $");
GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_GASNetConfig_obj, 
 "$GASNetConfig: (ppong.o) " GASNET_CONFIG_STRING " $");
GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_UPCRConfig_obj,
 "$UPCRConfig: (ppong.o) " UPCR_CONFIG_STRING UPCRI_THREADCONFIG_STR " $");
GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_translator, 
 "$UPCTranslator: (ppong.o) /usr/local/upc/nightly/translator/install/targ (aphid) $");
GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_upcver, 
 "$UPCVersion: (ppong.o) 2.19.3 (UNSTABLE) $");
GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_compileline, 
 "$UPCCompileLine: (ppong.o) /usr/local/upc/nightly/runtime/inst/bin/upcc.pl --at-remote-http --arch-size=64 --network=aries --lines -save-all-temps --trans --sizes-file=upcc-sizes ppong.i $");
GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_compiletime, 
 "$UPCCompileTime: (ppong.o) " __DATE__ " " __TIME__ " $");
#ifndef UPCRI_CC /* ensure backward compatilibity for http netcompile */
#define UPCRI_CC <unknown>
#endif
GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_backendcompiler, 
 "$UPCRBackendCompiler: (ppong.o) " _STRINGIFY(UPCRI_CC) " $");

#ifdef GASNETT_CONFIGURE_MISMATCH
  GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_configuremismatch, 
   "$UPCRConfigureMismatch: (ppong.o) 1 $");
  GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_configuredcompiler, 
   "$UPCRConfigureCompiler: (ppong.o) " GASNETT_PLATFORM_COMPILER_IDSTR " $");
  GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_buildcompiler, 
   "$UPCRBuildCompiler: (ppong.o) " PLATFORM_COMPILER_IDSTR " $");
#endif

/**************************************************************************/
GASNETT_IDENT(UPCRI_IdentString_ppong_o_1407793994_transver_2,
 "$UPCTranslatorVersion: (ppong.o) 2.19.3 (UNSTABLE), built on Aug 10 2014 at 22:22:58, host aphid linux-x86_64/64, gcc v4.2.4 (Ubuntu 4.2.4-1ubuntu4) $");
