#! /usr/bin/csh
setenv THOR_USE_PTHREADS 1
setenv THOR_SERVERS_PER_DOMAIN 1
setenv UPC_SERVERS_PER_DOMAIN 1
setenv SUPC_NODE_COUNT 2
setenv THOR_CORES_PER_NUMA_DOMAIN 12
setenv THOR_CORES_PER_DOMAIN 12
setenv THOR_CORES_PER_NODE 24
setenv THOR_DOMAINS_PER_NODE 1
setenv THOR_DOMAIN_COUNT 2
setenv UPC_DOMAIN_COUNT 2
module load valgrind
aprun -n 2 -N 2 valgrind --suppressions=/global/u2/n/nchaimov/upc-runtime-thor/gasnet/other/contrib/gasnet.supp ./simple-test

