#!/usr/bin/csh
set OPT_VER=opt
set UPC_PATH=/global/u2/n/nchaimov/edison/upc-inst/$OPT_VER
set UPC_INST=$UPC_PATH/include
set THOR_PATH=/global/u2/n/nchaimov/edison/upc-runtime-thor/thor_pth
set OPTS="MYCONDUIT=aries OPT_VER=$OPT_VER CC=icc THOR_ENABLED=-DTHOR_ENABLED THOR_PATH=$THOR_PATH UPC_PATH=$UPC_PATH UPC_INST=$UPC_INST"
echo "make $OPTS V=1 clean"
make $OPTS V=1 clean
echo "make $OPTS V=2 clean"
make $OPTS V=2 clean
echo "make $OPTS V=1 "
make $OPTS V=1 
echo "make $OPTS V=2"
make $OPTS V=2

