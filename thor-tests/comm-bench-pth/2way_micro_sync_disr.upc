#ifdef THOR_ENABLED 
#include "supc.h"
#if CRAY_UPC
#include <bupc2cupc.h>
#endif
#else 
#include <upc.h>
#endif

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sched.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

#define MAX_SIZE 16000000

#define TOTAL_SIZE (MAX_SIZE/2) - 1024

int POW2[] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512,1024, 2048, 4096, 8192, 16384, 32768  /*, 65536,  131072,262144,524288 */ };
int DEPTH[] = {2, 4, 8, 16};
int ITER_PER_MSG[] = {1024,1024,1024,1024,512,512,512,512,256,256,256,256,128,128,128,128,64,64,64,64};

#define BUF_DIST_SMALL 128
#define MAX_DEPTH  DEPTH[sizeof(DEPTH)/sizeof(int)  - 1]

int RUN_THREADS;
#define THREAD_RUN_CHECK(t) ((t) % (THREADS/2) < RUN_THREADS)


  extern void  thor_default_server_init();
  extern void thor_windup_comm();
  extern void thor_winddown_comm();
  extern void thor_killall();
  extern int get_thor_servers_per_domain();
  extern int thor_issue_immediate;  

size_t srcstride[4], dststride[4], count[4];

double *a, *b, *c, *d, *tmp;
shared [] double *remA, *remB;
bupc_handle_t handle[2048];
shared bupc_tick_t ticks_all[THREADS];
typedef shared [] double* sdblptr;
shared sdblptr buf[THREADS];

int main(int argc, char **argv) 
{
  int i,j,k,n,nbytes;
  bupc_tick_t start, end = 0;
  bupc_handle_t  h1, h2;
  double res;
  int ITERS;
  int iter, size, qd, depth;
  int cur_issue, cur_sync, burst, stages, cur_stage;
  int neighbor, pat;

  bupc_tick_t max_tick, min_tick, tick_sum;
  int doasync=0, doblocking=0, dooverhead=0;
  int doget=0, doput=0;
  const char *optype = "ABPGFL";
  depth = 0;

 if(argc < 4) {
    if(MYTHREAD == 0) {
      printf("Usage: 1 [ABO][GP] [DEPTH] [IMM?]\n");
    }
    return 0;;
  } else {
    pat = atoi(argv[1]);
    if(pat == 1) {
      neighbor = THREADS/2;
    } else if(pat == 2) {
      neighbor = THREADS/2;
      RUN_THREADS = 1;
    } else if(pat == 4) {
      neighbor = THREADS/2;
      RUN_THREADS = 4;
    } else {
      if(THREADS > 16) {	 
         neighbor = 16;
      } else {
         neighbor = 1;
      }
    }
  }
  RUN_THREADS = THREADS;
  optype = argv[2];
  if (strchr(optype,'A') || strchr(optype,'a')) doasync = 1;
  if (strchr(optype,'B') || strchr(optype,'b')) doblocking = 1;
  if (strchr(optype,'O') || strchr(optype,'o')) dooverhead = 1;
  if (strchr(optype,'G') || strchr(optype,'g')) doget = 1;
  if (strchr(optype,'P') || strchr(optype,'p')) doput = 1;

  depth = atoi(argv[3]);


#ifdef THOR_ENABLED
  if(argc == 5) {
    thor_issue_immediate = atoi(argv[4]);
  }
  thor_default_server_init();
  thor_windup_comm();
#endif
 
 //sanity checks
 if((POW2[sizeof(POW2)/sizeof(int)-1]+BUF_DIST_SMALL)*DEPTH[sizeof(DEPTH)/sizeof(int)-1] > MAX_SIZE) {
   printf("FOOTPRINT TOO LARGE\n");
   upc_global_exit(1);
 }
 
 
 buf[MYTHREAD] = upc_alloc(2*MAX_SIZE*sizeof(double));
 printf(">>>> Thread %d allocated %d bytes.\n", MYTHREAD, MAX_SIZE*sizeof(double));
 upc_barrier;
 
 remA = (shared [] double*)buf[(MYTHREAD+neighbor)%THREADS];
 remB  = remA + 524288;
 

 a = (double*)(buf[MYTHREAD]);
 printf(">>>> The local buffer for %d is at %p.\n", MYTHREAD, a);
 d = a + TOTAL_SIZE;
 
 for( i = 0; i < MAX_SIZE; i++) {
   a[i] = 1.11;
 }
 
 a = (double*)upc_alloc(MAX_SIZE*sizeof(double));
 b = a+TOTAL_SIZE;
 c  = malloc(MAX_SIZE*sizeof(double));

  for(i = 0; i < TOTAL_SIZE; i++) {
    a[i] = 1.1;
    b[i] = 0.1;
    d[i] = 0.99;
    c[i] = 0.111;
  }
  upc_memget(a, remA, TOTAL_SIZE*sizeof(double));
  printf(">>>> Done memget a\n");
  upc_memget(b, remB, TOTAL_SIZE*sizeof(double));
  printf(">>>> Done memget b\n");


  n = 8;
  ITERS = ITER_PER_MSG[0];
  //ITERS = 0;
  for(iter = 0; iter < ITERS; iter++) {
    //printf(">>>> Start memget_async iter %d.\n", iter);
  for(i = 0; i < depth; i++) {
    handle[i] = bupc_memget_async(a + (n+BUF_DIST_SMALL)*i, remA  + (n+BUF_DIST_SMALL)*i,
        n*sizeof(double));
	bupc_waitsync(handle[i]);
      }
    //printf(">>>> End memget_async iter %d.\n", iter);
  }
  //printf(">>>> Done memget_async, bupc_waitsync a\n");

 for(iter = 0; iter < ITERS; iter++) {
    //printf(">>>> Start memput_async iter %d.\n", iter);
  for(i = 0; i < depth; i++) {
    handle[i] = bupc_memput_async( remA  + (n+BUF_DIST_SMALL)*i, a + (n+BUF_DIST_SMALL)*i, 
        n*sizeof(double));
	bupc_waitsync(handle[i]);
    }
    //printf(">>>> End memput_async iter %d.\n", iter);
  }
  //printf(">>>> Done memput_async, bupc_waitsync a\n");

 bupc_tick_t barrier_overhead = 0;

 upc_barrier;
 start = bupc_ticks_now();
 for(i = 0; i < ITERS; i++)
  upc_barrier;
 end = bupc_ticks_now();
 if(ITERS > 0) {
    barrier_overhead = (end - start) / (ITERS);
 }
 barrier_overhead = 0;
 if (MYTHREAD == 0)
     printf("barrier overhead: %f us\n", (double) bupc_ticks_to_us(barrier_overhead)); 
 double usecs, usecs_min, usecs_max, usecs_sum;
 double bw_sum, bw, bw_min, bw_max;
 double mr_sum, mr, mr_min, mr_max;
 
 
 if (doget) {
   if (doblocking) {
     if (MYTHREAD==0) {
       printf("Starting GET BLOCKING-sync , threads %d ...\n", THREADS);
       printf("%-*s%-*s%-*s%-*s%*s%*s%*s%*s%*s%*s%*s\n", 5, "Op", 6, "Sync", 6, "Tasks", 5, "Size", 6, "Depth", 11, "maxbw", 
	      11, "minbw", 11, "aggbw", 11, "maxmr", 11, "minmr", 11, "aggmr");
     }
     
     
     for(size = 0; size < sizeof(POW2)/sizeof(int); size++) {
       n = POW2[size];
       nbytes = n*sizeof(double);
       ITERS = ITER_PER_MSG[size];
       
       bw_sum = 0;
       mr_sum = 0;
       upc_barrier;
       start = bupc_ticks_now();
       upc_barrier;
       for(iter = 0; iter < ITERS; iter++) {
	 for(i = 0; i < depth; i++) {
	   handle[i] = bupc_memget_async(a + (n+BUF_DIST_SMALL)*i, remA  + (n+BUF_DIST_SMALL)*i,
					 n*sizeof(double));
	   bupc_waitsync(handle[i]);
	 }
       }
       upc_barrier;
       end = (bupc_ticks_now() - start - 2*barrier_overhead);
       ticks_all[MYTHREAD] = end;
       upc_barrier;
       
       if (MYTHREAD == 0) {
	 max_tick = ticks_all[0];
	 min_tick = ticks_all[0];
	 for (j=0; j < THREADS; j++) {
          if (max_tick < ticks_all[j]) 
            max_tick = ticks_all[j];
          if (min_tick > ticks_all[j])
            min_tick = ticks_all[j];
	  
	  usecs = bupc_ticks_to_us(ticks_all[j]);
	  bw = (((double)nbytes)*depth*ITERS)/usecs;
	  mr = (bw * 1e6)/nbytes;
	  bw_sum += bw;
	  mr_sum += mr;
	 }
	 usecs_min = bupc_ticks_to_us(min_tick);
	 usecs_max = bupc_ticks_to_us(max_tick);
	 bw_max = (((double)nbytes)*depth*ITERS)/usecs_min;
	 bw_min = (((double)nbytes)*depth*ITERS)/usecs_max;
	 mr_max = (bw_max * 1e6)/nbytes;
	 mr_min = (bw_min * 1e6)/nbytes;
	 
	 printf("%s %s %d %d %5d %5d %10.2f %10.2f %10.2f  %10.2f  %10.2f  %10.2f\n", "GET", "BLOCK", THREADS, get_thor_servers_per_domain(),
		nbytes, depth, bw_max, bw_min, bw_sum,
		mr_max, mr_min, mr_sum);
       }
     }
   } // if doblocking
   
   upc_barrier;
   if (doasync) {
     if (MYTHREAD==0) {
       printf("Starting GET-PIPE-sync, threads %d...\n", THREADS);
       printf("%-*s%*s%*s%*s%*s%*s%*s%*s\n", 5, "Size", 6, "Depth", 11, "maxbw", 
	      11, "minbw", 11, "aggbw", 11, "maxmr", 11, "minmr", 11, "aggmr");
     }
     
     upc_barrier;
     for(size = 0; size < sizeof(POW2)/sizeof(int); size++) {
       n = POW2[size];
       nbytes = n*sizeof(double);
       ITERS = ITER_PER_MSG[size];
       
	  upc_barrier;
	  start = bupc_ticks_now();
	  upc_barrier;
	  for(iter = 0; iter < ITERS; iter++) {
	    for(i = 0; i < depth; i++) 
	      handle[i] = bupc_memget_async(a + (n+BUF_DIST_SMALL)*i, remA  + (n+BUF_DIST_SMALL)*i,
					    n*sizeof(double));
	    for(i = 0; i < depth; i++) 
	      bupc_waitsync(handle[i]);
	  }
	  upc_barrier;
	  
	  end = (bupc_ticks_now() - start - 2*barrier_overhead);
      ticks_all[MYTHREAD] = end;
      upc_barrier;
      
      if (MYTHREAD == 0) {
        max_tick = ticks_all[0];
        min_tick = ticks_all[0];
        bw_sum = 0;
        mr_sum = 0;
	
        for (j=0; j < THREADS; j++) {
          if (max_tick < ticks_all[j]) 
            max_tick = ticks_all[j];
          if (min_tick > ticks_all[j])
            min_tick = ticks_all[j];
	  
	  usecs = bupc_ticks_to_us(ticks_all[j]);
	  bw = (((double)nbytes)*depth*ITERS)/usecs;
	  mr = (bw * 1e6)/nbytes;
	  bw_sum += bw;
	  mr_sum += mr;
        }
        usecs_min = bupc_ticks_to_us(min_tick);
        usecs_max = bupc_ticks_to_us(max_tick);
        bw_max = (((double)nbytes)*depth*ITERS)/usecs_min;
        bw_min = (((double)nbytes)*depth*ITERS)/usecs_max;
        mr_max = (bw_max * 1e6)/nbytes;
        mr_min = (bw_min * 1e6)/nbytes;
      
        printf("%s %s %d %d %5d %5d %10.2f %10.2f %10.2f  %10.2f  %10.2f  %10.2f\n",  "GET", "PIPE", THREADS, get_thor_servers_per_domain(),
	       nbytes, depth, bw_max, bw_min, bw_sum,
	       mr_max, mr_min, mr_sum);
      }
      //    }
     }
   } // if doasync
   
   if (dooverhead) {
     if (MYTHREAD==0) {
       printf("Starting GET-PIPE-OVERHEAD...\n");
       printf("%-*s%*s%*s\n", 5, "Size", 6, "Depth", 11, "Overhead (us) min max average");
     }
     for(size = 0; size < sizeof(POW2)/sizeof(int); size++) {
       n = POW2[size];
       nbytes = n*sizeof(double);
       end = 0;
       upc_barrier;
       start = bupc_ticks_now();
       for(iter = 0; iter < ITERS; iter++) {
	 start = bupc_ticks_now();
	 for(i = 0; i < depth; i++) 
	   handle[i] = bupc_memget_async(a + (n+BUF_DIST_SMALL)*i, remA  + (n+BUF_DIST_SMALL)*i,
					 n*sizeof(double));
	 end += bupc_ticks_now() - start;
	 for(i = 0; i < depth; i++) 
	   bupc_waitsync(handle[i]);
       }
       ticks_all[MYTHREAD] = end;
       upc_barrier;
       
       if (MYTHREAD == 0) {
	 max_tick = ticks_all[0];
	 min_tick = ticks_all[0];
	 tick_sum = 0;
	 for (j=0; j < THREADS; j++) {
	   if (max_tick < ticks_all[j]) 
	     max_tick = ticks_all[j];
	   if (min_tick > ticks_all[j])
	     min_tick = ticks_all[j];
	   
	   tick_sum += ticks_all[j];
	 }
	 usecs_min = bupc_ticks_to_us(min_tick)/((double) (ITERS*depth));
	 usecs_max = bupc_ticks_to_us(max_tick)/((double) (ITERS*depth));
	 usecs_sum = bupc_ticks_to_us(tick_sum)/((double) (ITERS*depth));
	 
	 printf("%s %s %d %d %5d %5d %10.2f %10.2f %10.2f\n",  "GET", "OVHD", THREADS, get_thor_servers_per_domain(), 
		nbytes, depth, usecs_min, usecs_max, usecs_sum/THREADS);
       }
//    }
     }
   } // if dooverhead
 } // if doget
 
 upc_barrier;
 if (doput) {
   if (doblocking) {
     if (MYTHREAD==0) {
       printf("Starting PUT BLOCKING-sync, threads %d...\n", THREADS);
       printf("%-*s%*s%*s%*s%*s%*s%*s%*s\n", 5, "Size", 6, "Depth", 11, "maxbw", 
	      11, "minbw", 11, "aggbw", 11, "maxmr", 11, "minmr", 11, "aggmr");
     }
     //  upc_barrier;
     for(size = 0; size < sizeof(POW2)/sizeof(int); size++) {
       n = POW2[size];
       nbytes = n*sizeof(double);
       ITERS = ITER_PER_MSG[size];
       
      bw_sum = 0;
      mr_sum = 0;
      upc_barrier;
      start = bupc_ticks_now();
      upc_barrier;
      for(iter = 0; iter < ITERS; iter++) {
        for(i = 0; i < depth; i++) {
          handle[i] = bupc_memput_async(remA  + (n+BUF_DIST_SMALL)*i, a + (n+BUF_DIST_SMALL)*i,
					n*sizeof(double));
          bupc_waitsync(handle[i]);
        }
      }
      upc_barrier;
      end = (bupc_ticks_now() - start-2*barrier_overhead);
      ticks_all[MYTHREAD] = end;
      upc_barrier;
      
      if (MYTHREAD == 0) {
        max_tick = ticks_all[0];
        min_tick = ticks_all[0];
        for (j=0; j < THREADS; j++) {
          if (max_tick < ticks_all[j]) 
            max_tick = ticks_all[j];
          if (min_tick > ticks_all[j])
            min_tick = ticks_all[j];

	  usecs = bupc_ticks_to_us(ticks_all[j]);
	  bw = (((double)nbytes)*depth*ITERS)/usecs;
	  mr = (bw * 1e6)/nbytes;
	  bw_sum += bw;
	  mr_sum += mr;
        }
        usecs_min = bupc_ticks_to_us(min_tick);
        usecs_max = bupc_ticks_to_us(max_tick);
        bw_max = (((double) nbytes)*depth*ITERS)/usecs_min;
        bw_min = (((double) nbytes)*depth*ITERS)/usecs_max;
        mr_max = (bw_max * 1e6)/nbytes;
        mr_min = (bw_min * 1e6)/nbytes;
      
        printf("%s %s %d %d %5d %5d %10.2f %10.2f %10.2f  %10.2f  %10.2f  %10.2f\n",  "PUT", "BLOCK", THREADS, get_thor_servers_per_domain(), 
	       nbytes, depth, bw_max, bw_min, bw_sum,
	       mr_max, mr_min, mr_sum);
      }
      //    }
  }
   } // if do-blocking
   
   upc_barrier;
   if (doasync) {
     if (MYTHREAD==0) {
       printf("Starting PUT-PIPE-sync, threads %d...\n", THREADS);
       printf("%-*s%*s%*s%*s%*s%*s%*s%*s\n", 5, "Size", 6, "Depth", 11, "maxbw", 
	      11, "minbw", 11, "aggbw", 11, "maxmr", 11, "minmr", 11, "aggmr");
     }
       upc_barrier;
  for(size = 0; size < sizeof(POW2)/sizeof(int); size++) {
  //for(size = 0; size < 2; size++) {
    n = POW2[size];
    nbytes = n*sizeof(double);
    ITERS = ITER_PER_MSG[size];
    upc_barrier;
      upc_barrier;
      start = bupc_ticks_now();
      upc_barrier;
      for(iter = 0; iter < ITERS; iter++) {
        for(i = 0; i < depth; i++) 
          handle[i] = bupc_memput_async( remA  + (n+BUF_DIST_SMALL)*i, a + (n+BUF_DIST_SMALL)*i, 
					 n*sizeof(double));
        for(i = 0; i < depth; i++) 
          bupc_waitsync(handle[i]);
      }
      upc_barrier;
      end = (bupc_ticks_now() - start - 2*barrier_overhead);
    upc_barrier;
    ticks_all[MYTHREAD] = end;
    upc_barrier;
    
    if (MYTHREAD == 0) {
      max_tick = ticks_all[0];
      min_tick = ticks_all[0];
      bw_sum = 0;
      mr_sum = 0;
      
      for (j=0; j < THREADS; j++) {
	if (max_tick < ticks_all[j]) 
            max_tick = ticks_all[j];
	if (min_tick > ticks_all[j])
	  min_tick = ticks_all[j];
	
	usecs = bupc_ticks_to_us(ticks_all[j]);
	bw = (((double)nbytes)*depth*ITERS)/usecs;
	mr = (bw * 1e6)/nbytes;
	bw_sum += bw;
	mr_sum += mr;
      }
      usecs_min = bupc_ticks_to_us(min_tick);
      usecs_max = bupc_ticks_to_us(max_tick);
      bw_max = (((double) nbytes)*depth*ITERS)/usecs_min;
      bw_min = (((double) nbytes)*depth*ITERS)/usecs_max;
      mr_max = (bw_max * 1e6)/nbytes;
      mr_min = (bw_min * 1e6)/nbytes;
      
      printf("%s %s %d %d %5d %5d %10.2f %10.2f %10.2f  %10.2f  %10.2f  %10.2f\n", "PUT", "PIPE", THREADS, get_thor_servers_per_domain(), 
	     nbytes, depth, bw_max, bw_min, bw_sum,
          mr_max, mr_min, mr_sum);
    }
    //    }
  }
   } // if doasync
   
   if (dooverhead) {
     if (MYTHREAD==0) {
    printf("Starting PUT-PIPE-OVERHEAD...\n");
    printf("%-*s%*s%*s\n", 5, "Size", 6, "Depth", 11, "Overhead (us) min max average");
     }
     upc_barrier;
     for(size = 0; size < sizeof(POW2)/sizeof(int); size++) {
       n = POW2[size];
       nbytes = n*sizeof(double);
       ITERS = ITER_PER_MSG[size];

       end = 0;
       upc_barrier;
       for(iter = 0; iter < ITERS; iter++) {
	 upc_barrier;
	 start = bupc_ticks_now();
         upc_barrier;
        for(i = 0; i < depth; i++) 
          handle[i] = bupc_memput_async( remA  + (n+BUF_DIST_SMALL)*i, a + (n+BUF_DIST_SMALL)*i, 
					 n*sizeof(double));
        end += bupc_ticks_now() - start;
        for(i = 0; i < depth; i++) 
          bupc_waitsync(handle[i]);
       }
       ticks_all[MYTHREAD] = end;
       upc_barrier;

       if (MYTHREAD == 0) {
	 max_tick = ticks_all[0];
	 min_tick = ticks_all[0];
	 tick_sum = 0;
        for (j=0; j < THREADS; j++) {
          if (max_tick < ticks_all[j]) 
            max_tick = ticks_all[j];
          if (min_tick > ticks_all[j])
            min_tick = ticks_all[j];
	  
          tick_sum += ticks_all[j];
        }
        usecs_min = bupc_ticks_to_us(min_tick)/((double) (ITERS*depth));
        usecs_max = bupc_ticks_to_us(max_tick)/((double) (ITERS*depth));
        usecs_sum = bupc_ticks_to_us(tick_sum)/((double) (ITERS*depth));
	
        printf("%s %s %d %d %5d %5d %10.2f %10.2f %10.2f\n", "PUT", "OVHD", THREADS, get_thor_servers_per_domain(),
	       nbytes, depth, usecs_min, usecs_max, usecs_sum/THREADS);
       }
       //    }
     }
  } // if dooverhead
 } // if doput

 upc_barrier;

#ifdef THOR_ENABLED
 thor_winddown_comm();
 thor_killall();
#endif
}
