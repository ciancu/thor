#!/usr/bin/env csh
setenv PATH /global/homes/n/nchaimov/carver/upc-inst/opt/bin:$PATH
set MAKEFILE_VER=carver
set OPT_VER=opt
set UPC_PATH=/global/u2/n/nchaimov/carver/upc-inst/$OPT_VER
set UPC_INST=$UPC_PATH/include
set THOR_PATH=/global/u2/n/nchaimov/carver/upc-runtime-thor/thor_pth
set OPTS="MYCONDUIT=ibv OPT_VER=$OPT_VER CCOPT=-O0 THOR_PATH=$THOR_PATH UPC_PATH=$UPC_PATH UPC_INST=$UPC_INST"
echo "make -f Makefile.$MAKEFILE_VER $OPTS V=1 clean"
make -f Makefile.$MAKEFILE_VER $OPTS V=1 clean
echo "make -f Makefile.$MAKEFILE_VER $OPTS V=2 clean"
make $OPTS V=2 clean
#echo "make $OPTS V=3 clean"
#make $OPTS V=3 clean
#echo "make $OPTS V=4 clean"
#make $OPTS V=4 clean
echo "make -f Makefile.$MAKEFILE_VER $OPTS V=1 THOR_ENABLED=-DTHOR_ENABLED"
make -f Makefile.$MAKEFILE_VER $OPTS THOR_ENABLED="-DTHOR_ENABLED -DISSUE_DEBUG" CC="icc -O0 -g" UPCCOPT="-g" V=1 

echo "make -f Makefile.$MAKEFILE_VER $OPTS V=2"
make $-f Makefile.$MAKEFILE_VER OPTS V=2

#echo "make $OPTS V=3 THOR_ENABLED=-DTHOR_ENABLED"
#make $OPTS THOR_ENABLED=-DTHOR_ENABLED V=3 
#
#echo "make $OPTS V=4 THOR_ENABLED=-DTHOR_ENABLED"
#make $OPTS THOR_ENABLED=-DTHOR_ENABLED V=4 
