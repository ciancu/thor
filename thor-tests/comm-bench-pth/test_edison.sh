#!/bin/bash
#PBS -q regular
#PBS -l mppwidth=96
#PBS -l walltime=3:00:00
#PBS -N THOR-PTH
#PBS -e thor_pth_nonimm_2numa.$PBS_JOBID.err
#PBS -e thor_pth_nonimm_2numa.$PBS_JOBID.out

export THREADS=8
export PERNODE=2
export PERNUMA=1
export IMMEDIATE=1

cd $PBS_O_WORKDIR

echo ">>>>> ORIG"
unset THOR_ENABLED
unset THOR_USE_PTHREADS
unset GASNET_DOMAIN_COUNT
export UPC_SHARED_HEAP_SIZE=1G
export THOR_REQ_BATCH=1

aprun -n $THREADS -N $PERNODE -S $PERNUMA ./orig-comm-bench-aries 1 AP 512 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA ./orig-comm-bench-aries 1 AG 512 $IMMEDIATE

export THOR_USE_PTHREADS=1
export THOR_SERVERS_PER_DOMAIN=1
export GASNET_DOMAIN_COUNT=2
export UPC_SERVERS_PER_DOMAIN=1
export SUPC_NODE_COUNT=$THREADS
export THOR_CORES_PER_NUMA_DOMAIN=12
export THOR_CORES_PER_DOMAIN=12
export THOR_CORES_PER_NODE=24
export THOR_DOMAINS_PER_NODE=1
export THOR_DOMAIN_COUNT=$THREADS
export UPC_DOMAIN_COUNT=$THREADS

echo ">>>>> PTHREAD Servers per domain: 1"

aprun -n $THREADS -N $PERNODE -S $PERNUMA ./thor-comm-bench-aries 1 AP 512 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA ./thor-comm-bench-aries 1 AG 512 $IMMEDIATE

echo ">>>>> PTHREAD Servers per domain: 2"

export THOR_SERVERS_PER_DOMAIN=2
export GASNET_DOMAIN_COUNT=3
                                          

aprun -n $THREADS -N $PERNODE -S $PERNUMA ./thor-comm-bench-aries 1 AP 512 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA ./thor-comm-bench-aries 1 AG 512 $IMMEDIATE

echo ">>>>> PTHREAD Servers per domain: 3"

export THOR_SERVERS_PER_DOMAIN=3
export GASNET_DOMAIN_COUNT=4


aprun -n $THREADS -N $PERNODE -S $PERNUMA ./thor-comm-bench-aries 1 AP 512 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA ./thor-comm-bench-aries 1 AG 512 $IMMEDIATE

unset THOR_USE_PTHREADS
unset GASNET_DOMAIN_COUNT

echo ">>>>> Done!"
