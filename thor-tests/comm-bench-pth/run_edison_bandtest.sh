#!/bin/bash
#PBS -q premium
#PBS -A m2018
#PBS -l mppwidth=96
#PBS -l walltime=01:00:00
#PBS -N aries-bandtest
#PBS -e bandtest.$PBS_JOBID.err
#PBS -o bandtest.$PBS_JOBID.out

export THREADS=4
export PERNODE=1
export IMMEDIATE=0

cd $PBS_O_WORKDIR

echo ">>>>> BANDTEST"
unset THOR_ENABLED
unset THOR_USE_PTHREADS
unset GASNET_DOMAIN_COUNT
export UPC_SHARED_HEAP_SIZE=1G
export THOR_REQ_BATCH=1

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=8
export PERNODE=2


aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=12
export PERNODE=3

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE



export THREADS=16
export PERNODE=4

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=20
export PERNODE=5

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=24
export PERNODE=6

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=28
export PERNODE=7

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE



export THREADS=32
export PERNODE=8

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=36
export PERNODE=9

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=40
export PERNODE=10

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=44
export PERNODE=11

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=48
export PERNODE=12

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE



export THREADS=52
export PERNODE=13


aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=56
export PERNODE=14

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE



export THREADS=60
export PERNODE=15

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=64
export PERNODE=16

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=68
export PERNODE=17

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=72
export PERNODE=18

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE



export THREADS=76
export PERNODE=19

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=80
export PERNODE=20

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=84
export PERNODE=21

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=88
export PERNODE=22

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE



export THREADS=92
export PERNODE=23

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


export THREADS=96
export PERNODE=24

aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  ./orig-comm-bench-aries 1 AP 128 $IMMEDIATE


echo ">>>>> ORIG"
export THREADS=8
export PERNODE=2
unset THOR_ENABLED
unset THOR_USE_PTHREADS
unset GASNET_DOMAIN_COUNT
export UPC_SHARED_HEAP_SIZE=1G
export THOR_REQ_BATCH=4

aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE -S $PERNUMA  ./orig-comm-bench-aries 1 AP 64 $IMMEDIATE
 


export THOR_USE_PTHREADS=1
export THOR_SERVERS_PER_DOMAIN=1
export GASNET_DOMAIN_COUNT=2
export UPC_SERVERS_PER_DOMAIN=1
export SUPC_NODE_COUNT=$THREADS
export THOR_CORES_PER_NUMA_DOMAIN=12
export THOR_CORES_PER_DOMAIN=12
export THOR_CORES_PER_NODE=24
export THOR_DOMAINS_PER_NODE=1
export THOR_DOMAIN_COUNT=$THREADS
export UPC_DOMAIN_COUNT=$THREADS


echo ">>>>> PTHREAD Servers per domain: 3"

export THREADS=8
export PERNODE=2
export PERNUMA=1
export THOR_SERVERS_PER_DOMAIN=3
export GASNET_DOMAIN_COUNT=4


aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 64 $IMMEDIATE



echo ">>>>> PTHREAD Servers per domain: 5"

export THREADS=8
export PERNODE=2
export PERNUMA=1
export THOR_SERVERS_PER_DOMAIN=5
export GASNET_DOMAIN_COUNT=6

aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 64 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 4 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 8 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 16 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 32 $IMMEDIATE
aprun -n $THREADS -N $PERNODE  -S $PERNUMA ./thor-comm-bench-aries 1 AP 64 $IMMEDIATE

echo ">>>>> Done!"
