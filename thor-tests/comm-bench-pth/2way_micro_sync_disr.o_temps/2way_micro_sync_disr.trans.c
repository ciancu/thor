/* --- UPCR system headers --- */ 
#include "upcr.h" 
#include "whirl2c.h"
#include "upcr_proxy.h"
/*******************************************************
 * C file translated from WHIRL Thu Aug  7 13:53:22 2014
 *******************************************************/

/* UPC Runtime specification expected: 3.6 */
#define UPCR_WANT_MAJOR 3
#define UPCR_WANT_MINOR 6
/* UPC translator version: release 2.19.3, built on Aug  6 2014 at 22:22:57, host aphid linux-x86_64/64, gcc v4.2.4 (Ubuntu 4.2.4-1ubuntu4) */
/* Included code from the initialization script */
#include</global/homes/n/nchaimov/edison/upc-inst/opt/include/upcr_preinclude/upc_types.h>
#include "upcr_geninclude/stddef.h"
#include</global/homes/n/nchaimov/edison/upc-inst/opt/include/upcr_preinclude/upc_bits.h>
#include "upcr_geninclude/stdlib.h"
#include "upcr_geninclude/inttypes.h"
#include "upcr_geninclude/math.h"
#include "upcr_geninclude/stdio.h"
#include "upcr_geninclude/string.h"
#include</usr/include/sched.h>
#include</usr/include/unistd.h>
#include "upcr_geninclude/stdlib.h"
#include</usr/include/pthread.h>
#line 1 "2way_micro_sync_disr.w2c.h"
/* Include builtin types and operators */

#ifndef UPCR_TRANS_EXTRA_INCL
#define UPCR_TRANS_EXTRA_INCL
extern int upcrt_gcd (int _a, int _b);
extern int _upcrt_forall_start(int _start_thread, int _step, int _lo, int _scale);
#define upcrt_forall_start(start_thread, step, lo, scale)  \
       _upcrt_forall_start(start_thread, step, lo, scale)
int32_t UPCR_TLD_DEFINE_TENTATIVE(upcrt_forall_control, 4, 4);
#define upcr_forall_control upcrt_forall_control
#ifndef UPCR_EXIT_FUNCTION
#define UPCR_EXIT_FUNCTION() ((void)0)
#endif
#if UPCR_RUNTIME_SPEC_MAJOR > 3 || (UPCR_RUNTIME_SPEC_MAJOR == 3 && UPCR_RUNTIME_SPEC_MINOR >= 8)
  #define UPCRT_STARTUP_SHALLOC(sptr, blockbytes, numblocks, mult_by_threads, elemsz, typestr) \
      { &(sptr), (blockbytes), (numblocks), (mult_by_threads), (elemsz), #sptr, (typestr) }
#else
  #define UPCRT_STARTUP_SHALLOC(sptr, blockbytes, numblocks, mult_by_threads, elemsz, typestr) \
      { &(sptr), (blockbytes), (numblocks), (mult_by_threads) }
#endif
#define UPCRT_STARTUP_PSHALLOC UPCRT_STARTUP_SHALLOC

/**** Autonb optimization ********/

extern void _upcrt_memput_nb(upcr_shared_ptr_t _dst, const void *_src, size_t _n);
#define upcrt_memput_nb(dst, src, n) \
       (upcri_srcpos(), _upcrt_memput_nb(dst, src, n))

#endif


/* Types */
struct _upcri_eop;
  /* File-level vars and routines */
extern int user_main(int, char **);

extern int get_thor_servers_per_domain();


#define UPCR_SHARED_SIZE_ 16
#define UPCR_PSHARED_SIZE_ 16
upcr_pshared_ptr_t ticks_all;
upcr_pshared_ptr_t buf;
int UPCR_TLD_DEFINE_TENTATIVE(RUN_THREADS, 4, 4);
extern int thor_issue_immediate;
typedef unsigned long _type_srcstride[4];
_type_srcstride UPCR_TLD_DEFINE_TENTATIVE(srcstride, 32, 8);
typedef unsigned long _type_dststride[4];
_type_dststride UPCR_TLD_DEFINE_TENTATIVE(dststride, 32, 8);
typedef unsigned long _type_count[4];
_type_count UPCR_TLD_DEFINE_TENTATIVE(count, 32, 8);
double*  UPCR_TLD_DEFINE_TENTATIVE(a, 8, 8);
double*  UPCR_TLD_DEFINE_TENTATIVE(b, 8, 8);
double*  UPCR_TLD_DEFINE_TENTATIVE(c, 8, 8);
double*  UPCR_TLD_DEFINE_TENTATIVE(d, 8, 8);
double*  UPCR_TLD_DEFINE_TENTATIVE(tmp, 8, 8);
upcr_pshared_ptr_t UPCR_TLD_DEFINE_TENTATIVE(remA, 16, 8);
upcr_pshared_ptr_t UPCR_TLD_DEFINE_TENTATIVE(remB, 16, 8);
typedef struct _upcri_eop*  _type_handle[2048];
_type_handle UPCR_TLD_DEFINE_TENTATIVE(handle, 16384, 8);
typedef int _type_POW2[16];
_type_POW2 UPCR_TLD_DEFINE(POW2, 64, 4) = {1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,};
typedef int _type_DEPTH[4];
_type_DEPTH UPCR_TLD_DEFINE(DEPTH, 16, 4) = {2,4,8,16,};
typedef int _type_ITER_PER_MSG[20];
_type_ITER_PER_MSG UPCR_TLD_DEFINE(ITER_PER_MSG, 80, 4) = {1024,1024,1024,1024,512,512,512,512,256,256,256,256,128,128,128,128,64,64,64,64,};

void UPCRI_ALLOC__way_micro_sync_disr_12616619006698816357(void) { 
UPCR_BEGIN_FUNCTION();
upcr_startup_pshalloc_t _bupc_pinfo[] = { 
UPCRT_STARTUP_PSHALLOC(ticks_all, 8, 1, 1, 8, "A1H_R1_y"), 
UPCRT_STARTUP_PSHALLOC(buf, 16, 1, 1, 16, "A1H_R1_PR0_d"), 
 };

UPCR_SET_SRCPOS("__way_micro_sync_disr_12616619006698816357_ALLOC",0);
upcr_startup_pshalloc(_bupc_pinfo, sizeof(_bupc_pinfo) / sizeof(upcr_startup_pshalloc_t));
}

void UPCRI_INIT__way_micro_sync_disr_12616619006698816357(void) { 
UPCR_BEGIN_FUNCTION();
UPCR_SET_SRCPOS("__way_micro_sync_disr_12616619006698816357_INIT",0);
}

#line 50 "2way_micro_sync_disr.upc"
extern int user_main(
  int argc,
  char ** argv)
#line 50 "2way_micro_sync_disr.upc"
{
#line 50 "2way_micro_sync_disr.upc"
  UPCR_BEGIN_FUNCTION();
  register _INT32 _bupc_comma13;
  register _INT32 _bupc_comma14;
  register _UINT64 _bupc_comma15;
  register _UINT64 _bupc_comma16;
  register _UINT64 _bupc_comma17;
  register _UINT64 _bupc_comma18;
  register _UINT64 _bupc_comma19;
  register _UINT64 _bupc_comma20;
  register _UINT64 _bupc_comma21;
  register _UINT64 _bupc_comma22;
  register _INT32 _bupc_comma23;
  register _UINT64 _bupc_comma24;
  register _UINT64 _bupc_comma25;
  register _UINT64 _bupc_comma26;
  register _UINT64 _bupc_comma27;
  register _UINT64 _bupc_comma28;
  register _INT32 _bupc_comma29;
  register _UINT64 _bupc_comma30;
  register _UINT64 _bupc_comma31;
  register _UINT64 _bupc_comma32;
  register _UINT64 _bupc_comma33;
  register _UINT64 _bupc_comma34;
  register _UINT64 _bupc_comma35;
  register _INT32 _bupc_comma36;
  register _UINT64 _bupc_comma37;
  register _UINT64 _bupc_comma38;
  register _UINT64 _bupc_comma39;
  register _UINT64 _bupc_comma40;
  register _UINT64 _bupc_comma;
  register _INT32 _bupc_comma0;
  register _UINT64 _bupc_comma1;
  register _UINT64 _bupc_comma2;
  register _UINT64 _bupc_comma3;
  register _UINT64 _bupc_comma4;
  register _UINT64 _bupc_comma5;
  register _INT32 _bupc_comma6;
  register _UINT64 _bupc_comma7;
  register _UINT64 _bupc_comma8;
  register _UINT64 _bupc_comma9;
  register _UINT64 _bupc_comma10;
  register _UINT64 _bupc_comma11;
  register _INT32 _bupc_comma12;
  unsigned long end;
  int doasync;
  int doblocking;
  int dooverhead;
  int doget;
  int doput;
  const char * optype;
  int depth;
  int pat;
  int neighbor;
  char * _bupc__spilleq0;
  char * _bupc__spilleq1;
  char * _bupc__spilleq2;
  char * _bupc__spilleq3;
  char * _bupc__spilleq4;
  char * _bupc__spilleq5;
  char * _bupc__spilleq6;
  char * _bupc__spilleq7;
  char * _bupc__spilleq8;
  char * _bupc__spilleq9;
  int i;
  _IEEE64 * _bupc__casttmp10;
  int n;
  int ITERS;
  int iter;
  unsigned long barrier_overhead;
  unsigned long start;
  int size;
  int nbytes;
  _IEEE64 bw_sum;
  _IEEE64 mr_sum;
  unsigned long max_tick;
  unsigned long min_tick;
  int j;
  _IEEE64 usecs;
  _IEEE64 bw;
  _IEEE64 mr;
  _IEEE64 usecs_min;
  _IEEE64 usecs_max;
  _IEEE64 bw_max;
  _IEEE64 bw_min;
  _IEEE64 mr_max;
  _IEEE64 mr_min;
  unsigned long tick_sum;
  _IEEE64 usecs_sum;
  char * _bupc_call0;
  char * _bupc_call1;
  char * _bupc_call2;
  char * _bupc_call3;
  char * _bupc_call4;
  char * _bupc_call5;
  char * _bupc_call6;
  char * _bupc_call7;
  char * _bupc_call8;
  char * _bupc_call9;
  upcr_shared_ptr_t _bupc_call10;
  upcr_shared_ptr_t _bupc_call11;
  void * _bupc_call12;
  struct _upcri_eop * _bupc_call13;
  struct _upcri_eop * _bupc_call14;
  struct _upcri_eop * _bupc_call15;
  struct _upcri_eop * _bupc_call16;
  struct _upcri_eop * _bupc_call17;
  struct _upcri_eop * _bupc_call18;
  struct _upcri_eop * _bupc_call19;
  struct _upcri_eop * _bupc_call20;
  upcr_pshared_ptr_t _bupc_Mstopcvt21;
  upcr_pshared_ptr_t _bupc_Mptra22;
  upcr_pshared_ptr_t _bupc_Mptra23;
  upcr_pshared_ptr_t _bupc_spillld24;
  upcr_pshared_ptr_t _bupc_Mptra25;
  upcr_pshared_ptr_t _bupc_Mptra26;
  upcr_pshared_ptr_t _bupc_spillld27;
  _IEEE64 * _bupc_Mcvtptr28;
  _IEEE64 * _bupc_Mcvtptr29;
  upcr_shared_ptr_t _bupc_Mstopcvt30;
  upcr_shared_ptr_t _bupc_Mstopcvt31;
  upcr_pshared_ptr_t _bupc_Mptra32;
  upcr_shared_ptr_t _bupc_Mstopcvt33;
  upcr_pshared_ptr_t _bupc_Mptra34;
  upcr_shared_ptr_t _bupc_Mstopcvt35;
  upcr_pshared_ptr_t _bupc_Mptra36;
  upcr_shared_ptr_t _bupc_Mstopcvt37;
  upcr_pshared_ptr_t _bupc_Mptra38;
  unsigned long _bupc_spillld39;
  unsigned long _bupc_spillld40;
  upcr_pshared_ptr_t _bupc_Mptra41;
  unsigned long _bupc_spillld42;
  upcr_pshared_ptr_t _bupc_Mptra43;
  unsigned long _bupc_spillld44;
  upcr_pshared_ptr_t _bupc_Mptra45;
  unsigned long _bupc_spillld46;
  upcr_pshared_ptr_t _bupc_Mptra47;
  unsigned long _bupc_spillld48;
  upcr_pshared_ptr_t _bupc_Mptra49;
  unsigned long _bupc_spillld50;
  upcr_pshared_ptr_t _bupc_Mptra51;
  upcr_shared_ptr_t _bupc_Mstopcvt52;
  upcr_pshared_ptr_t _bupc_Mptra53;
  unsigned long _bupc_spillld54;
  unsigned long _bupc_spillld55;
  upcr_pshared_ptr_t _bupc_Mptra56;
  unsigned long _bupc_spillld57;
  upcr_pshared_ptr_t _bupc_Mptra58;
  unsigned long _bupc_spillld59;
  upcr_pshared_ptr_t _bupc_Mptra60;
  unsigned long _bupc_spillld61;
  upcr_pshared_ptr_t _bupc_Mptra62;
  unsigned long _bupc_spillld63;
  upcr_pshared_ptr_t _bupc_Mptra64;
  unsigned long _bupc_spillld65;
  upcr_pshared_ptr_t _bupc_Mptra66;
  upcr_shared_ptr_t _bupc_Mstopcvt67;
  upcr_pshared_ptr_t _bupc_Mptra68;
  unsigned long _bupc_spillld69;
  unsigned long _bupc_spillld70;
  upcr_pshared_ptr_t _bupc_Mptra71;
  unsigned long _bupc_spillld72;
  upcr_pshared_ptr_t _bupc_Mptra73;
  unsigned long _bupc_spillld74;
  upcr_pshared_ptr_t _bupc_Mptra75;
  unsigned long _bupc_spillld76;
  upcr_pshared_ptr_t _bupc_Mptra77;
  unsigned long _bupc_spillld78;
  upcr_pshared_ptr_t _bupc_Mptra79;
  unsigned long _bupc_spillld80;
  upcr_pshared_ptr_t _bupc_Mptra81;
  upcr_shared_ptr_t _bupc_Mstopcvt82;
  upcr_pshared_ptr_t _bupc_Mptra83;
  unsigned long _bupc_spillld84;
  unsigned long _bupc_spillld85;
  upcr_pshared_ptr_t _bupc_Mptra86;
  unsigned long _bupc_spillld87;
  upcr_pshared_ptr_t _bupc_Mptra88;
  unsigned long _bupc_spillld89;
  upcr_pshared_ptr_t _bupc_Mptra90;
  unsigned long _bupc_spillld91;
  upcr_pshared_ptr_t _bupc_Mptra92;
  unsigned long _bupc_spillld93;
  upcr_pshared_ptr_t _bupc_Mptra94;
  unsigned long _bupc_spillld95;
  upcr_pshared_ptr_t _bupc_Mptra96;
  upcr_shared_ptr_t _bupc_Mstopcvt97;
  upcr_pshared_ptr_t _bupc_Mptra98;
  unsigned long _bupc_spillld99;
  unsigned long _bupc_spillld100;
  upcr_pshared_ptr_t _bupc_Mptra101;
  unsigned long _bupc_spillld102;
  upcr_pshared_ptr_t _bupc_Mptra103;
  unsigned long _bupc_spillld104;
  upcr_pshared_ptr_t _bupc_Mptra105;
  unsigned long _bupc_spillld106;
  upcr_pshared_ptr_t _bupc_Mptra107;
  unsigned long _bupc_spillld108;
  upcr_pshared_ptr_t _bupc_Mptra109;
  unsigned long _bupc_spillld110;
  upcr_pshared_ptr_t _bupc_Mptra111;
  upcr_shared_ptr_t _bupc_Mstopcvt112;
  upcr_pshared_ptr_t _bupc_Mptra113;
  unsigned long _bupc_spillld114;
  unsigned long _bupc_spillld115;
  upcr_pshared_ptr_t _bupc_Mptra116;
  unsigned long _bupc_spillld117;
  upcr_pshared_ptr_t _bupc_Mptra118;
  unsigned long _bupc_spillld119;
  upcr_pshared_ptr_t _bupc_Mptra120;
  unsigned long _bupc_spillld121;
  upcr_pshared_ptr_t _bupc_Mptra122;
  unsigned long _bupc_spillld123;
  upcr_pshared_ptr_t _bupc_Mptra124;
  unsigned long _bupc_spillld125;
  
#line 52 "2way_micro_sync_disr.upc"
  end = 0ULL;
#line 61 "2way_micro_sync_disr.upc"
  doasync = 0;
#line 61 "2way_micro_sync_disr.upc"
  doblocking = 0;
#line 61 "2way_micro_sync_disr.upc"
  dooverhead = 0;
#line 62 "2way_micro_sync_disr.upc"
  doget = 0;
#line 62 "2way_micro_sync_disr.upc"
  doput = 0;
#line 63 "2way_micro_sync_disr.upc"
  optype = "ABPGFL";
#line 64 "2way_micro_sync_disr.upc"
  depth = 0;
#line 66 "2way_micro_sync_disr.upc"
  if(argc <= 3)
#line 66 "2way_micro_sync_disr.upc"
  {
#line 67 "2way_micro_sync_disr.upc"
    if(((int) upcr_mythread () ) == 0)
#line 67 "2way_micro_sync_disr.upc"
    {
#line 68 "2way_micro_sync_disr.upc"
      printf("Usage: 1 [ABO][GP] [DEPTH] [IMM?]\n");
    }
#line 70 "2way_micro_sync_disr.upc"
    UPCR_EXIT_FUNCTION();
#line 70 "2way_micro_sync_disr.upc"
    return 0;
  }
  else
#line 70 "2way_micro_sync_disr.upc"
  {
#line 72 "2way_micro_sync_disr.upc"
    _bupc_comma13 = atoi((const char *) * (argv + 1LL));
#line 72 "2way_micro_sync_disr.upc"
    pat = _bupc_comma13;
#line 73 "2way_micro_sync_disr.upc"
    if(pat == 1)
#line 73 "2way_micro_sync_disr.upc"
    {
#line 74 "2way_micro_sync_disr.upc"
      neighbor = ((int) upcr_threads () ) / 2;
    }
    else
#line 74 "2way_micro_sync_disr.upc"
    {
#line 75 "2way_micro_sync_disr.upc"
      if(pat == 2)
#line 75 "2way_micro_sync_disr.upc"
      {
#line 76 "2way_micro_sync_disr.upc"
        neighbor = ((int) upcr_threads () ) / 2;
#line 77 "2way_micro_sync_disr.upc"
        RUN_THREADS = 1;
      }
      else
#line 77 "2way_micro_sync_disr.upc"
      {
#line 78 "2way_micro_sync_disr.upc"
        if(pat == 4)
#line 78 "2way_micro_sync_disr.upc"
        {
#line 79 "2way_micro_sync_disr.upc"
          neighbor = ((int) upcr_threads () ) / 2;
#line 80 "2way_micro_sync_disr.upc"
          RUN_THREADS = 4;
        }
        else
#line 80 "2way_micro_sync_disr.upc"
        {
#line 82 "2way_micro_sync_disr.upc"
          if(((int) upcr_threads () ) > 16)
#line 82 "2way_micro_sync_disr.upc"
          {
#line 83 "2way_micro_sync_disr.upc"
            neighbor = 16;
          }
          else
#line 83 "2way_micro_sync_disr.upc"
          {
#line 85 "2way_micro_sync_disr.upc"
            neighbor = 1;
          }
        }
      }
    }
  }
#line 89 "2way_micro_sync_disr.upc"
  RUN_THREADS = ((int) upcr_threads () );
#line 90 "2way_micro_sync_disr.upc"
  optype = (const char *) * (argv + 2LL);
#line 91 "2way_micro_sync_disr.upc"
  _bupc_call0 = strchr(optype, (int) 65);
#line 91 "2way_micro_sync_disr.upc"
  _bupc__spilleq0 = _bupc_call0;
#line 91 "2way_micro_sync_disr.upc"
  if((_UINT64)(_bupc__spilleq0) != 0ULL)
    goto _48;
#line 91 "2way_micro_sync_disr.upc"
  _bupc_call1 = strchr(optype, (int) 97);
#line 91 "2way_micro_sync_disr.upc"
  _bupc__spilleq1 = _bupc_call1;
#line 91 "2way_micro_sync_disr.upc"
  if(!((_UINT64)(_bupc__spilleq1) != 0ULL))
    goto _49;
#line 91 "2way_micro_sync_disr.upc"
  _48 :;
#line 91 "2way_micro_sync_disr.upc"
  doasync = 1;
#line 91 "2way_micro_sync_disr.upc"
  _49 :;
#line 92 "2way_micro_sync_disr.upc"
  _bupc_call2 = strchr(optype, (int) 66);
#line 92 "2way_micro_sync_disr.upc"
  _bupc__spilleq2 = _bupc_call2;
#line 92 "2way_micro_sync_disr.upc"
  if((_UINT64)(_bupc__spilleq2) != 0ULL)
    goto _51;
#line 92 "2way_micro_sync_disr.upc"
  _bupc_call3 = strchr(optype, (int) 98);
#line 92 "2way_micro_sync_disr.upc"
  _bupc__spilleq3 = _bupc_call3;
#line 92 "2way_micro_sync_disr.upc"
  if(!((_UINT64)(_bupc__spilleq3) != 0ULL))
    goto _52;
#line 92 "2way_micro_sync_disr.upc"
  _51 :;
#line 92 "2way_micro_sync_disr.upc"
  doblocking = 1;
#line 92 "2way_micro_sync_disr.upc"
  _52 :;
#line 93 "2way_micro_sync_disr.upc"
  _bupc_call4 = strchr(optype, (int) 79);
#line 93 "2way_micro_sync_disr.upc"
  _bupc__spilleq4 = _bupc_call4;
#line 93 "2way_micro_sync_disr.upc"
  if((_UINT64)(_bupc__spilleq4) != 0ULL)
    goto _54;
#line 93 "2way_micro_sync_disr.upc"
  _bupc_call5 = strchr(optype, (int) 111);
#line 93 "2way_micro_sync_disr.upc"
  _bupc__spilleq5 = _bupc_call5;
#line 93 "2way_micro_sync_disr.upc"
  if(!((_UINT64)(_bupc__spilleq5) != 0ULL))
    goto _55;
#line 93 "2way_micro_sync_disr.upc"
  _54 :;
#line 93 "2way_micro_sync_disr.upc"
  dooverhead = 1;
#line 93 "2way_micro_sync_disr.upc"
  _55 :;
#line 94 "2way_micro_sync_disr.upc"
  _bupc_call6 = strchr(optype, (int) 71);
#line 94 "2way_micro_sync_disr.upc"
  _bupc__spilleq6 = _bupc_call6;
#line 94 "2way_micro_sync_disr.upc"
  if((_UINT64)(_bupc__spilleq6) != 0ULL)
    goto _57;
#line 94 "2way_micro_sync_disr.upc"
  _bupc_call7 = strchr(optype, (int) 103);
#line 94 "2way_micro_sync_disr.upc"
  _bupc__spilleq7 = _bupc_call7;
#line 94 "2way_micro_sync_disr.upc"
  if(!((_UINT64)(_bupc__spilleq7) != 0ULL))
    goto _58;
#line 94 "2way_micro_sync_disr.upc"
  _57 :;
#line 94 "2way_micro_sync_disr.upc"
  doget = 1;
#line 94 "2way_micro_sync_disr.upc"
  _58 :;
#line 95 "2way_micro_sync_disr.upc"
  _bupc_call8 = strchr(optype, (int) 80);
#line 95 "2way_micro_sync_disr.upc"
  _bupc__spilleq8 = _bupc_call8;
#line 95 "2way_micro_sync_disr.upc"
  if((_UINT64)(_bupc__spilleq8) != 0ULL)
    goto _60;
#line 95 "2way_micro_sync_disr.upc"
  _bupc_call9 = strchr(optype, (int) 112);
#line 95 "2way_micro_sync_disr.upc"
  _bupc__spilleq9 = _bupc_call9;
#line 95 "2way_micro_sync_disr.upc"
  if(!((_UINT64)(_bupc__spilleq9) != 0ULL))
    goto _61;
#line 95 "2way_micro_sync_disr.upc"
  _60 :;
#line 95 "2way_micro_sync_disr.upc"
  doput = 1;
#line 95 "2way_micro_sync_disr.upc"
  _61 :;
#line 97 "2way_micro_sync_disr.upc"
  _bupc_comma14 = atoi((const char *) * (argv + 3LL));
#line 97 "2way_micro_sync_disr.upc"
  depth = _bupc_comma14;
#line 109 "2way_micro_sync_disr.upc"
  if((((POW2)[15ULL] + 128) * (DEPTH)[3ULL]) > 16000000)
#line 109 "2way_micro_sync_disr.upc"
  {
#line 110 "2way_micro_sync_disr.upc"
    printf("FOOTPRINT TOO LARGE\n");
#line 111 "2way_micro_sync_disr.upc"
    upc_global_exit((int) 1);
  }
#line 115 "2way_micro_sync_disr.upc"
  _bupc_call10 = upc_alloc((unsigned long) 256000000ULL);
#line 115 "2way_micro_sync_disr.upc"
  _bupc_Mptra22 = UPCR_ADD_PSHARED1(buf, 16ULL, ((int) upcr_mythread () ));
#line 115 "2way_micro_sync_disr.upc"
  _bupc_Mstopcvt21 = UPCR_SHARED_TO_PSHARED(_bupc_call10);
#line 115 "2way_micro_sync_disr.upc"
  UPCR_PUT_PSHARED(_bupc_Mptra22, 0, &_bupc_Mstopcvt21, 16);
#line 116 "2way_micro_sync_disr.upc"
  printf(">>>> Thread %d allocated %d bytes.\n", ((int) upcr_mythread () ), 128000000ULL);
#line 117 "2way_micro_sync_disr.upc"
  upcr_barrier(280593253, 1);
#line 119 "2way_micro_sync_disr.upc"
  _bupc_Mptra23 = UPCR_ADD_PSHARED1(buf, 16ULL, (((int) upcr_mythread () ) + neighbor) % ((int) upcr_threads () ));
#line 119 "2way_micro_sync_disr.upc"
  UPCR_GET_PSHARED(&_bupc_spillld24, _bupc_Mptra23, 0, 16);
#line 119 "2way_micro_sync_disr.upc"
  remA = _bupc_spillld24;
#line 120 "2way_micro_sync_disr.upc"
  _bupc_Mptra25 = UPCR_ADD_PSHAREDI(remA, 8ULL, 524288);
#line 120 "2way_micro_sync_disr.upc"
  remB = _bupc_Mptra25;
#line 123 "2way_micro_sync_disr.upc"
  _bupc_Mptra26 = UPCR_ADD_PSHARED1(buf, 16ULL, ((int) upcr_mythread () ));
#line 123 "2way_micro_sync_disr.upc"
  UPCR_GET_PSHARED(&_bupc_spillld27, _bupc_Mptra26, 0, 16);
#line 123 "2way_micro_sync_disr.upc"
  _bupc_Mcvtptr28 = (_IEEE64 *) UPCR_PSHARED_TO_LOCAL(_bupc_spillld27);
#line 123 "2way_micro_sync_disr.upc"
  a = _bupc_Mcvtptr28;
#line 124 "2way_micro_sync_disr.upc"
  printf(">>>> The local buffer for %d is at %p.\n", ((int) upcr_mythread () ), a);
#line 125 "2way_micro_sync_disr.upc"
  d = (_IEEE64 *)((_UINT64)((a + 8000000)) - (_UINT64)(8192));
#line 127 "2way_micro_sync_disr.upc"
  i = 0;
#line 127 "2way_micro_sync_disr.upc"
  while(i <= 15999999)
#line 127 "2way_micro_sync_disr.upc"
  {
#line 128 "2way_micro_sync_disr.upc"
    * (a + i) = 1.11;
#line 129 "2way_micro_sync_disr.upc"
    _1 :;
#line 129 "2way_micro_sync_disr.upc"
    i = i + 1;
  }
#line 131 "2way_micro_sync_disr.upc"
  _bupc_call11 = upc_alloc((unsigned long) 128000000ULL);
#line 131 "2way_micro_sync_disr.upc"
  _bupc_Mcvtptr29 = (_IEEE64 *) UPCR_SHARED_TO_LOCAL(_bupc_call11);
#line 131 "2way_micro_sync_disr.upc"
  a = _bupc_Mcvtptr29;
#line 132 "2way_micro_sync_disr.upc"
  b = (_IEEE64 *)((_UINT64)((a + 8000000)) - (_UINT64)(8192));
#line 133 "2way_micro_sync_disr.upc"
  _bupc_call12 = malloc((unsigned long) 128000000ULL);
#line 133 "2way_micro_sync_disr.upc"
  _bupc__casttmp10 = _bupc_call12;
#line 133 "2way_micro_sync_disr.upc"
  c = _bupc__casttmp10;
#line 135 "2way_micro_sync_disr.upc"
  i = 0;
#line 135 "2way_micro_sync_disr.upc"
  while(i <= 7998975)
#line 135 "2way_micro_sync_disr.upc"
  {
#line 136 "2way_micro_sync_disr.upc"
    * (a + i) = 1.1;
#line 137 "2way_micro_sync_disr.upc"
    * (b + i) = 0.1;
#line 138 "2way_micro_sync_disr.upc"
    * (d + i) = 0.99;
#line 139 "2way_micro_sync_disr.upc"
    * (c + i) = 0.111;
#line 140 "2way_micro_sync_disr.upc"
    _2 :;
#line 140 "2way_micro_sync_disr.upc"
    i = i + 1;
  }
#line 141 "2way_micro_sync_disr.upc"
  _bupc_Mstopcvt30 = UPCR_PSHARED_TO_SHARED(remA);
#line 141 "2way_micro_sync_disr.upc"
  upc_memget(a, _bupc_Mstopcvt30, (unsigned long) 7991808ULL);
#line 142 "2way_micro_sync_disr.upc"
  printf(">>>> Done memget a\n");
#line 143 "2way_micro_sync_disr.upc"
  _bupc_Mstopcvt31 = UPCR_PSHARED_TO_SHARED(remB);
#line 143 "2way_micro_sync_disr.upc"
  upc_memget(b, _bupc_Mstopcvt31, (unsigned long) 7991808ULL);
#line 144 "2way_micro_sync_disr.upc"
  printf(">>>> Done memget b\n");
#line 147 "2way_micro_sync_disr.upc"
  n = 8;
#line 148 "2way_micro_sync_disr.upc"
  ITERS = (ITER_PER_MSG)[0];
#line 150 "2way_micro_sync_disr.upc"
  iter = 0;
#line 150 "2way_micro_sync_disr.upc"
  while(iter < ITERS)
#line 150 "2way_micro_sync_disr.upc"
  {
#line 152 "2way_micro_sync_disr.upc"
    i = 0;
#line 152 "2way_micro_sync_disr.upc"
    while(i < depth)
#line 152 "2way_micro_sync_disr.upc"
    {
#line 154 "2way_micro_sync_disr.upc"
      _bupc_Mptra32 = UPCR_ADD_PSHAREDI(remA, 8ULL, (n + 128) * i);
#line 154 "2way_micro_sync_disr.upc"
      _bupc_Mstopcvt33 = UPCR_PSHARED_TO_SHARED(_bupc_Mptra32);
#line 154 "2way_micro_sync_disr.upc"
      _bupc_call13 = bupc_memget_async((_IEEE64 *)((_UINT8 *)(a) + (((n * 8) + 1024) * i)), _bupc_Mstopcvt33, (unsigned long)((_UINT64)(n) * 8ULL));
#line 154 "2way_micro_sync_disr.upc"
      (handle)[i] = _bupc_call13;
#line 155 "2way_micro_sync_disr.upc"
      bupc_waitsync((handle)[i]);
#line 156 "2way_micro_sync_disr.upc"
      _4 :;
#line 156 "2way_micro_sync_disr.upc"
      i = i + 1;
    }
#line 158 "2way_micro_sync_disr.upc"
    _3 :;
#line 158 "2way_micro_sync_disr.upc"
    iter = iter + 1;
  }
#line 161 "2way_micro_sync_disr.upc"
  iter = 0;
#line 161 "2way_micro_sync_disr.upc"
  while(iter < ITERS)
#line 161 "2way_micro_sync_disr.upc"
  {
#line 163 "2way_micro_sync_disr.upc"
    i = 0;
#line 163 "2way_micro_sync_disr.upc"
    while(i < depth)
#line 163 "2way_micro_sync_disr.upc"
    {
#line 165 "2way_micro_sync_disr.upc"
      _bupc_Mptra34 = UPCR_ADD_PSHAREDI(remA, 8ULL, (n + 128) * i);
#line 165 "2way_micro_sync_disr.upc"
      _bupc_Mstopcvt35 = UPCR_PSHARED_TO_SHARED(_bupc_Mptra34);
#line 165 "2way_micro_sync_disr.upc"
      _bupc_call14 = bupc_memput_async(_bupc_Mstopcvt35, (_IEEE64 *)((_UINT8 *)(a) + (((n * 8) + 1024) * i)), (unsigned long)((_UINT64)(n) * 8ULL));
#line 165 "2way_micro_sync_disr.upc"
      (handle)[i] = _bupc_call14;
#line 166 "2way_micro_sync_disr.upc"
      bupc_waitsync((handle)[i]);
#line 167 "2way_micro_sync_disr.upc"
      _6 :;
#line 167 "2way_micro_sync_disr.upc"
      i = i + 1;
    }
#line 169 "2way_micro_sync_disr.upc"
    _5 :;
#line 169 "2way_micro_sync_disr.upc"
    iter = iter + 1;
  }
#line 172 "2way_micro_sync_disr.upc"
  barrier_overhead = 0ULL;
#line 174 "2way_micro_sync_disr.upc"
  upcr_barrier(280593254, 1);
#line 175 "2way_micro_sync_disr.upc"
  _bupc_comma15 = bupc_ticks_now();
#line 175 "2way_micro_sync_disr.upc"
  start = _bupc_comma15;
#line 176 "2way_micro_sync_disr.upc"
  i = 0;
#line 176 "2way_micro_sync_disr.upc"
  while(i < ITERS)
#line 176 "2way_micro_sync_disr.upc"
  {
#line 177 "2way_micro_sync_disr.upc"
    upcr_barrier(280593255, 1);
#line 177 "2way_micro_sync_disr.upc"
    _7 :;
#line 177 "2way_micro_sync_disr.upc"
    i = i + 1;
  }
#line 178 "2way_micro_sync_disr.upc"
  _bupc_comma16 = bupc_ticks_now();
#line 178 "2way_micro_sync_disr.upc"
  end = _bupc_comma16;
#line 179 "2way_micro_sync_disr.upc"
  if(ITERS > 0)
#line 179 "2way_micro_sync_disr.upc"
  {
#line 180 "2way_micro_sync_disr.upc"
    barrier_overhead = (end - start) / (_UINT64)(ITERS);
  }
#line 182 "2way_micro_sync_disr.upc"
  barrier_overhead = 0ULL;
#line 183 "2way_micro_sync_disr.upc"
  if(((int) upcr_mythread () ) == 0)
#line 183 "2way_micro_sync_disr.upc"
  {
#line 184 "2way_micro_sync_disr.upc"
    _bupc_comma17 = bupc_ticks_to_us(barrier_overhead);
#line 184 "2way_micro_sync_disr.upc"
    printf("barrier overhead: %f us\n", (_IEEE64)(_bupc_comma17));
  }
#line 190 "2way_micro_sync_disr.upc"
  if(doget != 0)
#line 190 "2way_micro_sync_disr.upc"
  {
#line 191 "2way_micro_sync_disr.upc"
    if(doblocking != 0)
#line 191 "2way_micro_sync_disr.upc"
    {
#line 192 "2way_micro_sync_disr.upc"
      if(((int) upcr_mythread () ) == 0)
#line 192 "2way_micro_sync_disr.upc"
      {
#line 193 "2way_micro_sync_disr.upc"
        printf("Starting GET BLOCKING-sync , threads %d ...\n", ((int) upcr_threads () ));
#line 195 "2way_micro_sync_disr.upc"
        printf("%-*s%-*s%-*s%-*s%*s%*s%*s%*s%*s%*s%*s\n", (int) 5, "Op", (int) 6, "Sync", (int) 6, "Tasks", (int) 5, "Size", (int) 6, "Depth", (int) 11, "maxbw", (int) 11, "minbw", (int) 11, "aggbw", (int) 11, "maxmr", (int) 11, "minmr", (int) 11, "aggmr");
      }
#line 199 "2way_micro_sync_disr.upc"
      size = 0;
#line 199 "2way_micro_sync_disr.upc"
      while((_UINT32)(size) <= 15U)
#line 199 "2way_micro_sync_disr.upc"
      {
#line 200 "2way_micro_sync_disr.upc"
        n = (POW2)[size];
#line 201 "2way_micro_sync_disr.upc"
        nbytes = (_UINT64)(n) * 8ULL;
#line 202 "2way_micro_sync_disr.upc"
        ITERS = (ITER_PER_MSG)[size];
#line 204 "2way_micro_sync_disr.upc"
        bw_sum = 0.0;
#line 205 "2way_micro_sync_disr.upc"
        mr_sum = 0.0;
#line 206 "2way_micro_sync_disr.upc"
        upcr_barrier(280593256, 1);
#line 207 "2way_micro_sync_disr.upc"
        _bupc_comma18 = bupc_ticks_now();
#line 207 "2way_micro_sync_disr.upc"
        start = _bupc_comma18;
#line 208 "2way_micro_sync_disr.upc"
        upcr_barrier(280593257, 1);
#line 209 "2way_micro_sync_disr.upc"
        iter = 0;
#line 209 "2way_micro_sync_disr.upc"
        while(iter < ITERS)
#line 209 "2way_micro_sync_disr.upc"
        {
#line 210 "2way_micro_sync_disr.upc"
          i = 0;
#line 210 "2way_micro_sync_disr.upc"
          while(i < depth)
#line 210 "2way_micro_sync_disr.upc"
          {
#line 212 "2way_micro_sync_disr.upc"
            _bupc_Mptra36 = UPCR_ADD_PSHAREDI(remA, 8ULL, (n + 128) * i);
#line 212 "2way_micro_sync_disr.upc"
            _bupc_Mstopcvt37 = UPCR_PSHARED_TO_SHARED(_bupc_Mptra36);
#line 212 "2way_micro_sync_disr.upc"
            _bupc_call15 = bupc_memget_async((_IEEE64 *)((_UINT8 *)(a) + (((n * 8) + 1024) * i)), _bupc_Mstopcvt37, (unsigned long)((_UINT64)(n) * 8ULL));
#line 212 "2way_micro_sync_disr.upc"
            (handle)[i] = _bupc_call15;
#line 213 "2way_micro_sync_disr.upc"
            bupc_waitsync((handle)[i]);
#line 214 "2way_micro_sync_disr.upc"
            _10 :;
#line 214 "2way_micro_sync_disr.upc"
            i = i + 1;
          }
#line 215 "2way_micro_sync_disr.upc"
          _9 :;
#line 215 "2way_micro_sync_disr.upc"
          iter = iter + 1;
        }
#line 216 "2way_micro_sync_disr.upc"
        upcr_barrier(280593258, 1);
#line 217 "2way_micro_sync_disr.upc"
        _bupc_comma19 = bupc_ticks_now();
#line 217 "2way_micro_sync_disr.upc"
        end = (_bupc_comma19 - start) - (barrier_overhead * 2ULL);
#line 218 "2way_micro_sync_disr.upc"
        _bupc_Mptra38 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, ((int) upcr_mythread () ));
#line 218 "2way_micro_sync_disr.upc"
        UPCR_PUT_PSHARED_VAL(_bupc_Mptra38, 0, end, 8);
#line 219 "2way_micro_sync_disr.upc"
        upcr_barrier(280593259, 1);
#line 221 "2way_micro_sync_disr.upc"
        if(((int) upcr_mythread () ) == 0)
#line 221 "2way_micro_sync_disr.upc"
        {
#line 222 "2way_micro_sync_disr.upc"
          UPCR_GET_PSHARED(&_bupc_spillld39, ticks_all, 0, 8);
#line 222 "2way_micro_sync_disr.upc"
          max_tick = _bupc_spillld39;
#line 223 "2way_micro_sync_disr.upc"
          UPCR_GET_PSHARED(&_bupc_spillld40, ticks_all, 0, 8);
#line 223 "2way_micro_sync_disr.upc"
          min_tick = _bupc_spillld40;
#line 224 "2way_micro_sync_disr.upc"
          j = 0;
#line 224 "2way_micro_sync_disr.upc"
          while(j < ((int) upcr_threads () ))
#line 224 "2way_micro_sync_disr.upc"
          {
#line 225 "2way_micro_sync_disr.upc"
            _bupc_Mptra41 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 225 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld42, _bupc_Mptra41, 0, 8);
#line 225 "2way_micro_sync_disr.upc"
            if(max_tick < _bupc_spillld42)
#line 225 "2way_micro_sync_disr.upc"
            {
#line 226 "2way_micro_sync_disr.upc"
              _bupc_Mptra43 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 226 "2way_micro_sync_disr.upc"
              UPCR_GET_PSHARED(&_bupc_spillld44, _bupc_Mptra43, 0, 8);
#line 226 "2way_micro_sync_disr.upc"
              max_tick = _bupc_spillld44;
            }
#line 227 "2way_micro_sync_disr.upc"
            _bupc_Mptra45 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 227 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld46, _bupc_Mptra45, 0, 8);
#line 227 "2way_micro_sync_disr.upc"
            if(min_tick > _bupc_spillld46)
#line 227 "2way_micro_sync_disr.upc"
            {
#line 228 "2way_micro_sync_disr.upc"
              _bupc_Mptra47 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 228 "2way_micro_sync_disr.upc"
              UPCR_GET_PSHARED(&_bupc_spillld48, _bupc_Mptra47, 0, 8);
#line 228 "2way_micro_sync_disr.upc"
              min_tick = _bupc_spillld48;
            }
#line 230 "2way_micro_sync_disr.upc"
            _bupc_Mptra49 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 230 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld50, _bupc_Mptra49, 0, 8);
#line 230 "2way_micro_sync_disr.upc"
            _bupc_comma20 = bupc_ticks_to_us(_bupc_spillld50);
#line 230 "2way_micro_sync_disr.upc"
            usecs = (_IEEE64)(_bupc_comma20);
#line 231 "2way_micro_sync_disr.upc"
            bw = (((_IEEE64)(nbytes) * (_IEEE64)(depth)) * (_IEEE64)(ITERS)) / usecs;
#line 232 "2way_micro_sync_disr.upc"
            mr = (bw * 1e+06) / (_IEEE64)(nbytes);
#line 233 "2way_micro_sync_disr.upc"
            bw_sum = bw_sum + bw;
#line 234 "2way_micro_sync_disr.upc"
            mr_sum = mr_sum + mr;
#line 235 "2way_micro_sync_disr.upc"
            _11 :;
#line 235 "2way_micro_sync_disr.upc"
            j = j + 1;
          }
#line 236 "2way_micro_sync_disr.upc"
          _bupc_comma21 = bupc_ticks_to_us(min_tick);
#line 236 "2way_micro_sync_disr.upc"
          usecs_min = (_IEEE64)(_bupc_comma21);
#line 237 "2way_micro_sync_disr.upc"
          _bupc_comma22 = bupc_ticks_to_us(max_tick);
#line 237 "2way_micro_sync_disr.upc"
          usecs_max = (_IEEE64)(_bupc_comma22);
#line 238 "2way_micro_sync_disr.upc"
          bw_max = (((_IEEE64)(nbytes) * (_IEEE64)(depth)) * (_IEEE64)(ITERS)) / usecs_min;
#line 239 "2way_micro_sync_disr.upc"
          bw_min = (((_IEEE64)(nbytes) * (_IEEE64)(depth)) * (_IEEE64)(ITERS)) / usecs_max;
#line 240 "2way_micro_sync_disr.upc"
          mr_max = (bw_max * 1e+06) / (_IEEE64)(nbytes);
#line 241 "2way_micro_sync_disr.upc"
          mr_min = (bw_min * 1e+06) / (_IEEE64)(nbytes);
#line 245 "2way_micro_sync_disr.upc"
          _bupc_comma23 = get_thor_servers_per_domain();
#line 245 "2way_micro_sync_disr.upc"
          printf("%s %s %d %d %5d %5d %10.2f %10.2f %10.2f  %10.2f  %10.2f  %10.2f\n", "GET", "BLOCK", ((int) upcr_threads () ), _bupc_comma23, nbytes, depth, bw_max, bw_min, bw_sum, mr_max, mr_min, mr_sum);
        }
#line 247 "2way_micro_sync_disr.upc"
        _8 :;
#line 247 "2way_micro_sync_disr.upc"
        size = size + 1;
      }
    }
#line 250 "2way_micro_sync_disr.upc"
    upcr_barrier(280593260, 1);
#line 251 "2way_micro_sync_disr.upc"
    if(doasync != 0)
#line 251 "2way_micro_sync_disr.upc"
    {
#line 252 "2way_micro_sync_disr.upc"
      if(((int) upcr_mythread () ) == 0)
#line 252 "2way_micro_sync_disr.upc"
      {
#line 253 "2way_micro_sync_disr.upc"
        printf("Starting GET-PIPE-sync, threads %d...\n", ((int) upcr_threads () ));
#line 255 "2way_micro_sync_disr.upc"
        printf("%-*s%*s%*s%*s%*s%*s%*s%*s\n", (int) 5, "Size", (int) 6, "Depth", (int) 11, "maxbw", (int) 11, "minbw", (int) 11, "aggbw", (int) 11, "maxmr", (int) 11, "minmr", (int) 11, "aggmr");
      }
#line 258 "2way_micro_sync_disr.upc"
      upcr_barrier(280593261, 1);
#line 259 "2way_micro_sync_disr.upc"
      size = 0;
#line 259 "2way_micro_sync_disr.upc"
      while((_UINT32)(size) <= 15U)
#line 259 "2way_micro_sync_disr.upc"
      {
#line 260 "2way_micro_sync_disr.upc"
        n = (POW2)[size];
#line 261 "2way_micro_sync_disr.upc"
        nbytes = (_UINT64)(n) * 8ULL;
#line 262 "2way_micro_sync_disr.upc"
        ITERS = (ITER_PER_MSG)[size];
#line 264 "2way_micro_sync_disr.upc"
        upcr_barrier(280593262, 1);
#line 265 "2way_micro_sync_disr.upc"
        _bupc_comma24 = bupc_ticks_now();
#line 265 "2way_micro_sync_disr.upc"
        start = _bupc_comma24;
#line 266 "2way_micro_sync_disr.upc"
        upcr_barrier(280593263, 1);
#line 267 "2way_micro_sync_disr.upc"
        iter = 0;
#line 267 "2way_micro_sync_disr.upc"
        while(iter < ITERS)
#line 267 "2way_micro_sync_disr.upc"
        {
#line 268 "2way_micro_sync_disr.upc"
          i = 0;
#line 268 "2way_micro_sync_disr.upc"
          while(i < depth)
#line 268 "2way_micro_sync_disr.upc"
          {
#line 270 "2way_micro_sync_disr.upc"
            _bupc_Mptra51 = UPCR_ADD_PSHAREDI(remA, 8ULL, (n + 128) * i);
#line 270 "2way_micro_sync_disr.upc"
            _bupc_Mstopcvt52 = UPCR_PSHARED_TO_SHARED(_bupc_Mptra51);
#line 270 "2way_micro_sync_disr.upc"
            _bupc_call16 = bupc_memget_async((_IEEE64 *)((_UINT8 *)(a) + (((n * 8) + 1024) * i)), _bupc_Mstopcvt52, (unsigned long)((_UINT64)(n) * 8ULL));
#line 270 "2way_micro_sync_disr.upc"
            (handle)[i] = _bupc_call16;
#line 270 "2way_micro_sync_disr.upc"
            _14 :;
#line 270 "2way_micro_sync_disr.upc"
            i = i + 1;
          }
#line 271 "2way_micro_sync_disr.upc"
          i = 0;
#line 271 "2way_micro_sync_disr.upc"
          while(i < depth)
#line 271 "2way_micro_sync_disr.upc"
          {
#line 272 "2way_micro_sync_disr.upc"
            bupc_waitsync((handle)[i]);
#line 272 "2way_micro_sync_disr.upc"
            _15 :;
#line 272 "2way_micro_sync_disr.upc"
            i = i + 1;
          }
#line 273 "2way_micro_sync_disr.upc"
          _13 :;
#line 273 "2way_micro_sync_disr.upc"
          iter = iter + 1;
        }
#line 274 "2way_micro_sync_disr.upc"
        upcr_barrier(280593264, 1);
#line 276 "2way_micro_sync_disr.upc"
        _bupc_comma25 = bupc_ticks_now();
#line 276 "2way_micro_sync_disr.upc"
        end = (_bupc_comma25 - start) - (barrier_overhead * 2ULL);
#line 277 "2way_micro_sync_disr.upc"
        _bupc_Mptra53 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, ((int) upcr_mythread () ));
#line 277 "2way_micro_sync_disr.upc"
        UPCR_PUT_PSHARED_VAL(_bupc_Mptra53, 0, end, 8);
#line 278 "2way_micro_sync_disr.upc"
        upcr_barrier(280593265, 1);
#line 280 "2way_micro_sync_disr.upc"
        if(((int) upcr_mythread () ) == 0)
#line 280 "2way_micro_sync_disr.upc"
        {
#line 281 "2way_micro_sync_disr.upc"
          UPCR_GET_PSHARED(&_bupc_spillld54, ticks_all, 0, 8);
#line 281 "2way_micro_sync_disr.upc"
          max_tick = _bupc_spillld54;
#line 282 "2way_micro_sync_disr.upc"
          UPCR_GET_PSHARED(&_bupc_spillld55, ticks_all, 0, 8);
#line 282 "2way_micro_sync_disr.upc"
          min_tick = _bupc_spillld55;
#line 283 "2way_micro_sync_disr.upc"
          bw_sum = 0.0;
#line 284 "2way_micro_sync_disr.upc"
          mr_sum = 0.0;
#line 286 "2way_micro_sync_disr.upc"
          j = 0;
#line 286 "2way_micro_sync_disr.upc"
          while(j < ((int) upcr_threads () ))
#line 286 "2way_micro_sync_disr.upc"
          {
#line 287 "2way_micro_sync_disr.upc"
            _bupc_Mptra56 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 287 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld57, _bupc_Mptra56, 0, 8);
#line 287 "2way_micro_sync_disr.upc"
            if(max_tick < _bupc_spillld57)
#line 287 "2way_micro_sync_disr.upc"
            {
#line 288 "2way_micro_sync_disr.upc"
              _bupc_Mptra58 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 288 "2way_micro_sync_disr.upc"
              UPCR_GET_PSHARED(&_bupc_spillld59, _bupc_Mptra58, 0, 8);
#line 288 "2way_micro_sync_disr.upc"
              max_tick = _bupc_spillld59;
            }
#line 289 "2way_micro_sync_disr.upc"
            _bupc_Mptra60 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 289 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld61, _bupc_Mptra60, 0, 8);
#line 289 "2way_micro_sync_disr.upc"
            if(min_tick > _bupc_spillld61)
#line 289 "2way_micro_sync_disr.upc"
            {
#line 290 "2way_micro_sync_disr.upc"
              _bupc_Mptra62 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 290 "2way_micro_sync_disr.upc"
              UPCR_GET_PSHARED(&_bupc_spillld63, _bupc_Mptra62, 0, 8);
#line 290 "2way_micro_sync_disr.upc"
              min_tick = _bupc_spillld63;
            }
#line 292 "2way_micro_sync_disr.upc"
            _bupc_Mptra64 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 292 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld65, _bupc_Mptra64, 0, 8);
#line 292 "2way_micro_sync_disr.upc"
            _bupc_comma26 = bupc_ticks_to_us(_bupc_spillld65);
#line 292 "2way_micro_sync_disr.upc"
            usecs = (_IEEE64)(_bupc_comma26);
#line 293 "2way_micro_sync_disr.upc"
            bw = (((_IEEE64)(nbytes) * (_IEEE64)(depth)) * (_IEEE64)(ITERS)) / usecs;
#line 294 "2way_micro_sync_disr.upc"
            mr = (bw * 1e+06) / (_IEEE64)(nbytes);
#line 295 "2way_micro_sync_disr.upc"
            bw_sum = bw_sum + bw;
#line 296 "2way_micro_sync_disr.upc"
            mr_sum = mr_sum + mr;
#line 297 "2way_micro_sync_disr.upc"
            _16 :;
#line 297 "2way_micro_sync_disr.upc"
            j = j + 1;
          }
#line 298 "2way_micro_sync_disr.upc"
          _bupc_comma27 = bupc_ticks_to_us(min_tick);
#line 298 "2way_micro_sync_disr.upc"
          usecs_min = (_IEEE64)(_bupc_comma27);
#line 299 "2way_micro_sync_disr.upc"
          _bupc_comma28 = bupc_ticks_to_us(max_tick);
#line 299 "2way_micro_sync_disr.upc"
          usecs_max = (_IEEE64)(_bupc_comma28);
#line 300 "2way_micro_sync_disr.upc"
          bw_max = (((_IEEE64)(nbytes) * (_IEEE64)(depth)) * (_IEEE64)(ITERS)) / usecs_min;
#line 301 "2way_micro_sync_disr.upc"
          bw_min = (((_IEEE64)(nbytes) * (_IEEE64)(depth)) * (_IEEE64)(ITERS)) / usecs_max;
#line 302 "2way_micro_sync_disr.upc"
          mr_max = (bw_max * 1e+06) / (_IEEE64)(nbytes);
#line 303 "2way_micro_sync_disr.upc"
          mr_min = (bw_min * 1e+06) / (_IEEE64)(nbytes);
#line 307 "2way_micro_sync_disr.upc"
          _bupc_comma29 = get_thor_servers_per_domain();
#line 307 "2way_micro_sync_disr.upc"
          printf("%s %s %d %d %5d %5d %10.2f %10.2f %10.2f  %10.2f  %10.2f  %10.2f\n", "GET", "PIPE", ((int) upcr_threads () ), _bupc_comma29, nbytes, depth, bw_max, bw_min, bw_sum, mr_max, mr_min, mr_sum);
        }
#line 310 "2way_micro_sync_disr.upc"
        _12 :;
#line 310 "2way_micro_sync_disr.upc"
        size = size + 1;
      }
    }
#line 313 "2way_micro_sync_disr.upc"
    if(dooverhead != 0)
#line 313 "2way_micro_sync_disr.upc"
    {
#line 314 "2way_micro_sync_disr.upc"
      if(((int) upcr_mythread () ) == 0)
#line 314 "2way_micro_sync_disr.upc"
      {
#line 315 "2way_micro_sync_disr.upc"
        printf("Starting GET-PIPE-OVERHEAD...\n");
#line 316 "2way_micro_sync_disr.upc"
        printf("%-*s%*s%*s\n", (int) 5, "Size", (int) 6, "Depth", (int) 11, "Overhead (us) min max average");
      }
#line 318 "2way_micro_sync_disr.upc"
      size = 0;
#line 318 "2way_micro_sync_disr.upc"
      while((_UINT32)(size) <= 15U)
#line 318 "2way_micro_sync_disr.upc"
      {
#line 319 "2way_micro_sync_disr.upc"
        n = (POW2)[size];
#line 320 "2way_micro_sync_disr.upc"
        nbytes = (_UINT64)(n) * 8ULL;
#line 321 "2way_micro_sync_disr.upc"
        end = 0ULL;
#line 322 "2way_micro_sync_disr.upc"
        upcr_barrier(280593266, 1);
#line 323 "2way_micro_sync_disr.upc"
        _bupc_comma30 = bupc_ticks_now();
#line 323 "2way_micro_sync_disr.upc"
        start = _bupc_comma30;
#line 324 "2way_micro_sync_disr.upc"
        iter = 0;
#line 324 "2way_micro_sync_disr.upc"
        while(iter < ITERS)
#line 324 "2way_micro_sync_disr.upc"
        {
#line 325 "2way_micro_sync_disr.upc"
          _bupc_comma31 = bupc_ticks_now();
#line 325 "2way_micro_sync_disr.upc"
          start = _bupc_comma31;
#line 326 "2way_micro_sync_disr.upc"
          i = 0;
#line 326 "2way_micro_sync_disr.upc"
          while(i < depth)
#line 326 "2way_micro_sync_disr.upc"
          {
#line 328 "2way_micro_sync_disr.upc"
            _bupc_Mptra66 = UPCR_ADD_PSHAREDI(remA, 8ULL, (n + 128) * i);
#line 328 "2way_micro_sync_disr.upc"
            _bupc_Mstopcvt67 = UPCR_PSHARED_TO_SHARED(_bupc_Mptra66);
#line 328 "2way_micro_sync_disr.upc"
            _bupc_call17 = bupc_memget_async((_IEEE64 *)((_UINT8 *)(a) + (((n * 8) + 1024) * i)), _bupc_Mstopcvt67, (unsigned long)((_UINT64)(n) * 8ULL));
#line 328 "2way_micro_sync_disr.upc"
            (handle)[i] = _bupc_call17;
#line 328 "2way_micro_sync_disr.upc"
            _19 :;
#line 328 "2way_micro_sync_disr.upc"
            i = i + 1;
          }
#line 329 "2way_micro_sync_disr.upc"
          _bupc_comma32 = bupc_ticks_now();
#line 329 "2way_micro_sync_disr.upc"
          end = end + (_bupc_comma32 - start);
#line 330 "2way_micro_sync_disr.upc"
          i = 0;
#line 330 "2way_micro_sync_disr.upc"
          while(i < depth)
#line 330 "2way_micro_sync_disr.upc"
          {
#line 331 "2way_micro_sync_disr.upc"
            bupc_waitsync((handle)[i]);
#line 331 "2way_micro_sync_disr.upc"
            _20 :;
#line 331 "2way_micro_sync_disr.upc"
            i = i + 1;
          }
#line 332 "2way_micro_sync_disr.upc"
          _18 :;
#line 332 "2way_micro_sync_disr.upc"
          iter = iter + 1;
        }
#line 333 "2way_micro_sync_disr.upc"
        _bupc_Mptra68 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, ((int) upcr_mythread () ));
#line 333 "2way_micro_sync_disr.upc"
        UPCR_PUT_PSHARED_VAL(_bupc_Mptra68, 0, end, 8);
#line 334 "2way_micro_sync_disr.upc"
        upcr_barrier(280593267, 1);
#line 336 "2way_micro_sync_disr.upc"
        if(((int) upcr_mythread () ) == 0)
#line 336 "2way_micro_sync_disr.upc"
        {
#line 337 "2way_micro_sync_disr.upc"
          UPCR_GET_PSHARED(&_bupc_spillld69, ticks_all, 0, 8);
#line 337 "2way_micro_sync_disr.upc"
          max_tick = _bupc_spillld69;
#line 338 "2way_micro_sync_disr.upc"
          UPCR_GET_PSHARED(&_bupc_spillld70, ticks_all, 0, 8);
#line 338 "2way_micro_sync_disr.upc"
          min_tick = _bupc_spillld70;
#line 339 "2way_micro_sync_disr.upc"
          tick_sum = 0ULL;
#line 340 "2way_micro_sync_disr.upc"
          j = 0;
#line 340 "2way_micro_sync_disr.upc"
          while(j < ((int) upcr_threads () ))
#line 340 "2way_micro_sync_disr.upc"
          {
#line 341 "2way_micro_sync_disr.upc"
            _bupc_Mptra71 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 341 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld72, _bupc_Mptra71, 0, 8);
#line 341 "2way_micro_sync_disr.upc"
            if(max_tick < _bupc_spillld72)
#line 341 "2way_micro_sync_disr.upc"
            {
#line 342 "2way_micro_sync_disr.upc"
              _bupc_Mptra73 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 342 "2way_micro_sync_disr.upc"
              UPCR_GET_PSHARED(&_bupc_spillld74, _bupc_Mptra73, 0, 8);
#line 342 "2way_micro_sync_disr.upc"
              max_tick = _bupc_spillld74;
            }
#line 343 "2way_micro_sync_disr.upc"
            _bupc_Mptra75 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 343 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld76, _bupc_Mptra75, 0, 8);
#line 343 "2way_micro_sync_disr.upc"
            if(min_tick > _bupc_spillld76)
#line 343 "2way_micro_sync_disr.upc"
            {
#line 344 "2way_micro_sync_disr.upc"
              _bupc_Mptra77 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 344 "2way_micro_sync_disr.upc"
              UPCR_GET_PSHARED(&_bupc_spillld78, _bupc_Mptra77, 0, 8);
#line 344 "2way_micro_sync_disr.upc"
              min_tick = _bupc_spillld78;
            }
#line 346 "2way_micro_sync_disr.upc"
            _bupc_Mptra79 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 346 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld80, _bupc_Mptra79, 0, 8);
#line 346 "2way_micro_sync_disr.upc"
            tick_sum = tick_sum + _bupc_spillld80;
#line 347 "2way_micro_sync_disr.upc"
            _21 :;
#line 347 "2way_micro_sync_disr.upc"
            j = j + 1;
          }
#line 348 "2way_micro_sync_disr.upc"
          _bupc_comma33 = bupc_ticks_to_us(min_tick);
#line 348 "2way_micro_sync_disr.upc"
          usecs_min = (_IEEE64)(_bupc_comma33) / (_IEEE64)((ITERS * depth));
#line 349 "2way_micro_sync_disr.upc"
          _bupc_comma34 = bupc_ticks_to_us(max_tick);
#line 349 "2way_micro_sync_disr.upc"
          usecs_max = (_IEEE64)(_bupc_comma34) / (_IEEE64)((ITERS * depth));
#line 350 "2way_micro_sync_disr.upc"
          _bupc_comma35 = bupc_ticks_to_us(tick_sum);
#line 350 "2way_micro_sync_disr.upc"
          usecs_sum = (_IEEE64)(_bupc_comma35) / (_IEEE64)((ITERS * depth));
#line 353 "2way_micro_sync_disr.upc"
          _bupc_comma36 = get_thor_servers_per_domain();
#line 353 "2way_micro_sync_disr.upc"
          printf("%s %s %d %d %5d %5d %10.2f %10.2f %10.2f\n", "GET", "OVHD", ((int) upcr_threads () ), _bupc_comma36, nbytes, depth, usecs_min, usecs_max, usecs_sum / (_IEEE64)(((int) upcr_threads () )));
        }
#line 356 "2way_micro_sync_disr.upc"
        _17 :;
#line 356 "2way_micro_sync_disr.upc"
        size = size + 1;
      }
    }
  }
#line 360 "2way_micro_sync_disr.upc"
  upcr_barrier(280593268, 1);
#line 361 "2way_micro_sync_disr.upc"
  if(doput != 0)
#line 361 "2way_micro_sync_disr.upc"
  {
#line 362 "2way_micro_sync_disr.upc"
    if(doblocking != 0)
#line 362 "2way_micro_sync_disr.upc"
    {
#line 363 "2way_micro_sync_disr.upc"
      if(((int) upcr_mythread () ) == 0)
#line 363 "2way_micro_sync_disr.upc"
      {
#line 364 "2way_micro_sync_disr.upc"
        printf("Starting PUT BLOCKING-sync, threads %d...\n", ((int) upcr_threads () ));
#line 366 "2way_micro_sync_disr.upc"
        printf("%-*s%*s%*s%*s%*s%*s%*s%*s\n", (int) 5, "Size", (int) 6, "Depth", (int) 11, "maxbw", (int) 11, "minbw", (int) 11, "aggbw", (int) 11, "maxmr", (int) 11, "minmr", (int) 11, "aggmr");
      }
#line 369 "2way_micro_sync_disr.upc"
      size = 0;
#line 369 "2way_micro_sync_disr.upc"
      while((_UINT32)(size) <= 15U)
#line 369 "2way_micro_sync_disr.upc"
      {
#line 370 "2way_micro_sync_disr.upc"
        n = (POW2)[size];
#line 371 "2way_micro_sync_disr.upc"
        nbytes = (_UINT64)(n) * 8ULL;
#line 372 "2way_micro_sync_disr.upc"
        ITERS = (ITER_PER_MSG)[size];
#line 374 "2way_micro_sync_disr.upc"
        bw_sum = 0.0;
#line 375 "2way_micro_sync_disr.upc"
        mr_sum = 0.0;
#line 376 "2way_micro_sync_disr.upc"
        upcr_barrier(280593269, 1);
#line 377 "2way_micro_sync_disr.upc"
        _bupc_comma37 = bupc_ticks_now();
#line 377 "2way_micro_sync_disr.upc"
        start = _bupc_comma37;
#line 378 "2way_micro_sync_disr.upc"
        upcr_barrier(280593270, 1);
#line 379 "2way_micro_sync_disr.upc"
        iter = 0;
#line 379 "2way_micro_sync_disr.upc"
        while(iter < ITERS)
#line 379 "2way_micro_sync_disr.upc"
        {
#line 380 "2way_micro_sync_disr.upc"
          i = 0;
#line 380 "2way_micro_sync_disr.upc"
          while(i < depth)
#line 380 "2way_micro_sync_disr.upc"
          {
#line 382 "2way_micro_sync_disr.upc"
            _bupc_Mptra81 = UPCR_ADD_PSHAREDI(remA, 8ULL, (n + 128) * i);
#line 382 "2way_micro_sync_disr.upc"
            _bupc_Mstopcvt82 = UPCR_PSHARED_TO_SHARED(_bupc_Mptra81);
#line 382 "2way_micro_sync_disr.upc"
            _bupc_call18 = bupc_memput_async(_bupc_Mstopcvt82, (_IEEE64 *)((_UINT8 *)(a) + (((n * 8) + 1024) * i)), (unsigned long)((_UINT64)(n) * 8ULL));
#line 382 "2way_micro_sync_disr.upc"
            (handle)[i] = _bupc_call18;
#line 383 "2way_micro_sync_disr.upc"
            bupc_waitsync((handle)[i]);
#line 384 "2way_micro_sync_disr.upc"
            _24 :;
#line 384 "2way_micro_sync_disr.upc"
            i = i + 1;
          }
#line 385 "2way_micro_sync_disr.upc"
          _23 :;
#line 385 "2way_micro_sync_disr.upc"
          iter = iter + 1;
        }
#line 386 "2way_micro_sync_disr.upc"
        upcr_barrier(280593271, 1);
#line 387 "2way_micro_sync_disr.upc"
        _bupc_comma38 = bupc_ticks_now();
#line 387 "2way_micro_sync_disr.upc"
        end = (_bupc_comma38 - start) - (barrier_overhead * 2ULL);
#line 388 "2way_micro_sync_disr.upc"
        _bupc_Mptra83 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, ((int) upcr_mythread () ));
#line 388 "2way_micro_sync_disr.upc"
        UPCR_PUT_PSHARED_VAL(_bupc_Mptra83, 0, end, 8);
#line 389 "2way_micro_sync_disr.upc"
        upcr_barrier(280593272, 1);
#line 391 "2way_micro_sync_disr.upc"
        if(((int) upcr_mythread () ) == 0)
#line 391 "2way_micro_sync_disr.upc"
        {
#line 392 "2way_micro_sync_disr.upc"
          UPCR_GET_PSHARED(&_bupc_spillld84, ticks_all, 0, 8);
#line 392 "2way_micro_sync_disr.upc"
          max_tick = _bupc_spillld84;
#line 393 "2way_micro_sync_disr.upc"
          UPCR_GET_PSHARED(&_bupc_spillld85, ticks_all, 0, 8);
#line 393 "2way_micro_sync_disr.upc"
          min_tick = _bupc_spillld85;
#line 394 "2way_micro_sync_disr.upc"
          j = 0;
#line 394 "2way_micro_sync_disr.upc"
          while(j < ((int) upcr_threads () ))
#line 394 "2way_micro_sync_disr.upc"
          {
#line 395 "2way_micro_sync_disr.upc"
            _bupc_Mptra86 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 395 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld87, _bupc_Mptra86, 0, 8);
#line 395 "2way_micro_sync_disr.upc"
            if(max_tick < _bupc_spillld87)
#line 395 "2way_micro_sync_disr.upc"
            {
#line 396 "2way_micro_sync_disr.upc"
              _bupc_Mptra88 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 396 "2way_micro_sync_disr.upc"
              UPCR_GET_PSHARED(&_bupc_spillld89, _bupc_Mptra88, 0, 8);
#line 396 "2way_micro_sync_disr.upc"
              max_tick = _bupc_spillld89;
            }
#line 397 "2way_micro_sync_disr.upc"
            _bupc_Mptra90 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 397 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld91, _bupc_Mptra90, 0, 8);
#line 397 "2way_micro_sync_disr.upc"
            if(min_tick > _bupc_spillld91)
#line 397 "2way_micro_sync_disr.upc"
            {
#line 398 "2way_micro_sync_disr.upc"
              _bupc_Mptra92 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 398 "2way_micro_sync_disr.upc"
              UPCR_GET_PSHARED(&_bupc_spillld93, _bupc_Mptra92, 0, 8);
#line 398 "2way_micro_sync_disr.upc"
              min_tick = _bupc_spillld93;
            }
#line 400 "2way_micro_sync_disr.upc"
            _bupc_Mptra94 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 400 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld95, _bupc_Mptra94, 0, 8);
#line 400 "2way_micro_sync_disr.upc"
            _bupc_comma39 = bupc_ticks_to_us(_bupc_spillld95);
#line 400 "2way_micro_sync_disr.upc"
            usecs = (_IEEE64)(_bupc_comma39);
#line 401 "2way_micro_sync_disr.upc"
            bw = (((_IEEE64)(nbytes) * (_IEEE64)(depth)) * (_IEEE64)(ITERS)) / usecs;
#line 402 "2way_micro_sync_disr.upc"
            mr = (bw * 1e+06) / (_IEEE64)(nbytes);
#line 403 "2way_micro_sync_disr.upc"
            bw_sum = bw_sum + bw;
#line 404 "2way_micro_sync_disr.upc"
            mr_sum = mr_sum + mr;
#line 405 "2way_micro_sync_disr.upc"
            _25 :;
#line 405 "2way_micro_sync_disr.upc"
            j = j + 1;
          }
#line 406 "2way_micro_sync_disr.upc"
          _bupc_comma40 = bupc_ticks_to_us(min_tick);
#line 406 "2way_micro_sync_disr.upc"
          usecs_min = (_IEEE64)(_bupc_comma40);
#line 407 "2way_micro_sync_disr.upc"
          _bupc_comma = bupc_ticks_to_us(max_tick);
#line 407 "2way_micro_sync_disr.upc"
          usecs_max = (_IEEE64)(_bupc_comma);
#line 408 "2way_micro_sync_disr.upc"
          bw_max = (((_IEEE64)(nbytes) * (_IEEE64)(depth)) * (_IEEE64)(ITERS)) / usecs_min;
#line 409 "2way_micro_sync_disr.upc"
          bw_min = (((_IEEE64)(nbytes) * (_IEEE64)(depth)) * (_IEEE64)(ITERS)) / usecs_max;
#line 410 "2way_micro_sync_disr.upc"
          mr_max = (bw_max * 1e+06) / (_IEEE64)(nbytes);
#line 411 "2way_micro_sync_disr.upc"
          mr_min = (bw_min * 1e+06) / (_IEEE64)(nbytes);
#line 415 "2way_micro_sync_disr.upc"
          _bupc_comma0 = get_thor_servers_per_domain();
#line 415 "2way_micro_sync_disr.upc"
          printf("%s %s %d %d %5d %5d %10.2f %10.2f %10.2f  %10.2f  %10.2f  %10.2f\n", "PUT", "BLOCK", ((int) upcr_threads () ), _bupc_comma0, nbytes, depth, bw_max, bw_min, bw_sum, mr_max, mr_min, mr_sum);
        }
#line 418 "2way_micro_sync_disr.upc"
        _22 :;
#line 418 "2way_micro_sync_disr.upc"
        size = size + 1;
      }
    }
#line 421 "2way_micro_sync_disr.upc"
    upcr_barrier(280593273, 1);
#line 422 "2way_micro_sync_disr.upc"
    if(doasync != 0)
#line 422 "2way_micro_sync_disr.upc"
    {
#line 423 "2way_micro_sync_disr.upc"
      if(((int) upcr_mythread () ) == 0)
#line 423 "2way_micro_sync_disr.upc"
      {
#line 424 "2way_micro_sync_disr.upc"
        printf("Starting PUT-PIPE-sync, threads %d...\n", ((int) upcr_threads () ));
#line 426 "2way_micro_sync_disr.upc"
        printf("%-*s%*s%*s%*s%*s%*s%*s%*s\n", (int) 5, "Size", (int) 6, "Depth", (int) 11, "maxbw", (int) 11, "minbw", (int) 11, "aggbw", (int) 11, "maxmr", (int) 11, "minmr", (int) 11, "aggmr");
      }
#line 428 "2way_micro_sync_disr.upc"
      upcr_barrier(280593274, 1);
#line 429 "2way_micro_sync_disr.upc"
      size = 0;
#line 429 "2way_micro_sync_disr.upc"
      while((_UINT32)(size) <= 15U)
#line 429 "2way_micro_sync_disr.upc"
      {
#line 431 "2way_micro_sync_disr.upc"
        n = (POW2)[size];
#line 432 "2way_micro_sync_disr.upc"
        nbytes = (_UINT64)(n) * 8ULL;
#line 433 "2way_micro_sync_disr.upc"
        ITERS = (ITER_PER_MSG)[size];
#line 434 "2way_micro_sync_disr.upc"
        upcr_barrier(280593275, 1);
#line 435 "2way_micro_sync_disr.upc"
        upcr_barrier(280593276, 1);
#line 436 "2way_micro_sync_disr.upc"
        _bupc_comma1 = bupc_ticks_now();
#line 436 "2way_micro_sync_disr.upc"
        start = _bupc_comma1;
#line 437 "2way_micro_sync_disr.upc"
        upcr_barrier(280593277, 1);
#line 438 "2way_micro_sync_disr.upc"
        iter = 0;
#line 438 "2way_micro_sync_disr.upc"
        while(iter < ITERS)
#line 438 "2way_micro_sync_disr.upc"
        {
#line 439 "2way_micro_sync_disr.upc"
          i = 0;
#line 439 "2way_micro_sync_disr.upc"
          while(i < depth)
#line 439 "2way_micro_sync_disr.upc"
          {
#line 441 "2way_micro_sync_disr.upc"
            _bupc_Mptra96 = UPCR_ADD_PSHAREDI(remA, 8ULL, (n + 128) * i);
#line 441 "2way_micro_sync_disr.upc"
            _bupc_Mstopcvt97 = UPCR_PSHARED_TO_SHARED(_bupc_Mptra96);
#line 441 "2way_micro_sync_disr.upc"
            _bupc_call19 = bupc_memput_async(_bupc_Mstopcvt97, (_IEEE64 *)((_UINT8 *)(a) + (((n * 8) + 1024) * i)), (unsigned long)((_UINT64)(n) * 8ULL));
#line 441 "2way_micro_sync_disr.upc"
            (handle)[i] = _bupc_call19;
#line 441 "2way_micro_sync_disr.upc"
            _28 :;
#line 441 "2way_micro_sync_disr.upc"
            i = i + 1;
          }
#line 442 "2way_micro_sync_disr.upc"
          i = 0;
#line 442 "2way_micro_sync_disr.upc"
          while(i < depth)
#line 442 "2way_micro_sync_disr.upc"
          {
#line 443 "2way_micro_sync_disr.upc"
            bupc_waitsync((handle)[i]);
#line 443 "2way_micro_sync_disr.upc"
            _29 :;
#line 443 "2way_micro_sync_disr.upc"
            i = i + 1;
          }
#line 444 "2way_micro_sync_disr.upc"
          _27 :;
#line 444 "2way_micro_sync_disr.upc"
          iter = iter + 1;
        }
#line 445 "2way_micro_sync_disr.upc"
        upcr_barrier(280593278, 1);
#line 446 "2way_micro_sync_disr.upc"
        _bupc_comma2 = bupc_ticks_now();
#line 446 "2way_micro_sync_disr.upc"
        end = (_bupc_comma2 - start) - (barrier_overhead * 2ULL);
#line 447 "2way_micro_sync_disr.upc"
        upcr_barrier(280593279, 1);
#line 448 "2way_micro_sync_disr.upc"
        _bupc_Mptra98 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, ((int) upcr_mythread () ));
#line 448 "2way_micro_sync_disr.upc"
        UPCR_PUT_PSHARED_VAL(_bupc_Mptra98, 0, end, 8);
#line 449 "2way_micro_sync_disr.upc"
        upcr_barrier(280593280, 1);
#line 451 "2way_micro_sync_disr.upc"
        if(((int) upcr_mythread () ) == 0)
#line 451 "2way_micro_sync_disr.upc"
        {
#line 452 "2way_micro_sync_disr.upc"
          UPCR_GET_PSHARED(&_bupc_spillld99, ticks_all, 0, 8);
#line 452 "2way_micro_sync_disr.upc"
          max_tick = _bupc_spillld99;
#line 453 "2way_micro_sync_disr.upc"
          UPCR_GET_PSHARED(&_bupc_spillld100, ticks_all, 0, 8);
#line 453 "2way_micro_sync_disr.upc"
          min_tick = _bupc_spillld100;
#line 454 "2way_micro_sync_disr.upc"
          bw_sum = 0.0;
#line 455 "2way_micro_sync_disr.upc"
          mr_sum = 0.0;
#line 457 "2way_micro_sync_disr.upc"
          j = 0;
#line 457 "2way_micro_sync_disr.upc"
          while(j < ((int) upcr_threads () ))
#line 457 "2way_micro_sync_disr.upc"
          {
#line 458 "2way_micro_sync_disr.upc"
            _bupc_Mptra101 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 458 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld102, _bupc_Mptra101, 0, 8);
#line 458 "2way_micro_sync_disr.upc"
            if(max_tick < _bupc_spillld102)
#line 458 "2way_micro_sync_disr.upc"
            {
#line 459 "2way_micro_sync_disr.upc"
              _bupc_Mptra103 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 459 "2way_micro_sync_disr.upc"
              UPCR_GET_PSHARED(&_bupc_spillld104, _bupc_Mptra103, 0, 8);
#line 459 "2way_micro_sync_disr.upc"
              max_tick = _bupc_spillld104;
            }
#line 460 "2way_micro_sync_disr.upc"
            _bupc_Mptra105 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 460 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld106, _bupc_Mptra105, 0, 8);
#line 460 "2way_micro_sync_disr.upc"
            if(min_tick > _bupc_spillld106)
#line 460 "2way_micro_sync_disr.upc"
            {
#line 461 "2way_micro_sync_disr.upc"
              _bupc_Mptra107 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 461 "2way_micro_sync_disr.upc"
              UPCR_GET_PSHARED(&_bupc_spillld108, _bupc_Mptra107, 0, 8);
#line 461 "2way_micro_sync_disr.upc"
              min_tick = _bupc_spillld108;
            }
#line 463 "2way_micro_sync_disr.upc"
            _bupc_Mptra109 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 463 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld110, _bupc_Mptra109, 0, 8);
#line 463 "2way_micro_sync_disr.upc"
            _bupc_comma3 = bupc_ticks_to_us(_bupc_spillld110);
#line 463 "2way_micro_sync_disr.upc"
            usecs = (_IEEE64)(_bupc_comma3);
#line 464 "2way_micro_sync_disr.upc"
            bw = (((_IEEE64)(nbytes) * (_IEEE64)(depth)) * (_IEEE64)(ITERS)) / usecs;
#line 465 "2way_micro_sync_disr.upc"
            mr = (bw * 1e+06) / (_IEEE64)(nbytes);
#line 466 "2way_micro_sync_disr.upc"
            bw_sum = bw_sum + bw;
#line 467 "2way_micro_sync_disr.upc"
            mr_sum = mr_sum + mr;
#line 468 "2way_micro_sync_disr.upc"
            _30 :;
#line 468 "2way_micro_sync_disr.upc"
            j = j + 1;
          }
#line 469 "2way_micro_sync_disr.upc"
          _bupc_comma4 = bupc_ticks_to_us(min_tick);
#line 469 "2way_micro_sync_disr.upc"
          usecs_min = (_IEEE64)(_bupc_comma4);
#line 470 "2way_micro_sync_disr.upc"
          _bupc_comma5 = bupc_ticks_to_us(max_tick);
#line 470 "2way_micro_sync_disr.upc"
          usecs_max = (_IEEE64)(_bupc_comma5);
#line 471 "2way_micro_sync_disr.upc"
          bw_max = (((_IEEE64)(nbytes) * (_IEEE64)(depth)) * (_IEEE64)(ITERS)) / usecs_min;
#line 472 "2way_micro_sync_disr.upc"
          bw_min = (((_IEEE64)(nbytes) * (_IEEE64)(depth)) * (_IEEE64)(ITERS)) / usecs_max;
#line 473 "2way_micro_sync_disr.upc"
          mr_max = (bw_max * 1e+06) / (_IEEE64)(nbytes);
#line 474 "2way_micro_sync_disr.upc"
          mr_min = (bw_min * 1e+06) / (_IEEE64)(nbytes);
#line 478 "2way_micro_sync_disr.upc"
          _bupc_comma6 = get_thor_servers_per_domain();
#line 478 "2way_micro_sync_disr.upc"
          printf("%s %s %d %d %5d %5d %10.2f %10.2f %10.2f  %10.2f  %10.2f  %10.2f\n", "PUT", "PIPE", ((int) upcr_threads () ), _bupc_comma6, nbytes, depth, bw_max, bw_min, bw_sum, mr_max, mr_min, mr_sum);
        }
#line 481 "2way_micro_sync_disr.upc"
        _26 :;
#line 481 "2way_micro_sync_disr.upc"
        size = size + 1;
      }
    }
#line 484 "2way_micro_sync_disr.upc"
    if(dooverhead != 0)
#line 484 "2way_micro_sync_disr.upc"
    {
#line 485 "2way_micro_sync_disr.upc"
      if(((int) upcr_mythread () ) == 0)
#line 485 "2way_micro_sync_disr.upc"
      {
#line 486 "2way_micro_sync_disr.upc"
        printf("Starting PUT-PIPE-OVERHEAD...\n");
#line 487 "2way_micro_sync_disr.upc"
        printf("%-*s%*s%*s\n", (int) 5, "Size", (int) 6, "Depth", (int) 11, "Overhead (us) min max average");
      }
#line 489 "2way_micro_sync_disr.upc"
      upcr_barrier(280593281, 1);
#line 490 "2way_micro_sync_disr.upc"
      size = 0;
#line 490 "2way_micro_sync_disr.upc"
      while((_UINT32)(size) <= 15U)
#line 490 "2way_micro_sync_disr.upc"
      {
#line 491 "2way_micro_sync_disr.upc"
        n = (POW2)[size];
#line 492 "2way_micro_sync_disr.upc"
        nbytes = (_UINT64)(n) * 8ULL;
#line 493 "2way_micro_sync_disr.upc"
        ITERS = (ITER_PER_MSG)[size];
#line 495 "2way_micro_sync_disr.upc"
        end = 0ULL;
#line 496 "2way_micro_sync_disr.upc"
        upcr_barrier(280593282, 1);
#line 497 "2way_micro_sync_disr.upc"
        iter = 0;
#line 497 "2way_micro_sync_disr.upc"
        while(iter < ITERS)
#line 497 "2way_micro_sync_disr.upc"
        {
#line 498 "2way_micro_sync_disr.upc"
          upcr_barrier(280593283, 1);
#line 499 "2way_micro_sync_disr.upc"
          _bupc_comma7 = bupc_ticks_now();
#line 499 "2way_micro_sync_disr.upc"
          start = _bupc_comma7;
#line 500 "2way_micro_sync_disr.upc"
          upcr_barrier(280593284, 1);
#line 501 "2way_micro_sync_disr.upc"
          i = 0;
#line 501 "2way_micro_sync_disr.upc"
          while(i < depth)
#line 501 "2way_micro_sync_disr.upc"
          {
#line 503 "2way_micro_sync_disr.upc"
            _bupc_Mptra111 = UPCR_ADD_PSHAREDI(remA, 8ULL, (n + 128) * i);
#line 503 "2way_micro_sync_disr.upc"
            _bupc_Mstopcvt112 = UPCR_PSHARED_TO_SHARED(_bupc_Mptra111);
#line 503 "2way_micro_sync_disr.upc"
            _bupc_call20 = bupc_memput_async(_bupc_Mstopcvt112, (_IEEE64 *)((_UINT8 *)(a) + (((n * 8) + 1024) * i)), (unsigned long)((_UINT64)(n) * 8ULL));
#line 503 "2way_micro_sync_disr.upc"
            (handle)[i] = _bupc_call20;
#line 503 "2way_micro_sync_disr.upc"
            _33 :;
#line 503 "2way_micro_sync_disr.upc"
            i = i + 1;
          }
#line 504 "2way_micro_sync_disr.upc"
          _bupc_comma8 = bupc_ticks_now();
#line 504 "2way_micro_sync_disr.upc"
          end = end + (_bupc_comma8 - start);
#line 505 "2way_micro_sync_disr.upc"
          i = 0;
#line 505 "2way_micro_sync_disr.upc"
          while(i < depth)
#line 505 "2way_micro_sync_disr.upc"
          {
#line 506 "2way_micro_sync_disr.upc"
            bupc_waitsync((handle)[i]);
#line 506 "2way_micro_sync_disr.upc"
            _34 :;
#line 506 "2way_micro_sync_disr.upc"
            i = i + 1;
          }
#line 507 "2way_micro_sync_disr.upc"
          _32 :;
#line 507 "2way_micro_sync_disr.upc"
          iter = iter + 1;
        }
#line 508 "2way_micro_sync_disr.upc"
        _bupc_Mptra113 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, ((int) upcr_mythread () ));
#line 508 "2way_micro_sync_disr.upc"
        UPCR_PUT_PSHARED_VAL(_bupc_Mptra113, 0, end, 8);
#line 509 "2way_micro_sync_disr.upc"
        upcr_barrier(280593285, 1);
#line 511 "2way_micro_sync_disr.upc"
        if(((int) upcr_mythread () ) == 0)
#line 511 "2way_micro_sync_disr.upc"
        {
#line 512 "2way_micro_sync_disr.upc"
          UPCR_GET_PSHARED(&_bupc_spillld114, ticks_all, 0, 8);
#line 512 "2way_micro_sync_disr.upc"
          max_tick = _bupc_spillld114;
#line 513 "2way_micro_sync_disr.upc"
          UPCR_GET_PSHARED(&_bupc_spillld115, ticks_all, 0, 8);
#line 513 "2way_micro_sync_disr.upc"
          min_tick = _bupc_spillld115;
#line 514 "2way_micro_sync_disr.upc"
          tick_sum = 0ULL;
#line 515 "2way_micro_sync_disr.upc"
          j = 0;
#line 515 "2way_micro_sync_disr.upc"
          while(j < ((int) upcr_threads () ))
#line 515 "2way_micro_sync_disr.upc"
          {
#line 516 "2way_micro_sync_disr.upc"
            _bupc_Mptra116 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 516 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld117, _bupc_Mptra116, 0, 8);
#line 516 "2way_micro_sync_disr.upc"
            if(max_tick < _bupc_spillld117)
#line 516 "2way_micro_sync_disr.upc"
            {
#line 517 "2way_micro_sync_disr.upc"
              _bupc_Mptra118 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 517 "2way_micro_sync_disr.upc"
              UPCR_GET_PSHARED(&_bupc_spillld119, _bupc_Mptra118, 0, 8);
#line 517 "2way_micro_sync_disr.upc"
              max_tick = _bupc_spillld119;
            }
#line 518 "2way_micro_sync_disr.upc"
            _bupc_Mptra120 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 518 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld121, _bupc_Mptra120, 0, 8);
#line 518 "2way_micro_sync_disr.upc"
            if(min_tick > _bupc_spillld121)
#line 518 "2way_micro_sync_disr.upc"
            {
#line 519 "2way_micro_sync_disr.upc"
              _bupc_Mptra122 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 519 "2way_micro_sync_disr.upc"
              UPCR_GET_PSHARED(&_bupc_spillld123, _bupc_Mptra122, 0, 8);
#line 519 "2way_micro_sync_disr.upc"
              min_tick = _bupc_spillld123;
            }
#line 521 "2way_micro_sync_disr.upc"
            _bupc_Mptra124 = UPCR_ADD_PSHARED1(ticks_all, 8ULL, j);
#line 521 "2way_micro_sync_disr.upc"
            UPCR_GET_PSHARED(&_bupc_spillld125, _bupc_Mptra124, 0, 8);
#line 521 "2way_micro_sync_disr.upc"
            tick_sum = tick_sum + _bupc_spillld125;
#line 522 "2way_micro_sync_disr.upc"
            _35 :;
#line 522 "2way_micro_sync_disr.upc"
            j = j + 1;
          }
#line 523 "2way_micro_sync_disr.upc"
          _bupc_comma9 = bupc_ticks_to_us(min_tick);
#line 523 "2way_micro_sync_disr.upc"
          usecs_min = (_IEEE64)(_bupc_comma9) / (_IEEE64)((ITERS * depth));
#line 524 "2way_micro_sync_disr.upc"
          _bupc_comma10 = bupc_ticks_to_us(max_tick);
#line 524 "2way_micro_sync_disr.upc"
          usecs_max = (_IEEE64)(_bupc_comma10) / (_IEEE64)((ITERS * depth));
#line 525 "2way_micro_sync_disr.upc"
          _bupc_comma11 = bupc_ticks_to_us(tick_sum);
#line 525 "2way_micro_sync_disr.upc"
          usecs_sum = (_IEEE64)(_bupc_comma11) / (_IEEE64)((ITERS * depth));
#line 528 "2way_micro_sync_disr.upc"
          _bupc_comma12 = get_thor_servers_per_domain();
#line 528 "2way_micro_sync_disr.upc"
          printf("%s %s %d %d %5d %5d %10.2f %10.2f %10.2f\n", "PUT", "OVHD", ((int) upcr_threads () ), _bupc_comma12, nbytes, depth, usecs_min, usecs_max, usecs_sum / (_IEEE64)(((int) upcr_threads () )));
        }
#line 531 "2way_micro_sync_disr.upc"
        _31 :;
#line 531 "2way_micro_sync_disr.upc"
        size = size + 1;
      }
    }
  }
#line 535 "2way_micro_sync_disr.upc"
  upcr_barrier(280593286, 1);
  UPCR_EXIT_FUNCTION();
  return 0;
} /* user_main */

#line 1 "_SYSTEM"
/**************************************************************************/
/* upcc-generated strings for configuration consistency checks            */

GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_GASNetConfig_gen, 
 "$GASNetConfig: (/tmp/upcc--17002-1407444802/2way_micro_sync_disr.trans.c) RELEASE=1.22.5,SPEC=1.8,CONDUIT=ARIES(ARIES-0.3/ARIES-0.3),THREADMODEL=PAR,SEGMENT=FAST,PTR=64bit,noalign,pshm,nodebug,notrace,nostats,nodebugmalloc,nosrclines,timers_native,membars_native,atomics_native,atomic32_native,atomic64_native $");
GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_UPCRConfig_gen,
 "$UPCRConfig: (/tmp/upcc--17002-1407444802/2way_micro_sync_disr.trans.c) VERSION=2.19.1,PLATFORMENV=shared-distributed,SHMEM=pshm,SHAREDPTRREP=struct/p32:t32:a64,TRANS=berkeleyupc,nodebug,nogasp,notv,dynamicthreads $");
GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_translatetime, 
 "$UPCTranslateTime: (2way_micro_sync_disr.o) Thu Aug  7 13:53:22 2014 $");
GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_GASNetConfig_obj, 
 "$GASNetConfig: (2way_micro_sync_disr.o) " GASNET_CONFIG_STRING " $");
GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_UPCRConfig_obj,
 "$UPCRConfig: (2way_micro_sync_disr.o) " UPCR_CONFIG_STRING UPCRI_THREADCONFIG_STR " $");
GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_translator, 
 "$UPCTranslator: (2way_micro_sync_disr.o) /usr/local/upc/nightly/translator/install/targ (aphid) $");
GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_upcver, 
 "$UPCVersion: (2way_micro_sync_disr.o) 2.19.3 (UNSTABLE) $");
GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_compileline, 
 "$UPCCompileLine: (2way_micro_sync_disr.o) /usr/local/upc/nightly/runtime/inst/bin/upcc.pl --at-remote-http --arch-size=64 --network=aries -g -Wu,-Wf,-Wwrite-strings --lines -save-all-temps --trans --sizes-file=upcc-sizes 2way_micro_sync_disr.i $");
GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_compiletime, 
 "$UPCCompileTime: (2way_micro_sync_disr.o) " __DATE__ " " __TIME__ " $");
#ifndef UPCRI_CC /* ensure backward compatilibity for http netcompile */
#define UPCRI_CC <unknown>
#endif
GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_backendcompiler, 
 "$UPCRBackendCompiler: (2way_micro_sync_disr.o) " _STRINGIFY(UPCRI_CC) " $");

#ifdef GASNETT_CONFIGURE_MISMATCH
  GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_configuremismatch, 
   "$UPCRConfigureMismatch: (2way_micro_sync_disr.o) 1 $");
  GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_configuredcompiler, 
   "$UPCRConfigureCompiler: (2way_micro_sync_disr.o) " GASNETT_PLATFORM_COMPILER_IDSTR " $");
  GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_buildcompiler, 
   "$UPCRBuildCompiler: (2way_micro_sync_disr.o) " PLATFORM_COMPILER_IDSTR " $");
#endif

/**************************************************************************/
GASNETT_IDENT(UPCRI_IdentString_2way_micro_sync_disr_o_1407444802_transver_2,
 "$UPCTranslatorVersion: (2way_micro_sync_disr.o) 2.19.3 (UNSTABLE), built on Aug  6 2014 at 22:22:57, host aphid linux-x86_64/64, gcc v4.2.4 (Ubuntu 4.2.4-1ubuntu4) $");
GASNETT_IDENT(UPCRI_IdentString_ALLOC__way_micro_sync_disr_12616619006698816357,"$UPCRAllocFn: (2way_micro_sync_disr.trans.c) UPCRI_ALLOC__way_micro_sync_disr_12616619006698816357 $");
GASNETT_IDENT(UPCRI_IdentString_INIT__way_micro_sync_disr_12616619006698816357,"$UPCRInitFn: (2way_micro_sync_disr.trans.c) UPCRI_INIT__way_micro_sync_disr_12616619006698816357 $");
