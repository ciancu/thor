/* --- UPCR system headers --- */ 
#include "upcr.h" 
#include "whirl2c.h"
#include "upcr_proxy.h"
/*******************************************************
 * C file translated from WHIRL Thu Aug  7 13:53:16 2014
 *******************************************************/

/* UPC Runtime specification expected: 3.6 */
#define UPCR_WANT_MAJOR 3
#define UPCR_WANT_MINOR 6
/* UPC translator version: release 2.19.3, built on Aug  6 2014 at 22:22:57, host aphid linux-x86_64/64, gcc v4.2.4 (Ubuntu 4.2.4-1ubuntu4) */
/* Included code from the initialization script */
#line 1 "trans_extra_linkdummy.w2c.h"
/* Include builtin types and operators */

#ifndef UPCR_TRANS_EXTRA_INCL
#define UPCR_TRANS_EXTRA_INCL
extern int upcrt_gcd (int _a, int _b);
extern int _upcrt_forall_start(int _start_thread, int _step, int _lo, int _scale);
#define upcrt_forall_start(start_thread, step, lo, scale)  \
       _upcrt_forall_start(start_thread, step, lo, scale)
int32_t UPCR_TLD_DEFINE_TENTATIVE(upcrt_forall_control, 4, 4);
#define upcr_forall_control upcrt_forall_control
#ifndef UPCR_EXIT_FUNCTION
#define UPCR_EXIT_FUNCTION() ((void)0)
#endif
#if UPCR_RUNTIME_SPEC_MAJOR > 3 || (UPCR_RUNTIME_SPEC_MAJOR == 3 && UPCR_RUNTIME_SPEC_MINOR >= 8)
  #define UPCRT_STARTUP_SHALLOC(sptr, blockbytes, numblocks, mult_by_threads, elemsz, typestr) \
      { &(sptr), (blockbytes), (numblocks), (mult_by_threads), (elemsz), #sptr, (typestr) }
#else
  #define UPCRT_STARTUP_SHALLOC(sptr, blockbytes, numblocks, mult_by_threads, elemsz, typestr) \
      { &(sptr), (blockbytes), (numblocks), (mult_by_threads) }
#endif
#define UPCRT_STARTUP_PSHALLOC UPCRT_STARTUP_SHALLOC

/**** Autonb optimization ********/

extern void _upcrt_memput_nb(upcr_shared_ptr_t _dst, const void *_src, size_t _n);
#define upcrt_memput_nb(dst, src, n) \
       (upcri_srcpos(), _upcrt_memput_nb(dst, src, n))

#endif


/* Types */
/* File-level vars and routines */
static void upcc_trans_extra_dummyfn();


#define UPCR_SHARED_SIZE_ 16
#define UPCR_PSHARED_SIZE_ 16

void UPCRI_ALLOC_trans_extra_linkdummy_5759406357864122144(void) { 
UPCR_BEGIN_FUNCTION();

UPCR_SET_SRCPOS("_trans_extra_linkdummy_5759406357864122144_ALLOC",0);
}

void UPCRI_INIT_trans_extra_linkdummy_5759406357864122144(void) { 
UPCR_BEGIN_FUNCTION();
UPCR_SET_SRCPOS("_trans_extra_linkdummy_5759406357864122144_INIT",0);
}

#line 3 ""
static void upcc_trans_extra_dummyfn()
#line 3 ""
{
#line 3 ""
  UPCR_BEGIN_FUNCTION();
  
  UPCR_EXIT_FUNCTION();
  return;
} /* upcc_trans_extra_dummyfn */

#line 1 "_SYSTEM"
/**************************************************************************/
/* upcc-generated strings for configuration consistency checks            */

GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_GASNetConfig_gen, 
 "$GASNetConfig: (/tmp/upcc--16954-1407444796/trans_extra_linkdummy.trans.c) RELEASE=1.22.5,SPEC=1.8,CONDUIT=ARIES(ARIES-0.3/ARIES-0.3),THREADMODEL=PAR,SEGMENT=FAST,PTR=64bit,noalign,pshm,nodebug,notrace,nostats,nodebugmalloc,nosrclines,timers_native,membars_native,atomics_native,atomic32_native,atomic64_native $");
GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_UPCRConfig_gen,
 "$UPCRConfig: (/tmp/upcc--16954-1407444796/trans_extra_linkdummy.trans.c) VERSION=2.19.1,PLATFORMENV=shared-distributed,SHMEM=pshm,SHAREDPTRREP=struct/p32:t32:a64,TRANS=berkeleyupc,nodebug,nogasp,notv,dynamicthreads $");
GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_translatetime, 
 "$UPCTranslateTime: (trans_extra_linkdummy.o) Thu Aug  7 13:53:16 2014 $");
GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_GASNetConfig_obj, 
 "$GASNetConfig: (trans_extra_linkdummy.o) " GASNET_CONFIG_STRING " $");
GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_UPCRConfig_obj,
 "$UPCRConfig: (trans_extra_linkdummy.o) " UPCR_CONFIG_STRING UPCRI_THREADCONFIG_STR " $");
GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_translator, 
 "$UPCTranslator: (trans_extra_linkdummy.o) /usr/local/upc/nightly/translator/install/targ (aphid) $");
GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_upcver, 
 "$UPCVersion: (trans_extra_linkdummy.o) 2.19.3 (UNSTABLE) $");
GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_compileline, 
 "$UPCCompileLine: (trans_extra_linkdummy.o) /usr/local/upc/nightly/runtime/inst/bin/upcc.pl --at-remote-http --arch-size=64 --network=aries -g -Wu,-Wf,-Wwrite-strings --lines -save-all-temps --trans --sizes-file=upcc-sizes trans_extra_linkdummy.i $");
GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_compiletime, 
 "$UPCCompileTime: (trans_extra_linkdummy.o) " __DATE__ " " __TIME__ " $");
#ifndef UPCRI_CC /* ensure backward compatilibity for http netcompile */
#define UPCRI_CC <unknown>
#endif
GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_backendcompiler, 
 "$UPCRBackendCompiler: (trans_extra_linkdummy.o) " _STRINGIFY(UPCRI_CC) " $");

#ifdef GASNETT_CONFIGURE_MISMATCH
  GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_configuremismatch, 
   "$UPCRConfigureMismatch: (trans_extra_linkdummy.o) 1 $");
  GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_configuredcompiler, 
   "$UPCRConfigureCompiler: (trans_extra_linkdummy.o) " GASNETT_PLATFORM_COMPILER_IDSTR " $");
  GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_buildcompiler, 
   "$UPCRBuildCompiler: (trans_extra_linkdummy.o) " PLATFORM_COMPILER_IDSTR " $");
#endif

/**************************************************************************/
GASNETT_IDENT(UPCRI_IdentString_trans_extra_linkdummy_o_1407444796_transver_2,
 "$UPCTranslatorVersion: (trans_extra_linkdummy.o) 2.19.3 (UNSTABLE), built on Aug  6 2014 at 22:22:57, host aphid linux-x86_64/64, gcc v4.2.4 (Ubuntu 4.2.4-1ubuntu4) $");
