#include <upc.h>

shared int A[THREADS * 100];


int main() {
  shared int *p;
  int i;
  printf(" SIZEOF A = %d\n", sizeof(A)/sizeof(int));
  if(MYTHREAD == 0) {
    p = &A;
    for(i=0; i< THREADS*4; i++) {
      p = p + i;
    }
    p[i] = 7;
  }
  return 0;
}
