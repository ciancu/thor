/* --- UPCR system headers --- */ 
#include "upcr.h" 
#include "whirl2c.h"
#include "upcr_proxy.h"
/*******************************************************
 * C file translated from WHIRL Thu Jun 19 13:05:55 2014
 *******************************************************/

/* UPC Runtime specification expected: 3.6 */
#define UPCR_WANT_MAJOR 3
#define UPCR_WANT_MINOR 6
/* UPC translator version: release 2.19.3, built on Jun 18 2014 at 22:22:53, host aphid linux-x86_64/64, gcc v4.2.4 (Ubuntu 4.2.4-1ubuntu4) */
/* Included code from the initialization script */
#include</global/homes/n/nchaimov/upc-runtime-thor-inst-edison/dbg/include/upcr_preinclude/upc_types.h>
#include "upcr_geninclude/stddef.h"
#include</global/homes/n/nchaimov/upc-runtime-thor-inst-edison/dbg/include/upcr_preinclude/upc_bits.h>
#include "upcr_geninclude/stdlib.h"
#include "upcr_geninclude/inttypes.h"
#include</global/u2/n/nchaimov/upc-runtime-thor/thor_pth/support.h>
#include</usr/include/unistd.h>
#include</usr/include/pthread.h>
#include "upcr_geninclude/stdio.h"
#include "upcr_geninclude/stdlib.h"
#include</usr/include/utmpx.h>
#include</usr/include/errno.h>
#include</usr/include/semaphore.h>
#include "upcr_geninclude/time.h"
#include</usr/include/assert.h>
#include "upcr_geninclude/string.h"
#include "upcr_geninclude/stdint.h"
#line 1 "dispatch_client.w2c.h"
/* Include builtin types and operators */

#ifndef UPCR_TRANS_EXTRA_INCL
#define UPCR_TRANS_EXTRA_INCL
extern int upcrt_gcd (int _a, int _b);
extern int _upcrt_forall_start(int _start_thread, int _step, int _lo, int _scale);
#define upcrt_forall_start(start_thread, step, lo, scale)  \
       _upcrt_forall_start(start_thread, step, lo, scale)
int32_t UPCR_TLD_DEFINE_TENTATIVE(upcrt_forall_control, 4, 4);
#define upcr_forall_control upcrt_forall_control
#ifndef UPCR_EXIT_FUNCTION
#define UPCR_EXIT_FUNCTION() ((void)0)
#endif
#if UPCR_RUNTIME_SPEC_MAJOR > 3 || (UPCR_RUNTIME_SPEC_MAJOR == 3 && UPCR_RUNTIME_SPEC_MINOR >= 8)
  #define UPCRT_STARTUP_SHALLOC(sptr, blockbytes, numblocks, mult_by_threads, elemsz, typestr) \
      { &(sptr), (blockbytes), (numblocks), (mult_by_threads), (elemsz), #sptr, (typestr) }
#else
  #define UPCRT_STARTUP_SHALLOC(sptr, blockbytes, numblocks, mult_by_threads, elemsz, typestr) \
      { &(sptr), (blockbytes), (numblocks), (mult_by_threads) }
#endif
#define UPCRT_STARTUP_PSHALLOC UPCRT_STARTUP_SHALLOC

/**** Autonb optimization ********/

extern void _upcrt_memput_nb(upcr_shared_ptr_t _dst, const void *_src, size_t _n);
#define upcrt_memput_nb(dst, src, n) \
       (upcri_srcpos(), _upcrt_memput_nb(dst, src, n))

#endif


/* Types */
struct _bupc_anonymous6_779799861l {
    upcr_shared_ptr_t s_addr;
    upcr_shared_ptr_t l_addr;
    unsigned long size;
    unsigned int req_type;
    long h;
    unsigned long req_timestamp;
    int service_time;
    unsigned int req_id;
    unsigned int ready_to_serve;
    unsigned int req_state;
    char pad[72LL];
  };
  struct _bupc_anonymous26_1216583056l {
    int thr_per_domain;
    int domain_count;
    int node_count;
    int domain_id;
    int srv_per_domain;
    int relative_domain;
    int start_queue;
    int client_id;
    int queues_per_server;
    int client_dispatch_policy;
  };
  struct _bupc_anonymous17_1720742066l {
      int queue_num;
      int req_id;
      sem_t wait_sem;
    };
    struct _bupc_anonymous18_1895944952l {
    sem_t service_sem;
    struct _bupc_anonymous17_1720742066l waiting_client[64LL];
    char xxxpadding0[128LL];
    unsigned long waiting_bitmap;
    char xxxpadding1[128LL];
    int start_queue;
    int end_queue;
    unsigned int command;
    int server_id;
    int server_dispatch_policy;
    pthread_barrier_t bar;
    cpu_set_t affinity_mask;
    int clients_active;
  };
  struct _bupc_anonymous8_2005322018l {
    char xxxpadding0[128LL];
    unsigned long head;
    char xxxpadding1[128LL];
    unsigned long tail_issued;
    unsigned long tail_completed;
    char pad[128LL];
    unsigned long issue_tokens;
    int my_server_id;
    int avail_credit;
    int id;
  };
  struct _bupc_anonymous11_1380763888l {
    struct _bupc_anonymous8_2005322018l queue[11LL];
    volatile int queue_map[1024LL];
    int queue_hits[1024LL];
    int head;
    int used_count;
    sem_t queue_sem;
    int total_credit;
  };
  struct _bupc_anonymous23_650264093l {
      struct _bupc_anonymous18_1895944952l server[64LL];
      int thr_per_domain;
      int domain_id;
    };
    struct _bupc_anonymous25_692027308l {
    int domain_id;
    int thr_per_domain;
    pthread_barrier_t * pbar;
    struct _bupc_anonymous11_1380763888l * domain_queues;
    struct _bupc_anonymous18_1895944952l * scontrol;
    int * msg_credits;
    int * msg_min_wait;
    int gidx;
  };
  struct _upcri_eop;
  /* File-level vars and routines */
extern void * thor_upc_cast(upcr_shared_ptr_t);

extern void interconnect_poll(void);

extern void * wrap_upc_cast(upcr_shared_ptr_t);

extern int client_check_req_ready(struct _bupc_anonymous6_779799861l *, int);

extern void wait_memslot_ready(struct _bupc_anonymous26_1216583056l *, struct _bupc_anonymous18_1895944952l *, struct _bupc_anonymous8_2005322018l *, unsigned int);

extern void adjust_domain_map(struct _bupc_anonymous11_1380763888l *, int);

extern void init_peer_queue(struct _bupc_anonymous11_1380763888l *, int);

extern int dest_find_queue(struct _bupc_anonymous11_1380763888l *, int);

extern void mem_bar(void);

extern long get_online_procs();

extern int get_node_count();

extern void start_one_server(int, int, int, int, int, int *, int);

extern void * server_routine(void *);

extern void wakeup_one_server(int);

extern void initialize_one_domain(int, int, int);

extern void init_server(int, int, int, int, int *);

extern void thor_default_server_init(void);

extern void release_server(void);

extern void sbupc_waitsync_all(struct _upcri_eop **, unsigned long);

extern void sbupc_waitsync(struct _upcri_eop *);

extern void supc_memget(void *, upcr_shared_ptr_t, unsigned long);

extern void supc_memput(upcr_shared_ptr_t, void *, unsigned long);

extern int GET_MYTH();

extern int GET_ALLTH();

extern struct _bupc_anonymous18_1895944952l * get_scontrol(int, int, int);

extern struct _bupc_anonymous11_1380763888l * get_domq(int, int, int);

extern int server_fired(int);

extern struct _bupc_anonymous25_692027308l * get_server_info(int);

struct S_I_dir {
  upcr_pshared_ptr_t domain_server[256000LL];
  upcr_pshared_ptr_t domain_queue[256000LL];
  upcr_pshared_ptr_t server_info[256000LL];
};

#define UPCR_SHARED_SIZE_ 16
#define UPCR_PSHARED_SIZE_ 16
upcr_pshared_ptr_t thor_startup_dir;
upcr_pshared_ptr_t domain_queue;
upcr_pshared_ptr_t domain_server;
upcr_pshared_ptr_t server_info;
upcr_pshared_ptr_t server_wake;
extern int thor_enabled;
extern int thor_remap_enabled;
extern int thor_physical_nodes;
extern int thor_servers_per_domain;
extern int thor_domains_per_node;
extern int thor_domain_count;
extern int thor_cores_per_numa_domain;
extern int thor_cores_per_node;
extern int thor_cores_per_domain;
extern int thor_issue_immediate;
extern int thor_use_pthreads;
struct _bupc_anonymous26_1216583056l UPCR_TLD_DEFINE_TENTATIVE(my_info, 40, 4);
typedef int _type_my_rr_queue_seq[11];
_type_my_rr_queue_seq UPCR_TLD_DEFINE_TENTATIVE(my_rr_queue_seq, 44, 4);
struct _bupc_anonymous11_1380763888l*  UPCR_TLD_DEFINE_TENTATIVE(my_domain_queues, 8, 8);
struct _bupc_anonymous23_650264093l*  UPCR_TLD_DEFINE_TENTATIVE(my_domain_servers, 8, 8);
int UPCR_TLD_DEFINE_TENTATIVE(mpq, 4, 4);
int UPCR_TLD_DEFINE(rrQueueIndex, 4, 4) = 0;
int UPCR_TLD_DEFINE(rrQueuePos, 4, 4) = 0;
int UPCR_TLD_DEFINE(rrQueueNum, 4, 4) = 0;
int UPCR_TLD_DEFINE(_N5_count_N10_3120166115_, 4, 4) = 0;

void UPCRI_ALLOC_dispatch_client_7815053779325561610(void) { 
UPCR_BEGIN_FUNCTION();
upcr_startup_pshalloc_t _bupc_pinfo[] = { 
UPCRT_STARTUP_PSHALLOC(thor_startup_dir, 12288000, 1, 0, 12288000, "R1_N7S_I_dir"), 
UPCRT_STARTUP_PSHALLOC(domain_queue, 16, 8, 1, 16, "A8H_R1_PR1_N29_bupc_anonymous11_1380763888l"), 
UPCRT_STARTUP_PSHALLOC(domain_server, 16, 8, 1, 16, "A8H_R1_PR1_N28_bupc_anonymous23_650264093l"), 
UPCRT_STARTUP_PSHALLOC(server_info, 16, 8, 1, 16, "A8H_R1_PR1_N28_bupc_anonymous25_692027308l"), 
UPCRT_STARTUP_PSHALLOC(server_wake, 4, 1, 1, 4, "A1H_R1_i"), 
 };

UPCR_SET_SRCPOS("_dispatch_client_7815053779325561610_ALLOC",0);
upcr_startup_pshalloc(_bupc_pinfo, sizeof(_bupc_pinfo) / sizeof(upcr_startup_pshalloc_t));
}

void UPCRI_INIT_dispatch_client_7815053779325561610(void) { 
UPCR_BEGIN_FUNCTION();
UPCR_SET_SRCPOS("_dispatch_client_7815053779325561610_INIT",0);
}

#line 81 "dispatch_client.c"
extern void * thor_upc_cast(
  upcr_shared_ptr_t sptr)
#line 81 "dispatch_client.c"
{
#line 81 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  int flip;
  void * res;
  void * _bupc_call0;
  
#line 83 "dispatch_client.c"
  flip = 0;
#line 84 "dispatch_client.c"
  THOR_RESET_REMAP(flip);
#line 85 "dispatch_client.c"
  _bupc_call0 = _bupc_cast(sptr);
#line 85 "dispatch_client.c"
  res = _bupc_call0;
#line 86 "dispatch_client.c"
  THOR_RESTORE_REMAP(flip);
#line 87 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 87 "dispatch_client.c"
  return res;
} /* thor_upc_cast */


#line 91 "dispatch_client.c"
extern void interconnect_poll()
#line 91 "dispatch_client.c"
{
#line 91 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  
#line 97 "dispatch_client.c"
  upc_poll();
  UPCR_EXIT_FUNCTION();
  return;
} /* interconnect_poll */


#line 148 "dispatch_client.c"
extern void * wrap_upc_cast(
  upcr_shared_ptr_t sptr)
#line 148 "dispatch_client.c"
{
#line 148 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  void * _bupc_call1;
  
#line 149 "dispatch_client.c"
  _bupc_call1 = _bupc_cast(sptr);
#line 149 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 149 "dispatch_client.c"
  return _bupc_call1;
} /* wrap_upc_cast */


#line 180 "dispatch_client.c"
extern int client_check_req_ready(
  struct _bupc_anonymous6_779799861l * mem_req,
  int req_id)
#line 180 "dispatch_client.c"
{
#line 180 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  
#line 185 "dispatch_client.c"
  if((mem_req) -> req_id > (_UINT32)(req_id))
#line 185 "dispatch_client.c"
  {
#line 186 "dispatch_client.c"
    UPCR_EXIT_FUNCTION();
#line 186 "dispatch_client.c"
    return 1;
  }
#line 187 "dispatch_client.c"
  if(((_UINT32)(req_id) - (mem_req) -> req_id) > 16777215U)
#line 187 "dispatch_client.c"
  {
#line 188 "dispatch_client.c"
    UPCR_EXIT_FUNCTION();
#line 188 "dispatch_client.c"
    return 2;
  }
#line 190 "dispatch_client.c"
  if((mem_req) -> req_state == 0U)
#line 190 "dispatch_client.c"
  {
#line 191 "dispatch_client.c"
    if((mem_req) -> req_id == (_UINT32)(req_id))
#line 191 "dispatch_client.c"
    {
#line 192 "dispatch_client.c"
      UPCR_EXIT_FUNCTION();
#line 192 "dispatch_client.c"
      return 3;
    }
#line 193 "dispatch_client.c"
    if((mem_req) -> ready_to_serve == (_UINT32)(req_id))
#line 193 "dispatch_client.c"
    {
#line 194 "dispatch_client.c"
      UPCR_EXIT_FUNCTION();
#line 194 "dispatch_client.c"
      return 4;
    }
  }
#line 197 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 197 "dispatch_client.c"
  return 0;
} /* client_check_req_ready */


#line 203 "dispatch_client.c"
extern void wait_memslot_ready(
  struct _bupc_anonymous26_1216583056l * pinfo,
  struct _bupc_anonymous18_1895944952l * my_server,
  struct _bupc_anonymous8_2005322018l * my_peer_queue,
  unsigned int req_id)
#line 203 "dispatch_client.c"
{
#line 203 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  register _INT32 _bupc_comma;
  int queue_num;
  int slot;
  struct _bupc_anonymous6_779799861l * mem_req;
  int cid;
  unsigned long mask;
  struct _bupc_anonymous17_1720742066l(*_bupc_arr_base0)[64LL];
  struct _bupc_anonymous17_1720742066l(*_bupc_arr_base1)[64LL];
  int _bupc__spilleq2;
  
#line 204 "dispatch_client.c"
  queue_num = (my_peer_queue) -> id;
#line 206 "dispatch_client.c"
  if(queue_num == 9)
#line 206 "dispatch_client.c"
  {
#line 207 "dispatch_client.c"
    UPCR_EXIT_FUNCTION();
#line 207 "dispatch_client.c"
    return;
  }
#line 213 "dispatch_client.c"
  slot = (_INT32)(req_id & 2047U);
#line 214 "dispatch_client.c"
  mem_req = (struct _bupc_anonymous6_779799861l *)((struct _bupc_anonymous8_2005322018l *)((_UINT8 *)(my_peer_queue) + (slot * 152)));
#line 215 "dispatch_client.c"
  cid = (pinfo) -> client_id;
#line 216 "dispatch_client.c"
  mask = _U8SHL(1ULL, (_UINT64)(cid));
#line 217 "dispatch_client.c"
  _bupc_arr_base0 = (struct _bupc_anonymous17_1720742066l(*)[64LL])((_UINT8 *)(my_server) + 32ULL);
#line 217 "dispatch_client.c"
  ((*_bupc_arr_base0)[cid]).req_id = (_INT32)(req_id);
#line 218 "dispatch_client.c"
  _bupc_arr_base1 = (struct _bupc_anonymous17_1720742066l(*)[64LL])((_UINT8 *)(my_server) + 32ULL);
#line 218 "dispatch_client.c"
  ((*_bupc_arr_base1)[cid]).queue_num = queue_num;
#line 219 "dispatch_client.c"
  atomic_f_or((volatile unsigned long *)((unsigned long *)((volatile unsigned long *)(my_server) + 340ULL)), mask);
#line 220 "dispatch_client.c"
  _bupc_comma = client_check_req_ready(mem_req, (int)(_INT32)(req_id));
#line 220 "dispatch_client.c"
  _bupc__spilleq2 = _bupc_comma;
#line 220 "dispatch_client.c"
  while(_bupc__spilleq2 == 0)
#line 220 "dispatch_client.c"
  {
#line 220 "dispatch_client.c"
    _bupc_comma = client_check_req_ready(mem_req, (int)(_INT32)(req_id));
#line 220 "dispatch_client.c"
    _bupc__spilleq2 = _bupc_comma;
  }
#line 222 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 222 "dispatch_client.c"
  return;
} /* wait_memslot_ready */


#line 265 "dispatch_client.c"
extern void adjust_domain_map(
  struct _bupc_anonymous11_1380763888l * _bupc_w2c_domain_queue0,
  int domain_count)
#line 265 "dispatch_client.c"
{
#line 265 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  int smallest_max;
  int(*_bupc_arr_base3)[1024LL];
  int smallest_loc;
  int i;
  int current_hits;
  int(*_bupc_arr_base4)[1024LL];
  int(*_bupc_arr_base5)[1024LL];
  volatile int(*_bupc_arr_base6)[1024LL];
  int MaxHits[11LL];
  int Dests[11LL];
  int(*_bupc_arr_base7)[1024LL];
  int _bupc_w2c_current_hits0;
  int(*_bupc_arr_base8)[1024LL];
  volatile int(*_bupc_arr_base9)[1024LL];
  int j;
  volatile int(*_bupc_arr_base10)[1024LL];
  
#line 272 "dispatch_client.c"
  if((_INT32)((((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).domain_count <= 10)) || (_INT32)(((_bupc_w2c_domain_queue0) -> used_count <= 10)))
#line 272 "dispatch_client.c"
  {
#line 274 "dispatch_client.c"
    UPCR_EXIT_FUNCTION();
#line 274 "dispatch_client.c"
    return;
  }
#line 275 "dispatch_client.c"
  _bupc_arr_base3 = (int(*)[1024LL])((_UINT8 *)(_bupc_w2c_domain_queue0) + 3072656ULL);
#line 275 "dispatch_client.c"
  smallest_max = (*_bupc_arr_base3)[0];
#line 276 "dispatch_client.c"
  smallest_loc = 0;
#line 277 "dispatch_client.c"
  i = 0;
#line 277 "dispatch_client.c"
  while(i <= 10)
#line 277 "dispatch_client.c"
  {
#line 278 "dispatch_client.c"
    _bupc_arr_base4 = (int(*)[1024LL])((_UINT8 *)(_bupc_w2c_domain_queue0) + 3072656ULL);
#line 278 "dispatch_client.c"
    current_hits = (*_bupc_arr_base4)[i];
#line 279 "dispatch_client.c"
    _bupc_arr_base5 = (int(*)[1024LL])((_UINT8 *)(_bupc_w2c_domain_queue0) + 3072656ULL);
#line 279 "dispatch_client.c"
    (*_bupc_arr_base5)[i] = 0;
#line 280 "dispatch_client.c"
    _bupc_arr_base6 = (volatile int(*)[1024LL])((_UINT8 *)(_bupc_w2c_domain_queue0) + 3068560ULL);
#line 280 "dispatch_client.c"
    (*_bupc_arr_base6)[i] = -1;
#line 281 "dispatch_client.c"
    if(current_hits < smallest_max)
#line 281 "dispatch_client.c"
    {
#line 282 "dispatch_client.c"
      smallest_max = current_hits;
#line 283 "dispatch_client.c"
      smallest_loc = i;
    }
#line 285 "dispatch_client.c"
    (MaxHits)[i] = current_hits;
#line 286 "dispatch_client.c"
    (Dests)[i] = i;
#line 287 "dispatch_client.c"
    _1 :;
#line 287 "dispatch_client.c"
    i = i + 1;
  }
#line 288 "dispatch_client.c"
  while(i < ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).domain_count)
#line 288 "dispatch_client.c"
  {
#line 289 "dispatch_client.c"
    _bupc_arr_base7 = (int(*)[1024LL])((_UINT8 *)(_bupc_w2c_domain_queue0) + 3072656ULL);
#line 289 "dispatch_client.c"
    _bupc_w2c_current_hits0 = (*_bupc_arr_base7)[i];
#line 290 "dispatch_client.c"
    _bupc_arr_base8 = (int(*)[1024LL])((_UINT8 *)(_bupc_w2c_domain_queue0) + 3072656ULL);
#line 290 "dispatch_client.c"
    (*_bupc_arr_base8)[i] = 0;
#line 291 "dispatch_client.c"
    _bupc_arr_base9 = (volatile int(*)[1024LL])((_UINT8 *)(_bupc_w2c_domain_queue0) + 3068560ULL);
#line 291 "dispatch_client.c"
    (*_bupc_arr_base9)[i] = -1;
#line 292 "dispatch_client.c"
    if(_bupc_w2c_current_hits0 > smallest_max)
#line 292 "dispatch_client.c"
    {
#line 293 "dispatch_client.c"
      (MaxHits)[smallest_loc] = _bupc_w2c_current_hits0;
#line 294 "dispatch_client.c"
      (Dests)[smallest_loc] = i;
#line 295 "dispatch_client.c"
      smallest_max = _bupc_w2c_current_hits0;
#line 296 "dispatch_client.c"
      j = 0;
#line 296 "dispatch_client.c"
      while(j <= 10)
#line 296 "dispatch_client.c"
      {
#line 297 "dispatch_client.c"
        if(smallest_max > (MaxHits)[j])
#line 297 "dispatch_client.c"
        {
#line 298 "dispatch_client.c"
          smallest_max = (MaxHits)[j];
#line 299 "dispatch_client.c"
          smallest_loc = j;
        }
#line 301 "dispatch_client.c"
        _3 :;
#line 301 "dispatch_client.c"
        j = j + 1;
      }
    }
#line 303 "dispatch_client.c"
    _2 :;
#line 303 "dispatch_client.c"
    i = i + 1;
  }
#line 304 "dispatch_client.c"
  i = 0;
#line 304 "dispatch_client.c"
  while(i <= 10)
#line 304 "dispatch_client.c"
  {
#line 305 "dispatch_client.c"
    _bupc_arr_base10 = (volatile int(*)[1024LL])((_UINT8 *)(_bupc_w2c_domain_queue0) + 3068560ULL);
#line 305 "dispatch_client.c"
    (*_bupc_arr_base10)[(Dests)[i]] = i;
#line 306 "dispatch_client.c"
    init_peer_queue(_bupc_w2c_domain_queue0, i);
#line 307 "dispatch_client.c"
    _4 :;
#line 307 "dispatch_client.c"
    i = i + 1;
  }
  UPCR_EXIT_FUNCTION();
  return;
} /* adjust_domain_map */


#line 313 "dispatch_client.c"
extern int dest_find_queue(
  struct _bupc_anonymous11_1380763888l * _bupc_w2c_domain_queue0,
  int s_domain)
#line 313 "dispatch_client.c"
{
#line 313 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  register _INT32 _bupc_preg;
  int queue_index;
  volatile int(*_bupc_arr_base11)[1024LL];
  sem_t * _bupc_Mspillstruct12;
  volatile int(*_bupc_arr_base13)[1024LL];
  volatile int(*_bupc_arr_base14)[1024LL];
  sem_t * _bupc_Mspillstruct15;
  
#line 315 "dispatch_client.c"
  _bupc_arr_base11 = (volatile int(*)[1024LL])((_UINT8 *)(_bupc_w2c_domain_queue0) + 3068560ULL);
#line 315 "dispatch_client.c"
  queue_index = (*_bupc_arr_base11)[s_domain];
#line 316 "dispatch_client.c"
  if(queue_index >= 0)
#line 316 "dispatch_client.c"
  {
#line 317 "dispatch_client.c"
    UPCR_EXIT_FUNCTION();
#line 317 "dispatch_client.c"
    return queue_index;
  }
#line 318 "dispatch_client.c"
  if((_bupc_w2c_domain_queue0) -> used_count > 10)
#line 318 "dispatch_client.c"
  {
#line 319 "dispatch_client.c"
    UPCR_EXIT_FUNCTION();
#line 319 "dispatch_client.c"
    return 8;
  }
  _bupc_Mspillstruct12 = (sem_t *)((char *)(_bupc_w2c_domain_queue0) + 3076760ULL);
#line 325 "dispatch_client.c"
  sem_wait(_bupc_Mspillstruct12);
#line 328 "dispatch_client.c"
  while(1)
#line 328 "dispatch_client.c"
  {
#line 329 "dispatch_client.c"
    _bupc_arr_base13 = (volatile int(*)[1024LL])((_UINT8 *)(_bupc_w2c_domain_queue0) + 3068560ULL);
#line 329 "dispatch_client.c"
    queue_index = (*_bupc_arr_base13)[s_domain];
#line 330 "dispatch_client.c"
    if(queue_index >= 0)
#line 330 "dispatch_client.c"
    {
#line 331 "dispatch_client.c"
      goto _1;
    }
#line 332 "dispatch_client.c"
    if((_bupc_w2c_domain_queue0) -> used_count > 10)
#line 332 "dispatch_client.c"
    {
#line 333 "dispatch_client.c"
      queue_index = 8;
#line 334 "dispatch_client.c"
      goto _1;
    }
#line 338 "dispatch_client.c"
    _bupc_preg = (_bupc_w2c_domain_queue0) -> head;
#line 338 "dispatch_client.c"
    (_bupc_w2c_domain_queue0) -> head = _bupc_preg + 1;
#line 338 "dispatch_client.c"
    queue_index = _bupc_preg;
#line 339 "dispatch_client.c"
    (_bupc_w2c_domain_queue0) -> used_count = (_bupc_w2c_domain_queue0) -> used_count + 1;
#line 340 "dispatch_client.c"
    init_peer_queue(_bupc_w2c_domain_queue0, queue_index);
#line 342 "dispatch_client.c"
    mem_bar();
#line 343 "dispatch_client.c"
    _bupc_arr_base14 = (volatile int(*)[1024LL])((_UINT8 *)(_bupc_w2c_domain_queue0) + 3068560ULL);
#line 343 "dispatch_client.c"
    (*_bupc_arr_base14)[s_domain] = queue_index;
#line 344 "dispatch_client.c"
    goto _1;
  }
#line 345 "dispatch_client.c"
  _1 :;
  _bupc_Mspillstruct15 = (sem_t *)((char *)(_bupc_w2c_domain_queue0) + 3076760ULL);
#line 349 "dispatch_client.c"
  sem_post(_bupc_Mspillstruct15);
#line 351 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 351 "dispatch_client.c"
  return queue_index;
} /* dest_find_queue */


#line 355 "dispatch_client.c"
UPCRI_INLINE(src_find_queue) int src_find_queue(
  int s_domain,
  int l_thread)
#line 355 "dispatch_client.c"
{
#line 355 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  
#line 356 "dispatch_client.c"
  if(s_domain == ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).domain_id)
#line 356 "dispatch_client.c"
  {
#line 357 "dispatch_client.c"
    UPCR_EXIT_FUNCTION();
#line 357 "dispatch_client.c"
    return 9;
  }
  else
#line 356 "dispatch_client.c"
  {
#line 361 "dispatch_client.c"
    UPCR_EXIT_FUNCTION();
#line 361 "dispatch_client.c"
    return l_thread;
  }
} /* src_find_queue */


#line 370 "dispatch_client.c"
UPCRI_INLINE(rr_index_init) void rr_index_init()
#line 370 "dispatch_client.c"
{
#line 370 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  
#line 371 "dispatch_client.c"
  (*((int *) UPCR_TLD_ADDR( rrQueueIndex ))) = ((*((int(*)[11LL]) UPCR_TLD_ADDR( my_rr_queue_seq ))))[0];
#line 372 "dispatch_client.c"
  (*((int *) UPCR_TLD_ADDR( rrQueuePos ))) = 0;
#line 373 "dispatch_client.c"
  (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).start_queue = (*((int *) UPCR_TLD_ADDR( rrQueueIndex )));
#line 374 "dispatch_client.c"
  (*((int *) UPCR_TLD_ADDR( mpq ))) = 0;
  UPCR_EXIT_FUNCTION();
  return;
} /* rr_index_init */


#line 378 "dispatch_client.c"
extern long get_online_procs()
#line 378 "dispatch_client.c"
{
#line 378 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  register _INT64 _bupc_comma;
  long nprocs;
  int * _bupc_call2;
  char * _bupc_call3;
  
#line 379 "dispatch_client.c"
  nprocs = -1LL;
#line 381 "dispatch_client.c"
  _bupc_comma = sysconf((int) 84);
#line 381 "dispatch_client.c"
  nprocs = _bupc_comma;
#line 382 "dispatch_client.c"
  if(nprocs <= 0LL)
#line 382 "dispatch_client.c"
  {
#line 383 "dispatch_client.c"
    _bupc_call2 = __errno_location();
#line 383 "dispatch_client.c"
    _bupc_call3 = strerror(*_bupc_call2);
#line 383 "dispatch_client.c"
    fprintf(stderr, "Could not determine number of CPUs configured:\n%s\n", _bupc_call3);
  }
#line 385 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 385 "dispatch_client.c"
  return nprocs;
} /* get_online_procs */


#line 397 "dispatch_client.c"
extern int get_node_count()
#line 397 "dispatch_client.c"
{
#line 397 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  register _INT32 _bupc_comma;
  register _INT64 _bupc_comma0;
  char * env_node_count;
  int node_count;
  char * _bupc_call4;
  
#line 399 "dispatch_client.c"
  _bupc_call4 = bupc_getenv("SUPC_NODE_COUNT");
#line 399 "dispatch_client.c"
  env_node_count = _bupc_call4;
#line 400 "dispatch_client.c"
  if((_UINT64)(env_node_count) != 0ULL)
#line 400 "dispatch_client.c"
  {
#line 401 "dispatch_client.c"
    _bupc_comma = atoi(env_node_count);
#line 401 "dispatch_client.c"
    node_count = _bupc_comma;
  }
  else
#line 401 "dispatch_client.c"
  {
#line 407 "dispatch_client.c"
    _bupc_comma0 = get_online_procs();
#line 407 "dispatch_client.c"
    node_count = (_INT64)(((int) upcr_threads () )) / _bupc_comma0;
  }
#line 410 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 410 "dispatch_client.c"
  return node_count;
} /* get_node_count */


#line 416 "dispatch_client.c"
extern void start_one_server(
  int gidx,
  int sidx,
  int servers_per_domain,
  int queues_per_server,
  int server_dispatch_policy,
  int * needed_credits,
  int is_pthread)
#line 416 "dispatch_client.c"
{
#line 416 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  upcr_pshared_ptr_t _bupc_Mspillstruct16;
  struct _bupc_anonymous11_1380763888l * _bupc__casttmp17;
  upcr_pshared_ptr_t _bupc_Mspillstruct18;
  struct _bupc_anonymous23_650264093l * _bupc__casttmp19;
  int thr_per_server;
  int server_parent;
  int UPN;
  int node_rel;
  int my_numa;
  int cpu_id;
  cpu_set_t cpu_affinity;
  int servers_per_node;
  unsigned long __cpu;
  unsigned long _bupc___save_expr20;
  int server_id;
  struct _bupc_anonymous18_1895944952l * scontrol;
  int i;
  struct _bupc_anonymous8_2005322018l(*_bupc_arr_base21)[11LL];
  struct _bupc_anonymous17_1720742066l(*_bupc_Mspillstruct22)[64LL];
  struct _bupc_anonymous17_1720742066l(*_bupc_arr_base23)[64LL];
  struct _bupc_anonymous17_1720742066l(*_bupc_arr_base24)[64LL];
  struct _bupc_anonymous25_692027308l * info;
  struct _bupc_anonymous25_692027308l * _bupc__casttmp25;
  int flip;
  int * _bupc__casttmp26;
  int * msg_credits;
  unsigned long msg_server;
  void * _bupc_call5;
  void * _bupc_call6;
  _UINT64 _bupc_mcselect7;
  void * _bupc_call8;
  void * _bupc_call9;
  upcr_pshared_ptr_t _bupc_Mptra10;
  upcr_pshared_ptr_t _bupc_Mptra11;
  upcr_pshared_ptr_t _bupc_spillld12;
  upcr_shared_ptr_t _bupc_Mstopcvt13;
  upcr_pshared_ptr_t _bupc_Mptra14;
  upcr_pshared_ptr_t _bupc_spillld15;
  upcr_shared_ptr_t _bupc_Mstopcvt16;
  
#line 420 "dispatch_client.c"
  if((_INT32)(((*((int *) UPCR_TLD_ADDR( thor_enabled ))) != 0)) || (_INT32)(((*((int *) UPCR_TLD_ADDR( thor_use_pthreads ))) != 0)))
#line 420 "dispatch_client.c"
  {
    _bupc_Mptra10 = UPCR_ADD_PSHAREDI(thor_startup_dir, 1ULL, 4096000);
    _bupc_Mspillstruct16 = _bupc_Mptra10;
#line 421 "dispatch_client.c"
    _bupc_Mptra11 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct16, 16ULL, (_INT64)(_UINT64)(gidx));
#line 421 "dispatch_client.c"
    UPCR_GET_PSHARED(&_bupc_spillld12, _bupc_Mptra11, 0, 16);
#line 421 "dispatch_client.c"
    _bupc_Mstopcvt13 = UPCR_PSHARED_TO_SHARED(_bupc_spillld12);
#line 421 "dispatch_client.c"
    _bupc_call5 = thor_upc_cast(_bupc_Mstopcvt13);
#line 421 "dispatch_client.c"
    _bupc__casttmp17 = _bupc_call5;
#line 421 "dispatch_client.c"
    (*((struct _bupc_anonymous11_1380763888l **) UPCR_TLD_ADDR( my_domain_queues ))) = _bupc__casttmp17;
    _bupc_Mspillstruct18 = thor_startup_dir;
#line 422 "dispatch_client.c"
    _bupc_Mptra14 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct18, 16ULL, (_INT64)(_UINT64)(gidx));
#line 422 "dispatch_client.c"
    UPCR_GET_PSHARED(&_bupc_spillld15, _bupc_Mptra14, 0, 16);
#line 422 "dispatch_client.c"
    _bupc_Mstopcvt16 = UPCR_PSHARED_TO_SHARED(_bupc_spillld15);
#line 422 "dispatch_client.c"
    _bupc_call6 = thor_upc_cast(_bupc_Mstopcvt16);
#line 422 "dispatch_client.c"
    _bupc__casttmp19 = _bupc_call6;
#line 422 "dispatch_client.c"
    (*((struct _bupc_anonymous23_650264093l **) UPCR_TLD_ADDR( my_domain_servers ))) = _bupc__casttmp19;
  }
#line 426 "dispatch_client.c"
  thr_per_server = ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).thr_per_domain / ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).srv_per_domain;
#line 429 "dispatch_client.c"
  server_parent = 1;
#line 431 "dispatch_client.c"
  if(server_parent != 0)
#line 431 "dispatch_client.c"
  {
#line 437 "dispatch_client.c"
    UPN = (((int) upcr_threads () ) + ((*((int *) UPCR_TLD_ADDR( thor_domain_count ))) * (*((int *) UPCR_TLD_ADDR( thor_servers_per_domain ))))) / (*((int *) UPCR_TLD_ADDR( thor_physical_nodes )));
#line 438 "dispatch_client.c"
    node_rel = (gidx % UPN) - (((int) upcr_threads () ) / (*((int *) UPCR_TLD_ADDR( thor_physical_nodes ))));
#line 440 "dispatch_client.c"
    my_numa = node_rel / (*((int *) UPCR_TLD_ADDR( thor_servers_per_domain )));
#line 441 "dispatch_client.c"
    cpu_id = ((my_numa * (*((int *) UPCR_TLD_ADDR( thor_cores_per_numa_domain )))) + (node_rel % (*((int *) UPCR_TLD_ADDR( thor_servers_per_domain ))))) + 1;
#line 444 "dispatch_client.c"
    do
#line 444 "dispatch_client.c"
    {
#line 444 "dispatch_client.c"
      memset(&cpu_affinity, (int) 0, 512ULL);
#line 444 "dispatch_client.c"
      _1 :;
    }
#line 444 "dispatch_client.c"
    while(0);
#line 445 "dispatch_client.c"
    servers_per_node = (servers_per_domain * ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).domain_count) / ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).node_count;
#line 446 "dispatch_client.c"
    __cpu = (_UINT64)(cpu_id);
#line 446 "dispatch_client.c"
    if(__cpu <= 4095ULL)
#line 446 "dispatch_client.c"
    {
#line 446 "dispatch_client.c"
      _bupc___save_expr20 = __cpu / 64ULL;
#line 446 "dispatch_client.c"
      * ((unsigned long *)(&cpu_affinity) + _bupc___save_expr20) = *((unsigned long *)(&cpu_affinity) + _bupc___save_expr20) | _U8SHL(1ULL, __cpu & 63ULL);
#line 446 "dispatch_client.c"
      _bupc_mcselect7 = *((unsigned long *)(&cpu_affinity) + _bupc___save_expr20);
    }
    else
#line 446 "dispatch_client.c"
    {
#line 446 "dispatch_client.c"
      _bupc_mcselect7 = 0ULL;
    }
    _bupc_mcselect7;
#line 449 "dispatch_client.c"
    server_id = sidx;
#line 450 "dispatch_client.c"
    scontrol = (struct _bupc_anonymous18_1895944952l *)((struct _bupc_anonymous23_650264093l *)((_UINT8 *)((*((struct _bupc_anonymous23_650264093l **) UPCR_TLD_ADDR( my_domain_servers )))) + (server_id * 3432)));
    (scontrol) -> affinity_mask = cpu_affinity;
#line 452 "dispatch_client.c"
    (scontrol) -> start_queue = server_id * queues_per_server;
#line 453 "dispatch_client.c"
    (scontrol) -> server_id = server_id;
#line 454 "dispatch_client.c"
    (scontrol) -> end_queue = (server_id + 1) * queues_per_server;
#line 455 "dispatch_client.c"
    (scontrol) -> clients_active = 0;
#line 457 "dispatch_client.c"
    if((scontrol) -> end_queue == 8)
#line 457 "dispatch_client.c"
    {
#line 458 "dispatch_client.c"
      (scontrol) -> end_queue = 11;
    }
#line 459 "dispatch_client.c"
    (scontrol) -> command = 0U;
#line 460 "dispatch_client.c"
    (scontrol) -> server_dispatch_policy = server_dispatch_policy;
#line 462 "dispatch_client.c"
    i = (scontrol) -> start_queue;
#line 462 "dispatch_client.c"
    while(i < (scontrol) -> end_queue)
#line 462 "dispatch_client.c"
    {
#line 463 "dispatch_client.c"
      _bupc_arr_base21 = (struct _bupc_anonymous8_2005322018l(*)[11LL])((_UINT8 *)((*((struct _bupc_anonymous11_1380763888l **) UPCR_TLD_ADDR( my_domain_queues )))));
#line 463 "dispatch_client.c"
      ((*_bupc_arr_base21)[i]).my_server_id = server_id;
#line 464 "dispatch_client.c"
      _2 :;
#line 464 "dispatch_client.c"
      i = i + 1;
    }
#line 466 "dispatch_client.c"
    i = 0;
#line 466 "dispatch_client.c"
    while(i < ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).thr_per_domain)
#line 466 "dispatch_client.c"
    {
      _bupc_Mspillstruct22 = (struct _bupc_anonymous17_1720742066l(*)[64LL])((char *)(scontrol) + 32ULL);
#line 470 "dispatch_client.c"
      sem_init((sem_t *)(struct _bupc_anonymous17_1720742066l(*)[64LL])((_UINT8 *)((struct _bupc_anonymous17_1720742066l(*)[64LL])((_UINT8 *)(_bupc_Mspillstruct22) + (i * 40))) + 8ULL), (int) 1, (unsigned int) 0U);
#line 472 "dispatch_client.c"
      _bupc_arr_base23 = (struct _bupc_anonymous17_1720742066l(*)[64LL])((_UINT8 *)(scontrol) + 32ULL);
#line 472 "dispatch_client.c"
      ((*_bupc_arr_base23)[i]).req_id = 0;
#line 472 "dispatch_client.c"
      _bupc_arr_base24 = (struct _bupc_anonymous17_1720742066l(*)[64LL])((_UINT8 *)(scontrol) + 32ULL);
#line 472 "dispatch_client.c"
      ((*_bupc_arr_base24)[i]).queue_num = ((*_bupc_arr_base23)[i]).req_id;
#line 473 "dispatch_client.c"
      _3 :;
#line 473 "dispatch_client.c"
      i = i + 1;
    }
#line 475 "dispatch_client.c"
    _bupc_call8 = malloc((unsigned long) 56ULL);
#line 475 "dispatch_client.c"
    _bupc__casttmp25 = _bupc_call8;
#line 475 "dispatch_client.c"
    info = _bupc__casttmp25;
#line 477 "dispatch_client.c"
    flip = 0;
#line 479 "dispatch_client.c"
    _bupc_call9 = calloc((unsigned long) 4ULL, (unsigned long) 20ULL);
#line 479 "dispatch_client.c"
    _bupc__casttmp26 = _bupc_call9;
#line 479 "dispatch_client.c"
    msg_credits = _bupc__casttmp26;
#line 480 "dispatch_client.c"
    memcpy(msg_credits, (const void *)(needed_credits), (unsigned long) 80ULL);
#line 481 "dispatch_client.c"
    (info) -> scontrol = scontrol;
#line 482 "dispatch_client.c"
    (info) -> domain_queues = (*((struct _bupc_anonymous11_1380763888l **) UPCR_TLD_ADDR( my_domain_queues )));
#line 483 "dispatch_client.c"
    (info) -> thr_per_domain = ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).thr_per_domain;
#line 484 "dispatch_client.c"
    (info) -> domain_id = ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).domain_id;
#line 485 "dispatch_client.c"
    (info) -> msg_credits = msg_credits;
#line 486 "dispatch_client.c"
    (info) -> gidx = gidx;
#line 488 "dispatch_client.c"
    pthread_create(&msg_server, (const pthread_attr_t *) 0ULL, &server_routine, info);
  }
  UPCR_EXIT_FUNCTION();
  return;
} /* start_one_server */


#line 495 "dispatch_client.c"
extern void wakeup_one_server(
  int gidx)
#line 495 "dispatch_client.c"
{
#line 495 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  int flip;
  upcr_pshared_ptr_t _bupc_Mptra17;
  
#line 497 "dispatch_client.c"
  flip = 0;
#line 498 "dispatch_client.c"
  THOR_RESET_REMAP(flip);
#line 499 "dispatch_client.c"
  _bupc_Mptra17 = UPCR_ADD_PSHARED1(server_wake, 4ULL, gidx);
#line 499 "dispatch_client.c"
  UPCR_PUT_PSHARED_VAL_STRICT(_bupc_Mptra17, 0, 1, 4);
#line 500 "dispatch_client.c"
  THOR_RESTORE_REMAP(flip);
  UPCR_EXIT_FUNCTION();
  return;
} /* wakeup_one_server */


#line 507 "dispatch_client.c"
extern void initialize_one_domain(
  int gidx,
  int threads_per_domain,
  int max_credit)
#line 507 "dispatch_client.c"
{
#line 507 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  int flip;
  upcr_pshared_ptr_t a_domain_queue;
  struct _bupc_anonymous11_1380763888l * l_my_domain_queues;
  struct _bupc_anonymous11_1380763888l * _bupc__casttmp27;
  int j;
  volatile int(*_bupc_arr_base28)[1024LL];
  sem_t * _bupc_Mspillstruct29;
  upcr_pshared_ptr_t a_domain_server;
  struct _bupc_anonymous23_650264093l * l_my_domain_servers;
  struct _bupc_anonymous23_650264093l * _bupc__casttmp30;
  upcr_pshared_ptr_t a_sinfo;
  upcr_pshared_ptr_t _bupc_Mspillstruct31;
  upcr_pshared_ptr_t _bupc_Mspillstruct32;
  upcr_pshared_ptr_t _bupc_Mspillstruct33;
  upcr_shared_ptr_t _bupc_call18;
  void * _bupc_call19;
  upcr_shared_ptr_t _bupc_call20;
  void * _bupc_call21;
  upcr_shared_ptr_t _bupc_call22;
  upcr_pshared_ptr_t _bupc_Mstopcvt23;
  upcr_shared_ptr_t _bupc_Mstopcvt24;
  upcr_pshared_ptr_t _bupc_Mptra25;
  upcr_pshared_ptr_t _bupc_Mptra26;
  upcr_pshared_ptr_t _bupc_Mstopcvt27;
  upcr_shared_ptr_t _bupc_Mstopcvt28;
  upcr_pshared_ptr_t _bupc_Mstopcvt29;
  upcr_pshared_ptr_t _bupc_Mptra30;
  upcr_pshared_ptr_t _bupc_Mptra31;
  upcr_pshared_ptr_t _bupc_Mptra32;
  upcr_pshared_ptr_t _bupc_Mptra33;
  upcr_pshared_ptr_t _bupc_Mptra34;
  upcr_pshared_ptr_t _bupc_Mptra35;
  upcr_pshared_ptr_t _bupc_Mptra36;
  upcr_pshared_ptr_t _bupc_Mptra37;
  
#line 510 "dispatch_client.c"
  flip = 0;
#line 514 "dispatch_client.c"
  _bupc_call18 = upc_alloc((unsigned long) 3076800ULL);
#line 514 "dispatch_client.c"
  _bupc_Mstopcvt23 = UPCR_SHARED_TO_PSHARED(_bupc_call18);
#line 514 "dispatch_client.c"
  a_domain_queue = _bupc_Mstopcvt23;
#line 515 "dispatch_client.c"
  _bupc_Mstopcvt24 = UPCR_PSHARED_TO_SHARED(a_domain_queue);
#line 515 "dispatch_client.c"
  _bupc_call19 = thor_upc_cast(_bupc_Mstopcvt24);
#line 515 "dispatch_client.c"
  _bupc__casttmp27 = _bupc_call19;
#line 515 "dispatch_client.c"
  l_my_domain_queues = _bupc__casttmp27;
#line 517 "dispatch_client.c"
  memset(l_my_domain_queues, (int) 0, (unsigned long) 3076800ULL);
#line 518 "dispatch_client.c"
  (l_my_domain_queues) -> total_credit = max_credit;
#line 519 "dispatch_client.c"
  if(((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).client_dispatch_policy == 0)
#line 519 "dispatch_client.c"
  {
#line 520 "dispatch_client.c"
    j = 0;
#line 520 "dispatch_client.c"
    while(j <= 1023)
#line 520 "dispatch_client.c"
    {
#line 521 "dispatch_client.c"
      _bupc_arr_base28 = (volatile int(*)[1024LL])((_UINT8 *)(l_my_domain_queues) + 3068560ULL);
#line 521 "dispatch_client.c"
      (*_bupc_arr_base28)[j] = -1;
#line 521 "dispatch_client.c"
      _1 :;
#line 521 "dispatch_client.c"
      j = j + 1;
    }
#line 522 "dispatch_client.c"
    _bupc_Mptra25 = UPCR_ADD_PSHAREDI(a_domain_queue, 1ULL, (_INT64) 3068560ULL);
#line 522 "dispatch_client.c"
    _bupc_Mptra26 = UPCR_ADD_PSHAREDI(_bupc_Mptra25, 4ULL, (_INT64)(_UINT64)(((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).domain_id));
#line 522 "dispatch_client.c"
    UPCR_PUT_PSHARED_VAL(_bupc_Mptra26, 0, 9, 4);
#line 523 "dispatch_client.c"
    init_peer_queue(l_my_domain_queues, (int) 8);
#line 524 "dispatch_client.c"
    init_peer_queue(l_my_domain_queues, (int) 9);
#line 525 "dispatch_client.c"
    (l_my_domain_queues) -> used_count = 2;
  }
  else
#line 525 "dispatch_client.c"
  {
#line 530 "dispatch_client.c"
    j = 0;
#line 530 "dispatch_client.c"
    while(j <= 10)
#line 530 "dispatch_client.c"
    {
#line 531 "dispatch_client.c"
      init_peer_queue(l_my_domain_queues, j);
#line 531 "dispatch_client.c"
      _2 :;
#line 531 "dispatch_client.c"
      j = j + 1;
    }
  }
  _bupc_Mspillstruct29 = (sem_t *)((char *)(l_my_domain_queues) + 3076760ULL);
#line 537 "dispatch_client.c"
  sem_init(_bupc_Mspillstruct29, (int) 1, (unsigned int) 1U);
#line 541 "dispatch_client.c"
  _bupc_call20 = upc_alloc((unsigned long) 219656ULL);
#line 541 "dispatch_client.c"
  _bupc_Mstopcvt27 = UPCR_SHARED_TO_PSHARED(_bupc_call20);
#line 541 "dispatch_client.c"
  a_domain_server = _bupc_Mstopcvt27;
#line 542 "dispatch_client.c"
  _bupc_Mstopcvt28 = UPCR_PSHARED_TO_SHARED(a_domain_server);
#line 542 "dispatch_client.c"
  _bupc_call21 = thor_upc_cast(_bupc_Mstopcvt28);
#line 542 "dispatch_client.c"
  _bupc__casttmp30 = _bupc_call21;
#line 542 "dispatch_client.c"
  l_my_domain_servers = _bupc__casttmp30;
#line 543 "dispatch_client.c"
  memset(l_my_domain_servers, (int) 0, (unsigned long) 219656ULL);
#line 544 "dispatch_client.c"
  (l_my_domain_servers) -> domain_id = ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).domain_id;
#line 545 "dispatch_client.c"
  (l_my_domain_servers) -> thr_per_domain = threads_per_domain;
#line 548 "dispatch_client.c"
  _bupc_call22 = upc_alloc((unsigned long) 56ULL);
#line 548 "dispatch_client.c"
  _bupc_Mstopcvt29 = UPCR_SHARED_TO_PSHARED(_bupc_call22);
#line 548 "dispatch_client.c"
  a_sinfo = _bupc_Mstopcvt29;
#line 551 "dispatch_client.c"
  _bupc_Mptra30 = UPCR_ADD_PSHARED1(domain_server, 16ULL, gidx);
#line 551 "dispatch_client.c"
  UPCR_PUT_PSHARED(_bupc_Mptra30, 0, &a_domain_server, 16);
#line 552 "dispatch_client.c"
  _bupc_Mptra31 = UPCR_ADD_PSHARED1(server_info, 16ULL, gidx);
#line 552 "dispatch_client.c"
  UPCR_PUT_PSHARED(_bupc_Mptra31, 0, &a_sinfo, 16);
#line 553 "dispatch_client.c"
  _bupc_Mptra32 = UPCR_ADD_PSHARED1(domain_queue, 16ULL, gidx);
#line 553 "dispatch_client.c"
  UPCR_PUT_PSHARED(_bupc_Mptra32, 0, &a_domain_queue, 16);
  _bupc_Mspillstruct31 = thor_startup_dir;
#line 555 "dispatch_client.c"
  _bupc_Mptra33 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct31, 16ULL, (_INT64)(_UINT64)(gidx));
#line 555 "dispatch_client.c"
  UPCR_PUT_PSHARED(_bupc_Mptra33, 0, &a_domain_server, 16);
  _bupc_Mptra34 = UPCR_ADD_PSHAREDI(thor_startup_dir, 1ULL, 8192000);
  _bupc_Mspillstruct32 = _bupc_Mptra34;
#line 556 "dispatch_client.c"
  _bupc_Mptra35 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct32, 16ULL, (_INT64)(_UINT64)(gidx));
#line 556 "dispatch_client.c"
  UPCR_PUT_PSHARED(_bupc_Mptra35, 0, &a_sinfo, 16);
  _bupc_Mptra36 = UPCR_ADD_PSHAREDI(thor_startup_dir, 1ULL, 4096000);
  _bupc_Mspillstruct33 = _bupc_Mptra36;
#line 557 "dispatch_client.c"
  _bupc_Mptra37 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct33, 16ULL, (_INT64)(_UINT64)(gidx));
#line 557 "dispatch_client.c"
  UPCR_PUT_PSHARED(_bupc_Mptra37, 0, &a_domain_queue, 16);
#line 561 "dispatch_client.c"
  if((*((int *) UPCR_TLD_ADDR( thor_issue_immediate ))) != 0)
#line 561 "dispatch_client.c"
  {
#line 562 "dispatch_client.c"
    init_peer_queue(l_my_domain_queues, (int) 10);
  }
  UPCR_EXIT_FUNCTION();
  return;
} /* initialize_one_domain */


#line 567 "dispatch_client.c"
extern void init_server(
  int domain_count,
  int threads_per_domain,
  int servers_per_domain,
  int max_credit,
  int * needed_credits)
#line 567 "dispatch_client.c"
{
#line 567 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  register _INT32 _bupc_comma;
  register _INT32 _bupc_comma0;
  register _INT32 _bupc_comma1;
  register _INT32 _bupc_comma2;
  register _INT32 _bupc_comma3;
  register _INT32 _bupc_comma4;
  register _INT32 _bupc_comma5;
  register _UINT64 _bupc_comma6;
  register _INT32 _bupc_comma7;
  register _INT32 _bupc_comma8;
  int upn;
  char * env_client_dispatch;
  char * env_server_dispatch;
  int server_dispatch_policy;
  char * env_server_max_credit;
  int queues_per_server;
  int domain_lead;
  int node_id;
  int node_rel;
  int ndom;
  int start_dom;
  int rnd;
  cpu_set_t cpu_affinity;
  int cpu_id;
  unsigned long __cpu;
  unsigned long _bupc___save_expr34;
  int _bupc__spilleq35;
  unsigned int cpuNum;
  unsigned int nodeNum;
  int didx;
  int gidx;
  int cs;
  upcr_pshared_ptr_t _bupc_Mspillstruct36;
  upcr_pshared_ptr_t _bupc_Mspillstruct37;
  upcr_pshared_ptr_t _bupc_Mspillstruct38;
  upcr_pshared_ptr_t _bupc_Mspillstruct39;
  upcr_pshared_ptr_t _bupc_Mspillstruct40;
  upcr_pshared_ptr_t _bupc_Mspillstruct41;
  int i;
  int bypass_local;
  int bypass_puts;
  int upc_index;
  const char * machines[3LL];
  const char * _bupc_w2c_machines_7815053779325561610init[3LL] = {"Berkeley UPC", "Cray UPC", "Unknown"};
  const char * server_dispatch[5LL];
  const char * _bupc_w2c_server_dispatch_7815053779325561611init[5LL] = {"Eager/queue-by-queue", "Round Robin", "Weighted Credit", "throttle waitsync", "Coalescing"};
  const char * client_dispatch[3LL];
  const char * _bupc_w2c_client_dispatch_7815053779325561612init[3LL] = {"Destination-based", "Client-based", "Round-Robin"};
  char * _bupc_call38;
  char * _bupc_call39;
  char * _bupc_call40;
  _UINT64 _bupc_mcselect41;
  upcr_pshared_ptr_t _bupc_Mptra42;
  upcr_pshared_ptr_t _bupc_spillld43;
  upcr_pshared_ptr_t _bupc_Mptra44;
  upcr_pshared_ptr_t _bupc_Mptra45;
  upcr_pshared_ptr_t _bupc_Mptra46;
  upcr_pshared_ptr_t _bupc_Mptra47;
  upcr_pshared_ptr_t _bupc_spillld48;
  upcr_pshared_ptr_t _bupc_Mptra49;
  upcr_pshared_ptr_t _bupc_Mptra50;
  upcr_pshared_ptr_t _bupc_Mptra51;
  upcr_pshared_ptr_t _bupc_Mptra52;
  upcr_pshared_ptr_t _bupc_spillld53;
  upcr_pshared_ptr_t _bupc_Mptra54;
  
#line 571 "dispatch_client.c"
  upn = ((int) upcr_threads () ) / (*((int *) UPCR_TLD_ADDR( thor_physical_nodes )));
#line 584 "dispatch_client.c"
  printf("%d: INIT SERVER : dc= %d, tpd = %d, spd = %d\n", ((int) upcr_mythread () ), domain_count, threads_per_domain, servers_per_domain);
#line 586 "dispatch_client.c"
  _bupc_call38 = bupc_getenv("CLIENT_DISPATCH_POLICY");
#line 586 "dispatch_client.c"
  env_client_dispatch = _bupc_call38;
#line 587 "dispatch_client.c"
  if((_UINT64)(env_client_dispatch) != 0ULL)
#line 587 "dispatch_client.c"
  {
#line 588 "dispatch_client.c"
    _bupc_comma = atoi(env_client_dispatch);
#line 588 "dispatch_client.c"
    (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).client_dispatch_policy = _bupc_comma;
  }
  else
#line 588 "dispatch_client.c"
  {
#line 590 "dispatch_client.c"
    (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).client_dispatch_policy = 2;
  }
#line 591 "dispatch_client.c"
  if(((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).client_dispatch_policy == 2)
#line 591 "dispatch_client.c"
  {
#line 591 "dispatch_client.c"
    rr_index_init();
  }
#line 592 "dispatch_client.c"
  _bupc_call39 = bupc_getenv("SERVER_DISPATCH_POLICY");
#line 592 "dispatch_client.c"
  env_server_dispatch = _bupc_call39;
#line 594 "dispatch_client.c"
  if((_UINT64)(env_server_dispatch) != 0ULL)
#line 594 "dispatch_client.c"
  {
#line 595 "dispatch_client.c"
    _bupc_comma0 = atoi(env_server_dispatch);
#line 595 "dispatch_client.c"
    server_dispatch_policy = _bupc_comma0;
#line 596 "dispatch_client.c"
    if(server_dispatch_policy == 2)
#line 596 "dispatch_client.c"
    {
#line 597 "dispatch_client.c"
      _bupc_call40 = bupc_getenv("SERVER_DISPATCH_MAX_CREDIT");
#line 597 "dispatch_client.c"
      env_server_max_credit = _bupc_call40;
#line 598 "dispatch_client.c"
      if((_UINT64)(env_server_max_credit) != 0ULL)
#line 598 "dispatch_client.c"
      {
#line 599 "dispatch_client.c"
        _bupc_comma1 = atoi(env_server_max_credit);
#line 599 "dispatch_client.c"
        max_credit = _bupc_comma1;
      }
    }
  }
  else
#line 599 "dispatch_client.c"
  {
#line 603 "dispatch_client.c"
    server_dispatch_policy = 0;
  }
#line 605 "dispatch_client.c"
  _bupc_comma2 = get_node_count();
#line 605 "dispatch_client.c"
  (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).node_count = _bupc_comma2;
#line 606 "dispatch_client.c"
  (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).thr_per_domain = threads_per_domain;
#line 607 "dispatch_client.c"
  (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).domain_count = domain_count;
#line 608 "dispatch_client.c"
  (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).srv_per_domain = servers_per_domain;
#line 611 "dispatch_client.c"
  if(((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).client_dispatch_policy == 0)
#line 611 "dispatch_client.c"
  {
#line 612 "dispatch_client.c"
    _bupc_comma3 = imin((int) 8, ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).domain_count);
#line 612 "dispatch_client.c"
    queues_per_server = _bupc_comma3 / servers_per_domain;
  }
  else
#line 612 "dispatch_client.c"
  {
#line 614 "dispatch_client.c"
    _bupc_comma4 = imin((int) 8, ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).thr_per_domain);
#line 614 "dispatch_client.c"
    queues_per_server = _bupc_comma4 / servers_per_domain;
  }
#line 615 "dispatch_client.c"
  _bupc_comma5 = imax(queues_per_server, (int) 1);
#line 615 "dispatch_client.c"
  queues_per_server = _bupc_comma5;
#line 616 "dispatch_client.c"
  (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).queues_per_server = queues_per_server;
#line 618 "dispatch_client.c"
  (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).domain_id = ((int) upcr_mythread () ) / ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).thr_per_domain;
#line 619 "dispatch_client.c"
  domain_lead = ((int) upcr_mythread () ) == (((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).thr_per_domain * ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).domain_id);
#line 624 "dispatch_client.c"
  if((((int) upcr_threads () ) / (*((int *) UPCR_TLD_ADDR( thor_physical_nodes )))) != (*((int *) UPCR_TLD_ADDR( thor_domains_per_node ))))
#line 624 "dispatch_client.c"
  {
#line 625 "dispatch_client.c"
    printf("WTF : %d ? %d\n", (int)(((int) upcr_threads () ) / (*((int *) UPCR_TLD_ADDR( thor_physical_nodes )))), (*((int *) UPCR_TLD_ADDR( thor_domains_per_node ))));
  }
#line 627 "dispatch_client.c"
  if((((int) upcr_threads () ) / (*((int *) UPCR_TLD_ADDR( thor_physical_nodes )))) == (*((int *) UPCR_TLD_ADDR( thor_domains_per_node ))))
#line 627 "dispatch_client.c"
  {
    0;
  }
  else
#line 627 "dispatch_client.c"
  {
#line 627 "dispatch_client.c"
    __assert_fail("THREADS/thor_physical_nodes == thor_domains_per_node", "dispatch_client.c", (unsigned int) 627U, "init_server");
  }
#line 628 "dispatch_client.c"
  if((*((int *) UPCR_TLD_ADDR( thor_use_pthreads ))) != 0)
#line 628 "dispatch_client.c"
  {
    0;
  }
  else
#line 628 "dispatch_client.c"
  {
#line 628 "dispatch_client.c"
    __assert_fail("thor_use_pthreads", "dispatch_client.c", (unsigned int) 628U, "init_server");
  }
#line 633 "dispatch_client.c"
  node_id = ((int) upcr_mythread () ) / upn;
#line 634 "dispatch_client.c"
  node_rel = ((int) upcr_mythread () ) % upn;
#line 635 "dispatch_client.c"
  ndom = (*((int *) UPCR_TLD_ADDR( thor_domains_per_node ))) / upn;
#line 636 "dispatch_client.c"
  start_dom = (node_id * (*((int *) UPCR_TLD_ADDR( thor_domains_per_node )))) + (node_rel * ndom);
#line 637 "dispatch_client.c"
  rnd = node_rel / ((*((int *) UPCR_TLD_ADDR( thor_cores_per_node ))) / (*((int *) UPCR_TLD_ADDR( thor_cores_per_numa_domain ))));
#line 639 "dispatch_client.c"
  (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).client_id = ((int) upcr_mythread () ) % ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).thr_per_domain;
#line 640 "dispatch_client.c"
  (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).relative_domain = ((node_rel % (*((int *) UPCR_TLD_ADDR( thor_domains_per_node )))) * (*((int *) UPCR_TLD_ADDR( thor_servers_per_domain )))) * queues_per_server;
#line 641 "dispatch_client.c"
  if(((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).client_dispatch_policy == 2)
#line 641 "dispatch_client.c"
  {
#line 641 "dispatch_client.c"
    rr_index_init();
  }
#line 646 "dispatch_client.c"
  do
#line 646 "dispatch_client.c"
  {
#line 646 "dispatch_client.c"
    memset(&cpu_affinity, (int) 0, 512ULL);
#line 646 "dispatch_client.c"
    _1 :;
  }
#line 646 "dispatch_client.c"
  while(0);
#line 647 "dispatch_client.c"
  cpu_id = node_rel * (*((int *) UPCR_TLD_ADDR( thor_cores_per_numa_domain )));
#line 649 "dispatch_client.c"
  __cpu = (_UINT64)(cpu_id);
#line 649 "dispatch_client.c"
  if(__cpu <= 4095ULL)
#line 649 "dispatch_client.c"
  {
#line 649 "dispatch_client.c"
    _bupc___save_expr34 = __cpu / 64ULL;
#line 649 "dispatch_client.c"
    * ((unsigned long *)(&cpu_affinity) + _bupc___save_expr34) = *((unsigned long *)(&cpu_affinity) + _bupc___save_expr34) | _U8SHL(1ULL, __cpu & 63ULL);
#line 649 "dispatch_client.c"
    _bupc_mcselect41 = *((unsigned long *)(&cpu_affinity) + _bupc___save_expr34);
  }
  else
#line 649 "dispatch_client.c"
  {
#line 649 "dispatch_client.c"
    _bupc_mcselect41 = 0ULL;
  }
  _bupc_mcselect41;
#line 650 "dispatch_client.c"
  _bupc_comma6 = pthread_self();
#line 650 "dispatch_client.c"
  _bupc_comma7 = pthread_setaffinity_np(_bupc_comma6, (unsigned long) 512ULL, &cpu_affinity);
#line 650 "dispatch_client.c"
  _bupc__spilleq35 = _bupc_comma7;
#line 650 "dispatch_client.c"
  if(_bupc__spilleq35 != 0)
#line 650 "dispatch_client.c"
  {
#line 651 "dispatch_client.c"
    perror("pthread_setaffinity_np");
  }
#line 654 "dispatch_client.c"
  getcpu(&cpuNum, &nodeNum, (unsigned long *) 0ULL);
#line 655 "dispatch_client.c"
  _bupc_comma8 = GET_MYTH();
#line 655 "dispatch_client.c"
  printf("%d: Client @  core[%d]  %u:%u\n", _bupc_comma8, cpu_id, nodeNum, cpuNum);
#line 659 "dispatch_client.c"
  didx = start_dom;
#line 661 "dispatch_client.c"
  gidx = upn + ((node_id * (upn + ((*((int *) UPCR_TLD_ADDR( thor_domains_per_node ))) * (*((int *) UPCR_TLD_ADDR( thor_servers_per_domain )))))) + (node_rel * (*((int *) UPCR_TLD_ADDR( thor_servers_per_domain )))));
#line 663 "dispatch_client.c"
  (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).domain_id = didx;
#line 664 "dispatch_client.c"
  initialize_one_domain(gidx, threads_per_domain, max_credit);
#line 665 "dispatch_client.c"
  cs = 0;
#line 665 "dispatch_client.c"
  while(cs < (*((int *) UPCR_TLD_ADDR( thor_servers_per_domain ))))
#line 665 "dispatch_client.c"
  {
    _bupc_Mspillstruct36 = thor_startup_dir;
    _bupc_Mspillstruct37 = thor_startup_dir;
#line 666 "dispatch_client.c"
    _bupc_Mptra44 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct37, 16ULL, (_INT64)(_UINT64)((gidx + cs)));
#line 666 "dispatch_client.c"
    _bupc_Mptra42 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct36, 16ULL, (_INT64)(_UINT64)(gidx));
#line 666 "dispatch_client.c"
    UPCR_GET_PSHARED(&_bupc_spillld43, _bupc_Mptra42, 0, 16);
#line 666 "dispatch_client.c"
    UPCR_PUT_PSHARED(_bupc_Mptra44, 0, &_bupc_spillld43, 16);
    _bupc_Mptra45 = UPCR_ADD_PSHAREDI(thor_startup_dir, 1ULL, 8192000);
    _bupc_Mspillstruct38 = _bupc_Mptra45;
    _bupc_Mptra46 = UPCR_ADD_PSHAREDI(thor_startup_dir, 1ULL, 8192000);
    _bupc_Mspillstruct39 = _bupc_Mptra46;
#line 667 "dispatch_client.c"
    _bupc_Mptra49 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct39, 16ULL, (_INT64)(_UINT64)((gidx + cs)));
#line 667 "dispatch_client.c"
    _bupc_Mptra47 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct38, 16ULL, (_INT64)(_UINT64)(gidx));
#line 667 "dispatch_client.c"
    UPCR_GET_PSHARED(&_bupc_spillld48, _bupc_Mptra47, 0, 16);
#line 667 "dispatch_client.c"
    UPCR_PUT_PSHARED(_bupc_Mptra49, 0, &_bupc_spillld48, 16);
    _bupc_Mptra50 = UPCR_ADD_PSHAREDI(thor_startup_dir, 1ULL, 4096000);
    _bupc_Mspillstruct40 = _bupc_Mptra50;
    _bupc_Mptra51 = UPCR_ADD_PSHAREDI(thor_startup_dir, 1ULL, 4096000);
    _bupc_Mspillstruct41 = _bupc_Mptra51;
#line 668 "dispatch_client.c"
    _bupc_Mptra54 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct41, 16ULL, (_INT64)(_UINT64)((gidx + cs)));
#line 668 "dispatch_client.c"
    _bupc_Mptra52 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct40, 16ULL, (_INT64)(_UINT64)(gidx));
#line 668 "dispatch_client.c"
    UPCR_GET_PSHARED(&_bupc_spillld53, _bupc_Mptra52, 0, 16);
#line 668 "dispatch_client.c"
    UPCR_PUT_PSHARED(_bupc_Mptra54, 0, &_bupc_spillld53, 16);
#line 669 "dispatch_client.c"
    _2 :;
#line 669 "dispatch_client.c"
    cs = cs + 1;
  }
#line 670 "dispatch_client.c"
  upcr_barrier(-1589236982, 1);
#line 672 "dispatch_client.c"
  (*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info ))).domain_id = didx;
#line 673 "dispatch_client.c"
  cs = 0;
#line 673 "dispatch_client.c"
  while(cs < (*((int *) UPCR_TLD_ADDR( thor_servers_per_domain ))))
#line 673 "dispatch_client.c"
  {
#line 674 "dispatch_client.c"
    start_one_server((int)(gidx + cs), cs, servers_per_domain, queues_per_server, server_dispatch_policy, needed_credits, (int)((*((int *) UPCR_TLD_ADDR( thor_enabled ))) == 0));
#line 675 "dispatch_client.c"
    _3 :;
#line 675 "dispatch_client.c"
    cs = cs + 1;
  }
#line 681 "dispatch_client.c"
  upcr_barrier(-1589236981, 1);
#line 684 "dispatch_client.c"
  if((*((int *) UPCR_TLD_ADDR( thor_issue_immediate ))) != 0)
#line 684 "dispatch_client.c"
  {
#line 685 "dispatch_client.c"
    i = 0;
#line 685 "dispatch_client.c"
    while(i < (*((int *) UPCR_TLD_ADDR( thor_servers_per_domain ))))
#line 685 "dispatch_client.c"
    {
#line 686 "dispatch_client.c"
      ((*((int(*)[11LL]) UPCR_TLD_ADDR( my_rr_queue_seq ))))[i] = i;
#line 686 "dispatch_client.c"
      _4 :;
#line 686 "dispatch_client.c"
      i = i + 1;
    }
#line 687 "dispatch_client.c"
    ((*((int(*)[11LL]) UPCR_TLD_ADDR( my_rr_queue_seq ))))[i] = 10;
#line 688 "dispatch_client.c"
    (*((int *) UPCR_TLD_ADDR( rrQueueNum ))) = (*((int *) UPCR_TLD_ADDR( thor_servers_per_domain ))) + 1;
  }
  else
#line 688 "dispatch_client.c"
  {
#line 690 "dispatch_client.c"
    (*((int *) UPCR_TLD_ADDR( rrQueueNum ))) = (*((int *) UPCR_TLD_ADDR( thor_servers_per_domain )));
#line 691 "dispatch_client.c"
    i = 0;
#line 691 "dispatch_client.c"
    while(i < (*((int *) UPCR_TLD_ADDR( thor_servers_per_domain ))))
#line 691 "dispatch_client.c"
    {
#line 692 "dispatch_client.c"
      ((*((int(*)[11LL]) UPCR_TLD_ADDR( my_rr_queue_seq ))))[i] = i;
#line 692 "dispatch_client.c"
      _5 :;
#line 692 "dispatch_client.c"
      i = i + 1;
    }
  }
#line 695 "dispatch_client.c"
  if(((int) upcr_mythread () ) == -1)
#line 695 "dispatch_client.c"
  {
#line 696 "dispatch_client.c"
    bypass_local = 0;
#line 696 "dispatch_client.c"
    bypass_puts = 0;
#line 696 "dispatch_client.c"
    upc_index = 0;
#line 697 "dispatch_client.c"
    __MSTORE(_bupc_w2c_machines_7815053779325561610init, 0LL, machines, 0LL, 24U);
#line 699 "dispatch_client.c"
    bypass_local = 1;
#line 707 "dispatch_client.c"
    printf("Starting Communication Servers: Domains: %d, Nodes:%d\n", ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).domain_count, ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).node_count);
#line 709 "dispatch_client.c"
    printf("\tClients per domain %d, servers per domain: %d\n", ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).thr_per_domain, ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).srv_per_domain);
#line 711 "dispatch_client.c"
    printf("\tCompiler: %s\n\tBypassing local messages: %d\n\tBypassing put messages: %d\n", (machines)[upc_index], bypass_local, bypass_puts);
#line 712 "dispatch_client.c"
    __MSTORE(_bupc_w2c_server_dispatch_7815053779325561611init, 0LL, server_dispatch, 0LL, 40U);
#line 713 "dispatch_client.c"
    __MSTORE(_bupc_w2c_client_dispatch_7815053779325561612init, 0LL, client_dispatch, 0LL, 24U);
#line 720 "dispatch_client.c"
    printf("\tServer dispatch policy: %s (total credits: %d)\n\tqueues management: %s\n", (server_dispatch)[server_dispatch_policy], max_credit, (client_dispatch)[((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).client_dispatch_policy]);
#line 721 "dispatch_client.c"
    printf("\tQueue size: %d, Queue Count per domain: %d\n", (int) 2048, (int) 8);
  }
#line 723 "dispatch_client.c"
  upcr_barrier(-1589236980, 1);
  UPCR_EXIT_FUNCTION();
  return;
} /* init_server */


#line 729 "dispatch_client.c"
extern void thor_default_server_init()
#line 729 "dispatch_client.c"
{
#line 729 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  register _INT32 _bupc_comma;
  register _INT32 _bupc_comma0;
  char * env_domain_count;
  int domain_count;
  char * env_srv_per_domain;
  int threads_per_domain;
  int srv_per_domain;
  int msg_credits[20LL];
  int _bupc_w2c_msg_credits_7815053779325561613init[20LL] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
  int total_credits;
  char * _bupc_call55;
  char * _bupc_call56;
  
#line 730 "dispatch_client.c"
  _bupc_call55 = bupc_getenv("UPC_DOMAIN_COUNT");
#line 730 "dispatch_client.c"
  env_domain_count = _bupc_call55;
#line 733 "dispatch_client.c"
  if((*((int *) UPCR_TLD_ADDR( thor_use_pthreads ))) == 0)
#line 733 "dispatch_client.c"
  {
#line 733 "dispatch_client.c"
    printf("THOR pthreads disabled, can\'t initialize ..\n");
#line 733 "dispatch_client.c"
    upc_global_exit((int) 1);
  }
#line 734 "dispatch_client.c"
  _bupc_comma = atoi(env_domain_count);
#line 734 "dispatch_client.c"
  domain_count = _bupc_comma;
#line 736 "dispatch_client.c"
  _bupc_call56 = bupc_getenv("UPC_SERVERS_PER_DOMAIN");
#line 736 "dispatch_client.c"
  env_srv_per_domain = _bupc_call56;
#line 737 "dispatch_client.c"
  threads_per_domain = ((int) upcr_threads () ) / domain_count;
#line 738 "dispatch_client.c"
  if((*((int *) UPCR_TLD_ADDR( thor_enabled ))) != 0)
#line 738 "dispatch_client.c"
  {
#line 739 "dispatch_client.c"
    threads_per_domain = (((int) upcr_threads () ) / (*((int *) UPCR_TLD_ADDR( thor_physical_nodes )))) / (*((int *) UPCR_TLD_ADDR( thor_domains_per_node )));
  }
#line 742 "dispatch_client.c"
  if((_UINT64)(env_srv_per_domain) != 0ULL)
#line 742 "dispatch_client.c"
  {
#line 743 "dispatch_client.c"
    _bupc_comma0 = atoi(env_srv_per_domain);
#line 743 "dispatch_client.c"
    srv_per_domain = _bupc_comma0;
  }
  else
#line 743 "dispatch_client.c"
  {
#line 745 "dispatch_client.c"
    srv_per_domain = 1;
  }
#line 748 "dispatch_client.c"
  __MSTORE(_bupc_w2c_msg_credits_7815053779325561613init, 0LL, msg_credits, 0LL, 80U);
#line 749 "dispatch_client.c"
  total_credits = 2048;
#line 751 "dispatch_client.c"
  if(((int) upcr_threads () ) != (*((int *) UPCR_TLD_ADDR( thor_domain_count ))))
#line 751 "dispatch_client.c"
  {
#line 752 "dispatch_client.c"
    upcri_err("Initialization mismatch: total_procs (%d) != domains (%d)...\n Check the -np value and the THOR ENV variables.\n", ((int) upcr_threads () ), (*((int *) UPCR_TLD_ADDR( thor_domain_count ))));
  }
#line 754 "dispatch_client.c"
  init_server(domain_count, threads_per_domain, srv_per_domain, total_credits, msg_credits);
  UPCR_EXIT_FUNCTION();
  return;
} /* thor_default_server_init */


#line 758 "dispatch_client.c"
extern void release_server()
#line 758 "dispatch_client.c"
{
#line 758 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  int thr_per_server;
  int server_parent;
  int server_id;
  struct _bupc_anonymous18_1895944952l * scontrol;
  pthread_barrier_t * _bupc_Mspillstruct42;
  int domain_lead;
  
#line 759 "dispatch_client.c"
  thr_per_server = ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).thr_per_domain / ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).srv_per_domain;
#line 760 "dispatch_client.c"
  server_parent = (((int) upcr_mythread () ) % thr_per_server) == 0;
#line 761 "dispatch_client.c"
  if(server_parent != 0)
#line 761 "dispatch_client.c"
  {
#line 762 "dispatch_client.c"
    server_id = ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).client_id / thr_per_server;
#line 763 "dispatch_client.c"
    scontrol = (struct _bupc_anonymous18_1895944952l *)((struct _bupc_anonymous23_650264093l *)((_UINT8 *)((*((struct _bupc_anonymous23_650264093l **) UPCR_TLD_ADDR( my_domain_servers )))) + (server_id * 3432)));
#line 764 "dispatch_client.c"
    (scontrol) -> command = 1U;
    _bupc_Mspillstruct42 = (pthread_barrier_t *)((char *)(scontrol) + 2880ULL);
#line 765 "dispatch_client.c"
    pthread_barrier_wait(_bupc_Mspillstruct42);
  }
#line 767 "dispatch_client.c"
  upcr_barrier(-1589236979, 1);
#line 768 "dispatch_client.c"
  domain_lead = ((int) upcr_mythread () ) == (((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).thr_per_domain * ((*((struct _bupc_anonymous26_1216583056l *) UPCR_TLD_ADDR( my_info )))).domain_id);
#line 769 "dispatch_client.c"
  if(domain_lead != 0)
#line 769 "dispatch_client.c"
  {
  }
  UPCR_EXIT_FUNCTION();
  return;
} /* release_server */


#line 780 "dispatch_client.c"
UPCRI_INLINE(is_shared) int is_shared(
  const void * p)
#line 780 "dispatch_client.c"
{
#line 780 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  register _INT64 _bupc_reg8;
  upcr_shared_ptr_t _bupc__spilleq43;
  upcr_shared_ptr_t _bupc_call57;
  _INT32 _bupc_Mptreq58;
  
#line 782 "dispatch_client.c"
  _bupc_call57 = _bupc_inverse_cast(p);
#line 782 "dispatch_client.c"
  _bupc__spilleq43 = _bupc_call57;
#line 782 "dispatch_client.c"
  _bupc_reg8 = UPCR_ISNULL_SHARED(_bupc__spilleq43);
  _bupc_Mptreq58 = !(_INT32) _bupc_reg8;
#line 782 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 782 "dispatch_client.c"
  return _bupc_Mptreq58;
} /* is_shared */


#line 794 "dispatch_client.c"
extern void sbupc_waitsync_all(
  struct _upcri_eop ** ph,
  unsigned long n)
#line 794 "dispatch_client.c"
{
#line 794 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  int i;
  
#line 796 "dispatch_client.c"
  i = 0;
#line 796 "dispatch_client.c"
  while((_UINT64)(i) < n)
#line 796 "dispatch_client.c"
  {
#line 797 "dispatch_client.c"
    sbupc_waitsync(*(ph + i));
#line 797 "dispatch_client.c"
    _1 :;
#line 797 "dispatch_client.c"
    i = i + 1;
  }
  UPCR_EXIT_FUNCTION();
  return;
} /* sbupc_waitsync_all */


#line 801 "dispatch_client.c"
extern void supc_memget(
  void * dst,
  upcr_shared_ptr_t src,
  unsigned long n)
#line 801 "dispatch_client.c"
{
#line 801 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  struct _upcri_eop * h;
  struct _upcri_eop * _bupc_call59;
  
#line 802 "dispatch_client.c"
  _bupc_call59 = bupc_memget_async(dst, src, n);
#line 802 "dispatch_client.c"
  h = _bupc_call59;
#line 803 "dispatch_client.c"
  bupc_waitsync(h);
  UPCR_EXIT_FUNCTION();
  return;
} /* supc_memget */


#line 807 "dispatch_client.c"
extern void supc_memput(
  upcr_shared_ptr_t dst,
  void * src,
  unsigned long n)
#line 807 "dispatch_client.c"
{
#line 807 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  struct _upcri_eop * h;
  struct _upcri_eop * _bupc_call60;
  
#line 808 "dispatch_client.c"
  _bupc_call60 = bupc_memput_async(dst, src, n);
#line 808 "dispatch_client.c"
  h = _bupc_call60;
#line 809 "dispatch_client.c"
  bupc_waitsync(h);
  UPCR_EXIT_FUNCTION();
  return;
} /* supc_memput */


#line 812 "dispatch_client.c"
extern int GET_MYTH()
#line 812 "dispatch_client.c"
{
#line 812 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  int flip;
  int mth;
  
#line 814 "dispatch_client.c"
  flip = 0;
#line 815 "dispatch_client.c"
  mth = ((int) upcr_mythread () );
#line 816 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 816 "dispatch_client.c"
  return mth;
} /* GET_MYTH */


#line 819 "dispatch_client.c"
extern int GET_ALLTH()
#line 819 "dispatch_client.c"
{
#line 819 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  int flip;
  int mth;
  
#line 821 "dispatch_client.c"
  flip = 0;
#line 822 "dispatch_client.c"
  mth = ((int) upcr_threads () );
#line 823 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 823 "dispatch_client.c"
  return mth;
} /* GET_ALLTH */


#line 827 "dispatch_client.c"
extern struct _bupc_anonymous18_1895944952l * get_scontrol(
  int did,
  int slot,
  int sid)
#line 827 "dispatch_client.c"
{
#line 827 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  struct _bupc_anonymous18_1895944952l * res;
  int flip;
  upcr_pshared_ptr_t _bupc_Mspillstruct44;
  upcr_pshared_ptr_t cd;
  struct _bupc_anonymous23_650264093l * _bupc__casttmp45;
  struct _bupc_anonymous23_650264093l * lcd;
  void * _bupc_call61;
  upcr_pshared_ptr_t _bupc_Mptra62;
  upcr_pshared_ptr_t _bupc_spillld63;
  upcr_shared_ptr_t _bupc_Mstopcvt64;
  
#line 828 "dispatch_client.c"
  res = (struct _bupc_anonymous18_1895944952l *)(0ULL);
#line 831 "dispatch_client.c"
  flip = 0;
  _bupc_Mspillstruct44 = thor_startup_dir;
#line 833 "dispatch_client.c"
  _bupc_Mptra62 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct44, 16ULL, (_INT64)(_UINT64)(slot));
#line 833 "dispatch_client.c"
  UPCR_GET_PSHARED(&_bupc_spillld63, _bupc_Mptra62, 0, 16);
#line 833 "dispatch_client.c"
  cd = _bupc_spillld63;
#line 834 "dispatch_client.c"
  _bupc_Mstopcvt64 = UPCR_PSHARED_TO_SHARED(cd);
#line 834 "dispatch_client.c"
  _bupc_call61 = thor_upc_cast(_bupc_Mstopcvt64);
#line 834 "dispatch_client.c"
  _bupc__casttmp45 = _bupc_call61;
#line 834 "dispatch_client.c"
  lcd = _bupc__casttmp45;
#line 835 "dispatch_client.c"
  res = (struct _bupc_anonymous18_1895944952l *)((struct _bupc_anonymous23_650264093l *)((_UINT8 *)(lcd) + (sid * 3432)));
#line 836 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 836 "dispatch_client.c"
  return res;
} /* get_scontrol */


#line 840 "dispatch_client.c"
extern struct _bupc_anonymous11_1380763888l * get_domq(
  int did,
  int slot,
  int sid)
#line 840 "dispatch_client.c"
{
#line 840 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  struct _bupc_anonymous11_1380763888l * res;
  int flip;
  upcr_pshared_ptr_t _bupc_Mspillstruct46;
  upcr_pshared_ptr_t q;
  struct _bupc_anonymous11_1380763888l * _bupc__casttmp47;
  void * _bupc_call65;
  upcr_pshared_ptr_t _bupc_Mptra66;
  upcr_pshared_ptr_t _bupc_Mptra67;
  upcr_pshared_ptr_t _bupc_spillld68;
  upcr_shared_ptr_t _bupc_Mstopcvt69;
  
#line 841 "dispatch_client.c"
  res = (struct _bupc_anonymous11_1380763888l *)(0ULL);
#line 843 "dispatch_client.c"
  flip = 0;
  _bupc_Mptra66 = UPCR_ADD_PSHAREDI(thor_startup_dir, 1ULL, 4096000);
  _bupc_Mspillstruct46 = _bupc_Mptra66;
#line 844 "dispatch_client.c"
  _bupc_Mptra67 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct46, 16ULL, (_INT64)(_UINT64)(slot));
#line 844 "dispatch_client.c"
  UPCR_GET_PSHARED(&_bupc_spillld68, _bupc_Mptra67, 0, 16);
#line 844 "dispatch_client.c"
  q = _bupc_spillld68;
#line 845 "dispatch_client.c"
  _bupc_Mstopcvt69 = UPCR_PSHARED_TO_SHARED(q);
#line 845 "dispatch_client.c"
  _bupc_call65 = thor_upc_cast(_bupc_Mstopcvt69);
#line 845 "dispatch_client.c"
  _bupc__casttmp47 = _bupc_call65;
#line 845 "dispatch_client.c"
  res = _bupc__casttmp47;
#line 848 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 848 "dispatch_client.c"
  return res;
} /* get_domq */


#line 852 "dispatch_client.c"
extern int server_fired(
  int idx)
#line 852 "dispatch_client.c"
{
#line 852 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  int res;
  upcr_pshared_ptr_t _bupc_Mptra70;
  int _bupc_spillld71;
  
#line 859 "dispatch_client.c"
  _bupc_Mptra70 = UPCR_ADD_PSHARED1(server_wake, 4ULL, idx);
#line 859 "dispatch_client.c"
  UPCR_GET_PSHARED_STRICT(&_bupc_spillld71, _bupc_Mptra70, 0, 4);
#line 859 "dispatch_client.c"
  res = _bupc_spillld71;
#line 862 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 862 "dispatch_client.c"
  return res;
} /* server_fired */


#line 865 "dispatch_client.c"
extern struct _bupc_anonymous25_692027308l * get_server_info(
  int idx)
#line 865 "dispatch_client.c"
{
#line 865 "dispatch_client.c"
  UPCR_BEGIN_FUNCTION();
  int flip;
  upcr_pshared_ptr_t _bupc_Mspillstruct48;
  upcr_shared_ptr_t glob;
  struct _bupc_anonymous25_692027308l * _bupc__casttmp49;
  struct _bupc_anonymous25_692027308l * res;
  void * _bupc_call72;
  upcr_pshared_ptr_t _bupc_Mptra73;
  upcr_pshared_ptr_t _bupc_Mptra74;
  upcr_pshared_ptr_t _bupc_spillld75;
  upcr_shared_ptr_t _bupc_Mstopcvt76;
  
#line 866 "dispatch_client.c"
  flip = 0;
  _bupc_Mptra73 = UPCR_ADD_PSHAREDI(thor_startup_dir, 1ULL, 8192000);
  _bupc_Mspillstruct48 = _bupc_Mptra73;
#line 870 "dispatch_client.c"
  _bupc_Mptra74 = UPCR_ADD_PSHAREDI(_bupc_Mspillstruct48, 16ULL, (_INT64)(_UINT64)(idx));
#line 870 "dispatch_client.c"
  UPCR_GET_PSHARED(&_bupc_spillld75, _bupc_Mptra74, 0, 16);
#line 870 "dispatch_client.c"
  _bupc_Mstopcvt76 = UPCR_PSHARED_TO_SHARED(_bupc_spillld75);
#line 870 "dispatch_client.c"
  glob = _bupc_Mstopcvt76;
#line 871 "dispatch_client.c"
  _bupc_call72 = _bupc_cast(glob);
#line 871 "dispatch_client.c"
  _bupc__casttmp49 = _bupc_call72;
#line 871 "dispatch_client.c"
  res = _bupc__casttmp49;
#line 873 "dispatch_client.c"
  UPCR_EXIT_FUNCTION();
#line 873 "dispatch_client.c"
  return res;
} /* get_server_info */

#line 1 "_SYSTEM"
/**************************************************************************/
/* upcc-generated strings for configuration consistency checks            */

GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_GASNetConfig_gen, 
 "$GASNetConfig: (/tmp/upcc--30397-1403208355/dispatch_client.trans.c) RELEASE=1.22.5,SPEC=1.8,CONDUIT=ARIES(ARIES-0.3/ARIES-0.3),THREADMODEL=PAR,SEGMENT=FAST,PTR=64bit,noalign,pshm,debug,trace,stats,debugmalloc,srclines,timers_native,membars_native,atomics_native,atomic32_native,atomic64_native $");
GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_UPCRConfig_gen,
 "$UPCRConfig: (/tmp/upcc--30397-1403208355/dispatch_client.trans.c) VERSION=2.19.1,PLATFORMENV=shared-distributed,SHMEM=pthreads/pshm,SHAREDPTRREP=struct/p32:t32:a64,TRANS=berkeleyupc,debug,nogasp,notv,dynamicthreads $");
GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_translatetime, 
 "$UPCTranslateTime: (dispatch_client.o) Thu Jun 19 13:05:55 2014 $");
GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_GASNetConfig_obj, 
 "$GASNetConfig: (dispatch_client.o) " GASNET_CONFIG_STRING " $");
GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_UPCRConfig_obj,
 "$UPCRConfig: (dispatch_client.o) " UPCR_CONFIG_STRING UPCRI_THREADCONFIG_STR " $");
GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_translator, 
 "$UPCTranslator: (dispatch_client.o) /usr/local/upc/nightly/translator/install/targ (aphid) $");
GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_upcver, 
 "$UPCVersion: (dispatch_client.o) 2.19.3 (UNSTABLE) $");
GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_compileline, 
 "$UPCCompileLine: (dispatch_client.o) /usr/local/upc/nightly/runtime/inst/bin/upcc.pl --at-remote-http --arch-size=64 --network=aries -g -Wu,-Wf,-Wwrite-strings --pthreads 2 --lines --trans --sizes-file=upcc-sizes dispatch_client.i $");
GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_compiletime, 
 "$UPCCompileTime: (dispatch_client.o) " __DATE__ " " __TIME__ " $");
#ifndef UPCRI_CC /* ensure backward compatilibity for http netcompile */
#define UPCRI_CC <unknown>
#endif
GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_backendcompiler, 
 "$UPCRBackendCompiler: (dispatch_client.o) " _STRINGIFY(UPCRI_CC) " $");

#ifdef GASNETT_CONFIGURE_MISMATCH
  GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_configuremismatch, 
   "$UPCRConfigureMismatch: (dispatch_client.o) 1 $");
  GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_configuredcompiler, 
   "$UPCRConfigureCompiler: (dispatch_client.o) " GASNETT_PLATFORM_COMPILER_IDSTR " $");
  GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_buildcompiler, 
   "$UPCRBuildCompiler: (dispatch_client.o) " PLATFORM_COMPILER_IDSTR " $");
#endif

/**************************************************************************/
GASNETT_IDENT(UPCRI_IdentString_dispatch_client_o_1403208355_transver_2,
 "$UPCTranslatorVersion: (dispatch_client.o) 2.19.3 (UNSTABLE), built on Jun 18 2014 at 22:22:53, host aphid linux-x86_64/64, gcc v4.2.4 (Ubuntu 4.2.4-1ubuntu4) $");
GASNETT_IDENT(UPCRI_IdentString_INIT_dispatch_client_7815053779325561610,"$UPCRInitFn: (dispatch_client.trans.c) UPCRI_INIT_dispatch_client_7815053779325561610 $");
GASNETT_IDENT(UPCRI_IdentString_ALLOC_dispatch_client_7815053779325561610,"$UPCRAllocFn: (dispatch_client.trans.c) UPCRI_ALLOC_dispatch_client_7815053779325561610 $");
