#!/usr/bin/env csh
setenv THOR_USE_PTHREADS 1
setenv THOR_SERVERS_PER_DOMAIN 1
setenv UPC_SERVERS_PER_DOMAIN 1
setenv SUPC_NODE_COUNT 2
setenv THOR_CORES_PER_NUMA_DOMAIN 12
setenv THOR_CORES_PER_DOMAIN 12
setenv THOR_CORES_PER_NODE 24
setenv THOR_DOMAINS_PER_NODE 1
setenv THOR_DOMAIN_COUNT 2
setenv UPC_DOMAIN_COUNT 2
setenv UPC_FREEZE 0
upcrun -np 2 ./simple-test

