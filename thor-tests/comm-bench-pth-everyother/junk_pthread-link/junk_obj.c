/* --- UPCR system headers --- */ 
#include "upcr.h" 
#include "whirl2c.h"
#include "upcr_proxy.h"
/*******************************************************
 * C file translated from WHIRL Mon Jan  6 20:25:52 2014
 *******************************************************/

/* UPC Runtime specification expected: 3.6 */
#define UPCR_WANT_MAJOR 3
#define UPCR_WANT_MINOR 6
/* UPC translator version: release 2.16.2, built on Apr 29 2013 at 16:58:51, host edison08 linux-x86_64/64, gcc v4.3.4 [gcc-4_3-branch revision 152973] */
/* Included code from the initialization script */
#include</global/u2/c/ciancu/aries-inst/opt/include/upcr_preinclude/upc_types.h>
#include "upcr_geninclude/stddef.h"
#include</global/u2/c/ciancu/aries-inst/opt/include/upcr_preinclude/upc_bits.h>
#include "upcr_geninclude/stdlib.h"
#include "upcr_geninclude/inttypes.h"
#line 1 "junk.w2c.h"
/* Include builtin types and operators */

#ifndef UPCR_TRANS_EXTRA_INCL
#define UPCR_TRANS_EXTRA_INCL
extern int upcrt_gcd (int _a, int _b);
extern int _upcrt_forall_start(int _start_thread, int _step, int _lo, int _scale);
#define upcrt_forall_start(start_thread, step, lo, scale)  \
       _upcrt_forall_start(start_thread, step, lo, scale)
int32_t UPCR_TLD_DEFINE_TENTATIVE(upcrt_forall_control, 4, 4);
#define upcr_forall_control upcrt_forall_control
#ifndef UPCR_EXIT_FUNCTION
#define UPCR_EXIT_FUNCTION() ((void)0)
#endif
#if UPCR_RUNTIME_SPEC_MAJOR > 3 || (UPCR_RUNTIME_SPEC_MAJOR == 3 && UPCR_RUNTIME_SPEC_MINOR >= 8)
  #define UPCRT_STARTUP_SHALLOC(sptr, blockbytes, numblocks, mult_by_threads, elemsz, typestr) \
      { &(sptr), (blockbytes), (numblocks), (mult_by_threads), (elemsz), #sptr, (typestr) }
#else
  #define UPCRT_STARTUP_SHALLOC(sptr, blockbytes, numblocks, mult_by_threads, elemsz, typestr) \
      { &(sptr), (blockbytes), (numblocks), (mult_by_threads) }
#endif
#define UPCRT_STARTUP_PSHALLOC UPCRT_STARTUP_SHALLOC

/**** Autonb optimization ********/

extern void _upcrt_memput_nb(upcr_shared_ptr_t _dst, const void *_src, size_t _n);
#define upcrt_memput_nb(dst, src, n) \
       (upcri_srcpos(), _upcrt_memput_nb(dst, src, n))

#endif


/* Types */
/* File-level vars and routines */
extern void thor_server_event_loop();

extern int user_main();


#define UPCR_SHARED_SIZE_ 16
#define UPCR_PSHARED_SIZE_ 16
upcr_pshared_ptr_t a;

void UPCRI_ALLOC_junk_6953680316820(void) { 
UPCR_BEGIN_FUNCTION();
upcr_startup_pshalloc_t _bupc_pinfo[] = { 
UPCRT_STARTUP_PSHALLOC(a, 4, 100000000, 1, 4, "A100000000H_R1_i"), 
 };

UPCR_SET_SRCPOS("_junk_6953680316820_ALLOC",0);
upcr_startup_pshalloc(_bupc_pinfo, sizeof(_bupc_pinfo) / sizeof(upcr_startup_pshalloc_t));
}

void UPCRI_INIT_junk_6953680316820(void) { 
UPCR_BEGIN_FUNCTION();
UPCR_SET_SRCPOS("_junk_6953680316820_INIT",0);
}

#line 4 "junk.upc"
extern void thor_server_event_loop()
#line 4 "junk.upc"
{
#line 4 "junk.upc"
  UPCR_BEGIN_FUNCTION();
  
  UPCR_EXIT_FUNCTION();
  return;
} /* thor_server_event_loop */


#line 10 "junk.upc"
extern int user_main()
#line 10 "junk.upc"
{
#line 10 "junk.upc"
  UPCR_BEGIN_FUNCTION();
  
#line 11 "junk.upc"
  printf("DONE %d\n", ((int) upcr_mythread () ));
#line 12 "junk.upc"
  UPCR_EXIT_FUNCTION();
#line 12 "junk.upc"
  return 0;
} /* user_main */

#line 1 "_SYSTEM"
/**************************************************************************/
/* upcc-generated strings for configuration consistency checks            */

GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_GASNetConfig_gen, 
 "$GASNetConfig: (/scratch2/scratchdirs/ciancu/upcc-ciancu-63413-1389068748/junk.trans.c) RELEASE=1.22.2g,SPEC=1.8,CONDUIT=ARIES(ARIES-0.3/ARIES-0.3),THREADMODEL=PAR,SEGMENT=FAST,PTR=64bit,noalign,pshm,nodebug,notrace,nostats,nodebugmalloc,nosrclines,timers_native,membars_native,atomics_native,atomic32_native,atomic64_native $");
GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_UPCRConfig_gen,
 "$UPCRConfig: (/scratch2/scratchdirs/ciancu/upcc-ciancu-63413-1389068748/junk.trans.c) VERSION=2.19.1,PLATFORMENV=shared-distributed,SHMEM=pthreads/pshm,SHAREDPTRREP=struct/p32:t32:a64,TRANS=berkeleyupc,nodebug,nogasp,notv,dynamicthreads $");
GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_translatetime, 
 "$UPCTranslateTime: (junk.o) Mon Jan  6 20:25:52 2014 $");
GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_GASNetConfig_obj, 
 "$GASNetConfig: (junk.o) " GASNET_CONFIG_STRING " $");
GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_UPCRConfig_obj,
 "$UPCRConfig: (junk.o) " UPCR_CONFIG_STRING UPCRI_THREADCONFIG_STR " $");
GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_translator, 
 "$UPCTranslator: (junk.o) /usr/common/ftg/upc/2.16.2/translator/install/targ (edison08) $");
GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_upcver, 
 "$UPCVersion: (junk.o) 2.19.1 (UNSTABLE) $");
GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_compileline, 
 "$UPCCompileLine: (junk.o) /global/u2/c/ciancu/aries-inst/opt/bin/upcc.pl -network aries -pthreads junk.upc -o junk $");
GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_compiletime, 
 "$UPCCompileTime: (junk.o) " __DATE__ " " __TIME__ " $");
#ifndef UPCRI_CC /* ensure backward compatilibity for http netcompile */
#define UPCRI_CC <unknown>
#endif
GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_backendcompiler, 
 "$UPCRBackendCompiler: (junk.o) " _STRINGIFY(UPCRI_CC) " $");

#ifdef GASNETT_CONFIGURE_MISMATCH
  GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_configuremismatch, 
   "$UPCRConfigureMismatch: (junk.o) 1 $");
  GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_configuredcompiler, 
   "$UPCRConfigureCompiler: (junk.o) " GASNETT_PLATFORM_COMPILER_IDSTR " $");
  GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_buildcompiler, 
   "$UPCRBuildCompiler: (junk.o) " PLATFORM_COMPILER_IDSTR " $");
#endif

/**************************************************************************/
GASNETT_IDENT(UPCRI_IdentString_junk_o_1389068752_transver_2,
 "$UPCTranslatorVersion: (junk.o) 2.16.2, built on Apr 29 2013 at 16:58:51, host edison08 linux-x86_64/64, gcc v4.3.4 [gcc-4_3-branch revision 152973] $");
GASNETT_IDENT(UPCRI_IdentString_ALLOC_junk_6953680316820,"$UPCRAllocFn: (junk.trans.c) UPCRI_ALLOC_junk_6953680316820 $");
GASNETT_IDENT(UPCRI_IdentString_INIT_junk_6953680316820,"$UPCRInitFn: (junk.trans.c) UPCRI_INIT_junk_6953680316820 $");
