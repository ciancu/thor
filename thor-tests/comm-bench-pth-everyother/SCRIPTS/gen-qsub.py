#!/usr/bin/python -u

import sys
import os

if len(sys.argv) != 3:
    sys.exit( "Usage %s  nodes, cpn " % sys.argv[0])

nodes = int(sys.argv[1])
cpn = int(sys.argv[2])
sheap = nodes/2

print "#PBS -q regular"
print "#PBS -l nodes=%d:ppn=%d" % (nodes, cpn)
print "#PBS -l walltime=00:30:00"
print "#PBS -N costin-orig-%d-%d" % (nodes, cpn)
print "#PBS -e my_job.$PBS_JOBID.err"
print "#PBS -o my_job.$PBS_JOBID.out"
print "#PBS -V"

print "cd ~/THOR/tests/comm-bench"
print "upcrun -shared-heap %dG -np %d comm-bench-orig-ibv 1 APG  2 >> ORIG-%d-%d-carver.$PBS_JOBID" % (sheap, nodes*cpn, nodes, cpn)
print "upcrun -shared-heap %dG -np %d comm-bench-orig-ibv 1 APG  4 >> ORIG-%d-%d-carver.$PBS_JOBID" % (sheap, nodes*cpn, nodes, cpn)
print "upcrun -shared-heap %dG -np %d comm-bench-orig-ibv 1 APG  8 >> ORIG-%d-%d-carver.$PBS_JOBID" % (sheap, nodes*cpn, nodes, cpn)
print "upcrun -shared-heap %dG -np %d comm-bench-orig-ibv 1 APG  16 >> ORIG-%d-%d-carver.$PBS_JOBID" % (sheap, nodes*cpn, nodes, cpn)
print "upcrun -shared-heap %dG -np %d comm-bench-orig-ibv 1 APG  32 >> ORIG-%d-%d-carver.$PBS_JOBID" % (sheap, nodes*cpn, nodes, cpn)
print "upcrun -shared-heap %dG -np %d comm-bench-orig-ibv 1 APG  64 >> ORIG-%d-%d-carver.$PBS_JOBID" % (sheap, nodes*cpn, nodes, cpn)
print "upcrun -shared-heap %dG -np %d comm-bench-orig-ibv 1 APG  128 >> ORIG-%d-%d-carver.$PBS_JOBID" % (sheap, nodes*cpn, nodes, cpn)
