#!/usr/bin/python -u

import sys
import os

if len(sys.argv) != 6:
    sys.exit( "Usage %s  upc_per_node  servers_per_upc  nodes cores_per_node cores_per_NUMA" % sys.argv[0])
    
upn = int(sys.argv[1])
spu = int(sys.argv[2])
nodes = int(sys.argv[3])
cpn = int(sys.argv[4])
cpN = int(sys.argv[5])

#print(upn, spu, nodes, nodes*spu) 
print "setenv THOR_ENABLED 1"
print "setenv THOR_DOMAINS_PER_NODE", upn
print "setenv THOR_DOMAIN_COUNT", upn*nodes
print "setenv UPC_DOMAIN_COUNT", upn*nodes
print "setenv THOR_SERVERS_PER_DOMAIN", spu
print "setenv UPC_SERVERS_PER_DOMAIN", spu
print "setenv SUPC_NODE_COUNT", nodes
print "setenv THOR_CORES_PER_NUMA_DOMAIN", cpN
print "setenv THOR_CORES_PER_DOMAIN", cpN
print "setenv THOR_CORES_PER_NODE", cpn
