/* 
 * This file was generated at link time by the upcc compiler to handle
 * initialization busywork.   Please avert your eyes...
 */
#define UPCR_NO_SRCPOS
#include <upcr.h>
 /* startup allocation functions */
void UPCRI_ALLOC__way_micro_sync_disr_12616619006698816357(void);
void UPCRI_ALLOC_dispatch_client_7815053779325561610(void);


 /* startup initialization functions */
void UPCRI_INIT__way_micro_sync_disr_12616619006698816357(void);
void UPCRI_INIT_dispatch_client_7815053779325561610(void);


static
void perfile_allocs(void) 
{
        UPCRI_ALLOC__way_micro_sync_disr_12616619006698816357();
    UPCRI_ALLOC_dispatch_client_7815053779325561610();

}

static
void perfile_inits(void)
{
        UPCRI_INIT__way_micro_sync_disr_12616619006698816357();
    UPCRI_INIT_dispatch_client_7815053779325561610();

}

void (*UPCRL_trans_extra_procinit)(void);
void (*UPCRL_trans_extra_threadinit)(void);

static
void static_init(void *start, uintptr_t len)
{
    UPCR_BEGIN_FUNCTION();
    /* we ignore the start/len params, since we allocate all static 
     * data off the heap */

    if (UPCRL_trans_extra_procinit) {
      upcr_notify(0, UPCR_BARRIERFLAG_ANONYMOUS);
      upcr_wait(0, UPCR_BARRIERFLAG_ANONYMOUS);
      if (upcri_mypthread() == 0) (*UPCRL_trans_extra_procinit)();
      upcr_notify(0, UPCR_BARRIERFLAG_ANONYMOUS);
      upcr_wait(0, UPCR_BARRIERFLAG_ANONYMOUS);
    }

    /* Call per-file alloc/init functions */
    perfile_allocs();

    /* Do a barrier to make sure all allocations finish, before calling
     * initialization functions.  */
    /* UPCR_SET_SRCPOS() was already done in perfile_allocs() */ ;
    upcr_notify(0, UPCR_BARRIERFLAG_ANONYMOUS);
    upcr_wait(0, UPCR_BARRIERFLAG_ANONYMOUS);

    /* now set any initial values (also for TLD) */
    perfile_inits();

    if (UPCRL_trans_extra_threadinit) {
      upcr_notify(0, UPCR_BARRIERFLAG_ANONYMOUS);
      upcr_wait(0, UPCR_BARRIERFLAG_ANONYMOUS);
      (*UPCRL_trans_extra_threadinit)();
      upcr_notify(0, UPCR_BARRIERFLAG_ANONYMOUS);
      upcr_wait(0, UPCR_BARRIERFLAG_ANONYMOUS);
    }
}

extern void upcri_init_heaps(void *start, uintptr_t len);
extern void upcri_init_cache(void *start, uintptr_t len);

/* Magic linker variables, for initialization */
upcr_thread_t   UPCRL_static_thread_count       = 0;
uintptr_t       UPCRL_default_shared_size       = ((uint64_t)64) << 20;
uintptr_t       UPCRL_default_shared_offset     = ((uint64_t)0) << 20;
/* uint64_t gasnet_max_segsize = 0; */ /* not set */
int             UPCRL_progress_thread           = 0;
uintptr_t	UPCRL_default_cache_size        = 0;
int		UPCRL_attach_flags              = UPCR_ATTACH_ENV_OVERRIDE|UPCR_ATTACH_SIZE_WARN;
const char *	UPCRL_main_name;
upcr_thread_t   UPCRL_default_pthreads_per_node = 0;
int             UPCRL_segsym_pow2_opt		= 0;
void (*UPCRL_pre_spawn_init)(void)                   = NULL;
void (*UPCRL_per_pthread_init)(void)                 = NULL;
void (*UPCRL_static_init)(void *, uintptr_t)         = &static_init;
void (*UPCRL_heap_init)(void * start, uintptr_t len) = &upcri_init_heaps;
void (*UPCRL_cache_init)(void *start, uintptr_t len) = &upcri_init_cache;
void (*UPCRL_mpi_init)(int *pargc, char ***pargv);
void (*UPCRL_mpi_finalize)(void);
void (*UPCRL_profile_finalize)(void);

/* strings for configuration consistency checks */
GASNETT_IDENT(UPCRI_IdentString_link_GASNetConfig, 
 "$GASNetConfig: (<link>) " GASNET_CONFIG_STRING " $");
GASNETT_IDENT(UPCRI_IdentString_link_UPCRConfig,
 "$UPCRConfig: (<link>) " UPCR_CONFIG_STRING UPCRI_THREADCONFIG_STR " $");
GASNETT_IDENT(UPCRI_IdentString_link_upcver, 
 "$UPCVersion: (<link>) 2.19.1 (UNSTABLE) $");
GASNETT_IDENT(UPCRI_IdentString_link_compileline, 
 "$UPCCompileLine: (<link>) /global/u2/n/nchaimov/edison/upc-inst/opt/bin/upcc.pl -save-all-temps -network aries -g -uses-threads 2way_micro_sync_disr.o /global/u2/n/nchaimov/edison/upc-runtime-thor/thor_pth/dispatch_server.o /global/u2/n/nchaimov/edison/upc-runtime-thor/thor_pth/support.o /global/u2/n/nchaimov/edison/upc-runtime-thor/thor_pth/dispatch_client.o -o orig-comm-bench-aries $");
GASNETT_IDENT(UPCRI_IdentString_link_compiletime, 
 "$UPCCompileTime: (<link>) " __DATE__ " " __TIME__ " $");
GASNETT_IDENT(UPCRI_IdentString_HeapSz, 
 "$UPCRDefaultHeapSizes: UPC_SHARED_HEAP_OFFSET=0MB UPC_SHARED_HEAP_SIZE=64MB $");
GASNETT_IDENT(UPCRI_IdentString_link_UPCRConfigureArgs,
 "$UPCRConfigureArgs: '--with-translator=http://upc-translator.lbl.gov/upcc-nightly.cgi' '--enable-cross-compile' '--host=x86_64-unknown-linux-gnu' '--build=x86_64-cnl-linux-gnu' '--target=x86_64-cnl-linux-gnu' '--program-prefix=' '--disable-auto-conduit-detect' '--enable-mpi' '--enable-smp' '--disable-aligned-segments' '--enable-pshm' '--disable-pshm-posix' '--enable-pshm-xpmem' '--enable-throttle-poll' '--with-feature-list=os_cnl' '--enable-backtrace-execinfo' '--enable-aries' '--enable-gni-multi-domain' '--disable-mpi' '--disable-smp' '--enable-sptr-struct' '--prefix=/global/homes/n/nchaimov/edison/upc-inst/opt' '--with-multiconf-magic=opt' 'build_alias=x86_64-cnl-linux-gnu' 'host_alias=x86_64-unknown-linux-gnu' 'target_alias=x86_64-cnl-linux-gnu' 'CC=cc -O0 -wd10120' $");
GASNETT_IDENT(UPCRI_IdentString_link_UPCRFeatures,
 "$UPCRConfigureFeatures: berkeleyupc,upcr,gasnet,upc_collective,upc_io,upc_memcpy_async,upc_memcpy_vis,upc_ptradd,upc_thread_distance,upc_tick,upc_sem,upc_dump_shared,upc_trace_printf,upc_trace_mask,upc_local_to_shared,upc_cast,upc_all_free,pupc,upc_types,upc_castable,upc_nb,nodebug,notrace,nostats,nodebugmalloc,nogasp,nothrille,segment_fast,os_linux,cpu_x86_64,cpu_64,cc_intel,structsptr,upc_io_64,os_cnl $");
GASNETT_IDENT(UPCRI_IdentString_link_UPCRConfId,
 "$UPCRConfigureId: edison10 Mon Jul 14 12:38:09 PDT 2014 nchaimov $");
GASNETT_IDENT(UPCRI_IdentString_link_UPCRABI,
 "$UPCRBinaryInterface: 64-bit x86_64-cnl-linux-gnu $");
GASNETT_IDENT(UPCRI_IdentString_link_backendcompiler, 
 "$UPCRBackendCompiler: (<link>) " _STRINGIFY(UPCRI_CC) " $");
GASNETT_IDENT(UPCRI_IdentString_link_backendlinker, 
 "$UPCRBackendLinker: " _STRINGIFY(UPCRI_LD) " $");

#ifdef GASNETT_CONFIGURE_MISMATCH
  GASNETT_IDENT(UPCRI_IdentString__link__1406839947_configuremismatch, 
   "$UPCRConfigureMismatch: (<link>) 1 $");
  GASNETT_IDENT(UPCRI_IdentString__link__1406839947_configuredcompiler, 
   "$UPCRConfigureCompiler: (<link>) " GASNETT_PLATFORM_COMPILER_IDSTR " $");
  GASNETT_IDENT(UPCRI_IdentString__link__1406839947_buildcompiler, 
   "$UPCRBuildCompiler: (<link>) " PLATFORM_COMPILER_IDSTR " $");
#endif

/* NO Totalview debugger support */
GASNETT_IDENT(upcri_IdentString_Totalview, "$UPCRTotalview: 0 $");
GASNETT_IDENT(__TV_s2s_disabled, "This executable lacks Totalview support: recompile with 'upcc -tv'.");
/* The main() event */
extern int user_main(int, char**);
const char * UPCRL_main_name = "user_main";

int main(int argc, char **argv)
{
    bupc_init_reentrant(&argc, &argv, &user_main);
    return 0;
}
#line 1 "/scratch1/scratchdirs/nchaimov/upcc-nchaimov-22937-1406839947/upcr_trans_extra.c"
/* UPC Translator runtime helpers
 * ------------------------------
 * Rules for this interface:
 * - these helper code files must be named matching the pattern upcr_trans_extra* 
 *   and output by the translator along with *any* UPC-to-C translation,
 *   although optimization options may be used to decide which files to send
 * - must use only the upcr_* public interfaces and must *not* rely upon any 
 *   upcri_* symbols, to avoid breakage with future runtime versions
 * - any global symbols should be named upcrt_* to prevent name collisions
 * - must follow all the regular upcr coding rules (strict ANSI C, no C++ comments, etc)
 * - should not include any headers - upcr.h will be automatically included, and it 
 *   already includes most of the standard headers with appropriate autoconf magic
 * - any functions from this file used by generated code should also be declared in 
 *   upcr_trans_extra.w2c.h so they'll show up in w2c.h (or other w2c.h files that the
 *   translator appends to the translated file)
 */

/* ------ Initialization callbacks that can be enabled if necessary ------ */

/* called once-per-process, before global var setup */
static void upcrt_proc_init(void) { 

  /* upcrt_auto_nb_proc_init(); */
} 
void (*UPCRL_trans_extra_procinit)(void) = &upcrt_proc_init;

#ifdef UPCRT_VECT_INCLUDE
extern void upcrt_vect_thread_init(void);
#endif 

/* called once-per-thread, after global var setup */
static void upcrt_thread_init(void) { 

#ifdef UPCRT_VECT_INCLUDE   
  upcrt_vect_thread_init();
#endif

  /* upcrt_auto_nb_thread_init(); */
} 
void (*UPCRL_trans_extra_threadinit)(void) = &upcrt_thread_init;


/* ------ Helper functions for UPC forall optimizations -------- */

/*
 *  Compute the GCD of two integers
 *  FIXME: does this work for negative ints?
 */
int upcrt_gcd(int a, int b) {
  return (b == 0) ? a : upcrt_gcd(b, a % b);
}

/* Compute val such that (i = val) is the first iteration in the forall loop 
 * that MYTHREAD will execute.
 * start_thread is the thread that executes the first ieration (i.e., i = lower_bound)
 * step is the step of the loop.
 * scale is the multiplier for the affinity expression (i.e., for 2*i scale is 2)
 * the affinity expression is thus incremented by scale * step in each iteration
 * lo is the lower bound of the loop (the initial value of i)
 */
int _upcrt_forall_start(int start_thread, int step, int lo, int scale) {
  UPCR_BEGIN_FUNCTION();
  int threads = upcr_threads();
  int mythread = upcr_mythread();
  int gcd_val = upcrt_gcd(step, threads);
  int dist = start_thread - mythread;
  int ans;
  if (dist % gcd_val != 0) {
    /* this thread never executes the loop */
    return step > 0 ? (int)~(((unsigned int)1) << (8*sizeof(int)-1)) /*INT_MAX*/ :
                      (int)(((unsigned int)1) << (8*sizeof(int)-1)) /*INT_MIN*/;
  }
  if (step > 0) {
    for (ans = lo; ans < lo + step * threads; ans += step) {
      if (dist == 0) {
        /* fprintf(stderr, "th %d: forall_start(%d, %d, %d) = %d\n", mythread, start_thread, step, lo, ans); */
        return ans;
      }
      dist = (dist + step * scale) % threads;
    }
  } else {
    for (ans = lo; ans > lo + step * threads; ans += step) {
      if (dist == 0) {
        /* fprintf(stderr, "th %d: forall_start(%d, %d, %d) = %d\n", mythread, start_thread, step, lo, ans); */
        return ans;
      }
      dist = (dist + step * scale) % threads;
    }
  }

  abort(); /* shouldn't reach here */
  return 0;
}


