#if CRAY_UPC
#define _GNU_SOURCE
#endif

#include <upc.h>
#include "support.h"
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <utmpx.h>
#include <sched.h>
#include <errno.h>
#include <semaphore.h>
#include <time.h>
#include <assert.h>
#include <string.h>

#if CRAY_UPC
#include "bupc2cupc.h"
#define HOPPER_CRAY 1
#include <string.h>
#endif

#include <cliserv_dispatch.h>

extern int thor_enabled;
extern int thor_remap_enabled;
extern int thor_physical_nodes;
extern int thor_servers_per_domain;
extern int thor_domains_per_node;
extern int thor_domain_count;
extern int thor_cores_per_numa_domain;
extern int thor_cores_per_node;
extern int thor_cores_per_domain;
extern int thor_issue_immediate;
extern int thor_use_pthreads;
#ifdef DYNAMIC_REQ_BATCH
extern int thor_req_batch;
#endif

supc_thread_info_t my_info;

#define MAX_ENTRIES 256000
typedef struct S_I_dir {
shared domain_server_t *domain_server[MAX_ENTRIES];
shared domain_queue_t *domain_queue[MAX_ENTRIES];
shared  service_info_t *server_info[MAX_ENTRIES];
} thor_startup_dir_t;

shared thor_startup_dir_t thor_startup_dir;
int my_rr_queue_seq[QUEUE_COUNT];

// typedef shared domain_queue_t *shared sp_domain_queue_t;
/* TO DO - redefine the size to something meaninful */
#define MAXQPN 8
shared domain_queue_t *shared domain_queue[THREADS*MAXQPN];
shared domain_server_t *shared domain_server[THREADS*MAXQPN];
shared  service_info_t * shared server_info[THREADS*MAXQPN];
shared int server_wake[THREADS];
domain_queue_t * my_domain_queues;
domain_server_t *my_domain_servers;

#if defined(__BERKELEY_UPC__)
void * upcri_mypthreadinfo();
#endif

void *thor_upc_cast(shared void* sptr) {
  void *res;
  res = bupc_cast(sptr);
  return res;
}

void interconnect_poll(void) 
{
#if defined(__BERKELEY_UPC__)
// Need to build with --uses-threads to make this work
	upc_poll();
#endif
}

void * wrap_upc_cast(shared void *sptr)
{
	return bupc_cast(sptr);
}


#if (UPC_SEMAPHORE && !CRAY_UPC)
void wrap_bupc_sem_post(sbupc_sem_t * sem)
{
	bupc_sem_t * upc_sem;
    uint64_t * p_upc_sem = (uint64_t*)& upc_sem;
	*p_upc_sem = (uint64_t)sem;
	bupc_sem_post(upc_sem);
}

void wrap_bupc_sem_wait(sbupc_sem_t * sem)
{
	bupc_sem_t * upc_sem;
    uint64_t * p_upc_sem = (uint64_t*)& upc_sem;
	*p_upc_sem = (uint64_t)sem;
	bupc_sem_wait(upc_sem);
}

void upc_sem_init( sbupc_sem_t **sem) 
{
	bupc_sem_t *ptr = bupc_sem_alloc(BUPC_SEM_INTEGER);
	uint64_t bypass_cast = (uint64_t) ptr;
	*sem = (sbupc_sem_t *) bypass_cast;
}
#endif


int client_check_req_ready(mem_req_t *mem_req, int req_id)
{
  // Condition for an old request that was retired                                                                             
  // Note we are checking for wrap around effect                                                                               
  // the maximum difference in request id should be REQ_QUEUE_LENGTH*THREADS                                                   
  // if greater than  maximum this means we wrapped around.                                                                    
  if(mem_req->req_id>req_id)
    return 1;
  if((req_id - mem_req->req_id) > 0xffffff)
    return 2;
  /* Only the request and the immediately next waiter can be released */
  if (mem_req->req_state == r_completed) {
    if(mem_req->req_id == req_id)
      return 3;
    if(mem_req->ready_to_serve == req_id)
      return 4;
  }

  return 0;
}



void wait_memslot_ready(supc_thread_info_t *pinfo,server_control_t * my_server, peer_queue_t * my_peer_queue, uint32_t req_id)
{
  THOR_TRACER;
  int queue_num = my_peer_queue->id;
#if BYPASS_LOCAL_DOMAIN_MESSAGES
  if(queue_num == MYDOMAIN_REQ_QUEUE)
    return;
#endif
#if VERBOSE_OUTPUT
  printf("Enter spin for completion: queue_number: %d, req_id %d, domain %d, client %d\n",
	 queue_num, req_id, pinfo->domain_id,pinfo->client_id);
#endif
  int slot = req_id % REQ_QUEUE_LENGTH;
  mem_req_t * mem_req = my_peer_queue->mem_request + slot;
  int cid = pinfo->client_id;
  uint64_t mask = ((uint64_t)1) << cid;
  my_server->waiting_client[cid].req_id = req_id;
  my_server->waiting_client[cid].queue_num = queue_num;
  atomic_f_or((uint64_t *)&(my_server->waiting_bitmap),mask);
  while(!client_check_req_ready(mem_req, req_id));
  
  return;
  
  //printf(">>>> QUEUE # = %d\n", queue_num);	\
                                                                                                                    
#ifndef SIMPLE_RR
  if(client_check_req_ready(mem_req, req_id)) {
    return;
  }
#error "Old code path.."
  
  my_server->waiting_client[cid].req_id = req_id;
  my_server->waiting_client[cid].queue_num = queue_num;
  uint64_t mask = ((uint64_t)1) << cid;
  atomic_f_or((uint64_t *)&(my_server->waiting_bitmap),mask);
#if     CONCURRENCY_DEBUG
  atomic_fadd(&my_peer_queue->completion_tokens,1);
#endif
  
#if VERBOSE_OUTPUT
  printf("Modify Bitmap and Wakeup server: queue_number: %d, req_id %d, domain %d, client %d\n",
	 queue_num, req_id, pinfo->domain_id, pinfo->client_id);
#endif
#if UPC_SEMAPHORE
  //wrap_bupc_sem_post(my_server->pservice_sem);	\
  
  wrap_bupc_sem_wait(my_server->waiting_client[cid].pwait_sem);
#else
  //ASSERTZ(sem_post(&my_server->service_sem));	\
  
  ASSERTZ(sem_wait((sem_t *)&(my_server->waiting_client[cid].wait_sem)));
#endif
#if VERBOSE_OUTPUT
  printf("Received server notification: queue_number: %d, req_id %d, domain %d, client %d state %d\n",
	 queue_num, req_id, pinfo->domain_id,pinfo->client_id, mem_req->req_state);
#endif
#endif
}

void adjust_domain_map(domain_queue_t *domain_queue, int domain_count)
{
    THOR_TRACER;
	// based on the hits adjust which destination domains should be assigned 
	// a separate queue. This can be done using selection algorithm. 
	int i, j;
	int MaxHits[QUEUE_COUNT];
	int Dests[QUEUE_COUNT];
	if((my_info.domain_count<QUEUE_COUNT) ||
		(domain_queue->used_count < QUEUE_COUNT))
		// some queues are not used why bother
		return;
	int smallest_max = domain_queue->queue_hits[0];
	int smallest_loc = 0, current_hits;
	for(i=0;i<QUEUE_COUNT;i++) {
		int current_hits = domain_queue->queue_hits[i];
		domain_queue->queue_hits[i] = 0;
		domain_queue->queue_map[i]=-1;
		if(current_hits<smallest_max) {
			smallest_max = current_hits;
			smallest_loc = i;
		}
		MaxHits[i] = current_hits;
		Dests[i]=i;
	}
	for(;i<my_info.domain_count;i++) {
		current_hits = domain_queue->queue_hits[i];
		domain_queue->queue_hits[i] = 0;
		domain_queue->queue_map[i]=-1; 
		if(current_hits > smallest_max) { 
			MaxHits[smallest_loc] = current_hits;
			Dests[smallest_loc] = i;
			smallest_max = current_hits;
			for(j=0;j<QUEUE_COUNT;j++) {
				if(smallest_max > MaxHits[j]) {
					smallest_max = MaxHits[j];
					smallest_loc = j;
				}
			}
		}
	}
	for(i=0;i<QUEUE_COUNT;i++) {
		domain_queue->queue_map[Dests[i]] = i;
		init_peer_queue(domain_queue, i);
	}
}



int dest_find_queue(domain_queue_t *domain_queue, int s_domain)
{
    THOR_TRACER;
	// First start in loosy check for a quick action
	int queue_index = domain_queue->queue_map[s_domain];
	if(queue_index>=0)
		return queue_index;
	if(domain_queue->used_count >= QUEUE_COUNT)
		return OVERFLOW_REQ_QUEUE;

	//Now let us be more rigorous
	#if UPC_SEMAPHORE
	wrap_bupc_sem_wait(domain_queue->pqueue_sem);
	#else
	ASSERTZ(sem_wait(&domain_queue->queue_sem));
	#endif
// check if there were an update
	while(1) {
		queue_index = domain_queue->queue_map[s_domain];
		if(queue_index >= 0)
			break; // do not return before releasing the semaphore
		if(domain_queue->used_count >= QUEUE_COUNT) {
			queue_index  = OVERFLOW_REQ_QUEUE;
			break;
		}
		// first time to communicate with this destination domain
		// this should be easy just find an empty entry
		queue_index = domain_queue->head++;
		domain_queue->used_count++;
		init_peer_queue(domain_queue, queue_index);
		// We should initialize the queue before indicating it is ready
		mem_bar();
		domain_queue->queue_map[s_domain] = queue_index;
		break;
	}
	#if UPC_SEMAPHORE
	wrap_bupc_sem_post(domain_queue->pqueue_sem);
	#else
	ASSERTZ(sem_post(&domain_queue->queue_sem));
	#endif
	return queue_index;
}

inline int src_find_queue(int s_domain, int l_thread)
{
	if (s_domain == my_info.domain_id)
		return MYDOMAIN_REQ_QUEUE;
	else
		return l_thread;
}


int rrQueueIndex = 0;
int mpq;
int rrQueuePos = 0;
int rrQueueNum = 0;

inline void rr_index_init() {
  THOR_TRACER;
  rrQueueIndex = my_rr_queue_seq[0];
  rrQueuePos = 0;
  my_info.start_queue = rrQueueIndex;
  mpq = 0;
}


long get_online_procs() {
  long nprocs = -1;
  nprocs = sysconf(_SC_NPROCESSORS_ONLN);
  if (nprocs < 1)
    fprintf(stderr, "Could not determine number of CPUs configured:\n%s\n", strerror (errno));
  
  return nprocs;
}

int get_node_count()
{
  int node_count;
  char * env_node_count = bupc_getenv("SUPC_NODE_COUNT");
  if (env_node_count) {
    node_count = atoi(env_node_count);
  }
  else {
    #if CRAY_UPC
      node_count = NODES;
    #else
      node_count = THREADS/get_online_procs();
    #endif
  }
    return node_count;
}

void rr_index_init();

void start_one_server(int gidx, int sidx, int servers_per_domain, int queues_per_server, int server_dispatch_policy, int* needed_credits, int is_pthread, int client_cpu) 
{
  THOR_TRACER; 
  int i;
  char hostname[128];
  server_control_t *scontrol;

  if(thor_enabled ||  thor_use_pthreads) {
    ASSERTNZ(my_domain_queues = (domain_queue_t *)thor_upc_cast(thor_startup_dir.domain_queue[gidx]));
    ASSERTNZ(my_domain_servers = (domain_server_t *)thor_upc_cast(thor_startup_dir.domain_server[gidx]));
  }

  int thr_per_server;
  ASSERTNZ(thr_per_server= my_info.thr_per_domain / my_info.srv_per_domain);
  
  /* everybody is a server parent ...*/
  int server_parent = 1;
   
  if(server_parent) {
    pthread_t msg_server;
    int server_id;
    cpu_set_t cpu_affinity;
    // XXX Assigning server affinity
    
    int UPN = (THREADS+thor_domain_count*thor_servers_per_domain)/thor_physical_nodes;
    int node_rel = my_info.domain_id+sidx%UPN - THREADS/thor_physical_nodes;
    /* assume 1 UPC task per NUMA domain */    
    int my_numa = node_rel / thor_servers_per_domain;
    int cpu_id = my_numa * thor_cores_per_numa_domain + node_rel % (thor_servers_per_domain)+ 1;
    int server_cpu = client_cpu + sidx + 1;

    gethostname(hostname, 128);
    printf("---> [%d] server gidx %d sidx %d on core %d of hostname %s.\n", MYTHREAD, gidx, sidx, server_cpu, hostname);
   
    {
      CPU_ZERO(&cpu_affinity);
      if(my_info.node_count == 0) {
        upcri_err("THOR cannot start because the node count is zero.\n");
      }
      int servers_per_node = servers_per_domain *my_info.domain_count/ my_info.node_count;
      CPU_SET(server_cpu, &cpu_affinity);
    }

    server_id = sidx;
    scontrol = my_domain_servers->server+server_id;
    scontrol->affinity_mask = cpu_affinity;
    scontrol->start_queue = (server_id)*queues_per_server;
    scontrol->server_id = server_id;
    scontrol->end_queue = (server_id+1)*queues_per_server;
    scontrol->clients_active = 0;

    //last scontrol needs some update
    if(scontrol->end_queue == REQ_QUEUE_COUNT) // setup an overflow queue
      scontrol->end_queue = QUEUE_COUNT;
#ifdef THOR_TRACE
    printf("---> [%d] server sidx %d on cpu %d of hostname %s has QUEUES from %d to %d.\n", MYTHREAD, sidx, server_cpu, hostname, scontrol->start_queue, scontrol->end_queue);
#endif
    scontrol->command = service;
    scontrol->server_dispatch_policy = server_dispatch_policy;

    for(i=scontrol->start_queue;i<scontrol->end_queue;i++) {
#ifdef THOR_TRACE
        printf("---> [%d] setting queue %d at %p to be owned by server sidx %d at %p on cpu %d of hostname %s.\n", MYTHREAD, i, my_domain_queues->queue+i, sidx, scontrol, server_cpu, hostname);
        printf("---> [%d] address of server id is %p.\n", MYTHREAD, &(my_domain_queues->queue[i].my_server_id));
#endif
      my_domain_queues->queue[i].my_server_id = server_id;
    }

    for(i=0;i<my_info.thr_per_domain;i++) {
#if UPC_SEMAPHORE
      upc_sem_init(&scontrol->waiting_client[i].pwait_sem);
#else
      ASSERTZ(sem_init((sem_t *)&(scontrol->waiting_client[i].wait_sem),PSHM_SUPPORT,0));
#endif
      scontrol->waiting_client[i].queue_num = scontrol->waiting_client[i].req_id = 0;
      }
      
    service_info_t * info = (service_info_t*)malloc(sizeof(service_info_t));
    int * msg_credits;
    int flip = 0;
    //info = thor_startup_dir.server_info[gidx]; 
    ASSERTNZ(msg_credits = (int *) calloc(sizeof(int),MESSAGE_SIZE_LEVELS));
    memcpy(msg_credits,needed_credits,sizeof(int)*MESSAGE_SIZE_LEVELS);
    info->scontrol = scontrol;
#if ENABLE_PTHRDS
    info->upcr_parent_info = (void *) upcri_mypthreadinfo();
#endif
    info->domain_queues = my_domain_queues;
    info->thr_per_domain = my_info.thr_per_domain;
    info->domain_id = my_info.domain_id;
    info->msg_credits = msg_credits;
    info->gidx = gidx;
    /* printf("%d: STARTING +++ SERVER AT  gi=%d <--> (%d, %d) : %p, %p, %p\n", MYTHREAD, gidx, my_info.domain_id, server_id, my_domain_servers->server, scontrol, info)  ;  */ 
    pthread_create(&msg_server, NULL, server_routine, (void*)info);
  }
}



void wakeup_one_server(int gidx)
{  
#pragma upc strict
  THOR_TRACER;
  int flip =0;
  server_wake[gidx] = 1;
}


void initialize_one_domain(int gidx, int threads_per_domain, int max_credit) {
  THOR_TRACER;

  int j;
  int flip =0;
  
  shared domain_queue_t * a_domain_queue;
  
  if(offsetof(peer_queue_t, xxxpadding0) != REQ_QUEUE_LENGTH * sizeof(mem_req_t)) {
    upcri_err("*** COMPILER ERROR! mem_request array has wrong length!\n");
  }

#ifdef DYNAMIC_REQ_BATCH
  printf("In domain initialization -- THOR batch size: %d.\n", thor_req_batch);
#endif 

  ASSERTNZ(a_domain_queue = (shared domain_queue_t *) upc_alloc(2*sizeof(domain_queue_t)));
  domain_queue_t * l_my_domain_queues = (domain_queue_t *) thor_upc_cast(a_domain_queue);

  memset((void*)l_my_domain_queues,0,2*sizeof(domain_queue_t));
  l_my_domain_queues->total_credit = max_credit;

  if(my_info.client_dispatch_policy == DEST_BASED_DISPATCH) {
      for(j=0;j<MAX_DOMAIN_COUNT;j++)
          l_my_domain_queues->queue_map[j] = -1;
      a_domain_queue->queue_map[my_info.domain_id] = MYDOMAIN_REQ_QUEUE; //??
      init_peer_queue(l_my_domain_queues,OVERFLOW_REQ_QUEUE);
      init_peer_queue(l_my_domain_queues,MYDOMAIN_REQ_QUEUE);
      l_my_domain_queues->used_count = 2;
  } else {
#if	CONCURRENCY_DEBUG
      assert(my_info.thr_per_domain <= QUEUE_COUNT);
#endif
      for(j=0;j<QUEUE_COUNT;j++)
          init_peer_queue(l_my_domain_queues,j);
  }
    
#if UPC_SEMAPHORE
    upc_sem_init(&l_my_domain_queues->pqueue_sem);
#else
    ASSERTZ(sem_init(&(l_my_domain_queues->queue_sem),PSHM_SUPPORT,1));
#endif
    
    shared domain_server_t * a_domain_server;
    ASSERTNZ(a_domain_server = upc_alloc(sizeof(domain_server_t)));
    domain_server_t *l_my_domain_servers = (domain_server_t *)thor_upc_cast(a_domain_server);
    memset(l_my_domain_servers,0,sizeof(domain_server_t));
    l_my_domain_servers->domain_id = my_info.domain_id;
    l_my_domain_servers->thr_per_domain = threads_per_domain;
    
    shared  service_info_t *a_sinfo;
    ASSERTNZ(a_sinfo = upc_alloc(sizeof(service_info_t)));
    
    
    domain_server[gidx] = a_domain_server;
    server_info[gidx] = a_sinfo;
    domain_queue[gidx] = a_domain_queue;
   
    thor_startup_dir.domain_server[gidx] = a_domain_server;
    thor_startup_dir.server_info[gidx] = a_sinfo;
    thor_startup_dir.domain_queue[gidx] = a_domain_queue;
  
#ifdef THOR_TRACE
    printf("%d: INITIALIZING AT %d TPD= %d  (%p, %p, %p)\n", MYTHREAD, gidx, threads_per_domain,  l_my_domain_servers, l_my_domain_queues, thor_upc_cast(a_sinfo));
#endif
}

/* threads_per_domain == UPC threads per domain */
/* 
     For the initialization, we distinguish two cases:
    
     1. More UPC threads than domains_per_node. In this case, we select as many UPC tasks per node as there are domains
     and have each initialize a domain. For the time being, each UPC task spawns servers_per_domain pthreads to execute the server event loop.
     TO DO: If it makes sense, we could use the THOR path to spawn processes. Probably not,  since there already is enough injection concurrency.

     2. Less UPC threads than domains_per_node*servers_per_domain. This is the THOR process based case where every server is an independent process.
     
*/
void init_server(int domain_count, int threads_per_domain, int servers_per_domain, int max_credit, int *needed_credits)
{
  THOR_TRACER; 
  int s, j, i, cs, cd;
  int domain_id, ndom, start_dom, rnd;
  cpu_set_t cpuset;
  char hostname[128];
  int queues_per_server;
  server_control_t * scontrol;
  char * env_client_dispatch;
  int upn = THREADS/thor_physical_nodes;
  int node_id = MYTHREAD/upn;
  int node_rel = MYTHREAD%upn;
  int server_dispatch_policy = DEFAULT_SERVER_DISPATCH_POLICY;

  char * env_server_dispatch = bupc_getenv("SERVER_DISPATCH_POLICY");
  my_info.client_dispatch_policy = (NULL ==  (env_client_dispatch = bupc_getenv("CLIENT_DISPATCH_POLICY"))) ? DEFAULT_CLIENT_DISPATCH_POLICY \
                                    : atoi(env_client_dispatch);

  printf("%d: INIT SERVER : dc= %d, tpd = %d, spd = %d\n", MYTHREAD, domain_count, threads_per_domain, servers_per_domain);

  if(my_info.client_dispatch_policy == RR_BASED_DISPATCH) rr_index_init();

  if(env_server_dispatch) {
    server_dispatch_policy = atoi(env_server_dispatch);
    if(server_dispatch_policy == WEIGHTED_CREDIT_DISPATCH) {
        char * env_server_max_credit = bupc_getenv("SERVER_DISPATCH_MAX_CREDIT");
        if(env_server_max_credit) 
            max_credit = atoi(env_server_max_credit);			
    }
  }
  
  my_info.node_count = get_node_count();
  my_info.thr_per_domain = threads_per_domain;
  my_info.domain_count = domain_count;
  my_info.srv_per_domain = servers_per_domain;

  printf("[%d] node_count %d, thr_per_domain %d, domain_count %d, srv_per_domain %d.\n",
          MYTHREAD, my_info.node_count, my_info.thr_per_domain, my_info.domain_count, my_info.srv_per_domain);
 
  /* init queues */
  /*For destination based, need a queue for each destination */
  if(my_info.client_dispatch_policy == DEST_BASED_DISPATCH)
    queues_per_server = imin(REQ_QUEUE_COUNT,my_info.domain_count) / servers_per_domain;
  else 
    queues_per_server = imin(REQ_QUEUE_COUNT,my_info.thr_per_domain) / servers_per_domain;

  queues_per_server = imax(queues_per_server,1);

  my_info.queues_per_server = queues_per_server;
  my_info.domain_id = (MYTHREAD)/my_info.thr_per_domain;
  domain_lead = (MYTHREAD == (my_info.thr_per_domain * my_info.domain_id));

  if(queues_per_server > 1) {
    upcri_err("More than 1 queue per server. This conflicts with workaround for queue-ownership bug in client_request_completion.\n");
  }
 
  /* more servers than UPC threads */
  /* Expect UPC tasks per node == thor_domains_per_node */
  if(THREADS/thor_physical_nodes != thor_domains_per_node) {
    printf("WTF : %d ? %d\n", THREADS/thor_physical_nodes, thor_domains_per_node); 
  }
  assert(THREADS/thor_physical_nodes == thor_domains_per_node);
  assert(thor_use_pthreads);   
  
  gethostname(hostname, 128);
  printf("Thread %d of %d lives on node %d of %d (hostname %s).\n", MYTHREAD, THREADS, upcr_mynode(), upcr_nodes(), hostname);
  
  /* assume for the time being 1 UPC task per NUMA domain */
  ndom = thor_domains_per_node/upn;
  start_dom = node_id*thor_domains_per_node + node_rel*ndom;
  rnd = node_rel / (thor_cores_per_node/thor_cores_per_numa_domain);
  
  my_info.client_id = MYTHREAD % my_info.thr_per_domain;
  my_info.relative_domain = (node_rel%thor_domains_per_node)*thor_servers_per_domain*queues_per_server;
  if(my_info.client_dispatch_policy == RR_BASED_DISPATCH) rr_index_init();
  
  int numa_domains_per_node = thor_cores_per_node / thor_cores_per_numa_domain;
  int my_cpu = (MYTHREAD % numa_domains_per_node) * thor_cores_per_numa_domain;

  // XXX Assigning client affinity (should be outside server_parent condition)
  cpu_set_t cpu_affinity;
  CPU_ZERO(&cpu_affinity);
  int cpu_id = my_cpu;

  CPU_SET(cpu_id, &cpu_affinity);
  if (pthread_setaffinity_np(pthread_self(), sizeof(cpu_affinity), &cpu_affinity) !=0) {
      perror("pthread_setaffinity_np");
  }
  unsigned cpuNum, nodeNum;
  getcpu(&cpuNum, &nodeNum, 0);
  printf("---> [%d]: Client @ core[%d]  %u:%u hostname %s\n", GET_MYTH(), cpu_id,  nodeNum, cpuNum, hostname);

  /* asssume 1 domain per client */
  /*     for( cd = 0; cd < ndom; cd++) { */
  int didx = start_dom;
  int gidx = node_id*(upn+thor_domains_per_node*thor_servers_per_domain) + upn +
    node_rel*thor_servers_per_domain;
  //     printf("GIDX: MTH = %d, (%d + %d + %d + %d) = %d\n", MYTHREAD,  node_id*(upn+thor_domains_per_node*thor_servers_per_domain), upn, node_rel*ndom*thor_servers_per_domain, cd*thor_servers_per_domain, gidx); 
  my_info.domain_id = didx;
  initialize_one_domain(gidx, threads_per_domain, max_credit);

  for(cs = 0; cs < thor_servers_per_domain; cs++) {
    thor_startup_dir.domain_server[gidx+cs] =  thor_startup_dir.domain_server[gidx];
    thor_startup_dir.server_info[gidx+cs] = thor_startup_dir.server_info[gidx];
    thor_startup_dir.domain_queue[gidx+cs] = thor_startup_dir.domain_queue[gidx];
  }
  upc_barrier;
  
  my_info.domain_id = didx;
  for(cs = 0; cs < thor_servers_per_domain; cs++) {
    printf("---> [%d] Client at cpu %d hostname %s is launching server %d.\n", GET_MYTH(), my_cpu, hostname, cs);
    start_one_server(gidx+cs, cs, servers_per_domain, queues_per_server, server_dispatch_policy, needed_credits, !thor_enabled, my_cpu);
  }

  upc_barrier;
  
  rrQueueNum = thor_servers_per_domain;
  for(i=0; i < thor_servers_per_domain; i++)
      my_rr_queue_seq[i] = i;

  if(MYTHREAD==-1) {
    int bypass_local=0, bypass_puts=0, upc_index=0;
    const char *machines[3] = {"Berkeley UPC", "Cray UPC", "Unknown"};
#if	BYPASS_LOCAL_DOMAIN_MESSAGES 
    bypass_local=1;
#endif
#if BYPASS_PUTS
    bypass_puts=1;
#endif
#if CRAY_UPC
    upc_index=1;
#endif
    printf("Starting Communication Servers: Domains: %d, Nodes:%d\n",my_info.domain_count, my_info.node_count);

    printf("\tClients per domain %d, servers per domain: %d\n",my_info.thr_per_domain,my_info.srv_per_domain);
    printf("\tCompiler: %s\n\tBypassing local messages: %d\n\tBypassing put messages: %d\n", 
	   machines[upc_index], bypass_local, bypass_puts);
    const char *server_dispatch[] = {"Eager/queue-by-queue","Round Robin","Weighted Credit","throttle waitsync","Coalescing"};
    const char *client_dispatch[] = {"Destination-based","Client-based","Round-Robin"};
#if CONCURRENCY_DEBUG
    assert(server_dispatch_policy<=MAX_SERVER_DISPATCH);
    assert(my_info.client_dispatch_policy<=MAX_CLIENT_DISPATCH);
#endif
    printf("\tServer dispatch policy: %s (total credits: %d)\n\tqueues management: %s\n",
	   server_dispatch[server_dispatch_policy],max_credit, 
	   client_dispatch[my_info.client_dispatch_policy]);
    printf("\tQueue size: %d, Queue Count per domain: %d\n", REQ_QUEUE_LENGTH, REQ_QUEUE_COUNT);
  }
  upc_barrier;
}



void thor_default_server_init(void)
{
  THOR_TRACER;
  int domain_count;
  int srv_per_domain;
  int threads_per_domain;
  char * env_srv_per_domain;
  int total_credits=REQ_QUEUE_LENGTH;
  int msg_credits[MESSAGE_SIZE_LEVELS] =  
                      { 1,1,1,1, 1,1,1,1,
					    1,1,1,1, 1,1,1,1,
					    1,1,1,1 };

  if(!thor_use_pthreads) { printf("THOR pthreads disabled, can't initialize ..\n"); upc_global_exit(1);}

  srv_per_domain = (NULL == (env_srv_per_domain = bupc_getenv("UPC_SERVERS_PER_DOMAIN"))) ? 1 : atoi(env_srv_per_domain);

  domain_count=atoi(bupc_getenv("UPC_DOMAIN_COUNT"));
  threads_per_domain = THREADS/domain_count;

  if(thor_enabled) {
    threads_per_domain = (THREADS/thor_physical_nodes)/thor_domains_per_node;
  }

  if(THREADS != thor_domain_count)  {
    upcri_err("Initialization mismatch: total_procs (%d) != domains (%d)...\n Check the -np value and the THOR ENV variables.\n", THREADS, thor_domain_count);
  }

  init_server(domain_count, threads_per_domain,srv_per_domain,total_credits,msg_credits);
}

void release_server(void)
{
    THOR_TRACER; 
	int thr_per_server = my_info.thr_per_domain / my_info.srv_per_domain;
	int server_parent = (MYTHREAD % thr_per_server) == 0;
	if(server_parent) {
		int server_id = my_info.client_id / thr_per_server;
		server_control_t *	scontrol = my_domain_servers->server+server_id;
		scontrol->command = terminate;
		pthread_barrier_wait(&scontrol->bar);
	}
	upc_barrier;
	int domain_lead = (MYTHREAD == (my_info.thr_per_domain * my_info.domain_id));
	if(domain_lead) {
		// FIXME: Do cleanup
		int i;
		//upc_free(domain_queue[my_info.domain_id]);
		//upc_free(domain_server[my_info.domain_id]);
	}
}

void sbupc_waitsync(bupc_handle_t h);
bupc_handle_t sbupc_memput_async(shared void *dst, void * src, size_t n);
bupc_handle_t sbupc_memget_async(void *  dst, shared void*  src, size_t n);

void sbupc_waitsync_all (bupc_handle_t *ph, size_t n)
{
	int i;
	for(i=0;i<n;i++)
 		sbupc_waitsync(ph[i]);
}

void supc_memget(void *dst, shared void *src, size_t n)
{
	bupc_handle_t h =  bupc_memget_async(dst,src, n);
	bupc_waitsync(h);
}

void supc_memput(shared void *dst, void *src, size_t n)
{
	bupc_handle_t h = bupc_memput_async(dst,src,n);
	bupc_waitsync(h);
}

int GET_MYTH() {
  int mth;
  int flip = 0;
  mth = MYTHREAD;
 return mth;
}

int GET_ALLTH() {
  int mth;
  int flip = 0;
  mth = THREADS;
 return mth;
}


server_control_t *get_scontrol(int did, int slot,int sid) {
  server_control_t * res = 0;
  domain_server_t *lcd;
  shared  domain_server_t  *cd;
  int flip =0;

  cd = thor_startup_dir.domain_server[slot];
  lcd = thor_upc_cast((shared void*)cd);
  res = lcd->server+sid;
  return res;
}


domain_queue_t *get_domq(int did, int slot,int sid) {
  domain_queue_t * res = 0;
  shared domain_queue_t *q;
  int flip =0;
  q = thor_startup_dir.domain_queue[slot];
  res  = thor_upc_cast((shared void*)q);
  //  printf("%d : DOMAIN QUEUE %d\n", GET_MYTH(), slot);
  //upcri_print_pshared(q);
  return res;
}


int server_fired(int idx) {
  int res;
  int flip;
  static int count = 0;

{
#pragma upc strict
  res = server_wake[idx];
}

  return res;  
}

service_info_t * get_server_info(int idx) {
  int flip = 0;
  service_info_t *res;
  shared void *glob;

  glob = (shared void*)thor_startup_dir.server_info[idx];  
  res = bupc_cast(glob) ;
  
  return res;
}

