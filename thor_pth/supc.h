#ifndef __INLINE_SERVER__
#define __INLINE_SERVER__
#include <upc.h>

#warning THOR is redefining bupc_memget_async, bupc_memput_async, bupc_waitsync, and bupc_waitsync_all

#define USING_UPC_DS 1

extern void client_request_completion(bupc_handle_t);
#define bupc_memget_async sbupc_memget_async
#define bupc_memput_async sbupc_memput_async
#define bupc_waitsync sbupc_waitsync


#define bupc_waitsync_all sbupc_waitsync_all

#define upc_memget supc_memget
#define upc_memput supc_memput

#define REAL_TIMERS	1

#if REAL_TIMERS
#define bupc_tick_t unsigned long long
#define bupc_ticks_now current_time_us
#define bupc_ticks_to_us(x) x
#endif

#if CRAY_UPC
#include <bupc2cupc.h>
#endif

void default_server_init(void);
void init_server(int * params, int count, int node_count);

bupc_handle_t sbupc_memget_async(void * restrict dst, shared const void * restrict src, size_t n);
bupc_handle_t sbupc_memput_async(shared const void * restrict dst, const void *src, size_t n);
void sbupc_waitsync(bupc_handle_t h);
void sbupc_waitsync_all (bupc_handle_t *ph, size_t _n);

void supc_memget(void *, shared const void *, size_t);
void supc_memput(shared void *, const void *, size_t);

void update_server_param(int index, int value);


#endif
