#ifndef __SIMPLE_OPS
#define __SIMPLE_OPS

extern int thor_enabled;
extern int thor_remap_enabled;
extern int thor_physical_nodes;
extern int thor_servers_per_domain;
extern int thor_domains_per_node;
extern int thor_domain_count;
extern void client_request_completion(bupc_handle_t);

#define bupc_waitsync(h) \
  if(h != BUPC_COMPLETE_HANDLE)			\
    client_request_completion(h);


inline int thor_is_shared(const void *p)
{
  int flip=0;
  int res;
  THOR_RESET_REMAP(flip);
  res = (bupc_inverse_cast(((void *)p)) != NULL);
  THOR_RESTORE_REMAP(flip);
  return res;
}

inline void *thor_upc_cast(shared void* sptr) {
  void *res;
  int flip = 0;
  THOR_RESET_REMAP(flip);
  res = bupc_cast(sptr);
  THOR_RESTORE_REMAP(flip);
  return res;
}

inline bupc_handle_t sbupc_memput_async(shared void * restrict dst, const void * src, size_t n)
{
  if(!thor_is_shared(src)) {
    upc_memput(dst,src,n);
    return BUPC_COMPLETE_HANDLE;
  }
  return add_request((void *)src,(shared void *) dst, n, q_memput);
}

#endif
