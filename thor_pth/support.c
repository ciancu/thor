#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <sys/syscall.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <sched.h>
#include <sys/resource.h>
#include <asm/vsyscall.h>


void atomic_inc(int * operand) {
	__sync_fetch_and_add(operand, 1);
}

void atomic_dec(int * operand) {
	__sync_fetch_and_sub(operand, 1);
}


void ** atomic_nexti(void ** array, uint64_t * index, void * mtVal, void * newVal, int iMask) {
  int try = 0;
  uint64_t i;
  do { 
    i = __sync_add_and_fetch(index, 1);
    if (++try == iMask*8) return NULL; // fail after a bunch of tries
  } while (!__sync_bool_compare_and_swap(&array[i&iMask], mtVal, newVal));
  return &array[i&iMask];
}

int atomic_cas(void ** operand, void * oldval, void * newval) {
	return __sync_bool_compare_and_swap(operand, oldval, newval);
}

uint64_t atomic_fadd(uint64_t * operand, uint64_t incr)
{
	return __sync_fetch_and_add (operand, incr);
}


uint64_t atomic_f_or(uint64_t * operand, uint64_t mask) {
	return __sync_fetch_and_or(operand, mask);
}

uint64_t atomic_f_xor(uint64_t * operand, uint64_t mask) {
	return __sync_fetch_and_xor(operand, mask);
}


static volatile float x, y;
static volatile float z = (1.00001);

void ops_delay (int64_t n)
{
   int64_t i;
   y = z;
   x = 1.0;
   for (i=0; i<n; i++) 
    x *= y;
}

void mem_bar(void) {
	__sync_synchronize();
}


#if defined(__i386__)

unsigned long long rdtsc(void)
{
  unsigned long long int x;
     __asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
     return x;
}
#elif defined(__x86_64__)

unsigned long long rdtsc(void)
{
  unsigned hi, lo;
  __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
  return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}

#elif defined(__powerpc__)

unsigned long long rdtsc(void)
{
  unsigned long long int result=0;
  unsigned long int upper, lower,tmp;
  __asm__ volatile(
                "0:                  \n"
                "\tmftbu   %0           \n"
                "\tmftb    %1           \n"
                "\tmftbu   %2           \n"
                "\tcmpw    %2,%0        \n"
                "\tbne     0b         \n"
                : "=r"(upper),"=r"(lower),"=r"(tmp)
                );
  result = upper;
  result = result<<32;
  result = result|lower;

  return(result);
}

#else

#error "No tick counter is available!"

#endif


int32_t GCD(int32_t n, int32_t m)  
{  
    int32_t temp;  
    while( m > 0)  {  
        temp = n;  
        n = m;  
        m = temp % m;  
    }  
    return n;  
}  
  
int32_t LCM(int32_t n, int32_t m) {  
    int product = n*m;  
    int gcd = GCD(n,m);  
    int lcm = product/gcd;  
    return lcm;  
}


int32_t MSB(uint32_t a) {
	int msb=0;
	while(a) {
		a >>= 1;
		msb ++;
	}
	return msb-1;
}

int imin(int a, int b) {
	if(a<b) return a;
	else return b;
}

int imax(int a, int b){
	if(a<b) return b;
	else return a;
}
	
uint64_t max_u64(uint64_t a, uint64_t b)
{
	if(a<b) return b;
	else return a;
}

uint64_t min_u64(uint64_t a, uint64_t b)
{
	if(a<b) return a;
	else return b;
}



#define MONTONIC_TIME 1

static void get_time(struct timespec *ts)
{
#if MONTONIC_TIME
	int clock_type = CLOCK_MONOTONIC;
#else
	int clock_type = CLOCK_REALTIME;
#endif
	if (syscall(__NR_clock_gettime, clock_type, ts)) {
		printf("clock_gettime(MONOTONIC/REALTIME) failed");
		exit(-1);
	}
}


unsigned long long current_time_ns(void)
{
	struct timespec ts;
	get_time(&ts);
	return ts.tv_sec * 1000000000ULL + ts.tv_nsec;
}
unsigned long long current_time_us(void)
{
	struct timespec ts;
	get_time(&ts);
	return ts.tv_sec * 1000000ULL + ts.tv_nsec/1000;
}
unsigned current_time_sec(void)
{
	struct timespec ts;
	get_time(&ts);
	return ts.tv_sec;
}

int getcpu(unsigned *cpu, unsigned *node, unsigned long *tcache)
 {
   typedef long (*vgetcpu_t)(unsigned int *cpu, unsigned int *node, unsigned     long *tcache);
   vgetcpu_t vgetcpu = (vgetcpu_t)VSYSCALL_ADDR(__NR_vgetcpu);
  return vgetcpu(cpu, node, tcache);
}
