/* UPC shared variable %qE is declared
   both strict and relaxed. */

#include <upc.h>

shared strict relaxed int x;
