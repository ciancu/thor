// All of these cases are valid uses of incomplete types:

struct incomplete_s;
union  incomplete_u;
enum   incomplete_e;

typedef struct incomplete_s my_struct_t;
typedef union  incomplete_u my_union_t;
typedef enum   incomplete_e my_enum_t;

// arrays of pointers to incomplete types:
my_struct_t * array0[4];
my_union_t  * array1[4];
my_enum_t   * array2[4];
struct incomplete_s * array0a[4];
union  incomplete_u * array1a[4];
enum   incomplete_e * array2a[4];
