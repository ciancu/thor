// All of these cases are valid uses of incomplete types:

struct incomplete_s;
union  incomplete_u;
enum   incomplete_e;

typedef struct incomplete_s my_struct_t;
typedef union  incomplete_u my_union_t;
typedef enum   incomplete_e my_enum_t;

// Shared arrays of pointers to private incomplete types:
my_struct_t * shared array0[THREADS];
my_union_t  * shared array1[THREADS];
my_enum_t   * shared array2[THREADS];
struct incomplete_s * shared array0a[THREADS];
union  incomplete_u * shared array1a[THREADS];
enum   incomplete_e * shared array2a[THREADS];

// Shared arrays of pointers to shared incomplete types:
shared my_struct_t * shared array3[THREADS];
shared my_union_t  * shared array4[THREADS];
shared my_enum_t   * shared array5[THREADS];
shared struct incomplete_s * shared array3a[THREADS];
shared union  incomplete_u * shared array4a[THREADS];
shared enum   incomplete_e * shared array5a[THREADS];
