#!/bin/bash

DATA_DIR=nonblocking-random-compl-1024

FIRST_LINE=20
LAST_LINE=100
DATA_COLUMN=2

PATTERN="random"
SIZE="compl"
COMPLETE_RESULT=final_result_$PATTERN"_non_equal"

PRINT_SIZE=1

cd $DATA_DIR
rm $COMPLETE_RESULT
rm Result-*
#compare 12, 14, 24
for NP in 1024 #8 16 32 64 96
    do
        if [[ "$NP" == "8" ]]
        then
            FIRST_LINE=14
            LAST_LINE=26
        elif [[ "$NP" == "16" ]]
        then
            FIRST_LINE=22
            LAST_LINE=34
        elif [[ "$NP" == "32" ]]
        then
            FIRST_LINE=38
            LAST_LINE=50
        elif [[ "$NP" == "64" ]]
        then
            FIRST_LINE=70
            LAST_LINE=82
        elif [[ "$NP" == "96" ]]
        then
            FIRST_LINE=102
            LAST_LINE=114
        elif [[ "$NP" == "1024" ]]
        then
            FIRST_LINE=1030
            LAST_LINE=1042
        fi
   for message in 8 16 32 
       do  
       for first_op in put 
           do 
               for second_op in get 
                   do
                      if [[ "$first_op" != "$second_op" ]]
                      then
                       for num_first_op in 1 2 4
                           do
                               for num_second_op in 1 2 4
                                   do 
                                       for blocking in 0 #1
                                           do
                                               if [[ "$num_first_op" != "$num_second_op" ]]
                                               then
                                                   if [[ "$num_first_op" == "1" && "$num_second_op" == "2" ]]
                                                   then
                                                       rm _temp
                                                       for i in `seq $FIRST_LINE $LAST_LINE` 
                                                           do
                                                               head -$i $PATTERN-$NP-$SIZE-$message-$first_op-$num_first_op-$second_op-$num_second_op-$blocking | tail -1 | awk '{ print $1 }' >> _temp
                                                           done
                                                       mv _temp "Result"-$PATTERN-$NP-"Msg"-$message
                                                       PRINT_SIZE=0

                                                       rm _temp
                                                       for i in `seq $FIRST_LINE $LAST_LINE` 
                                                           do
                                                               head -$i $PATTERN-$NP-$SIZE-$message-$first_op-$num_first_op-$first_op-$num_first_op-$blocking | tail -1 | awk '{ print $'$DATA_COLUMN' }' >> _temp
                                                           done
                                                           paste "Result"-$PATTERN-$NP-"Msg"-$message  _temp > _combine
                                                           mv _combine "Result"-$PATTERN-$NP-"Msg"-$message

                                                       rm _temp
                                                       for i in `seq $FIRST_LINE $LAST_LINE` 
                                                           do
                                                               head -$i $PATTERN-$NP-$SIZE-$message-$second_op-$num_first_op-$second_op-$num_first_op-$blocking | tail -1 | awk '{ print $'$DATA_COLUMN' }' >> _temp
                                                           done
                                                           paste "Result"-$PATTERN-$NP-"Msg"-$message  _temp > _combine
                                                           mv _combine "Result"-$PATTERN-$NP-"Msg"-$message

                                                   fi

                                                       rm _temp
                                                       for i in `seq $FIRST_LINE $LAST_LINE` 
                                                           do
                                                               head -$i $PATTERN-$NP-$SIZE-$message-$first_op-$num_first_op-$second_op-$num_second_op-$blocking | tail -1 | awk '{ print $'$DATA_COLUMN' }' >> _temp
                                                           done
                                                           paste "Result"-$PATTERN-$NP-"Msg"-$message  _temp > _combine
                                                           mv _combine "Result"-$PATTERN-$NP-"Msg"-$message

                                                       rm _temp
                                                       for i in `seq $FIRST_LINE $LAST_LINE` 
                                                           do
                                                               head -$i $PATTERN-$NP-$SIZE-$message-$second_op-$num_second_op-$first_op-$num_first_op-$blocking | tail -1 | awk '{ print $'$DATA_COLUMN' }' >> _temp
                                                           done
                                                           paste "Result"-$PATTERN-$NP-"Msg"-$message _temp > _combine
                                                           mv _combine "Result"-$PATTERN-$NP-"Msg"-$message
                                               fi

                                           done
                                   done
                           done
                           echo  "Result"-$PATTERN-$NP-"Msg"-$message >> $COMPLETE_RESULT
                           paste "Result"-$PATTERN-$NP-"Msg"-$message >> $COMPLETE_RESULT
                       fi
                   done
           done
       done
done
