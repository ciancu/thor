#!/usr/bin/python
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.collections import PolyCollection
from matplotlib.colors import colorConverter
import matplotlib.pyplot as plt
import numpy as np
import re

NP = 513
interval = 100000
total_execution_time = 160
#FILE_NAME = "NWCHEM-Trace-File/test-trace"
FILE_NAME ="/Users/Ming/Downloads/global-communication-scheduling/NWCHEM-Trace-File/512-analyzed-trace-pvdz"
File_obj = open(FILE_NAME, 'r')


#for line in File_obj:
#    if "Operations in rank:" in line:
#        NP += 1

begin_time = [0 for i in range(NP)]
current_position = [0 for i in range(NP)]

current_rank = 0
for line in File_obj:
    if "Operations in rank" in line:
        words = line.split()
        current_rank = words[3]
    if "["+str(current_rank)+"] GET_NBI " in line and begin_time[int(current_rank)] is 0:
        words = line.split()
        begin_time[int(current_rank)] = float(words[5][2:-2])
    if "["+str(current_rank)+"] PUT_NBI " in line and begin_time[int(current_rank)] is 0:
        words = line.split()
        begin_time[int(current_rank)] = float(words[5][2:-2])

File_obj.seek(0)

##For the first run, get these two timers, after that just plugin to save time
min_time = 100
max_time = 0

for line in File_obj:
    if "GET_NBI" in line or "PUT_NBI" in line:
        words = line.split()
        if re.match("^\d+?\.\d+?$", words[5][2:-2]) and float(words[5][2:-2]) > max_time and \
            float(words[5][2:-2]) < total_execution_time:
            max_time = float(words[5][2:-2])
        elif re.match("^\d+?\.\d+?$", words[5][2:-2]) and float(words[5][2:-2]) < min_time:
            min_time = float(words[5][2:-2])
File_obj.seek(0)

start_time = int(min_time * 10e5)
end_time = int(max_time * 10e5)

#start_time = 8957305 
#end_time = 1991198349

time_range = end_time - start_time
num_columns  = (end_time - start_time) / interval + 10

print "time_range: ", time_range, "num_columns: ", num_columns

NP += 1
visual_output = [[0 for i in range(num_columns)] for j in range(NP)]

for i in range(0, num_columns):
    visual_output[0][i] = i * interval
#"{0}".format(str(i).ljust(format_length))

File_obj.seek(0)

print "Start analyzing..."

column_column = 0
select_column = 0

min_size = 100
max_size = 0

try:
    for line in File_obj:
        if "Size" in line:
            words = line.split()
            size = words[4]
            rank = int(words[0][1:-1])
            if re.match("^\d+?\.\d+?$", words[5][2:-2]):
                timestampe = float(words[5][2:-2]) * 10e5
                begin_time_value = float(begin_time[int(rank)])* 10e5
                column_index = int(timestampe - begin_time_value)

#               interval is larger than one  
                select_column = int(column_index / interval)
                if rank < NP and select_column < num_columns :
                    visual_output[rank + 1][select_column + 1] += int(size)
                
#               interval is one 
#                   visual_output[rank + 1][column_index] = int(size)
    
                    if int(size) > max_size:
                        max_size = int(size)
                    elif int(size) < min_size:
                        min_size = int(size)
                    
except Exception as ex: 
    print ex
    print "Error happens", line, column_index, "select_column:", select_column

print "Minimum message size: ", min_size, "Max message size: ", max_size
print "Size before truncate: ", len(visual_output[0])

try: 
    for index in range(num_columns -1, -1, -1):
        if visual_output[1][index] == 0:
            remove = 1
            for jj in range (1, NP):
                if visual_output[jj][index] != 0:
                    remove = 0
            if remove == 1:
                for m in range(0, NP):
                    del visual_output[m][index]
except Exception as ex: 
    print ex
    print "Error happens", index

print "Size after truncate: ", len(visual_output[0])
#colors = np.random.rand(N)
#area = np.pi * (15 * np.random.rand(N))**2 # 0 to 15 point radiuses
#
#plt.scatter(x, y, s=area, c=colors, alpha=0.5)

truncated_column = len(visual_output[0])
fig = plt.figure()
ax = fig.gca(projection='3d')

cc = lambda arg: colorConverter.to_rgba(arg, alpha=0.6)

verts = []
zs = [None] * (NP - 1)
xs = [None] * truncated_column
facecolors = [None]*NP

for i in range(0, NP - 1):
    zs[i] = i
    facecolors[i] = cc('#eeefff')

for i in range(0, truncated_column):
    xs[i] = visual_output[0][i]
    
try: 
    for z in zs:
        rank = zs.index(z)
        verts.append(list(zip(xs, visual_output[rank + 1])))
except Exception as ex:
    print "error happens in: ", z
    pass

poly = PolyCollection(verts, facecolors)
poly.set_alpha(0.7)
ax.add_collection3d(poly, zs=zs, zdir='y')

ax.set_title('NWChem 512 Theads on Edison Messages on Each Thread')
ax.set_xlabel('Timestampe')
ax.set_xlim3d(0, time_range)
ax.set_ylabel('Thread Number')
ax.set_ylim3d(0, NP)
ax.set_zlabel('Message Size (Bytes)')
ax.set_zlim3d(0, 10000000)

plt.show()
