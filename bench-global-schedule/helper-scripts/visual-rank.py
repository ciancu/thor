from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.colors import colorConverter
from matplotlib.collections import PolyCollection
from matplotlib.colors import colorConverter
import matplotlib.pyplot as plt
import numpy as np


NP = 512 + 1

FILE_NAME ="/Users/Ming/Downloads/global-communication-scheduling/NWCHEM-Trace-File/512-analyzed-trace-pvqz"
File_obj = open(FILE_NAME, 'r')


num_rank = 0
for line in File_obj:
    if "Operations in rank:" in line:
        num_rank += 1

visual_output = [[0 for i in range(NP)] for j in range(NP)]

File_obj.seek(0)

try:
    for line in File_obj:
        if "Size" in line:
            words = line.split()
            start = str(words[0].lstrip('['))[:-1]
            end = str(words[2].lstrip('['))[:-1]
            if start.isdigit() and end.isdigit() and int(start) < NP and \
            int(end) < NP:
                visual_output[int(start)][int(end)] += 1
    
except Exception as ex: 
    print ex
    print line
    pass

X = [0] * NP
Y = [0] * NP
for i in range(0, NP):
    X[i] = i
    Y[i] = i


cc = lambda arg: colorConverter.to_rgba(arg, alpha=0.6)

xs = [0] * NP
zs = [0] * NP
facecolors = [0]*NP

fig = plt.figure()
ax = fig.gca(projection='3d')

for i in range(0, NP):
    xs[i] = i
    zs[i] = i
    facecolors[i] = np.random.rand()

verts = []
for z in zs:
    rank = zs.index(z)
    verts.append(list(zip(xs, visual_output[rank])))

poly = PolyCollection(verts, facecolors)
poly.set_alpha(0.7)
ax.add_collection3d(poly, zs=zs, zdir='y')

ax.set_title('NWChem 512 Theads AUG-PVQZ on Edison Communication Counts')
ax.set_xlabel('Target Thread')
ax.set_xlim3d(0, NP)
ax.set_ylabel('Origin Thread')
ax.set_ylim3d(0, NP)
ax.set_zlabel('Number of Messages')
ax.set_zlim3d(0, 10000)

plt.show()
