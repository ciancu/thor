#!/bin/bash

DATA_DIR=nonblocking-random-bimodal-1024

FIRST_LINE=20
LAST_LINE=100
DATA_COLUMN=2

PATTERN="random"
COMPLETE_RESULT=final_result_$PATTERN"_eq_op"

cd $DATA_DIR
rm $COMPLETE_RESULT
rm Result-*
#compare 11, 22, 44 pattern
for NP in 1024 #8 16 32 64 96
    do
        if [[ "$NP" == "8" ]]
        then
            FIRST_LINE=14
            LAST_LINE=15
        elif [[ "$NP" == "16" ]]
        then
            FIRST_LINE=22
            LAST_LINE=23
        elif [[ "$NP" == "32" ]]
        then
            FIRST_LINE=38
            LAST_LINE=39
        elif [[ "$NP" == "64" ]]
        then
            FIRST_LINE=70
            LAST_LINE=71
        elif [[ "$NP" == "96" ]]
        then
            FIRST_LINE=102
            LAST_LINE=103
        elif [[ "$NP" == "1024" ]]
        then
            FIRST_LINE=1030
            LAST_LINE=1031
        fi

        for j in bimodal
        do 
           for m in 8 16 32
           do
               for f in get #put  #first operation
               do
                   for s in put #get put    #second operation
                   do 
                       for f_size in 256 16384
                       do
                           for s_size in 256 16384
                           do
                               if [[ "$f" != "$s" ]]
                               then
                                   if [[ "$f_size" != "$s_size" ]]
                                   then
                       
                                   for rf in 1 2 4 #repeation in first operation
                                   do
                                       for rs in 1 2 4 #repeating in second operation
                                       do
                                          for b in 0   #blocking or nonblocking
                                          do
                                              if [[ "$rf" == "$rs" ]]
                                              then
                                                  if [[ "$rf" == "1" ]]
                                                  then 
                                                       rm _temp

                                                       for i in `seq $FIRST_LINE $LAST_LINE` 
                                                           do
                                                               head -$i $PATTERN-$NP-$j-$m-$f-$rf-$s-$rs-$f_size-$s_size-$b | tail -1 | awk '{ print $1 }' >> _temp
                                                           done
                                                       mv _temp "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b 

                                                       # the following fours numbers are getgetsmalllarge/largesmall, putputsmalllarge/largesmall
                                                       small_size=256
                                                       large_size=16384
                                                       rm _temp
                                                       for i in `seq $FIRST_LINE $LAST_LINE` 
                                                           do
                                                               head -$i $PATTERN-$NP-$j-$m-$f-$rf-$f-$rf-$small_size-$large_size-$b | tail -1 | awk '{ print $'$DATA_COLUMN' }' >> _temp
                                                           done
                                                           paste "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b   _temp > _combine
                                                           mv _combine "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b 

                                                       rm _temp
                                                       for i in `seq $FIRST_LINE $LAST_LINE` 
                                                           do
                                                               head -$i $PATTERN-$NP-$j-$m-$f-$rf-$f-$rf-$large_size-$small_size-$b | tail -1 | awk '{ print $'$DATA_COLUMN' }' >> _temp
                                                           done
                                                           paste "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b   _temp > _combine
                                                           mv _combine "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b 

                                                       rm _temp
                                                       for i in `seq $FIRST_LINE $LAST_LINE` 
                                                           do
                                                               head -$i $PATTERN-$NP-$j-$m-$s-$rf-$s-$rf-$small_size-$large_size-$b | tail -1 | awk '{ print $'$DATA_COLUMN' }' >> _temp
                                                           done
                                                           paste "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b   _temp > _combine
                                                           mv _combine "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b 

                                                       rm _temp
                                                       for i in `seq $FIRST_LINE $LAST_LINE` 
                                                           do
                                                               head -$i $PATTERN-$NP-$j-$m-$s-$rf-$s-$rf-$large_size-$small_size-$b | tail -1 | awk '{ print $'$DATA_COLUMN' }' >> _temp
                                                           done
                                                           paste "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b   _temp > _combine
                                                           mv _combine "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b 
                                              

                                                  fi

                                                       rm _temp
                                                       for i in `seq $FIRST_LINE $LAST_LINE` 
                                                           do
                                                               head -$i $PATTERN-$NP-$j-$m-$f-$rf-$s-$rs-$f_size-$s_size-$b | tail -1 | awk '{ print $'$DATA_COLUMN' }' >> _temp
                                                           done
                                                           paste "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b   _temp > _combine
                                                           mv _combine "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b 

                                                       rm _temp
                                                       for i in `seq $FIRST_LINE $LAST_LINE` 
                                                           do
                                                               head -$i $PATTERN-$NP-$j-$m-$s-$rf-$f-$rs-$s_size-$f_size-$b | tail -1 | awk '{ print $'$DATA_COLUMN' }' >> _temp
                                                           done
                                                           paste  "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b _temp > _combine
                                                           mv _combine "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b

                                                  
                                              fi

                                          done
                                       done
                                  done

                                  echo  "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b >> $COMPLETE_RESULT
                                  paste "Result"-$PATTERN-$NP-$j-"Msg"-$m-$f-$f_size-$b >> $COMPLETE_RESULT
                                  fi
                              fi
                        done
                    done
                  done
              done
           done
        done
done
