#!/usr/bin/python

FILE_NAME="/global/homes/l/limin/edison/global-communication-scheduling/MERACUOUS-Trace-File/48-mera-trace-second-run"
File_obj = open(FILE_NAME, 'r')
total_trace=[]

try: 
    for line in File_obj:
        if "gasnet_nodes" in line:
            nodes = line.split()[2]
            for x in range(0, int(nodes)):
                total_trace.append([])    

        if "PUT_VAL:" in line and "sz" not in line and \
            "0x" in line and "bytes" in line:
            words = line.split()
            #my rank, tempstamp, target_rank, data_size
            #words[0],  words[1], words[4][1:].split(',')[0], words[9][1:]
            if words[0].isdigit() and len(words) > 10 and words[10][1:].isdigit() \
                and words[5][1:].split(',')[0].isdigit():
                total_trace[int(words[0])].append("[" + words[0] + "]" + " PUT_VAL " + "[" +
                words[5][1:].split(',')[0] + "]" + "    Size: " + words[10][1:] +
                "    T:" + words[1])
            else:
                print "Errors in line: ", line

        elif "PUT_NBI:" in line and "sz" not in line and \
            "0x" in line and "bytes" in line:
            words = line.split()
            #my rank, tempstamp, target_rank, data_size
            #words[0],  words[1], words[4][1:].split(',')[0], words[-2][1:]
            if words[0].isdigit() and len(words) > 10 and words[5][1:].split(',')[0].isdigit() and \
                words[10][1:].isdigit(): 
                total_trace[int(words[0])].append("[" + words[0] + "]" + " PUT_NBI " + "[" +
                        words[5][1:].split(',')[0] + "]" + "    Size: " + words[10][1:] +
                        "    T:" + words[1])
            else:
                print "Errors in line: ", line

        elif "PUT_BULK:" in line and "sz" not in line and \
            "0x" in line and "bytes" in line:
            words = line.split()
            #my rank, tempstamp, target_rank, data_size
            #print line, words[0],  words[1], words[5][1:].split(',')[0], words[10][1:]
            if words[0].isdigit() and len(words) > 10 and words[5][1:].split(',')[0].isdigit() and \
                words[10][1:].isdigit(): 
                total_trace[int(words[0])].append("[" + words[0] + "]" + " PUT_BULK " + "[" +
                        words[5][1:].split(',')[0] + "]" + "    Size: " + words[10][1:] +
                        "    T:" + words[1])
            else:
                print "Errors in line: ", line

        elif "PUT:" in line and "PUT_VAL" not in line and "sz" not in line and \
            "0x" in line and "bytes" in line:
            words = line.split()
            #my rank, tempstamp, target_rank, data_size
            #words[0],  words[1], words[4][1:].split(',')[0], words[-2][1:]
            if words[0].isdigit() and len(words) > 5 and  words[5][1:].split(',')[0].isdigit() and \
                words[-2][1:].isdigit():
                total_trace[int(words[0])].append("[" + words[0] + "]" + " PUT     " + "[" +
                        words[5][1:].split(',')[0] + "]" + "    Size: " + words[-2][1:] +
                        "    T:" + words[1])
            else:
                print "Errors in line: ", line

        elif "GET_NBI:" in line and "sz" not in line and \
            "0x" in line and "bytes" in line:
            words = line.split()
            #my rank, tempstampe, target_rank, data_size 
            #words[0], words[1], words[8][1:].split(',')[0], words[-2][1:]
            if words[0].isdigit() and len(words) > 8 and words[8][1:].split(',')[0].isdigit() and \
                words[-2][1:].isdigit() : 
                total_trace[int(words[0])].append("[" + words[0] + "]" + " GET_NBI " + "[" +
                        words[8][1:].split(',')[0] + "]" + "    Size: " + words[-2][1:] +
                        "    T:" + words[1])
            else:
                print "Errors in line: ", line, "length: ", len(words)
        elif "GET_BULK:" in line and "sz" not in line and \
            "0x" in line and "bytes" in line:
            words = line.split()
            #my rank, tempstampe, target_rank, data_size 
            #words[0], words[1], words[8][1:].split(',')[0], words[-2][1:]
            if words[0].isdigit() and len(words) > 8 and words[8][1:].split(',')[0].isdigit() and \
                words[-2][1:].isdigit() : 
                total_trace[int(words[0])].append("[" + words[0] + "]" + " GET_BULK " + "[" +
                        words[8][1:].split(',')[0] + "]" + "    Size: " + words[-2][1:] +
                        "    T:" + words[1])
            else:
                print "Errors in line: ", line
      
        elif "GET:" in line and "sz" not in line and \
            "0x" in line and "bytes" in line:
            words = line.split()
            #my rank, tempstampe, target_rank, data_size 
            if words[0].isdigit() and len(words) > 8 and words[8][1:].split(',')[0].isdigit() and  \
                words[-2][1:].isdigit(): 
                total_trace[int(words[0])].append("[" + words[0] + "]" + " GET     " + "[" +
                        words[8][1:].split(',')[0] + "]" + "    Size: " + words[-2][1:] +
                        "    T:" + words[1])
            else:
                print "Errors in line: ", line
            #else:
            #    print "First word is not digit, raise error\n"
            #    print line, len(words)

except Exception as ex: 
    print ex
    print line, "length: ", len(line.split()), "split:", line.split()
    pass


for rank in total_trace:
    if len(rank) > 0:
        print "=============================================="
        print "Operations in rank: " , total_trace.index(rank)
        print "=============================================="
        for operation in rank:
            print operation
