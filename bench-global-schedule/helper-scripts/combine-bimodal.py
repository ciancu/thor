#!/usr/bin/python
import os

output_file_name = "final_file_bimodal_same_task.txt"
output_file = open(output_file_name, "w")

data = {}
path = '/global/homes/l/limin/edison/global-communication-scheduling/debug-bimodal'
all_same_task=[[[] for i in range(3)] for j in range(3)]

for dir_entry in os.listdir(path):
    dir_entry_path = os.path.join(path, dir_entry)
    if os.path.isfile(dir_entry_path):
        with open(dir_entry_path, 'r') as File_obj:
            num_lines = sum(1 for line in File_obj)
            data_start = num_lines
            
            File_obj.seek(0)
            
            tasks = 0
            depth = 0
            throughput = ""
            name = ""
            
            for i, line in enumerate(File_obj):
                if "Process" in line and "Pattern" in line:
                    data_start = i
                    words = line.split('\t')
                    tasks = int(words[3].split(':')[1].count('-')) + 1
                    depth = words[2].split(':')[1].lstrip(' ') 
            
                    name = words[3].split(':')[1].split('-')[0].lstrip(' ') +words[4].split(':')[1].split('-')[0].lstrip(' ')
                    if tasks > 1:
                        for index in range(1, int(tasks)):
                            name += "-" + words[3].split(':')[1].split('-')[index]+words[4].split(':')[1].split('-')[index]
                
                if i > data_start and i < num_lines - 1:
                    throughput = str(line.split()[1])
                    all_same_task[int(int(tasks) / int(2))][int (int(depth) / int(16))].append(name.rstrip() + ": " + throughput) 
            


operation_list = ""
throughput_list = ""

for task in all_same_task:
    output_file.write(str("Tasks: " + str(all_same_task.index(task)) + "\n"))
    for msg in task:
        output_file.write(str( "Number of Msgs: " + str(task.index(msg)) + "\n"))
        entry = str(msg)
        totalWords = sum(1 for word in entry.split(',')) - 1
        for i in range(0, totalWords):
            operation_list += entry.split(',')[i].split(':')[0][2:] + " "
            throughput_list += entry.split(',')[i].split(':')[1][1:-1] + " "

        if len(entry.split(',')[totalWords].split(':'))  == 2 :
            operation_list += entry.split(',')[totalWords].split(':')[0][2:]
            throughput_list += entry.split(',')[totalWords].split(':')[1][1:-2]

        output_file.write(str(operation_list) + "\n")
        output_file.write(str(throughput_list) + "\n")

    operation_list = ""
    throughput_list = ""


# Fix number of messages, change number of tasks
for i in all_same_task[0]:
    curr_index = all_same_task[0].index(i)
    one_task_list = str(all_same_task[0][curr_index]).split(',')
    num_op = sum(1 for word in one_task_list) 
    for index in range(0, num_op): 
        op = one_task_list[index].split(':')[0]
#for task in all_same_task:
#    output_file.write(str("Tasks: " + str(all_same_task.index(task)) + "\n"))
#    for msg in task:
#        output_file.write(str( "Number of Msgs: " + str(task.index(msg)) + "\n"))
#        entry = str(msg)
#        totalWords = sum(1 for word in entry.split(',')) - 1
#        for i in range(0, totalWords):
#            operation_list += entry.split(',')[i].split(':')[0][2:] + " "
#            throughput_list += entry.split(',')[i].split(':')[1][1:-1] + " "
#
#        if len(entry.split(',')[totalWords].split(':'))  == 2 :
#            operation_list += entry.split(',')[totalWords].split(':')[0][2:]
#            throughput_list += entry.split(',')[totalWords].split(':')[1][1:-2]
#
#        output_file.write(str(operation_list) + "\n")
#        output_file.write(str(throughput_list) + "\n")
