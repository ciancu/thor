#define BENCHMARK "Random Write Read One-sided Benchmark"
#include <mpi.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#define MAX_ALIGNMENT 65536
#define MAX_SIZE (1<<22)
#define MYBUFSIZE (MAX_SIZE + MAX_ALIGNMENT)
#define FIELD_WIDTH 20
#define HEADER "# " BENCHMARK "\n"
#define FLOAT_PRECISION 2


int main (int argc, char *argv[])
{
    int         loop = 100;
    int         size = 2048;
    int         page_size, i;
    uint64_t    total_size = 0;
    double      t_start = 0.0, t_end = 0.0;
    int         rank, nprocs, target_rank;
    char        sbuf_original[MYBUFSIZE];
    char        winbuf_original[MYBUFSIZE];
    char        *sbuf=NULL, *win_buf=NULL;
    MPI_Win     win;
    MPI_Aint    disp = 0;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    page_size = getpagesize();

    sbuf =
        (char *) (((unsigned long) sbuf_original + (page_size - 1)) /
                page_size * page_size);
    win_buf =
        (char *) (((unsigned long) winbuf_original + (page_size - 1)) /
                page_size * page_size);

    MPI_Win_allocate(MYBUFSIZE*2, 1, MPI_INFO_NULL, MPI_COMM_WORLD, win_buf, &win);

    MPI_Win_lock_all(0, win);
    MPI_Barrier(MPI_COMM_WORLD);

    t_start = MPI_Wtime ();

    for (i = 0; i < loop; i++) {
        target_rank = rand() % nprocs;
        disp = rand() % (MYBUFSIZE - size - 1);
        total_size += size;
        MPI_Put(sbuf, size, MPI_CHAR, target_rank, disp, size, MPI_CHAR, win);
        MPI_Win_flush(target_rank, win);
    }

    MPI_Barrier(MPI_COMM_WORLD);

    for (i = 0; i < loop; i++) {
        target_rank = rand() % nprocs;
        disp = rand() % (MYBUFSIZE - size - 1);
        MPI_Get(sbuf, size, MPI_CHAR, target_rank, disp, size, MPI_CHAR, win);
        MPI_Win_flush(target_rank, win);
    }

    MPI_Barrier(MPI_COMM_WORLD);

    t_end = MPI_Wtime ();
    if (rank == 0) {
        fprintf(stdout, "%-*d%*.*f\n", 10, total_size, FIELD_WIDTH,
                FLOAT_PRECISION, (t_end - t_start) * 1.0e6 / loop);
        fflush(stdout);
    }

    MPI_Win_unlock_all(win);
    MPI_Win_free(&win);

    MPI_Finalize();

    return EXIT_SUCCESS;
}
/* vi: set sw=4 sts=4 tw=80: */
