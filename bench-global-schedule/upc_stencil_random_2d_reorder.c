#define BENCHMARK "UPC Throughput Benchmark (MB/s)"

#include <upc_relaxed.h>
#include <upc_collective.h>
#include <upc_nb.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#define MAX_MSG_SIZE         (1<<22)
#define LOOP_LARGE 1
#define LARGE_MESSAGE_SIZE 8192
int     loop = 1;

int max_random_peers = 1;
int small_message = 7;
int medium_message = 3;
int large_message = 1;
int complete_message[] = {8, 64, 128, 256, 512, 1024, 2048, 16384, 32768, 65536, 131072, 1048576};
int interval_base[] = {1};
char *operation_type[20] = {"Put", "Get","MIX"};
char *pattern_string[20] = {"Stencil", "Random" ,"2D"};

int task = 1;
int num_sizes = 0; /* Number of messages in small/medium/large/bimodal array*/
int nonblocking; /* Number of intervals in interval_base array*/
int num_first, num_second;
int x_dimension, y_dimension, z_dimension;
char input_op_sequence[40];
char bimodal_size_sequence[20];
char *bimodal_first_string, *bimodal_second_string;
int *bimodal_message, bimodal_large_size, bimodal_small_size;

char *local;
shared double total_size[THREADS];
shared double all_total_size;
shared double total_messages[THREADS];
shared double all_total_messages;
relaxed shared char *data;

#   define HEADER "# " BENCHMARK "\n"

#ifndef FIELD_WIDTH
#   define FIELD_WIDTH 30
#endif

#ifndef FLOAT_PRECISION
#   define FLOAT_PRECISION 2
#endif

#define SYNC_MODE (UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC)

/* Pattern */
typedef enum {
    STENCIL=0, 
    RANDOM, 
    TWODECOMP
} PATTERN;

/* Operation */
typedef enum {
    PUT=0,
    GET,
    MIX 
} OPERATION;

OPERATION first, second;
void run_with_stencil(int *, OPERATION *, int);
void run_with_random(int *, OPERATION *, int);
void run_with_2ddecomp(int *, OPERATION *, int);

int process_options(int argc, char *argv[], PATTERN *pattern, int **message, int *depth)
{
    extern char *optarg;
    extern int  optind;
    extern int opterr;
    int c;

    char const * optstring = "p:m:d:r:n:l:s:x:y:z:o:b:";

    while((c = getopt(argc, argv, optstring)) != -1) {
        switch (c) {
            case 'p':
                if (0 == strcasecmp(optarg, "stencil")) {
                    *pattern = STENCIL;
                }
                else if (0 == strcasecmp(optarg, "random")) {
                    *pattern = RANDOM;
                }
                else if (0 == strcasecmp(optarg, "2ddecomp")) {
                    *pattern = TWODECOMP;
                }
                else {
                    return 1;
                }
                break;
            case 'm':
                if (0 == strcasecmp(optarg, "small")) {
                    *message = complete_message;
                    num_sizes = small_message;
                }
                else if (0 == strcasecmp(optarg, "medium")) {
                    *message = complete_message + small_message;
                    num_sizes = medium_message;
                }
                else if (0 == strcasecmp(optarg, "large")) {
                    *message = complete_message + small_message + medium_message;
                    num_sizes = large_message;
                }
                else if (0 == strcasecmp(optarg, "compl")) {
                    *message = complete_message;
                    num_sizes = sizeof(complete_message) / sizeof(complete_message[0]);
                }
#ifdef _BIMODAL_
                else if (0 == strcasecmp(optarg, "bimodal")) {
                    *message = complete_message + small_message + medium_message;
                    loop = LOOP_LARGE;
                    /*Only one throughput output same as large_message */
                    num_sizes = large_message;
                }
#endif
                else {
                    return 1;
                }
                break;
            case 'd':
                *depth = atoi(optarg);
                break;
            case 'r':
                max_random_peers = atoi(optarg);
                break;
            case 'n':
                nonblocking = atoi(optarg);
                break;
            case 'l' :
                bimodal_large_size = atoi(optarg);
                break;
            case 's' :
                bimodal_small_size = atoi(optarg);
                break;
            case 'x' :
                x_dimension = atoi(optarg);
                break;
            case 'y' :
                y_dimension = atoi(optarg);
                break;
            case 'z' :
                z_dimension = atoi(optarg);
                break;
            case 'o' :
                strcpy(input_op_sequence, optarg);
                break;
#ifdef _BIMODAL_
            case 'b' :
                strcpy(bimodal_size_sequence, optarg);
                break;
#endif
            default:
                return 1;
        }
    }

    return 0;
}

/*Get information from this kind of format: 1P2P*/
void generate_op_sequence(OPERATION *op_sequence, int depth)
{
    int i = 0, f = 0, s = 0, index;
    int bimodal_size_index, bimodal_first_size, bimodal_second_size;
    while (input_op_sequence[i] != '\0') {
        if (input_op_sequence[i] == '-') {
            task++;
        }
        i++;
    }

    index = (MYTHREAD % task) * 5; 

#ifdef _BIMODAL_
    bimodal_size_index = (MYTHREAD % task) * 3; 
    bimodal_first_size = (bimodal_size_sequence[bimodal_size_index + 0] == 'L') ? \
                         bimodal_large_size : bimodal_small_size;
    bimodal_second_size = (bimodal_size_sequence[bimodal_size_index + 1] == 'L' ) ? \
                          bimodal_large_size : bimodal_small_size;
#endif

    num_first = (int)(input_op_sequence[index + 0] - '0');
    num_second = (int)(input_op_sequence[index + 2] - '0');
    first = (input_op_sequence[index + 1] == 'P') ? PUT : GET;
    second = (input_op_sequence[index + 3] == 'P') ? PUT : GET;

    i = 0;
    while (i < depth) {
        if (f < num_first) {
            *(op_sequence + i) = first;
#ifdef _BIMODAL_
            *(bimodal_message + i) = bimodal_first_size;
#endif
            f++;
            i++;
        }

        if (f == num_first && (s < num_second)){
            *(op_sequence + i) = second;
#ifdef _BIMODAL_
            *(bimodal_message + i) = bimodal_second_size;
#endif
            s++;
            i++;
        } else if (f == num_first && s == num_second) {
            f = 0;
            s = 0;
        }
    }

#ifdef _BIMODAL_
    bimodal_first_string = (bimodal_first_size > bimodal_second_size) ? "large" : "small";
    bimodal_second_string = (bimodal_first_size > bimodal_second_size) ? "small" : "large";
#endif
}

void wtime(double *t)
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  *t = tv.tv_sec + tv.tv_usec / 1e6;
}

int main(int argc, char **argv) 
{
    PATTERN pattern;
    int *message;
    int depth;
    OPERATION *op_sequence;
    
    if (process_options( argc, argv, &pattern, &message, &depth))
        if (!MYTHREAD)
            fprintf(stderr, "Argument invalid\n"); 

    data = upc_all_alloc(THREADS, MAX_MSG_SIZE*2);
    local = ((char *)(data+MYTHREAD)) + MAX_MSG_SIZE;

    op_sequence = malloc(depth * sizeof(OPERATION));
#ifdef _BIMODAL_
    bimodal_message = malloc(depth * sizeof(int));
#endif

    generate_op_sequence(op_sequence, depth);

    switch (pattern){
        case STENCIL:
            run_with_stencil(message, op_sequence, depth);
            break;
        case RANDOM:
            run_with_random(message, op_sequence, depth);
            break;
        case TWODECOMP:
            run_with_2ddecomp(message, op_sequence, depth);
            break;
        default:
           fprintf(stderr, "Unknow pattern\n");
           break; 
    }

    free(op_sequence);
    free(bimodal_message);

    return 0;
}

int get_peer_id(int *peer_id, int index, int coordinate[])
{
    switch (index){
        case 0: /*west*/
            if (coordinate[0] != 0)
                *peer_id = MYTHREAD - 1;
            else
                return 0;
            break;
        case 1: /*east*/
            if (coordinate[0] < x_dimension - 1)
                *peer_id = MYTHREAD + 1;
            else
                return 0;
            break;
        case 2: /*north*/
            if (coordinate[1] != 0)
                *peer_id = MYTHREAD - x_dimension;
            else
                return 0;
            break;
        case 3: /*south*/
            if (coordinate[1] < y_dimension -1)
                *peer_id = MYTHREAD + x_dimension;
            else
                return 0;
            break;
        case 4: /*down*/
            if (coordinate[2] != 0)
                *peer_id = MYTHREAD - (x_dimension * y_dimension);
            else
                return 0;
            break;
        case 5: /*up*/
            if (coordinate[2] < z_dimension -1)
                *peer_id = MYTHREAD + (x_dimension * y_dimension);
            else
                return 0;
            break;
    }
    if (*peer_id > THREADS || *peer_id < 0)
        return 0;

    return 1;
}

void print_header(PATTERN pattern, int x_dimension, int depth, OPERATION *op)
{
    fprintf(stdout, HEADER);

#ifdef _BIMODAL_
    fprintf(stdout, "Processes: %d \t Pattern: %s\t Depth: %d\t Op: %s\t Size: %s\n", \
                    THREADS, pattern_string[pattern], depth, input_op_sequence, \
                    bimodal_size_sequence);
#else
    fprintf(stdout, "Processes: %d \t Pattern: %s\t Depth: %d\t Op: %s\n", \
                    THREADS, pattern_string[pattern], depth,  \
                    input_op_sequence);
#endif
}

void print_result(double t, double total, int single_msg_size, int total_num_messages)
{
    double throughput = (total / 1e6) / t;
#ifdef _BIMODAL_
    fprintf(stdout, "%-d_%d%*.*f\n", bimodal_small_size, bimodal_large_size, FIELD_WIDTH,
            FLOAT_PRECISION, throughput);
#else
    fprintf(stdout, "%-*d%*.*f\n", 10, single_msg_size, FIELD_WIDTH,
            FLOAT_PRECISION, throughput);
#endif
    fflush(stdout);
}

int force_2nic_traffic_random(int *peer_id)
{
    int range = THREADS/2;
    if (MYTHREAD < range) {
        *peer_id = ((*peer_id + range) < THREADS) ? (*peer_id + range) : -1;
    } else {
        *peer_id = ((*peer_id - range) >= 0) ? (*peer_id - range) : -1;
    }

    if (*peer_id == -1)
        return 0;

    return 1;
}

int force_2nic_traffic(int *peer_id, int index)
{
    int range = THREADS/2;
    int is_add;

    if (index == 0 || index == 2 || index == 4) {
        is_add = 0;
    } else {
        is_add = 1;
    }

    if (is_add) {
        *peer_id = ((*peer_id + range) < THREADS) ? (*peer_id + range) : -1;
    } else {
        *peer_id = ((*peer_id - range) >= 0) ? (*peer_id - range) : -1;
    } 

    if (*peer_id == -1)
        return 0;

    return 1;
}

void start_comm(int depth, OPERATION *op, int size, int peer_id) 
{
    int k;
    shared [] char *remote = (shared [] char *)(data + peer_id);

    for (k = 0; k < depth; k++) {
#ifdef _BIMODAL_
        size = bimodal_message[k];
#endif
        if (op[k] == PUT) {
            if (!nonblocking) {
                upc_memput(remote, local, size);
            } else {
                upc_memput_nbi(remote, local, size);
            }

            total_size[MYTHREAD] += size;
            total_messages[MYTHREAD]++;
        } else if (op[k] == GET) {
            if (!nonblocking) {
                upc_memget(local, remote, size);
            } else {
                upc_memget_nbi(local, remote, size);
            }
            total_size[MYTHREAD] += size;
            total_messages[MYTHREAD]++;
        }

        if ((k == depth -1 ) || !(!nonblocking || ((k+1) % (num_first + num_second)))) {
            upc_synci();
        } 
    }
}

void run_with_stencil(int *data_size, OPERATION  *op, int depth)
{
    int i, peer_id, m, size, n, is_add;
    double t_start, t_end;

    if (!MYTHREAD && (THREADS != (x_dimension * y_dimension * z_dimension))) {
        fprintf(stderr, "Ajust processes in X, Y, Z dimension\n");
        exit(1);
    }

    /*x,y,z coordinate*/
    int my_coord[] = {MYTHREAD % x_dimension, ((MYTHREAD / x_dimension) % y_dimension), ((MYTHREAD / (x_dimension * y_dimension)) % z_dimension)};

    if ( !MYTHREAD ) {
        print_header(STENCIL, x_dimension, depth, op);
    }

    /* message size array */
    for (m = 0; m < num_sizes; m++) {
        size = *(data_size + m);

        total_size[MYTHREAD] = 0;
        total_messages[MYTHREAD] = 0;

        if(size > LARGE_MESSAGE_SIZE) {
            loop = LOOP_LARGE;
        }

        upc_barrier;
        wtime(&t_start);

        for (n = 0; n < loop; n++) {

            /*6 directions */
            for (i = 0; i < 6 ; i++) {

                if (!get_peer_id(&peer_id, i, my_coord)) {
                    continue;
                }
#if 0
                if (!force_2nic_traffic(&peer_id, i))
                    continue;
#endif

                start_comm(depth, op, size, peer_id);

            }  
        }

        upc_barrier;
        wtime(&t_end);

        upc_all_reduceD(&all_total_size, total_size, UPC_ADD, THREADS, 1, NULL, SYNC_MODE);
        upc_all_reduceD(&all_total_messages, total_messages, UPC_ADD, THREADS, 1, NULL, SYNC_MODE);

        if( !MYTHREAD )
        {
            print_result((t_end - t_start), all_total_size, size, all_total_messages);
        }
    }
}

void run_with_random(int *data_size, OPERATION *op, int depth)
{

    int m, j, size, peer_id, n;
    double t_start, t_end;

    if ( !MYTHREAD ) {
        print_header(RANDOM, 0, depth, op);
    }

    /* message size array */
    for (m = 0; m < num_sizes; m++) {
        size = *(data_size + m);

        total_size[MYTHREAD] = 0;
        total_messages[MYTHREAD] = 0;

        if(size > LARGE_MESSAGE_SIZE) {
            loop = LOOP_LARGE;
        }

        srand(clock());
        upc_barrier;
        wtime(&t_start);

        for (n = 0; n < loop; n++) {
            for (j = 0; j < max_random_peers; j++) {
                peer_id = random() % (THREADS - 1);

#if 0
                if (!force_2nic_traffic_random(&peer_id))
                    continue;
#endif

                start_comm(depth, op, size, peer_id); 

            }
        }

        upc_barrier;
        wtime(&t_end);

        upc_all_reduceD(&all_total_size, total_size, UPC_ADD, THREADS, 1, NULL, SYNC_MODE);
        upc_all_reduceD(&all_total_messages, total_messages, UPC_ADD, THREADS, 1, NULL, SYNC_MODE);

        if( !MYTHREAD )
        {
            print_result((t_end - t_start), all_total_size, size, all_total_messages);
        }
    }
}

/*send data to the east, west, north and south direction*/
void run_with_2ddecomp(int *data_size, OPERATION *op, int depth)
{
    int m, i, size, peer_id, j;
    double t_start, t_end;

    x_dimension = y_dimension = sqrt(THREADS); 
    /*x,y coordinate*/
    int my_coord[] = {MYTHREAD % x_dimension, MYTHREAD / x_dimension};

    if ( !MYTHREAD ) {
        print_header(TWODECOMP, x_dimension, depth, op);
    }

    /* message size array */
    for (m = 0; m < num_sizes; m++) {
        size = *(data_size + m);

        total_size[MYTHREAD] = 0;

        if(size > LARGE_MESSAGE_SIZE) {
            loop = LOOP_LARGE;
        }

        upc_barrier;
        wtime(&t_start);

        for (j = 0; j < loop; j++) {

            /*four direction */
            for (i = 0; i < 4 ; i++) {

                if (!get_peer_id(&peer_id, i, my_coord)) {
                    continue;
                }
#if 0
                if (!force_2nic_traffic(&peer_id, i))
                    continue;
#endif

                start_comm(depth, op, size, peer_id);

            }
        }

        upc_barrier;
        wtime(&t_end);

        upc_all_reduceD(&all_total_size, total_size, UPC_ADD, THREADS, 1, NULL, SYNC_MODE);
        upc_all_reduceD(&all_total_messages, total_messages, UPC_ADD, THREADS, 1, NULL, SYNC_MODE);

        if( !MYTHREAD )
        {
            print_result((t_end - t_start), all_total_size, size, all_total_messages);
        }
    }
}
