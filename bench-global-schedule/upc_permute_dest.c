#define BENCHMARK "UPC Destination Permutation Benchmark (MB/s)"

#include <upc_relaxed.h>
#include <upc_collective.h>
#include <upc_nb.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#define MAX_MSG_SIZE         (1<<22)
#define LOOP_LARGE 100
#define LARGE_MESSAGE_SIZE 8192


#define SMALL_SIZE 256
#define LARGE_SIZE 8192

#   define HEADER "# " BENCHMARK "\n"

#ifndef FIELD_WIDTH
#   define FIELD_WIDTH 30
#endif

#ifndef FLOAT_PRECISION
#   define FLOAT_PRECISION 2
#endif

#define SYNC_MODE (UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC)

/* Operation */
typedef enum {
    PUT=0,
    GET,
} OPERATION;

typedef enum {
    SL=0,
    SSL,
    SSLL,
    LS,
    LSS,
    LLSS
} SIZE_TYPE;

typedef struct latency_element{
    int rank;
    double latency;
}latency_element;

int loop = 1000;
int *size_sequence;
int *dest_sequence;
//int depth_sequence[] = {1, 8};
int depth_sequence[] = {1};
OPERATION *op_sequence;
char *operation_type[20] = {"Put", "Get"};
SIZE_TYPE size_pattern[] = {SL, SSL, SSLL, LS, LSS, LLSS};
int sync_depth[] = {2, 3, 4, 2, 3, 4};
int sync_counter = 0;
char *size_pattern_string[20] = {"SL", "SSL", "SSLL", "LS", "LSS", "LLSS"};
char behavior[20];
int RANDOM_PEERS;


char *local;
int dest_gap[] = {1, 24, 48, 76};
latency_element *node_latency;
//int dest_special_gap[] = {1, 0}; /* 1: start with middle, then nearst. 0: start with nearst, then middle*/
int dest_special_gap[] = {1}; /* 1: start with middle, then nearst. 0: start with nearst, then middle*/
int x_dimension, y_dimension, z_dimension;
void (* run_test) (int, SIZE_TYPE);
void (* generate_size) (SIZE_TYPE);
void (* reorder_size) (SIZE_TYPE);
shared double total_size[THREADS];
shared double all_total_size;
shared double total_messages[THREADS];
shared double all_total_messages;
relaxed shared char *data;

void permute_dest();
void permute_size();
void run_alltoall(int , SIZE_TYPE);
void run_random(int , SIZE_TYPE);
void run_stencil(int , SIZE_TYPE);
void print_header(int , SIZE_TYPE);
void generate_alltoall_size(SIZE_TYPE);
void reorder_alltoall_size(SIZE_TYPE);
void generate_stencil_size(SIZE_TYPE);
void reorder_stencil_size(SIZE_TYPE);
void generate_random_size(SIZE_TYPE);
void reorder_random_size(SIZE_TYPE);
void measure_node_latency(int node_id, latency_element *);
int get_peer_id(int index, int coordinate[]);
int compare (const void * a, const void * b);
void print_result(double t, double total, int single_msg_size);

void generate_sequential_dest(int index)
{
    int i;
    int j = 1, start = 0;
    for (i = 0; i < THREADS; i++){
        if (MYTHREAD + start + j * dest_gap[index] >= THREADS) {
            start++;
            j = 0;
        }
        dest_sequence[i] = MYTHREAD + j * dest_gap[index] + start;
        j++;
    }
}

void generate_special_dest(int index)
{
    int i;
    int end = THREADS / 24 - 1;
    int start = 1;
    int m = 0, n = 0;
    for (i = 0; i < THREADS; i++){
        if (i / 24 == MYTHREAD / 24) {
            dest_sequence[i] = MYTHREAD;
            continue;
        }
        if ((!(i%2)) || start == end) {
            dest_sequence[i] = node_latency[end].rank + m;
            m++;
        } else {
            dest_sequence[i] = node_latency[start].rank + n;
            n++;
        }
        if (m == 24) {
            m = 0;
            end--;
        }
        if (n == 24) {
            n = 0;
            start++;
        }
#if 0
            if (MYTHREAD == 191)
                fprintf(stderr, "%d-", dest_sequence[i]);
#endif
    }
#if 0
    start = end = MYTHREAD;
    for (i = 0; i < THREADS; i++){
        if (!(i % 2)) {
            if (dest_special_gap[index])  {
                dest_sequence[i] = (end + THREADS/2) % THREADS;
                end++;
            } else {
                dest_sequence[i] = (start + 1) % THREADS;
                start++;
            }
        } else {
            if (dest_special_gap[index])  {
                dest_sequence[i] = (start + 1) % THREADS;
                start++;
            } else {
                dest_sequence[i] = (end + THREADS/2) % THREADS;
                end++;
            }
        }
    }
#endif
}

void wtime(double *t)
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  *t = tv.tv_sec + tv.tv_usec / 1e6;
}

int main(int argc, char **argv) 
{
    int i, j, depth; 
    RANDOM_PEERS = THREADS / 4;

    run_test = &run_alltoall;
    generate_size = &generate_alltoall_size;
    reorder_size = &reorder_alltoall_size;
    strcpy(behavior, "AlltoAll");

    if (!MYTHREAD) fprintf(stdout, HEADER);

    size_sequence = malloc(THREADS * sizeof(int));
    dest_sequence = malloc(THREADS * sizeof(int));
    op_sequence = malloc(THREADS * sizeof(OPERATION));
    node_latency = malloc(THREADS / 24 * sizeof(latency_element));

    data = upc_all_alloc(THREADS, MAX_MSG_SIZE*2);
    local = ((char *)(data+MYTHREAD)) + MAX_MSG_SIZE;


    for (i = 1; i < THREADS / 24; i++) {
        measure_node_latency(i, node_latency);
    }

    (node_latency + MYTHREAD / 24)->latency = 0;
    (node_latency + MYTHREAD / 24)->rank = MYTHREAD;

    qsort (node_latency, THREADS / 24, sizeof(latency_element), compare);

    for (i = 0; i < THREADS / 24; i++) {
        if (MYTHREAD == 0)
        fprintf(stderr, "[%d->%d] latency: %.*f\n", MYTHREAD, node_latency[i].rank, 2, node_latency[i].latency);
    }
    upc_barrier;

    permute_dest();
//    permute_size();


    upc_barrier;

    free(op_sequence);
    free(size_sequence);
    free(dest_sequence);
    free(node_latency);

    return 0;
}

void generate_stencil_size(SIZE_TYPE type)
{
    int i; 

    x_dimension = y_dimension = z_dimension = cbrt(THREADS);
    int my_coord[] = {MYTHREAD % x_dimension, ((MYTHREAD / x_dimension) % y_dimension), ((MYTHREAD / (x_dimension * y_dimension)) % z_dimension)};

    for (i = 0; i < 6; i++) {
        dest_sequence[i] = get_peer_id(i, my_coord);
        switch (type) {
            case SL:
                size_sequence[i] = i % 2 ? LARGE_SIZE : SMALL_SIZE;
                break;
            case SSL:
                size_sequence[i] = SMALL_SIZE;
                if (!((i + 1) % 3)) size_sequence[i] = LARGE_SIZE;
                break;
            case SSLL:
                size_sequence[i] = SMALL_SIZE;
                if ((!((i + 1) % 3)) || (!((i + 1) % 4))) size_sequence[i] = LARGE_SIZE; 
                break;
            default:
                if (!MYTHREAD)
                    fprintf(stderr, "Type is wrong\n");
        }
    }
}

void reorder_stencil_size(SIZE_TYPE type)
{
    int temp, i;
    for (i = 0; i < 6; i++) {
        switch (type) {
            case SL:
                size_sequence[i] = i % 2 ? SMALL_SIZE : LARGE_SIZE;
                if (!(i % 2)) {
                    temp = dest_sequence[i];
                    dest_sequence[i] = dest_sequence[i+1];
                    dest_sequence[i+1] = temp;
                }
                break;
            case SSL:
                size_sequence[i] = SMALL_SIZE;
                if (!(i % 3)) size_sequence[i] = LARGE_SIZE;
                if (!(i % 3)) {
                    temp = dest_sequence[i];
                    dest_sequence[i] = dest_sequence[i+2];
                    dest_sequence[i+2] = temp;
                }
                break;
            case SSLL:
                if (i % 4 < 2) {
                    temp = dest_sequence[i];
                    dest_sequence[i] = dest_sequence[i+2];
                    dest_sequence[i+2] = temp;
                } 
                size_sequence[i] = LARGE_SIZE;
                if ((!((i + 1) % 3)) || (!((i + 1) % 4))) size_sequence[i] = SMALL_SIZE; 
                break;
            default:
                if (!MYTHREAD)
                    fprintf(stderr, "Type is wrong\n");

        }
    }
}


void generate_random_size(SIZE_TYPE type)
{
    int i; 
    srand(clock());

    for (i = 0; i < RANDOM_PEERS; i++) {
        dest_sequence[i] = random() % (THREADS - 1);

        switch (type) {
            case SL:
                size_sequence[i] = i % 2 ? LARGE_SIZE : SMALL_SIZE;
                break;
            case SSL:
                size_sequence[i] = SMALL_SIZE;
                if (!((i + 1) % 3)) size_sequence[i] = LARGE_SIZE;
                break;
            case SSLL:
                size_sequence[i] = SMALL_SIZE;
                if ((!((i + 1) % 3)) || (!((i + 1) % 4))) size_sequence[i] = LARGE_SIZE; 
                break;
            default:
                if (!MYTHREAD)
                    fprintf(stderr, "Type Random is wrong\n");
        }
    }
}

void reorder_random_size(SIZE_TYPE type)
{
    int temp, i;

    for (i = 0; i < RANDOM_PEERS; i++) {
        switch (type) {
            case SL:
                size_sequence[i] = i % 2 ? SMALL_SIZE : LARGE_SIZE;
                if (!(i % 2)) {
                    temp = dest_sequence[i];
                    dest_sequence[i] = dest_sequence[i+1];
                    dest_sequence[i+1] = temp;
                }
                break;
            case SSL:
                size_sequence[i] = SMALL_SIZE;
                if (!(i % 3)) size_sequence[i] = LARGE_SIZE;
                if (!(i % 3)) {
                    temp = dest_sequence[i];
                    dest_sequence[i] = dest_sequence[i+2];
                    dest_sequence[i+2] = temp;
                }
                break;
            case SSLL:
                if (i % 4 < 2) {
                    temp = dest_sequence[i];
                    dest_sequence[i] = dest_sequence[i+2];
                    dest_sequence[i+2] = temp;
                } 
                size_sequence[i] = LARGE_SIZE;
                if ((!((i + 1) % 3)) || (!((i + 1) % 4))) size_sequence[i] = SMALL_SIZE; 
                break;
            default:
                if (!MYTHREAD)
                    fprintf(stderr, "Type Random is wrong\n");
                
        }
    }
}

void generate_alltoall_size(SIZE_TYPE type)
{
    int i; 

    for (i = 0; i < THREADS; i++) {
        dest_sequence[i] = i;

        switch (type) {
            case SL:
                size_sequence[i] = i % 2 ? LARGE_SIZE : SMALL_SIZE;
                break;
            case SSL:
                size_sequence[i] = SMALL_SIZE;
                if (!((i + 1) % 3)) size_sequence[i] = LARGE_SIZE;
                break;
            case SSLL:
                size_sequence[i] = SMALL_SIZE;
                if ((!((i + 1) % 3)) || (!((i + 1) % 4))) size_sequence[i] = LARGE_SIZE; 
                break;
            default:
                if (!MYTHREAD)
                    fprintf(stderr, "Type is wrong\n");
        }
    }
}

void reorder_alltoall_size(SIZE_TYPE type)
{
    int i;
    int first = 1, second = 0, temp;
    for (i = 0; i < THREADS; i++) {
        switch (type) {
            case SL:
                size_sequence[i] = i % 2 ? SMALL_SIZE : LARGE_SIZE;
                if (!(i % 2)) {
                    temp = dest_sequence[i];
                    dest_sequence[i] = dest_sequence[i+1];
                    dest_sequence[i+1] = temp;
                }
                break;
            case SSL:
                size_sequence[i] = SMALL_SIZE;
                if (!(i % 3)) size_sequence[i] = LARGE_SIZE;
                if (!(i % 3)) {
                    temp = dest_sequence[i];
                    dest_sequence[i] = dest_sequence[i+2];
                    dest_sequence[i+2] = temp;
                }
                break;
            case SSLL:
                if (i % 4 < 2) {
                    temp = dest_sequence[i];
                    dest_sequence[i] = dest_sequence[i+2];
                    dest_sequence[i+2] = temp;
                } 
                size_sequence[i] = LARGE_SIZE;
                if ((!((i + 1) % 3)) || (!((i + 1) % 4))) size_sequence[i] = SMALL_SIZE; 
                break;
            default:
                if (!MYTHREAD)
                    fprintf(stderr, "Type is wrong\n");
                
        }
    }
}

void permute_size()
{
    int i, j = 0, depth;

    for (i = 0; i < THREADS; i++) {
        op_sequence[i] = PUT;
    }

    for (i = 0; i < ((sizeof(size_pattern) / sizeof(SIZE_TYPE) / 2)); i++) {
        generate_size(size_pattern[i]);

        for (j = 0; j < (sizeof(depth_sequence) / sizeof(int)); j++) {
            depth = depth_sequence[j];
            run_test(depth, size_pattern[i]);
        }

        reorder_size(size_pattern[i]);
        for (j = 0; j < (sizeof(depth_sequence) / sizeof(int)); j++) {
            depth = depth_sequence[j];
            run_test(depth, size_pattern[i + 3]);
        }
    }
}

void permute_dest()
{
    int i, j = 0, depth;

    for (i = 0; i < THREADS; i++) {
        size_sequence[i] = 256; 
        op_sequence[i] = PUT;
    }

    for (i = 0; i < (sizeof(dest_gap) / sizeof(int)); i++) {
        generate_sequential_dest(i);

        for (j = 0; j < (sizeof(depth_sequence) / sizeof(int)); j++) {
            depth = depth_sequence[j];
            run_test(depth, SL);
        }
    }

    for (i = 0; i < (sizeof(dest_special_gap) / sizeof(int)); i++) {
        generate_special_dest(i);

        for (j = 0; j < (sizeof(depth_sequence) / sizeof(int)); j++) {
            depth = depth_sequence[j];
            run_test(depth, SL);
        }
    }
}

void start_comm(int depth, OPERATION op, int size, int peer_id, SIZE_TYPE type) 
{
    int k;
    shared [] char *remote = (shared [] char *)(data + peer_id);

    if (peer_id / 24 != MYTHREAD / 24) {
        sync_counter++;
        for (k = 0; k < depth; k++) {
            if (op == PUT) {
                upc_memput_nbi(remote, local, size);
                total_size[MYTHREAD] += size;
                total_messages[MYTHREAD]++;
            } else if (op == GET) {
                upc_memget_nbi(local, remote, size);
                total_size[MYTHREAD] += size;
                total_messages[MYTHREAD]++;
            }

            if (sync_counter == sync_depth[type] || ((depth != 1) && (k == depth -1))) {
                upc_synci();
                sync_counter = 0;
#if 0
                if (MYTHREAD == 191)
                    fprintf(stderr, "[sync at: %d]\n", peer_id);
#endif
            }
#if 0
            if (MYTHREAD == 191)
                fprintf(stderr, "%d-", peer_id);
#endif
        }
    }
}

void run_alltoall(int depth, SIZE_TYPE type)
{
    int i, j, size, peer_id;
    OPERATION op;
    double t_start, t_end;

    if (!MYTHREAD) print_header(depth, type);

    total_size[MYTHREAD] = 0;
    total_messages[MYTHREAD] = 0;

    srand(clock());
    upc_barrier;
    wtime(&t_start);

    for (j = 0; j < loop; j++) {
        for (i = 0; i < THREADS; i++) {
            peer_id = dest_sequence[i];
            size = size_sequence[i];
            op = op_sequence[i];

            if(size > LARGE_MESSAGE_SIZE) {
                loop = LOOP_LARGE;
            }
            start_comm(depth, op, size, peer_id, type); 
        }
    }

    upc_barrier;
    wtime(&t_end);

    upc_all_reduceD(&all_total_size, total_size, UPC_ADD, THREADS, 1, NULL, SYNC_MODE);
    upc_all_reduceD(&all_total_messages, total_messages, UPC_ADD, THREADS, 1, NULL, SYNC_MODE);

    if( !MYTHREAD )
    {
        print_result((t_end - t_start), all_total_size, size);
    }
}

void run_stencil(int depth, SIZE_TYPE type)
{
    int i, peer_id, size, n;
    OPERATION op;
    double t_start, t_end;


    if ( !MYTHREAD ) print_header(depth, type);

    total_size[MYTHREAD] = 0;
    total_messages[MYTHREAD] = 0;

    if(size > LARGE_MESSAGE_SIZE) {
        loop = LOOP_LARGE;
    }

    upc_barrier;
    wtime(&t_start);


    for (n = 0; n < loop; n++) {
        for (i = 0; i < 6 ; i++) {
            peer_id = dest_sequence[i];
            size = size_sequence[i];
            op = op_sequence[i];
            start_comm(depth, op, size, peer_id, type);
        }  
    }

    upc_barrier;
    wtime(&t_end);

    upc_all_reduceD(&all_total_size, total_size, UPC_ADD, THREADS, 1, NULL, SYNC_MODE);
    upc_all_reduceD(&all_total_messages, total_messages, UPC_ADD, THREADS, 1, NULL, SYNC_MODE);

    if( !MYTHREAD )
    {
        print_result((t_end - t_start), all_total_size, size);
    }
}

void run_random(int depth, SIZE_TYPE type)
{
    int size, peer_id, n, i;
    OPERATION op;
    double t_start, t_end;

    if ( !MYTHREAD ) print_header(depth, type);

    total_size[MYTHREAD] = 0;
    total_messages[MYTHREAD] = 0;

    if(size > LARGE_MESSAGE_SIZE) {
        loop = LOOP_LARGE;
    }

    upc_barrier;
    wtime(&t_start);

    for (n = 0; n < loop; n++) {
        for (i = 0; i < RANDOM_PEERS; i++) {
            peer_id = dest_sequence[i];
            size = size_sequence[i];
            op = op_sequence[i];
            start_comm(depth, op, size, peer_id, type); 
        }
    }

    upc_barrier;
    wtime(&t_end);

    upc_all_reduceD(&all_total_size, total_size, UPC_ADD, THREADS, 1, NULL, SYNC_MODE);
    upc_all_reduceD(&all_total_messages, total_messages, UPC_ADD, THREADS, 1, NULL, SYNC_MODE);

    if( !MYTHREAD )
    {
        print_result((t_end - t_start), all_total_size, size);
    }
}

void print_header(int depth, SIZE_TYPE type)
{
    fprintf(stdout, "Processes: %d \t Op: %s%s\t Depth: %d\t Size-Pattern: %s Dest: %d-%d-%d-%d Behvior: %s\n", \
                    THREADS, \
                    operation_type[op_sequence[0]], \
                    operation_type[op_sequence[1]], \
                    depth, \
                    size_pattern_string[type], \
                    dest_sequence[0 + 24],  \
                    dest_sequence[1 + 24],  \
                    dest_sequence[2 + 24],  \
                    dest_sequence[3 + 24], \
                    behavior);

}

void print_result(double t, double total, int single_msg_size)
{
    double throughput = (total / 1e6) / t;
    fprintf(stdout, "%-*d%*.*f\n", 10, single_msg_size, FIELD_WIDTH,
            FLOAT_PRECISION, throughput);
    fflush(stdout);
}

void timer(double *t)
{
  static int sec = -1;
  struct timeval tv;
  gettimeofday(&tv, (void *)0);
  if (sec < 0) sec = tv.tv_sec;
  *t = (tv.tv_sec - sec)*1.0e+6 + tv.tv_usec;
}

void measure_node_latency(int node_id, latency_element *node_latency)
{
    double t_start, t_end;
    int i;
    int remote_id = ((MYTHREAD / 24 + node_id) % (THREADS / 24)) * 24 + MYTHREAD % 24;

    shared char *node_data = upc_all_alloc(THREADS, 8*2);
    shared [] char *remote_node = (shared [] char *)(data + remote_id);
    char *local_data = ((char *)(data+MYTHREAD)) + 8;

    timer(&t_start);
    for (i = 0; i < loop; i++) {
        upc_memput(remote_node, local_data, 8);
        upc_fence;
    }
    timer(&t_end);

    (node_latency + node_id)->latency = (t_end - t_start)/(1.0 * loop);
    (node_latency + node_id)->rank = node_id * 24;
}


int get_peer_id(int index, int coordinate[])
{
    int peer_id;
    switch (index){
        case 0: /*west*/
            if (coordinate[0] != 0)
                peer_id = MYTHREAD - 1;
            else
                return 0;
            break;
        case 1: /*east*/
            if (coordinate[0] < x_dimension - 1)
                peer_id = MYTHREAD + 1;
            else
                return 0;
            break;
        case 2: /*north*/
            if (coordinate[1] != 0)
                peer_id = MYTHREAD - x_dimension;
            else
                return 0;
            break;
        case 3: /*south*/
            if (coordinate[1] < y_dimension -1)
                peer_id = MYTHREAD + x_dimension;
            else
                return 0;
            break;
        case 4: /*down*/
            if (coordinate[2] != 0)
                peer_id = MYTHREAD - (x_dimension * y_dimension);
            else
                return 0;
            break;
        case 5: /*up*/
            if (coordinate[2] < z_dimension -1)
                peer_id = MYTHREAD + (x_dimension * y_dimension);
            else
                return 0;
            break;
        default:
            if (!MYTHREAD)
                fprintf(stderr, "Type is wrong\n");
    }
    return peer_id;
}

int compare (const void * a, const void * b)
{
    if ( ((latency_element*)a)->latency <  ((latency_element*)b)->latency ) 
        return -1;
    else if ( ((latency_element*)a)->latency ==  ((latency_element*)b)->latency ) 
        return 0;
    else 
        return 1;
}
