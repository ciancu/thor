#include <upc.h>
#include <stdio.h>
#include <upc_nb.h>
#include <string.h>
#include <sys/time.h>
#include <upc_relaxed.h>

#define MAX_MSG_SIZE         (1<<22)
#define SKIP_LARGE  10
#define LOOP_LARGE  100
#define LARGE_MESSAGE_SIZE  8192

int skip = 1000;
int loop = 10000;

#ifndef FIELD_WIDTH
#   define FIELD_WIDTH 20
#endif

#ifndef FLOAT_PRECISION
#   define FLOAT_PRECISION 2
#endif


void wtime(double *t)
{
  static int sec = -1;
  struct timeval tv;
  gettimeofday(&tv, (void *)0);
  if (sec < 0) sec = tv.tv_sec;
  *t = (tv.tv_sec - sec)*1.0e+6 + tv.tv_usec;
}

int main(int argc, char **argv) 
{
    int iters=0;
    double t_start, t_end;
    int peerid = (MYTHREAD + THREADS/2)%THREADS; 
    int iamsender = 0;
    int i;

    shared char *data = upc_all_alloc(THREADS, MAX_MSG_SIZE*2);
    shared [] char *remote = (shared [] char *)(data + peerid);
    char *local = ((char *)(data+MYTHREAD)) + MAX_MSG_SIZE;

    if ( !MYTHREAD ) {
        fprintf(stdout, "# UPC Nonblocking Timer For PUT_NB\n");
        fprintf(stdout, "# [ pairs: %d ]\n", THREADS/2);
        fprintf(stdout, "%-*s%*s\n", 10, "# Size", FIELD_WIDTH, "Latency (us)");
        fflush(stdout);
    }

    for (int size = 1; size <= MAX_MSG_SIZE; size*=2) {

        if(size > LARGE_MESSAGE_SIZE) {
            loop = LOOP_LARGE;
            skip = SKIP_LARGE;
        }

        for (i = 0; i < loop; i++) {
            if(i == 0) {
                wtime(&t_start);
            }

            upc_memput_nbi(remote, local, size);
        }

        wtime(&t_end);
        if( !MYTHREAD )
        {
            double latency = (t_end - t_start)/(1.0 * loop);
            fprintf(stdout, "%-*d%*.*f\n", 10, size, FIELD_WIDTH,
                    FLOAT_PRECISION, latency);
            fflush(stdout);
        }
        upc_synci();
    }

    if ( !MYTHREAD ) {
        fprintf(stdout, "\n\n\n");
        fprintf(stdout, "# UPC Nonblocking Timer For GET_NB\n");
        fprintf(stdout, "# [ pairs: %d ]\n", THREADS/2);
        fprintf(stdout, "%-*s%*s\n", 10, "# Size", FIELD_WIDTH, "Latency (us)");
        fflush(stdout);
    }

    for (int size = 1; size <= MAX_MSG_SIZE; size*=2) {

        if(size > LARGE_MESSAGE_SIZE) {
            loop = LOOP_LARGE;
            skip = SKIP_LARGE;
        }

        for (i = 0; i < loop; i++) {
            if(i == 0) {
                wtime(&t_start);
            }

            upc_memget_nbi(local, remote, size);
        }

        wtime(&t_end);
        if( !MYTHREAD )
        {
            double latency = (t_end - t_start)/(1.0 * loop);
            fprintf(stdout, "%-*d%*.*f\n", 10, size, FIELD_WIDTH,
                    FLOAT_PRECISION, latency);
            fflush(stdout);
        }
        upc_synci();
    }

    if ( !MYTHREAD ) {
        fprintf(stdout, "\n\n\n");
        fprintf(stdout, "# UPC SYNCI For GET\n");
        fprintf(stdout, "# [ pairs: %d ]\n", THREADS/2);
        fprintf(stdout, "%-*s%*s\n", 10, "# Size", FIELD_WIDTH, "Latency (us)");
        fflush(stdout);
    }

    for (int size = 1; size <= MAX_MSG_SIZE; size*=2) {

        if(size > LARGE_MESSAGE_SIZE) {
            loop = LOOP_LARGE;
            skip = SKIP_LARGE;
        }
        for (i = 0; i < loop; i++) {
            upc_memget_nbi(local, remote, size);
        }

        wtime(&t_start);
        upc_synci();
        wtime(&t_end);
        if( !MYTHREAD )
        {
            double latency = (t_end - t_start)/(1.0 * loop);
            fprintf(stdout, "%-*d%*.*f\n", 10, size, FIELD_WIDTH,
                    FLOAT_PRECISION, latency);
            fflush(stdout);
        }
    }

    if ( !MYTHREAD ) {
        fprintf(stdout, "\n\n\n");
        fprintf(stdout, "# UPC Nonblocking Timer For GET_NB and PUT_NB\n");
        fprintf(stdout, "# [ pairs: %d ]\n", THREADS/2);
        fprintf(stdout, "%-*s%*s\n", 10, "# Size", FIELD_WIDTH, "Latency (us)");
        fflush(stdout);
    }

    for (int size = 1; size <= MAX_MSG_SIZE; size*=2) {

        if(size > LARGE_MESSAGE_SIZE) {
            loop = LOOP_LARGE;
            skip = SKIP_LARGE;
        }

        for (i = 0; i < loop; i++) {
            if(i == 0) {
                wtime(&t_start);
            }

            upc_memget_nbi(local, remote, size);
            upc_memput_nbi(remote, local, size);
        }

        wtime(&t_end);
        if( !MYTHREAD )
        {
            double latency = (t_end - t_start)/(1.0 * loop);
            fprintf(stdout, "%-*d%*.*f\n", 10, size, FIELD_WIDTH,
                    FLOAT_PRECISION, latency);
            fflush(stdout);
        }
        upc_synci();
    }

    if ( !MYTHREAD ) {
        fprintf(stdout, "\n\n\n");
        fprintf(stdout, "# UPC Nonblocking Timer For PUT_NB and GET_NB\n");
        fprintf(stdout, "# [ pairs: %d ]\n", THREADS/2);
        fprintf(stdout, "%-*s%*s\n", 10, "# Size", FIELD_WIDTH, "Latency (us)");
        fflush(stdout);
    }

    for (int size = 1; size <= MAX_MSG_SIZE; size*=2) {

        if(size > LARGE_MESSAGE_SIZE) {
            loop = LOOP_LARGE;
            skip = SKIP_LARGE;
        }

        for (i = 0; i < loop; i++) {
            if(i == 0) {
                wtime(&t_start);
            }

            upc_memput_nbi(remote, local, size);
            upc_memget_nbi(local, remote, size);
        }

        wtime(&t_end);
        if( !MYTHREAD )
        {
            double latency = (t_end - t_start)/(1.0 * loop);
            fprintf(stdout, "%-*d%*.*f\n", 10, size, FIELD_WIDTH,
                    FLOAT_PRECISION, latency);
            fflush(stdout);
        }
        upc_synci();
    }
  
    return 0;
}
