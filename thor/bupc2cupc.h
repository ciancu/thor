#ifndef __BUPC_TO_CRAY_COMPATIBILITY__
#define __BUPC_TO_CRAY_COMPATIBILITY__
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include <upc_cray.h>

#ifndef bupc_handle_t  
#define bupc_handle_t upc_handle_t
#endif

//#ifndef __INLINE_SERVER__
#ifndef bupc_memget_async 
#define bupc_memget_async upc_memget_nb
#endif
#ifndef bupc_memput_async  
#define bupc_memput_async upc_memput_nb
#endif
#ifndef bupc_waitsync  
#define bupc_waitsync upc_sync_nb
#endif
#ifndef bupc_waitsync_all
#define bupc_waitsync_all(ph,n) {\
	int i;\
	for(i=0;i<n;i++)\
		upc_sync_nb(ph[i]);\
  }
#endif
//#endif

#ifndef bupc_tick_t 
#define bupc_tick_t upc_tick_t
#endif
#ifndef bupc_ticks_now 
#define bupc_ticks_now upc_ticks_now
#endif
#ifndef bupc_cast 
#define bupc_cast upc_cast 
#endif

#ifndef bupc_trysync 
#define bupc_trysync upc_test_nb 
#endif



#ifndef bupc_ticks_to_us 
#define bupc_ticks_to_us(n) (upc_ticks_to_ns(n)/1000) 
#endif 

typedef struct {
   	void* addr;
    uint32_t phase;
   	int32_t pe;
} cray_pts_t;

inline shared void * bupc_inverse_cast(void * addr) {
	shared void * ret_ptr;
	cray_pts_t ptr;
	ptr.addr = addr;
	ptr.phase = 0;
	ptr.pe = MYTHREAD;
	memcpy(&ret_ptr,&ptr,sizeof(cray_pts_t));
	return ret_ptr;
}


#ifndef BUPC_COMPLETE_HANDLE
#define BUPC_COMPLETE_HANDLE UPC_COMPLETE_HANDLE
#endif

#ifndef bupc_getenv
#define bupc_getenv getenv 
#endif


#endif
