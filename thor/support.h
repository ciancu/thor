#ifndef __SUPPORT_UPC_LIB__
#define __SUPPORT_UPC_LIB__

#include <stdint.h>

void atomic_inc(volatile int * operand);
void atomic_dec(volatile int *operand);
void ** atomic_nexti(void ** array, uint64_t * index, void * mtVal, void * newVal, int iMask);

uint64_t atomic_fsub(volatile int64_t *operand, int64_t incr);
uint64_t atomic_fadd(volatile uint64_t *operand, uint64_t incr);
uint64_t atomic_f_or(volatile uint64_t *operand, uint64_t mask);
uint64_t atomic_f_xor(volatile uint64_t *operand, uint64_t mask);


void ops_delay(int64_t n);
unsigned long long rdtsc(void);
int32_t GCD(int32_t n, int32_t m);
int32_t LCM(int32_t n, int32_t m);
int32_t MSB(uint32_t a);
int imin(int a, int b);
int imax(int a, int b);


uint64_t max_u64(uint64_t a, uint64_t b);
uint64_t min_u64(uint64_t a, uint64_t b);





void mem_bar(void); 


unsigned long long current_time_ns(void);
unsigned long long current_time_us(void);
unsigned current_time_sec(void);

int getcpu(unsigned *cpu, unsigned *node, unsigned long *tcache);

#endif

