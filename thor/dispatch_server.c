#include "support.h"
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <utmpx.h>
#include <sched.h>
#include <errno.h>
#include <semaphore.h>
#include <time.h>
#include "support.h"
#include <upcr.h>

#include "cliserv_dispatch.h"



static service_info_t * sinfo;
static int cycles_per_us;
extern int thor_enabled;
extern int thor_physical_nodes;
extern int thor_servers_per_domain;
extern int thor_remap_enabled;
extern int thor_domains_per_node;
extern int thor_domain_count; 
extern void* thor_upc_cast(upcr_shared_ptr_t);
extern service_info_t *get_server_info(int idx);
extern server_control_t *get_scontrol(int, int, int);
domain_queue_t *get_domq(int did, int slot,int sid);
extern domain_queue_t * my_domain_queues;
extern domain_server_t *my_domain_servers;
extern supc_thread_info_t my_info;

extern int GET_MYTH();
extern int GET_TH();
#if CRAY_UPC
#include "supc.h"
#include "bupc2cupc.h"
#define HOPPER_CRAY 1
#define BUPC_PTHREADS_ARG 
#define BUPC_PTHREADS_PASS

#else
/*
#define bupc_memget_async _bupc_memget_async
#define bupc_memput_async _bupc_memput_async
#define bupc_memput_vlist_async  _bupc_memput_vlist_async
#define bupc_memget_vlist_async  _bupc_memget_vlist_async
#define bupc_trysync _bupc_trysync


*/

#define bupc_cast wrap_upc_cast

typedef  struct {
  void *addr;
  size_t len;
} smemvec_t;

typedef  struct {
  void *addr;
  size_t len;
} lmemvec_t;


#if ENABLE_PTHRDS
#define  BUPC_PTHREADS_ARG_ALONE  void *
#define BUPC_PTHREADS_ARG , BUPC_PTHREADS_ARG_ALONE  
#define BUPC_PTHREADS_PASS_ALONE sinfo->upcr_parent_info
#define BUPC_PTHREADS_PASS , BUPC_PTHREADS_PASS_ALONE
#else
#define BUPC_PTHREADS_ARG 
#define BUPC_PTHREADS_PASS
#endif

#if 1
  #define MY_INLINE
#else
  #define MY_INLINE inline
#endif

/*
uint64_t bupc_memput_async(void * s_addr, void * l_addr, size_t size BUPC_PTHREADS_ARG);
uint64_t bupc_memget_async(void * l_addr, void * s_addr, size_t size BUPC_PTHREADS_ARG);
uint64_t bupc_memput_vlist_async(size_t dstcount, smemvec_t const dstlist[], 
                                 size_t srccount, lmemvec_t const srclist[] BUPC_PTHREADS_ARG);
uint64_t bupc_memget_vlist_async(size_t dstcount, lmemvec_t const dstlist[], 
                                 size_t srccount, smemvec_t const srclist[] BUPC_PTHREADS_ARG);
int _bupc_trysync(uint64_t BUPC_PTHREADS_ARG);
*/

#if PSHM_SUPPORT
void * wrap_upc_cast(void *sptr);
#endif



#endif


#if UPC_SEMAPHORE
void wrap_bupc_sem_post(volatile sbupc_sem_t *);
void wrap_bupc_sem_wait(volatile sbupc_sem_t *);
#endif



inline int find_msg_credits(int msg_size)
{
	int msg_index = imin(imax(0,MSB(msg_size) - MESSAGE_MIN_LEVEL),MESSAGE_SIZE_LEVELS - 1);
	return sinfo->msg_credits[msg_index];
}

inline int32_t find_msg_service(int msg_size) 
{
	return msg_size/5000+12;
}


inline int server_check_req_completion(mem_req_t *mem_req, int req_id) 
{
	// Condition for an old request that was retired
	// Note we are checking for wrap around effect
	// the maximum difference in request id should be REQ_QUEUE_LENGTH*THREADS
	// if greater than  maximum this means we wrapped around.
	if(mem_req->req_id>req_id)
			return 1;
	if((req_id - mem_req->req_id) > 0xffffff)
			return 2;
	/* Only the request and the immediately next waiter can be released */
	if (mem_req->req_state == r_completed) {
		if(mem_req->req_id == req_id)
			return 3;
		if(mem_req->ready_to_serve == req_id)
			return 4;
		return 5;
	}

	return 0;
}


/* fire up all my servers, mark myself active */
void thor_windup_comm() 
{
    int i;
    server_control_t *one_server;
    for (i = 0; i < thor_servers_per_domain; i++) {
        one_server = my_domain_servers->server+i;
        atomic_inc(&one_server->clients_active);
#if UPC_SEMAPHORE
        wrap_bupc_sem_post(one_server->pservice_sem);
#else
        ASSERTZ(sem_post((sem_t*)&one_server->service_sem));
#endif

    }
}


void thor_winddown_comm() 
{
    int i;
    server_control_t *one_server;
    for (i = 0; i < thor_servers_per_domain; i++) {
        one_server = my_domain_servers->server+i;
        atomic_dec(&one_server->clients_active);
    }
}

void thor_killall() 
{
    int i;
    server_control_t *one_server;
    for (i = 0; i < thor_servers_per_domain; i++) {
        one_server = my_domain_servers->server+i;
        one_server->command = terminate;
#if UPC_SEMAPHORE
        wrap_bupc_sem_post(one_server->pservice_sem);
#else
        ASSERTZ(sem_post((sem_t*)&one_server->service_sem));
#endif
    }
}


/////////////////////////////// CLIENT SIDE ///////////////////////
////// Code that does not compile correctly with upcc (ignores volatile on struct fields) //////////////////////

inline void init_peer_queue(domain_queue_t *domain_queue, int id) 
{ 
    printf("[%d] init_peer_queue id %d.\n", GET_MYTH(), id);
    if(id == IMMEDIATE_QUEUE) {
        printf("[%d] creating IMMEDIATE queue.\n", GET_MYTH());
    }
	peer_queue_t * pqueue = domain_queue->queue+id;
	pqueue->head = REQ_QUEUE_LENGTH;
	pqueue->id = id;
    printf("[%d] id of the queue at %p is now %d.\n", GET_MYTH(), pqueue, pqueue->id);
	pqueue->tail_issued = pqueue->tail_completed = REQ_QUEUE_LENGTH-1;
	domain_queue->queue_hits[id]=0;
	pqueue->avail_credit = domain_queue->total_credit;
	int i;
	for(i=0;i<REQ_QUEUE_LENGTH;i++) {
        mem_req_t * req =   pqueue->mem_request+i;
		req->s_addr         = (upcr_shared_ptr_t)UPCR_NULL_SHARED;
        req->l_addr         = (upcr_shared_ptr_t)UPCR_NULL_SHARED;
        req->size           = 0;
        req->req_type       = 0;
        req->h              = 0;
        req->req_timestamp  = 0;
        req->service_time   = 0;
        req->req_id         = 0;
        req->ready_to_serve = i+REQ_QUEUE_LENGTH;
        req->req_state      = r_completed;
	}
}


inline void synthsize_handle(bupc_handle_t handle, uint32_t * queue, uint32_t * req_id)
{
	uint64_t h = (uint64_t) handle;
	*queue = (uint32_t) (h >> 32);
	*req_id = (uint32_t)(h & REQ_MASK);
}

inline int client_check_req_complete(mem_req_t *mem_req, int req_id)
{
  return (mem_req->req_state == r_completed) ? 1 : 0;
}


inline void wait_memslot_complete(supc_thread_info_t *pinfo,server_control_t * my_server, peer_queue_t * my_peer_queue, uint32_t req_id)
{

  int queue_num = my_peer_queue->id;
#if BYPASS_LOCAL_DOMAIN_MESSAGES
  if(queue_num == MYDOMAIN_REQ_QUEUE)
    return;
#endif
  int slot = req_id % REQ_QUEUE_LENGTH;
  mem_req_t * mem_req = my_peer_queue->mem_request + slot;
  int cid = pinfo->client_id;
   uint64_t mask = ((uint64_t)1) << cid;
   my_server->waiting_client[cid].req_id = req_id;
   
  my_server->waiting_client[cid].queue_num = queue_num;
  atomic_f_or((uint64_t *)&(my_server->waiting_bitmap),mask);
  
  //printf("%d: Client waiting at %d : %d\n", GET_MYTH(), slot, queue_num);
  while(!client_check_req_complete(mem_req, req_id));
  //  printf("%d : SET WAITBITMAP %d\n", GET_MYTH(), my_server->waiting_bitmap);
  atomic_f_xor((uint64_t *)&(my_server->waiting_bitmap),mask);
  my_peer_queue->issue_tokens--;
  //printf("%d : RESET WAITBITMAP %d\n", GET_MYTH(), my_server->waiting_bitmap);
#if 0
  printf("%d: Received server notification: queue_number: %d, req_id %d, domain %d, client %d state %d\n", GET_MYTH(),
	 queue_num, req_id, pinfo->domain_id,pinfo->client_id, mem_req->req_state);
#endif
return;
  
}


inline void client_request_completion(bupc_handle_t h) 
{
	uint32_t queue_id, req_id;
	synthsize_handle(h, &queue_id, &req_id);
	peer_queue_t * my_peer_queue = my_domain_queues->queue+queue_id;
	//server_control_t * my_server = my_domain_servers->server+ my_peer_queue->my_server_id;
	server_control_t * my_server = my_domain_servers->server+queue_id;
	
	//printf("REQUEST COMPLETE \n");
	if(queue_id == IMMEDIATE_QUEUE) {
	  bupc_waitsync((bupc_handle_t)my_peer_queue->mem_request[req_id%REQ_QUEUE_LENGTH].h);
	} else {
#ifdef SIMPLE_RR
	  wait_memslot_complete(&my_info,my_server, my_peer_queue, req_id);
#else 
	  wait_memslot_ready(&my_info,my_server, my_peer_queue, req_id);
#endif
	}
}

void sbupc_waitsync(bupc_handle_t h) 
{
  //printf("WAITSYNC CALLED %p\n", h);
  if(h != BUPC_COMPLETE_HANDLE)
    client_request_completion(h);
}



inline int client_check_req_available(mem_req_t *mem_req, int req_id)
{
  // Condition for an old request that was retired                                                                             
  // Note we are checking for wrap around effect                                                                               
  // the maximum difference in request id should be REQ_QUEUE_LENGTH*THREADS                                                   
  // if greater than  maximum this means we wrapped around.                                                                    
  
  /* Only the request and the immediately next waiter can be released */
  
  if (mem_req->req_state == r_completed) {
    if(mem_req->req_id == req_id)
      return 3;
    if(mem_req->ready_to_serve == req_id)
      return 4;
    return 5;
  }

  return 0;
}


inline void wait_memslot_available(supc_thread_info_t *pinfo,server_control_t * my_server, peer_queue_t * my_peer_queue, uint32_t req_id)
{
  int queue_num = my_peer_queue->id;
#if BYPASS_LOCAL_DOMAIN_MESSAGES
  if(queue_num == MYDOMAIN_REQ_QUEUE)
    return;
#endif

#if 0
  printf("%d: Enter spin for availability: queue_number: %d, req_id %d, domain %d, client %d\n", GET_MYTH(),
	 queue_num, req_id, pinfo->domain_id,pinfo->client_id);
#endif
  int slot = req_id % REQ_QUEUE_LENGTH;
  mem_req_t * mem_req = my_peer_queue->mem_request + slot;
  int cid = pinfo->client_id;
  uint64_t mask = ((uint64_t)1) << cid;
  /*my_server->waiting_client[cid].req_id = req_id;
  my_server->waiting_client[cid].queue_num = queue_num;
  */
  //  printf("%d : STATE is %d \n",GET_MYTH(), mem_req->req_state);
  
  //atomic_f_or((uint64_t *)&(my_server->waiting_bitmap),mask);
  while(!client_check_req_available(mem_req, req_id));
  //atomic_f_xor((uint64_t *)&(my_server->waiting_bitmap),mask);
#if 0
  printf("%d : Received availability:: queue_number: %d, req_id %d, domain %d, client %d state %d\n", GET_MYTH(),
	 queue_num, req_id, pinfo->domain_id,pinfo->client_id, mem_req->req_state);
#endif  
  return;
}


inline int64_t create_handle(uint32_t queue, uint32_t req_id)
{
	uint64_t handle = ((req_id)| ((uint64_t)queue<<32));
#if CONCURRENCY_DEBUG	
	assert(handle != ((uint64_t)BUPC_COMPLETE_HANDLE));
#endif
	return handle;
}


inline bupc_handle_t enqueue_request(peer_queue_t *my_queue, void  *l_addr, upcr_shared_ptr_t  s_addr, size_t size, req_type_t type)
{
	int queue_num = my_queue->id;
	int flip = 0;
	static int count = 0;
	uint32_t req_id = REQ_MASK & atomic_fadd((uint64_t *)&my_queue->head,1);
	int slot = req_id % REQ_QUEUE_LENGTH;
	mem_req_t * my_mem_req = my_queue->mem_request+slot;
    //printf("[%d] enqueue_request %d in slot %d of queue %d (server %d).\n", GET_MYTH(), req_id, slot, my_queue->id, my_queue->my_server_id);
	//server_control_t * my_server = my_domain_servers->server+ my_queue->my_server_id;
	server_control_t * my_server = my_domain_servers->server+queue_num;
	//sprintf(my_mem_req->pad, "START %d %d %d -- %d", GET_MYTH(), slot, req_id, count++); 
	if(my_queue->issue_tokens == REQ_QUEUE_LENGTH) {
	  upcri_err("Queue [%d] is full, need to call waitsync before issuing... \n", my_queue->id);
	}
	my_queue->issue_tokens++;

#ifdef SIMPLE_RR
  // >>> I think this blocks if the queue is full
	wait_memslot_available(&my_info,my_server, my_queue, req_id);
#else 
	wait_memslot_ready(&my_info,my_server, my_queue, req_id);
#endif
  #if CONCURRENCY_DEBUG
	assert(upc_threadof(s_addr) != MYTHREAD);
  #endif


	my_mem_req->s_addr = s_addr;
	THOR_RESET_REMAP(flip);
	my_mem_req->l_addr = _bupc_inverse_cast(l_addr);
	THOR_RESTORE_REMAP(flip);
	
	my_mem_req->size = size;
	my_mem_req->req_id = req_id;
	my_mem_req->req_type = type;
#ifndef SIMPLE_RR
	my_mem_req->req_timestamp = rdtsc();
#endif

#if	CONCURRENCY_DEBUG
	assert(my_mem_req->req_id == my_mem_req->ready_to_serve);
	atomic_fadd(&my_queue->issue_tokens,1);
#endif

	my_mem_req->req_state = r_pending;
	mem_bar();
#if 0
	if(queue_num != MYDOMAIN_REQ_QUEUE) {
	  printf("%d : Issuing [%d]  at %d - %d: %p\n", GET_MYTH(), my_queue->id, slot, req_id, my_queue);
		  //printf("ENQ_REQ (t %d):  %p, size %d type %d queue %d: VAL = %d\n", upcr_mythread(), l_addr,  (int)size, type, queue_num, *((int*)l_addr));
	  /* upcri_print_shared(my_mem_req->s_addr);
	     upcri_print_shared(my_mem_req->l_addr); */
	  }
	
#endif

	return (bupc_handle_t) create_handle(queue_num,req_id);
}

extern int rrQueueIndex;
extern int mpq;
extern int rrQueuePos;
extern int my_rr_queue_seq[QUEUE_COUNT];
extern int rrQueueNum;

inline int rr_find_queue()
{
/* the code assumes 1 client per domain
 and REQ_IMM <= REQ_BATCH */
  // static int do_local = 0;

  if(mpq == REQ_BATCH) {
    //printf("%d : ADVANCE QUEUE %d -> %d\n", GET_MYTH(), rrQueueIndex,  my_rr_queue_seq[ (rrQueuePos+1)%rrQueueNum]);
    rrQueuePos = (rrQueuePos+1)%rrQueueNum;
    rrQueueIndex = my_rr_queue_seq[rrQueuePos];
    mpq = 0;
  } else mpq += 1;
  
 
  return rrQueueIndex;
}


/* There is no flow control here - it wraps around without checks ... */
inline bupc_handle_t issue_put_immediate(peer_queue_t* my_queue, void *l_addr, upcr_shared_ptr_t s_addr, size_t size) 
{
  int queue_num = my_queue->id;
  int flip = 0;
  
  uint32_t req_id = REQ_MASK & my_queue->head;
  my_queue->head++;
  int slot = req_id % REQ_QUEUE_LENGTH;
  mem_req_t * my_mem_req = my_queue->mem_request+slot;  
  my_mem_req->h = (int64_t)bupc_memput_async(s_addr, l_addr, size BUPC_PTHREADS_PASS);
  //printf("%d: IMMEDIATE put %d - %d [%p]\n",GET_MYTH(), queue_num, slot, my_mem_req->h); 
  return (bupc_handle_t) create_handle(queue_num,req_id);
	
}

inline bupc_handle_t issue_get_immediate(peer_queue_t* my_queue, void *l_addr, upcr_shared_ptr_t s_addr, size_t size) 
{
  int queue_num = my_queue->id;
  int flip = 0;
  
  uint32_t req_id = REQ_MASK & my_queue->head;
  my_queue->head++;
  int slot = req_id % REQ_QUEUE_LENGTH;
  mem_req_t * my_mem_req = my_queue->mem_request+slot;  
  my_mem_req->h = (int64_t)bupc_memget_async(l_addr, s_addr, size BUPC_PTHREADS_PASS);
  //printf("%d: IMMEDIATE get %d - %d [%p]\n",GET_MYTH(), my_queue->id, slot, my_mem_req->h); 
  return (bupc_handle_t) create_handle(queue_num,req_id);
}

inline bupc_handle_t add_put_request(void *l_addr, upcr_shared_ptr_t  s_addr, size_t size)
{
	int queue_num;
#if CHECK_CPU_AFFINITIES
  unsigned cpuNum, nodeNum;
  getcpu(&cpuNum, &nodeNum, 0);
  printf("Client @ %d:%d on core %u:%u\n", my_info.domain_id, my_info.client_id, nodeNum, cpuNum);
#endif

#ifdef SIMPLE_RR
  queue_num = rr_find_queue();  
#else 
#endif
  peer_queue_t * my_queue = my_domain_queues->queue+queue_num;
#if BYPASS_LOCAL_DOMAIN_MESSAGES
	if(my_queue->id == MYDOMAIN_REQ_QUEUE) {
	  // I should not wait for the server to do it for me
	  upcr_memput(s_addr,l_addr, size);
	  return BUPC_COMPLETE_HANDLE;
	}
#endif
//#ifdef _UPC_BYPASS_LARGE
	if (size > 8192) {
	  queue_num = IMMEDIATE_QUEUE;
	  my_queue = my_domain_queues->queue+queue_num;
	}
//#endif

	if(my_queue->id == IMMEDIATE_QUEUE) {
	  return issue_put_immediate(my_queue, l_addr, s_addr, size);
	} 

	return enqueue_request(my_queue, l_addr, s_addr, size, q_memput);
}



inline bupc_handle_t add_get_request(void *l_addr, upcr_shared_ptr_t  s_addr, size_t size)
{
	int queue_num;


#if CHECK_CPU_AFFINITIES

  unsigned cpuNum, nodeNum;
  getcpu(&cpuNum, &nodeNum, 0);
  printf("Client @ %d:%d on core %u:%u\n", my_info.domain_id, my_info.client_id, nodeNum, cpuNum);
#endif

#define SIMPLE_RR
#ifdef SIMPLE_RR
  queue_num = rr_find_queue();  
#else 
  
#endif
  peer_queue_t * my_queue = my_domain_queues->queue+queue_num;
#if BYPASS_LOCAL_DOMAIN_MESSAGES
	if(my_queue->id == MYDOMAIN_REQ_QUEUE) {
	  // I should not wait for the server to do it for me
	  upcr_memget(l_addr,s_addr, size);
	  return BUPC_COMPLETE_HANDLE;
	}
#endif

//#ifdef _UPC_BYPASS_LARGE
	if (size > 8192) {
	  queue_num = IMMEDIATE_QUEUE;
	  my_queue = my_domain_queues->queue+queue_num;
	}
//#endif
	
	if(my_queue->id == IMMEDIATE_QUEUE) {
	  //	  printf("%d : SHOULD ISSUE inline\n", GET_MYTH());
	  return issue_get_immediate(my_queue, l_addr, s_addr, size);
	} 
	return enqueue_request(my_queue, l_addr, s_addr, size, q_memget);
}


inline int thor_is_shared(const void *p)
{
  int flip=0;
  int res;
  THOR_RESET_REMAP(flip);
  res = upcr_isnull_shared(_bupc_inverse_cast(((void *)p)));
  THOR_RESTORE_REMAP(flip);
  return !res;
}

bupc_handle_t sbupc_memget_async(void *  dst, upcr_shared_ptr_t  src, size_t n)
{
	if(!thor_is_shared(dst)) {
		upcr_memget(dst,src,n);
		return BUPC_COMPLETE_HANDLE;
	}
	return add_get_request(dst, src, n);
}

bupc_handle_t sbupc_memput_async(upcr_shared_ptr_t dst, const void * src, size_t n)
{
	if(!thor_is_shared(src)) {
		upcr_memput(dst,src,n);
		return BUPC_COMPLETE_HANDLE;
	}
	return add_put_request((void *)src,dst, n);
}



//////////////////// Really the server the side ////////////////////////////////
void server_wait_timed(sem_t *sem, int microsec) 
{
	struct timespec ts;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts);
    ts.tv_nsec += microsec*1000;
 	sem_timedwait(sem, &ts);
}


#if UPC_SEMAPHORE
MY_INLINE void eat_token(sbupc_sem_t *sem) {
	wrap_bupc_sem_wait(sem);
}	
#else
MY_INLINE void eat_token(sem_t *sem) {
	ASSERTZ(sem_wait(sem));
}

#endif



#if 1
MY_INLINE int issue_coalesced_request(server_control_t * scontrol,peer_queue_t *inservice_queue) 
{
	printf("coalesced message optimization are not supported on Cray UPC\n");
	exit(0);
}

#else
MY_INLINE int issue_coalesced_request(server_control_t * scontrol,peer_queue_t *inservice_queue) 
{
  smemvec_t put_dstlist[REQ_QUEUE_LENGTH];
  lmemvec_t put_srclist[REQ_QUEUE_LENGTH];
  int put_indeces[REQ_QUEUE_LENGTH];
  size_t put_count=0;
  lmemvec_t get_dstlist[REQ_QUEUE_LENGTH];
  smemvec_t get_srclist[REQ_QUEUE_LENGTH];
  int get_indeces[REQ_QUEUE_LENGTH];
  size_t get_count=0;
  void *l_addr; 
  
  while(inservice_queue->head - 1 > inservice_queue->tail_issued)	{
    int index  = (inservice_queue->tail_issued+1) % REQ_QUEUE_LENGTH;
    // There is a possible race that the head is 
		// updated and the data are not filled in. 
    mem_req_t * mem_req = inservice_queue->mem_request + index;
    if(mem_req->req_state == r_pending) {
      if(mem_req->req_type == q_memput) {
	put_dstlist[put_count].addr = mem_req->s_addr; 
	put_dstlist[put_count].len = mem_req->size;
#if PSHM_SUPPORT	
	put_srclist[put_count].addr = bupc_cast(mem_req->l_addr); 
#else
	put_srclist[put_count].addr = mem_req->l_addr; 
#endif
	put_srclist[put_count].len = mem_req->size;
	put_indeces[put_count] = index;
	put_count++;
      } else {
#if PSHM_SUPPORT	
	get_dstlist[get_count].addr = bupc_cast(mem_req->l_addr); 
#else
	get_dstlist[get_count].addr = mem_req->l_addr; 
#endif
	get_dstlist[get_count].len = mem_req->size;
	get_srclist[get_count].addr = mem_req->s_addr; 
	get_srclist[get_count].len = mem_req->size;
	get_indeces[get_count] = index;
	get_count++;
      }
      mem_req->req_state = r_issued;
      // eat one token at the issue stage
#if	CONCURRENCY_DEBUG
      atomic_fadd(&inservice_queue->server_issue_tokens,1);
#endif
      inservice_queue->tail_issued++;
    }
    else 
      break;	
	}
  uint64_t put_handle, get_handle;
  if(put_count)	
    put_handle = bupc_memput_vlist_async(put_count, put_dstlist, 
					 put_count, put_srclist BUPC_PTHREADS_PASS);
  if(get_count)
    get_handle = bupc_memget_vlist_async(get_count, get_dstlist, 
					 get_count,get_srclist BUPC_PTHREADS_PASS);
  int i;
  for(i=0;i<put_count;i++) {
    mem_req_t * mem_req = inservice_queue->mem_request + put_indeces[i];
    mem_req->h = put_handle;
#if UPC_SEMAPHORE
    //eat_token(scontrol->pservice_sem);
#else
    //eat_token(&(scontrol->service_sem));
#endif
  }
  for(i=0;i<get_count;i++) {
    mem_req_t * mem_req = inservice_queue->mem_request + get_indeces[i];
    mem_req->h = get_handle;
#if UPC_SEMAPHORE
    //eat_token(scontrol->pservice_sem);
#else
    //eat_token(&(scontrol->service_sem));
#endif
  }
  return get_count+put_count;
}
#endif


MY_INLINE int issue_request(server_control_t * scontrol, peer_queue_t *inservice_queue, issue_policy_t policy, int param)
{
  int issued = 0;
  uint64_t current_timestamp;
  int count = REQ_QUEUE_LENGTH;
  int aging_period = 0;
  void *l_addr;
  int non_pending = 0;
  count = param;

  //  printf("%d ISSUE REQUEST\n", GET_MYTH());
  while(inservice_queue->head - 1 > inservice_queue->tail_issued)	{
    int index  = (inservice_queue->tail_issued+1) % REQ_QUEUE_LENGTH;

    // There is a possible race that the head is 
    // updated and the data are not filled in. 
    mem_req_t * mem_req = inservice_queue->mem_request + index;
    //    printf(">>>> Serving request %d, queue %d, server %d\n",
    //           mem_req->req_id, inservice_queue->id,scontrol->server_id);
#ifndef SIMPLE_RR    
    if(policy == only_aged) {
      current_timestamp = rdtsc();
      if((current_timestamp - mem_req->req_timestamp) >= param)
	break;
    }
#endif 
    if(mem_req->req_state == r_pending) {
#ifndef SIMPLE_RR
      if(policy == credit) {
	int needed_credits = find_msg_credits(mem_req->size);
#if VERBOSE_OUTPUT
	printf("Enqueue request %d, queue %d, server %d, credits %d, avail %d\n",
	       mem_req->req_id, inservice_queue->id,scontrol->server_id,
	       needed_credits,inservice_queue->avail_credit);
#endif
	if(inservice_queue->avail_credit<needed_credits) 
	  break;
	// only the server has access to avail_credit
	inservice_queue->avail_credit -= needed_credits;
      }	
#endif 
#if _VERBOSE_OUTPUT
      if(policy != credit)
	printf("Enqueue request %d, queue %d, server %d\n",
	       mem_req->req_id, inservice_queue->id,scontrol->server_id);
#endif
      
#if PSHM_SUPPORT		
      l_addr = thor_upc_cast(mem_req->l_addr);
#else 
#error "Only PSHM supported..."
#endif

#if 0
      printf("%d ISS_REQ (queue %d, server %d, THREAD %d): Rid %d: Mem Req s_add %llx l_add %llx size %lld: VAL = %d\n", GET_MYTH(),
	     inservice_queue->id,scontrol->server_id, GET_MYTH(), mem_req->req_id,mem_req->s_addr,l_addr,mem_req->size, *((int*)l_addr) );
      wrap_p_s(mem_req->s_addr);
#endif
#if 0

      if(mem_req->req_type == q_memput)
	mem_req->h = BUPC_COMPLETE_HANDLE; //bupc_memput_async(mem_req->s_addr,l_addr,mem_req->size BUPC_PTHREADS_PASS);
      else
	mem_req->h = BUPC_COMPLETE_HANDLE; //bupc_memget_async(l_addr,mem_req->s_addr,mem_req->size BUPC_PTHREADS_PASS);
#else
      if(mem_req->req_type == q_memput) {
	mem_req->h = (int64_t)bupc_memput_async(mem_req->s_addr,l_addr,mem_req->size BUPC_PTHREADS_PASS);
      } else
	mem_req->h = (int64_t)bupc_memget_async(l_addr,mem_req->s_addr,mem_req->size BUPC_PTHREADS_PASS);
#endif
#if _VERBOSE_OUTPUT
      printf("Rid %d issued: Mem Req s_add %llx l_add %llx size %lld\n",
	     mem_req->req_id,mem_req->s_addr,l_addr,mem_req->size);
#endif
#ifndef SIMPLE_RR
      if(policy == timed) {
	mem_req->req_timestamp = rdtsc();
	mem_req->service_time = find_msg_service(mem_req->size); 
      }
#endif      
      mem_req->req_state = r_issued;
      
      //printf("request issued: index %d handle: %llx\n", index, mem_req->h);
#if	CONCURRENCY_DEBUG
      atomic_fadd(&inservice_queue->server_issue_tokens,1);
#endif
      inservice_queue->tail_issued++;
      issued++;
      if(issued >= count) 
	break;
    }
    else {
      non_pending++;
      if(non_pending == 4)
	break;
      else 
	continue;
    }	
  }
  return issued;
}


MY_INLINE int issue_request_rr(server_control_t * scontrol, peer_queue_t *inservice_queue, issue_policy_t policy, int param)
{
  int issued = 0;
  uint64_t current_timestamp;
  int count = REQ_QUEUE_LENGTH;
  int aging_period = 0;
  void *l_addr;
  int non_pending = 0;
  count = param;
  static int reps=0;

  while(inservice_queue->head - 1 > inservice_queue->tail_issued)	{
    int index  = (inservice_queue->tail_issued+1) % REQ_QUEUE_LENGTH;
    if (inservice_queue->head == inservice_queue->tail_issued && inservice_queue->head == 0)
      index = inservice_queue->head;
    // There is a possible race that the head is 
    // updated and the data are not filled in. 
    mem_req_t * mem_req = inservice_queue->mem_request + index;

        if(reps < 24 /*|| GET_MYTH() == 2 || GET_MYTH() == 5*/) {
	  //            printf("%d: [%d][%d] (%d, %d) IR %s | %s\n", GET_MYTH(), inservice_queue->id, index, (int)inservice_queue->head, (int)inservice_queue->tail_issued,  mem_req->pad, inservice_queue->xxxpadding0);
      reps += 1;
      }
    
    if(mem_req->req_state == r_pending) { 
      
#if PSHM_SUPPORT		
      l_addr = thor_upc_cast(mem_req->l_addr);
#else 
#error "Only PSHM supported..."
#endif

#if 0
      printf("%d ISS_REQ (queue %d, server %d, THREAD %d): Rid %d: LOCAL %p Mem Req size %lld: VAL = %d\n", GET_MYTH(),
	     inservice_queue->id,scontrol->server_id, GET_MYTH(), mem_req->req_id, l_addr, mem_req->size, *((int*)l_addr) );
      upcri_print_shared(mem_req->s_addr);
      upcri_print_shared(mem_req->l_addr);
#endif
#if 0

      if(mem_req->req_type == q_memput)
	mem_req->h = BUPC_COMPLETE_HANDLE; 
      else
	mem_req->h = BUPC_COMPLETE_HANDLE; 
#else
      if(mem_req->req_type == q_memput) {
        mem_req->h = (int64_t)bupc_memput_async(mem_req->s_addr,l_addr,mem_req->size BUPC_PTHREADS_PASS);
        //printf("[%d] Issued memput queue %d mem_req %d handle %lld.\n", GET_MYTH(), inservice_queue->id, mem_req->req_id, mem_req->h);
      } else
        mem_req->h = (int64_t)bupc_memget_async(l_addr,mem_req->s_addr,mem_req->size BUPC_PTHREADS_PASS);
        //printf("[%d] Issued memget queue %d mem_req %d handle %lld.\n", GET_MYTH(), inservice_queue->id, mem_req->req_id, mem_req->h);
#endif
#if _VERBOSE_OUTPUT
      printf("Rid %d issued: Mem Req s_add %llx l_add %llx size %lld\n",
	     mem_req->req_id,mem_req->s_addr,l_addr,mem_req->size);
#endif

      mem_req->req_state = r_issued;
      
#if	CONCURRENCY_DEBUG
      atomic_fadd(&inservice_queue->server_issue_tokens,1);
#endif
      inservice_queue->tail_issued++;
      issued++;
      if(issued >= count) 
	break;
    } // state = pending
    else {
      non_pending++;
      if(non_pending == 4)
	break;
      else 
	continue;
    }	
  }
  return issued;
}


/*
 *  This should have a non-blocking semantic. 
 *  return 0 if no pending requests
 */

MY_INLINE int check_queue_completion(peer_queue_t *inservice_queue, int * queue_hits, int dispatch_policy)
{
	int update_tail = 1;
	int64_t tail_checked = inservice_queue->tail_completed;
	//printf("%d: CHECKING QUEUES: %d <-> %d \n", GET_MYTH(), tail_checked, inservice_queue->tail_issued);
	if(tail_checked == inservice_queue->tail_issued && tail_checked == REQ_QUEUE_LENGTH) {
	  upcri_err("Client issued more requests than queue slots, should call waitsync : %d %d \n", tail_checked, inservice_queue->tail_issued);
	}
	while (tail_checked<inservice_queue->tail_issued || (tail_checked == 0 && inservice_queue->tail_issued ) ) {

		int slot = (++tail_checked)%REQ_QUEUE_LENGTH;
		mem_req_t * mem_req = inservice_queue->mem_request + slot;
		//printf("%d: CQ %s\n", GET_MYTH(), mem_req->pad);
		if(mem_req->req_state == r_issued) {
			int completed;
			if(dispatch_policy == THROTTLE_WAITSYNC_DISPATCH) {
				uint64_t completion_cycle = mem_req->req_timestamp + mem_req->service_time * cycles_per_us; 
				uint64_t current_cycle = rdtsc();
				if(current_cycle >= completion_cycle)
				  completed = bupc_trysync((bupc_handle_t)mem_req->h BUPC_PTHREADS_PASS);
				else completed = 0;
			} else {
              //printf("[%d] trysync queue %d mem_req %d handle %lld.\n", GET_MYTH(), inservice_queue->id, mem_req->req_id, mem_req->h);
			  completed = bupc_trysync((bupc_handle_t)mem_req->h BUPC_PTHREADS_PASS);
			}
			if(completed) {
				mem_req->req_state = r_completed;
				//	printf("%d: COMPLETING  at %d : %d\n", GET_MYTH(), slot, inservice_queue->id);
				mem_bar();
#ifndef SIMPLE_RR
				if(dispatch_policy == WEIGHTED_CREDIT_DISPATCH) {
					int consumed_credits = find_msg_credits(mem_req->size);
					inservice_queue->avail_credit += consumed_credits;
				}
#endif
				mem_req->ready_to_serve += REQ_QUEUE_LENGTH;
				*queue_hits++;
				if(update_tail)
					inservice_queue->tail_completed = tail_checked;
			} else {
			  /*	struct timespec req, rem;
			    req.tv_sec = 0;
			    req.tv_nsec = 6000;
			    if(nanosleep(&req, &rem) < 0)
			    printf("Nano sleep system call failed \n");*/
			  update_tail = 0;
			}
		}
	}
	return (inservice_queue->head -1 > inservice_queue->tail_completed);
}

int service_request(server_control_t * scontrol, int thr_per_domain, domain_queue_t *domain_queue)
{
  int i;
  int not_served = 0;
  uint64_t wait_bitmap;
  uint64_t mask;
  int pending = 0;
  static int count = 0;
  while(scontrol->clients_active) {
    if(count < 16) {
      //            printf("%d SERVICE REQUEST\n", GET_MYTH());
      count+=1;
    }
    // issue only, do not check for completion
    for(i=scontrol->start_queue;i<scontrol->end_queue;i++) {
      peer_queue_t *inservice_queue = domain_queue->queue+i;
      //      printf("%d: SANITY CHECK %d || %s\n", GET_MYTH(), i, (domain_queue->queue+i)->xxxpadding0);
#ifdef SIMPLE_RR
      // printf("SERVER ISSUE REQUEST\n");
      issue_request_rr(scontrol,inservice_queue, fixed_count, 1);
#else 
      if (scontrol->server_dispatch_policy == ROUND_ROBIN_DISPATCH)
	issue_request(scontrol,inservice_queue, fixed_count, 1);
      else if (scontrol->server_dispatch_policy == WEIGHTED_CREDIT_DISPATCH)
	issue_request(scontrol,inservice_queue, credit, 1);
      else if(scontrol->server_dispatch_policy == THROTTLE_WAITSYNC_DISPATCH)
	issue_request(scontrol,inservice_queue, timed, 1);
      else if(scontrol->server_dispatch_policy == COALESCE_MESSAGE_DISPATCH) 
	issue_coalesced_request(scontrol, inservice_queue); 
      else
	issue_request(scontrol,inservice_queue, all, 0);
#endif
    }
    
    static int count1 = 0;
    if(count < 100000) {
      //printf("%d : STATE %d \n", GET_MYTH(), scontrol->waiting_bitmap);
      count++;
    }
    // check for completion if needed
    if(scontrol->waiting_bitmap) {
      for(i=0;i<thr_per_domain;i++) {
	mask = ((uint64_t)1) << i;
	wait_bitmap = scontrol->waiting_bitmap;
	if((mask&wait_bitmap) == 0)
	  continue;
	client_wait_info_t * cwait_info = (client_wait_info_t *) (scontrol->waiting_client + i);
	int qnum = cwait_info->queue_num;
	//if(qnum < scontrol->start_queue || qnum >= scontrol->end_queue)
	//  continue;
	peer_queue_t *inservice_queue = domain_queue->queue+qnum;
	int slot = cwait_info->req_id % REQ_QUEUE_LENGTH;
	mem_req_t * mem_req = inservice_queue->mem_request + slot;
	
	
#if 0
	  printf("%d: Reply for Thread %d, request %d, queue %d, server %d, slot %d \n", GET_MYTH(),
	   i, cwait_info->req_id, cwait_info->queue_num, scontrol->server_id, slot);
#endif	
	  
	  pending |= check_queue_completion(inservice_queue,domain_queue->queue_hits+qnum, scontrol->server_dispatch_policy);
	
	if(server_check_req_completion(mem_req,cwait_info->req_id)) {


#ifndef SIMPLE_RR
#if	CONCURRENCY_DEBUG
	  atomic_fadd(&inservice_queue->server_completion_tokens,1);
#endif

	  atomic_f_xor((uint64_t *)&(scontrol->waiting_bitmap),mask);

#if UPC_SEMAPHORE
	  wrap_bupc_sem_post(scontrol->waiting_client[i].pwait_sem);
#else
	  ASSERTZ(sem_post((sem_t *)&(scontrol->waiting_client[i].wait_sem)));
#endif
#endif
	}
      }
    }
    //        printf("SERVER AFTER CHECK COMPLETION\n");
    
    if(scontrol->command == terminate) {
      if(!pending) {
	// let us make a thorough check
	for(i=scontrol->start_queue;i<scontrol->end_queue;i++) {
	  peer_queue_t *inservice_queue = domain_queue->queue+i;
	  pending |= check_queue_completion(inservice_queue,domain_queue->queue_hits+i,scontrol->server_dispatch_policy);
	}
       	
	// let's check if something is pending
	if(!pending)
	  return 0;
      }
    }
  
    //    printf("SERVER AFTER CHECK TERMINATE \n");
  }
  return 1;
}
int service_request_old(server_control_t * scontrol, int thr_per_domain, domain_queue_t *domain_queue)
{
	int i;
	int not_served = 0;
	uint64_t wait_bitmap;
	uint64_t mask;
	int pending = 0;
	if(scontrol->waiting_bitmap) {
		for(i=0;i<thr_per_domain;i++) {
			mask = ((uint64_t)1) << i;
			wait_bitmap = scontrol->waiting_bitmap;
			if((mask&wait_bitmap) == 0)
				continue;
			client_wait_info_t * cwait_info = (client_wait_info_t *) (scontrol->waiting_client + i);
			int qnum = cwait_info->queue_num;
			peer_queue_t *inservice_queue = domain_queue->queue+qnum;
			if(scontrol->server_dispatch_policy == COALESCE_MESSAGE_DISPATCH)
				issue_coalesced_request(scontrol, inservice_queue);
			else if (scontrol->server_dispatch_policy == ROUND_ROBIN_DISPATCH)
				issue_request(scontrol,inservice_queue, fixed_count, 1);
			else if (scontrol->server_dispatch_policy == WEIGHTED_CREDIT_DISPATCH)
				issue_request(scontrol,inservice_queue, credit, 1);
			else if(scontrol->server_dispatch_policy == THROTTLE_WAITSYNC_DISPATCH)
				issue_request(scontrol,inservice_queue, timed, 1);
			else
				issue_request(scontrol,inservice_queue, all, 0);

			check_queue_completion(inservice_queue,domain_queue->queue_hits+qnum, scontrol->server_dispatch_policy);
			int slot = cwait_info->req_id % REQ_QUEUE_LENGTH;
			mem_req_t * mem_req = inservice_queue->mem_request + slot;
			if(server_check_req_completion(mem_req,cwait_info->req_id)) {
				atomic_f_xor((uint64_t *)&(scontrol->waiting_bitmap),mask);
			#if	CONCURRENCY_DEBUG
				atomic_fadd(&inservice_queue->server_completion_tokens,1);
			#endif
			#if VERBOSE_OUTPUT
				printf("Completion reply for Thread %d, request %d, queue %d, server %d\n",
						i, cwait_info->req_id, cwait_info->queue_num, scontrol->server_id);
			#endif
				//eat one token for ublocking a client
				#if UPC_SEMAPHORE
				wrap_bupc_sem_post(scontrol->waiting_client[i].pwait_sem);
				eat_token(scontrol->pservice_sem);
				#else
				ASSERTZ(sem_post((sem_t *)&(scontrol->waiting_client[i].wait_sem)));
				eat_token(&(scontrol->service_sem));
				#endif
			}
		}
	}
	for(i=scontrol->start_queue;i<scontrol->end_queue;i++) {
		peer_queue_t *inservice_queue = domain_queue->queue+i;
		if(scontrol->server_dispatch_policy == COALESCE_MESSAGE_DISPATCH)
			issue_coalesced_request(scontrol, inservice_queue);
		else if (scontrol->server_dispatch_policy == ROUND_ROBIN_DISPATCH)
			issue_request(scontrol,inservice_queue, fixed_count, 1);
		else if (scontrol->server_dispatch_policy == WEIGHTED_CREDIT_DISPATCH)
			issue_request(scontrol,inservice_queue, credit, 1);
		else if(scontrol->server_dispatch_policy == THROTTLE_WAITSYNC_DISPATCH)
			issue_request(scontrol,inservice_queue, timed, 1);
		else
			issue_request(scontrol,inservice_queue, all, 0);
		pending |= check_queue_completion(inservice_queue,domain_queue->queue_hits+i,scontrol->server_dispatch_policy);
	}

	if(scontrol->command == terminate) {
		if(!pending)
			// let's check if something is pending
			return 0;
	}
	return 1;
}


void thor_server_event_loop() {
  
  int flip = 0;
  if(thor_enabled && thor_remap_enabled) {
    flip = 1;
    thor_remap_enabled = 0;
  }
  
  _upcr_notify(400,0);                                                                                              
  _upcr_wait(400,0);      
  if(flip)
    thor_remap_enabled = 1;
 
  server_routine(NULL);
  
}


void print_all_queue_tags(int idx, int did) 
{
  domain_queue_t *q;
  int i;
  q = get_domq(did, idx, idx);
  for(i=0; i < QUEUE_COUNT; i++) 
    printf("%d: DUMP CHECK %d || %s\n", GET_MYTH(), i, q->queue[i].xxxpadding0);
}


void dump_a_queue(peer_queue_t *q) {

  printf("DUMP Q: H=%d TI = %d ID=%d SID=%d \n", q->head, q->tail_issued, q->id, q->my_server_id); 
}
// Server Routine is awakened when a request is enqueued and 
// when a a request is waited for its completion
// The server eats one token for each issued request
// It also eats one token when a waiting threads gets a reply of a completed 
// Note: when a thread is waiting for a request this frees a core for the server to start execution.

void * server_routine(void *args)
{
  server_control_t *scontrol;
  domain_queue_t *dom_q;
  int i, flip = 0;
 int my_idx,s_idx,n_idx;
  if(thor_enabled)  {
    int upn = (GET_ALLTH() - thor_domain_count*thor_servers_per_domain)/thor_physical_nodes;
    int TPN = (GET_ALLTH()/thor_physical_nodes);
   
    
    my_idx = GET_MYTH();
    n_idx = my_idx/TPN;

    while(!server_fired(my_idx)) sleep(1);
    sinfo = get_server_info(my_idx);
    s_idx = my_idx%TPN - upn - (sinfo->domain_id - n_idx*thor_domains_per_node) * thor_servers_per_domain;
    scontrol= get_scontrol(sinfo->domain_id, my_idx,s_idx);
    sinfo->scontrol = scontrol;
    sinfo->domain_queues  = get_domq(sinfo->domain_id, my_idx, s_idx);
    dom_q = get_domq(sinfo->domain_id, my_idx, s_idx); 
    for(i=0; i < scontrol->end_queue - scontrol->start_queue; i++) {
      sinfo->domain_queues->queue[i].my_server_id =  scontrol->server_id;
      //      dump_a_queue(sinfo->domain_queues->queue+i);
    }
    
  } else {
    sinfo = (service_info_t *) args;
    scontrol = sinfo->scontrol;
  }
 
    int thr_per_domain = sinfo->thr_per_domain;
#if !DISTRIBUTED_SERVER_PARENT
    if (pthread_setaffinity_np(pthread_self(), sizeof(scontrol->affinity_mask), &scontrol->affinity_mask) !=0) {
      perror("pthread_setaffinity_np");
    }
#endif
	if(scontrol->server_dispatch_policy ==  THROTTLE_WAITSYNC_DISPATCH) {
		uint64_t cycle = rdtsc();
		sleep(1);
	 	cycles_per_us = (rdtsc() - cycle)/1000000;
		if(cycles_per_us==0) {
			cycles_per_us = 2000;
			printf("Invalid cycles per us assumed %lld\n",cycles_per_us);
		}
	} else 
		cycles_per_us = 0;


//	pthread_barrier_wait(sinfo->pbar);
#if 0
	printf("%d: Starting server: domain: %d, sid: %d qstart:%d qend: %d || (%p, %p) || %s ||   %d\n", GET_MYTH(),
	       sinfo->domain_id, scontrol->server_id, scontrol->start_queue, scontrol->end_queue, sinfo->scontrol, sinfo->domain_queues, sinfo->domain_queues->queue[scontrol->start_queue].xxxpadding0, sinfo->domain_queues->queue[scontrol->start_queue].my_server_id);
	
	//	print_all_queue_tags(my_idx, sinfo->domain_id);

#endif

unsigned cpuNum, nodeNum;
	getcpu(&cpuNum, &nodeNum, 0);
	printf("%d: Server @ %d:%d on core %u:%u\n", GET_MYTH(), sinfo->domain_id, scontrol->server_id, nodeNum, cpuNum);	
	do {
	  /*  Wait for something started being produced */
#if 0
	  unsigned cpuNum, nodeNum;
	  getcpu(&cpuNum, &nodeNum, 0);
	  printf("%d: Server @ %d:%d on core %u:%u\n", GET_MYTH(), sinfo->domain_id, scontrol->server_id, nodeNum, cpuNum);
#endif
	  if(scontrol->command == terminate) {
	    break;
	  }
#if UPC_SEMAPHORE
	  eat_token(scontrol->pservice_sem);
#else
	  eat_token(&(scontrol->service_sem));
#endif
	  //printf("%d: WAKEUP ....................\n", GET_MYTH());
	  interconnect_poll();
	  
	} while(service_request(scontrol,thr_per_domain,dom_q));
	
	// pthread_barrier_wait(sinfo->pbar);
	//supc_shutdown_sync(); // custom barrier
	
	
#if VERBOSE_OUTPUT
	printf("Final Exit of Service routine\n");
#endif

	return NULL;
}


int get_thor_servers_per_domain() {
 return thor_servers_per_domain;
}




