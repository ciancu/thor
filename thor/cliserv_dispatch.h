#ifndef __CLISERV_UPC_LIB__
#define __CLISERV_UPC_LIB__

#include <stdint.h>


#define SIMPLE_RR
#define REQ_BATCH  1

#define REQ_QUEUE_LENGTH 8192

//#define REQ_QUEUE_COUNT  32
#define REQ_QUEUE_COUNT  8
#define MAX_CLIENTS 	64

#define MAX_SERVERS 	MAX_CLIENTS


#define DEST_BASED_DISPATCH 0
#define SRC_BASED_DISPATCH 	1
#define RR_BASED_DISPATCH 	2
#define MAX_CLIENT_DISPATCH RR_BASED_DISPATCH

#define DEFAULT_CLIENT_DISPATCH_POLICY RR_BASED_DISPATCH
#define DISTRIBUTED_SERVER_PARENT	0

#define OVERFLOW_REQ_QUEUE (REQ_QUEUE_COUNT)
#define MYDOMAIN_REQ_QUEUE (REQ_QUEUE_COUNT+1)
#define IMMEDIATE_QUEUE   (REQ_QUEUE_COUNT+2)
#define QUEUE_COUNT	(REQ_QUEUE_COUNT+4)

#define QUEUE_SHIFT 8
#define QUEUE_MASK ((1<<QUEUE_SHIFT)-1)

#define MAX_DOMAIN_COUNT 1024

#define	BYPASS_LOCAL_DOMAIN_MESSAGES 1


#define EAGER_MESSAGE_DISPATCH		0
#define ROUND_ROBIN_DISPATCH		1
#define WEIGHTED_CREDIT_DISPATCH	2
#define THROTTLE_WAITSYNC_DISPATCH	3
#define COALESCE_MESSAGE_DISPATCH	4
#define MAX_SERVER_DISPATCH  COALESCE_MESSAGE_DISPATCH

#define DEFAULT_SERVER_DISPATCH_POLICY EAGER_MESSAGE_DISPATCH

#define MESSAGE_SIZE_LEVELS	20
#define MESSAGE_MIN_LEVEL	3

#define PSHM_SUPPORT 		1

#define UPC_SEMAPHORE	0

#define VERBOSE_OUTPUT 		0
#define CONCURRENCY_DEBUG 	0
#define REQ_MASK 	0xffffffff

#define ENABLE_PROFILING	0

#define ENABLE_ASSERTIONS 0

#if ENABLE_ASSERTIONS
#include <assert.h>
#define ASSERTZ(x) (assert((x)==0))
#define ASSERTNZ(x) (assert((x)!=0))
#else
#define ASSERTZ(x) x
#define ASSERTNZ(x) x
#endif

#define CHECK_CPU_AFFINITIES 0

typedef void sbupc_sem_t;
typedef enum {r_completed=0, r_pending=1, r_issued=2} req_state_t;
typedef enum {q_memget=1, q_memput=2} req_type_t;

typedef enum {all, only_aged, fixed_count, credit, timed} issue_policy_t;


typedef struct {

#ifdef _UPC_CLIENT
  shared void * s_addr;
  shared void * l_addr;
#else 
  upcr_shared_ptr_t s_addr;
  upcr_shared_ptr_t l_addr;
#endif

  size_t size;
  req_type_t req_type;
  int64_t h;
  uint64_t req_timestamp;
  int32_t service_time;
  volatile uint32_t req_id;
  volatile uint32_t ready_to_serve;
  volatile req_state_t req_state;
  char pad[72];
} mem_req_t;

typedef  struct {
	mem_req_t mem_request[REQ_QUEUE_LENGTH];
#ifdef _UPC_CLIENT
    char fixcompilerbug[131072];
#endif
	// Updated only by client
  char xxxpadding0[128];
	volatile uint64_t head;
  char xxxpadding1[128];
	// updated only by server
	volatile uint64_t tail_issued, tail_completed;
  char pad[128];
  uint64_t  issue_tokens;
#if CONCURRENCY_DEBUG
	uint64_t completion_tokens, server_issue_tokens, server_completion_tokens;
#endif
#if ENABLE_PROFILING
	uint64_t get_count, put_count;
#endif
	int my_server_id;
	int avail_credit;
	int id;
} peer_queue_t;


typedef  struct {
	peer_queue_t queue[QUEUE_COUNT];
	volatile int queue_map[MAX_DOMAIN_COUNT];
	int queue_hits[MAX_DOMAIN_COUNT];
	int head;
	int used_count;
#if UPC_SEMAPHORE
	sbupc_sem_t *pqueue_sem;
#else
	sem_t queue_sem;
#endif
	int total_credit;
} domain_queue_t;

typedef enum {service, terminate} server_command_t;

typedef struct {
	int queue_num;
	int req_id;
#if UPC_SEMAPHORE
	sbupc_sem_t *pwait_sem;
#else
	sem_t wait_sem;
#endif
} client_wait_info_t;

typedef  struct {
  // A semaphore for the server to know that something is injected to the queue.
#if UPC_SEMAPHORE
  sbupc_sem_t *pservice_sem;
#else
  sem_t service_sem;
#endif
  client_wait_info_t waiting_client[MAX_CLIENTS];
  // support up to 64 clients
  char xxxpadding0[128];
  volatile uint64_t waiting_bitmap;
  char xxxpadding1[128];
  int start_queue, end_queue;
  volatile  server_command_t command;
  int server_id;
  // Server side properties
  int server_dispatch_policy;
  pthread_barrier_t bar;
  cpu_set_t affinity_mask;
  volatile int clients_active;
} server_control_t;


typedef struct {
	server_control_t  server[MAX_SERVERS];
	int thr_per_domain;
	int domain_id;
} domain_server_t;

typedef struct {
	int domain_id;
	int thr_per_domain;	
	pthread_barrier_t *pbar;
	domain_queue_t * domain_queues;
	server_control_t * scontrol;
#if ENABLE_PTHRDS
	void * upcr_parent_info;
#endif
	int *msg_credits;//[MESSAGE_SIZE_LEVELS]
	int *msg_min_wait;
} service_info_t;

typedef struct {
  int thr_per_domain;
  
  int domain_count;
  int node_count;
  int domain_id;  //global domain idx
  int srv_per_domain;  
  int relative_domain;//relative domain idx within the node...
  int start_queue;  //first queue for RR 
  int client_id;
  int queues_per_server;
  // client side properties
  int client_dispatch_policy;
} supc_thread_info_t;




void mem_bar(void);
void * server_routine(void *args);
int check_req_completion(mem_req_t *mem_req, int req_id);
void interconnect_poll(void);


////////////////// CLIENT ROUTINES //////////////////
void adjust_domain_map(domain_queue_t *domain_queue, int domain_count);
int find_queue(domain_queue_t *domain_queue, int param);
void wait_memslot_available(supc_thread_info_t *pinfo, server_control_t * my_server, peer_queue_t * my_peer_queue, uint32_t req_id); 

void wait_memslot_complete(supc_thread_info_t *pinfo, server_control_t * my_server, peer_queue_t * my_peer_queue, uint32_t req_id); 
void wait_memslot_ready(supc_thread_info_t *pinfo, server_control_t * my_server, peer_queue_t * my_peer_queue, uint32_t req_id); 
void init_peer_queue(domain_queue_t *domain_queue, int id);



#endif
