#ifndef __BLACK_ADDER
#define __BLACK_ADDER

#ifdef __cplusplus
extern "C" {
#endif

int ask_Adder(void);
int ask_BadAdder(int a, int b);


#ifdef __cplusplus
}
#endif

#endif /* __BLACK_ADDER */
