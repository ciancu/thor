#!/bin/bash
# This is a helpful script to run on n2001.lbl.gov to build
# the final release tarballs from a CVS tag.
# Note that by default it copies in the pre-built GASNet docs from
# a previous release - one can set env var GASNET_DOCS to use a
# local directory once they are finally revised.  -PHH 2011.10.26
TAG=$1
RC=$2
if test -z "$TAG" ; then
  echo Missing TAG argument
  exit 1
fi
# Edit the following if the network translator is already installed:
export UPCC_FLAGS='-translator=http://upc-translator.lbl.gov/upcc-nightly.cgi'
export UPCC_NORC=1
#
set -e
set -x
#
.  /usr/local/pkg/Modules/init/bash
module unload autotools vapi gm gm2 mpi gmake gcc
module load autotools/autotools-newest
module load gmake/3.82
module load gcc/gcc-4.4.2
#
# mkdirs (to fail if not "clean")
#
mkdir bld-upcr
mkdir bld-gasnet
mkdir bld-tools
#
# Fetch docs from a previous version
#
if test -z "$GASNET_DOCS"; then
  DOCS_FROM=GASNet-1.16.0
  curl -LsS http://gasnet.lbl.gov/${DOCS_FROM}.tar.gz | \
    tar xz ${DOCS_FROM}/docs
  GASNET_DOCS=`pwd`/${DOCS_FROM}/docs
fi
#
# CVS checkouts
#
cvs -Q -f co -r $TAG -P compilers upc_runtime gasnet
cp -rp gasnet gasnet-tools
#
# Compiler tarball
#
cd compilers
gmake dist
cd ..
#
TRANS_VER=`perl -ne 'if (m/release (\d+\.\d+\.\d+)/) { print "$1\n"; }' -- compilers/open64/osprey1.0/be/whirl2c/w2c_driver.cxx`
if test -z "$TRANS_VER" ; then
  echo Could not extract release from the translator sources
  exit 1
fi
if test -n "$RC"; then
  tar xfz compilers/berkeley_upc_translator-$TRANS_VER.tar.gz
  mv berkeley_upc_translator-$TRANS_VER berkeley_upc_translator-$TRANS_VER$RC
  GZIP=--best tar chfz berkeley_upc_translator-$TRANS_VER$RC.tar.gz berkeley_upc_translator-$TRANS_VER$RC
  rm -rf berkeley_upc_translator-$TRANS_VER$RC
else
  ln compilers/berkeley_upc_translator-$TRANS_VER.tar.gz .
fi
#
# Runtime tarball
#
cd upc_runtime
./Bootstrap -L
cd ../bld-upcr
../upc_runtime/configure --enable-totalview --without-multiconf
gmake distcheck GASNET_DOCS=$GASNET_DOCS
cd ..
#
UPCR_VER=`perl -ne 'if (m/VERSION=(\d+\.\d+\.\d+)/) { print "$1\n"; exit 0; }' -- upc_runtime/configure`
if test -z "$UPCR_VER" ; then
  echo Could not extract release from the runtime sources
  exit 1
fi
if test -n "$RC"; then
  tar xfz bld-upcr/berkeley_upc-$UPCR_VER.tar.gz
  mv berkeley_upc-$UPCR_VER berkeley_upc-$UPCR_VER$RC
  GZIP=--best tar chfz berkeley_upc-$UPCR_VER$RC.tar.gz berkeley_upc-$UPCR_VER$RC
  rm -rf berkeley_upc-$UPCR_VER$RC
else
  ln bld-upcr/berkeley_upc-$UPCR_VER.tar.gz .
fi
#
# GASNet tarball
#
cd gasnet
./Bootstrap
cd ../bld-gasnet
../gasnet/configure
gmake distcheck GASNET_DOCS=$GASNET_DOCS
cd ..
#
GAS_VER=`perl -ne 'if (m/VERSION=(\d+\.\d+\.\d+)/) { print "$1\n"; exit 0; }' -- upc_runtime/gasnet/configure`
if test -z "$GAS_VER" ; then
  echo Could not extract release from the gasnet sources
  exit 1
fi
if test -n "$RC"; then
  tar xfz bld-gasnet/GASNet-$GAS_VER.tar.gz
  mv GASNet-$GAS_VER GASNet-$GAS_VER$RC
  GZIP=--best tar chfz GASNet-$GAS_VER$RC.tar.gz GASNet-$GAS_VER$RC
  rm -rf GASNet-$GAS_VER$RC
else
  ln bld-gasnet/GASNet-$GAS_VER.tar.gz .
fi
#
# Tools tarball
#
cd gasnet-tools
./Bootstrap -o
cd ../bld-tools
../gasnet-tools/configure
gmake distcheck GASNET_DOCS=$GASNET_DOCS
cd ..
#
if test -n "$RC"; then
  tar xfz bld-tools/GASNet_Tools-$GAS_VER.tar.gz
  mv GASNet_Tools-$GAS_VER GASNet_Tools-$GAS_VER$RC
  GZIP=--best tar chfz GASNet_Tools-$GAS_VER$RC.tar.gz GASNet_Tools-$GAS_VER$RC
  rm -rf GASNet_Tools-$GAS_VER$RC
else
  ln bld-tools/GASNet_Tools-$GAS_VER.tar.gz .
fi
